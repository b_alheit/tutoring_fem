##| Daniel Pons | PNSDAN002 | MEC5063Z | Project 1: 
import numpy as np
import matplotlib.pyplot as plt
import scipy.linalg as sp
import statistics as stat
import matplotlib.pyplot as plt
import random as rand
## Constants
L = 2#m                      Length of bar
E = 100#N.m^-1               Young's Modulus
A = 1#m^2                    Cross-sectional Area

elnum = 20                   #Number of elements
#el_ord = 1#                  Order of the elements
#nodnum = elnum*el_ord+1
#NGP
ngp_l = 3   #linear ngp
ngp_quad = 4   #quadratic ngp

#BCs
p = 10#N.m^-2                Traction applied to free end
u_L = 0#m                    Displacement at L: u(L)

## Mesh
def MESH_1D(start,end,elnum,el_ord): #FN.that returns a list of x_pos and an ICA array
    x_pos = np.linspace(start,end,elnum*el_ord+1) #generates array of x positions along length
    ICA = np.zeros((elnum,(el_ord+1)))
    for i in range(elnum):
        j = (el_ord)*i #this shifts the index to allow the correct start and end point for x_pos
        ICA[i] = np.array([x_pos[j:j+1+el_ord]]) #calls x positions for each elemental node
    return [x_pos,ICA] 
    
## Shape FNS
#temporary variable value assignment
x1 = 1
x2 = 2
x3 = 3

def b(x):
    return np.exp(-x/2)
    
def Nlin(x): #Where x1&x2 must be globally defined
    if x1<=x<=x2:
        N_lin = np.array([(x2-x)/(x2-x1),(x1-x)/(x1-x2)])
        #print(x1,x2)
        return N_lin
    else: 
        return np.array([0,0])

def Nquad(x):
    if x1<=x<=x3:
        N1 = (x-x2)*(x-x3)/((x1-x2)*(x1-x3))
        N2 = (x-x1)*(x-x3)/((x2-x1)*(x2-x3))
        N3 = (x-x1)*(x-x2)/((x3-x2)*(x3-x1))
        N_quad = np.array([N1,N2,N3])
        return N_quad
    else: 
        return np.array([0,0,0])
    

# SF Derivatives
def Blin(x):
    if x1<=x<=x2:
        B_lin = np.array([-1/(x2-x1),-1/(x1-x2)])
        return B_lin
    else: 
        return np.array([0,0])
def Bquad(x):
    if x1<=x<=x3:
        B1 = ((x-x3)+(x-x2))/((x1-x2)*(x1-x3))
        B2 = ((x-x1)+(x-x3))/((x2-x1)*(x2-x3))
        B3 = ((x-x1)+(x-x2))/((x3-x2)*(x3-x1))
        B_quad = np.array([B1,B2,B3])
        return B_quad
    else: 
        return np.array([0,0,0])    
# Matrix operations for integration
def NT_Nquad(x):
    NT_N = np.reshape(Nquad(x),(3,1))*Nquad(x)
    return NT_N
    
def NT_Nlin(x):
    NT_N = np.reshape(Nlin(x),(2,1))*Nlin(x)
    return NT_N

def NT_bx_quad(x):
    return np.reshape(Nquad(x),(3,1))*b(x)

def NT_bx_lin(x):
    return np.reshape(Nlin(x),(2,1))*b(x)
    
#test = NT_bx_lin(x1)
    
def BT_Blin(x): #Btranspose into B for linear SF
    #print(x1,x2) #use to check if temporary variables are being overwritten
    BT_Blin = np.reshape(Blin(x),(2,1))*Blin(x) #Btranspose into B for linear SF
    return BT_Blin

def BT_Bquad(x):
    BT_B = np.reshape(Bquad(x),(3,1))*Bquad(x)
    return BT_B
    
        
    
    

## INTEGRATION

def integrate_func(Ngp,function,x_1_e,x_2_e): #function must be a previously defined function with an argument
    #x_2_e & x_1_e are the upper and lower limits of integration for the element
    #gauss quad array GQA
    GQA = [[0],[[0],[2]],[[-1/np.sqrt(3),1/np.sqrt(3)],[1,1]],[[-np.sqrt(3/5),0,np.sqrt(3/5)],[5/9,8/9,5/9]],[[-0.86113631,-0.33998104,0.33998104,0.86113631],[0.3478548451,0.6521451549,0.6521451549,0.3478548451]],[[-0.9061798459,-0.5384693101,0,0.5384693101,0.9061798459],[0.2369268851,0.4786286705,0.5688888889,0.4786286705,0.2369268851]],[[-0.9324695142,-0.6612093865,-0.2386191861,0.2386191861,0.6612093865,0.9324695142],[0.1713244924,0.3607615730,0.4679139346,0.4679139346,0.3607615730,0.1713244924]]]
    #Use this to call all values, format is [[gauss points],[location]] eg:GQA[3][0][1]
    
    # Constants
    l_int = x_2_e - x_1_e   #length of integral domain
    dx_dxi = l_int/2        #dx/dxi to transfer into xi coordinates
    # Integration
    I_g = 0
    for i in range(0,Ngp):
        xi = GQA[Ngp][0][i]             #Gauss point xi
        w_i = GQA[Ngp][1][i]            #weighting of the point
        x = 0.5*(x_1_e*(1-xi)+x_2_e*(1+xi))     #x as a function of xi (map
        #Integral
        I_g += function(x)*w_i
        
    Integral = I_g*dx_dxi
    return Integral
  
## Q2 b: Linear finite element solution
#elnum = 20#20                 Number of elements
el_ord_l = 1#                  Order of the elements
nodnum_l = elnum*el_ord_l+1
Ngp = ngp_l
#Derichler/Essential BCs
D_nodes = np.array([elnum+1]) #array of Derichler node number indices 1-indexed
D_disp =  np.array([0]) #Derichler displacement associated with the D_node position above

x_pos_l = MESH_1D(0,L,elnum,el_ord_l)[0]
ICA_lin = MESH_1D(0,L,elnum,el_ord_l)[1]


#Local element integration 
K_lin_loc = np.zeros( (elnum,2,2)) #array of local stiffness matrices
#F_lin_bod_loc = np.zeros(( elnum,2,1)) #Body force component
AF_lin_bod_loc = np.zeros(( elnum,2,1)) #Alternative force vector coding
F_lin_D = np.zeros(( nodnum_l,1)) #derichler force vector
#boundary condition assignment
x1 = ICA_lin[0][0]
x2 = ICA_lin[0][1]
F_lin_D[0] = (-A*p)*Nlin(0)[0]
F_lin_D[1] = (-A*p)*Nlin(0)[1]

#integration

for i in range(0,elnum):
    x1 = ICA_lin[i][0]
    x2 = ICA_lin[i][1]
    K_lin_loc[i] = A*E*integrate_func(Ngp,BT_Blin,x1,x2)
    #F_lin_bod_loc[i] = integrate_func(Ngp,NT_bx_lin,x1,x2) #Requires more gauss points
    AF_lin_bod_loc[i] = np.matmul(integrate_func(Ngp,NT_Nlin,x1,x2),np.array([[b(x1)],[b(x2)]])) #Alternative force vector coding

#Assembly
ICA_num_lin =  ICA_lin/(L/(elnum*el_ord_l))
K_lin = np.zeros( (nodnum_l,nodnum_l)) # globak K matrix of appropriate size to ndof
F_lin_bod = np.zeros((nodnum_l,1))
for i in range(elnum):
        g1 = int(ICA_num_lin[i][0]) #global index 1
        g2 = int(ICA_num_lin[i][1]) #global index 2
        #stiffness
        K_lin[g1][g1] += K_lin_loc[i][0][0]
        K_lin[g2][g1] += K_lin_loc[i][1][0]
        K_lin[g1][g2] += K_lin[g2][g1]
        K_lin[g2][g2] += K_lin_loc[i][1][1]
        #force
        F_lin_bod[g1] += AF_lin_bod_loc[i][0]
        F_lin_bod[g2] += AF_lin_bod_loc[i][1]

F_lin = F_lin_bod-F_lin_D #check signs

d_lin = np.zeros((nodnum_l,1)) #initialised displacement vector
for i in range(len(D_nodes)): #adds derichler BCs to the displacement vector
    pos = (D_nodes-1)[i]
    d_lin[pos] = D_disp[i]

#Partitioning
#Rearrangement of derichler rows
nodes = np.array(range(elnum*el_ord_l+1)) #array of node numbers
nod_r = np.delete(nodes,(D_nodes-1)) #deletes all Derichler indices (reduced nodal array)
new_order = np.concatenate(((D_nodes-1),nod_r)) #All derichler nodes zero indexed added to the front of the array
#Now index notation will help build a new array with the new_order
K_lin_alt = np.zeros((nodnum_l,nodnum_l))
F_lin_alt = np.zeros((nodnum_l,1))
d_lin_alt = np.zeros((nodnum_l,1))
#KALT =np.array([[K_lin[i][j] for j in new_order] for i in new_order])
for i in range(nodnum_l):
    row = new_order[i] #maps new to old indexing
    F_lin_alt[i] = F_lin[row] #Rearrangement of Force vector
    d_lin_alt[i] = d_lin[row]
    for j in range(nodnum_l):
        column = new_order[j]
        K_lin_alt[i][j] = K_lin[row][column] #Rearrangement of K matrix

#break up of the arrays for solving:
nn_cut = np.array(range(len(D_nodes))) #natural BC slice index
K_nn = np.delete(K_lin_alt,nn_cut,axis = 0)
K_nn = np.delete(K_nn,nn_cut,axis = 1)
#d_n = np.delete(d_lin_alt,nn_cut,axis = 0) #Displacement partitioning for force adjustment
F_n = np.delete(F_lin_alt,nn_cut,axis = 0)


ne_cut = np.array(range(len(D_nodes),nodnum_l)) #natural essential bc slice index
K_ne = np.delete(K_lin_alt,nn_cut,axis = 0)
K_ne = np.delete(K_ne,ne_cut,axis = 1)
d_e = np.delete(d_lin_alt,ne_cut,axis = 0) #Displacement partitioning for force adjustment

#invert K nn&ne
K_nn_inv = np.linalg.inv(K_nn)
#Solving for displacement:
d_n = np.matmul(K_nn_inv,(F_n-np.matmul(K_ne,d_e)))

d_lin_new = np.concatenate((d_e,d_n))

#swap rows back to original positions
u_lin = np.zeros((nodnum_l,1))
for i in range(nodnum_l):
    row = new_order[i] 
    u_lin[row] = d_lin_new[i] #maps old to new indexing
    
# Post processing

#displacement field:
def Nlin_glob(x): #Global Shape function array
    #x = 0.125
    glob = np.zeros((1,nodnum_l))
    for i in range(elnum):
        global x1,x2
        x1,x2 = ICA_lin[i]
        glob[0][i] += Nlin(x)[0]
        glob[0][i+1] += Nlin(x)[1]
        #print(x1,x2,glob[0][i],Nlin(x))
    if x in x_pos_l[1:-1]:
        glob = glob/2
    #glob = np.delete(glob,(nodnum_l+1))
    return glob

def u_linear(x): #displacement wrt x can be defined as follows
    u = (np.matmul(Nlin_glob(x),u_lin))
    return u[0][0] 
#stress field:
def Blin_glob(x): #Global Shape function array
    #x = 0.125
    glob = np.zeros((1,nodnum_l))
    for i in range(elnum):
        global x1,x2
        x1,x2 = ICA_lin[i]
        glob[0][i] += Blin(x)[0]
        glob[0][i+1] += Blin(x)[1]
        #print(x1,x2,glob[0][i],Nlin(x))
    '''if x in x_pos_l[1:-1]:
        glob = glob/2'''
    #glob = np.delete(glob,(nodnum_l+1))
    return glob
def s_linear(x): #displacement derivative wrt x can be defined as follows
    s = (np.matmul(Blin_glob(x),u_lin))
    return E*s[0][0]
## Q2 c) Quadratic element    
#elnum = 1#20                 Number of elements
el_ord_q = 2#                  Order of the elements
nodnum_q = elnum*el_ord_q+1 
Ngp = ngp_quad #used to move constants to top of code
#Derichler/Essential BCs
D_nodes = np.array([nodnum_q]) #array of Derichler node number indices 1-indexed
D_disp =  np.array([0]) #Derichler displacement associated with the D_node position above

x_pos_q = MESH_1D(0,L,elnum,el_ord_q)[0]
ICA_quad = MESH_1D(0,L,elnum,el_ord_q)[1]


#Local element integration 
K_quad_loc = np.zeros( (elnum,3,3)) #array of local stiffness matrices
#F_quad_bod_loc = np.zeros(( elnum,2,1)) #Body force component
AF_quad_bod_loc = np.zeros(( elnum,3,1)) #Alternative force vector coding
F_quad_D = np.zeros(( nodnum_q,1)) #derichler force vector
#boundary condition assignment
x1 = ICA_quad[0][0]
x2 = ICA_quad[0][1]
x3 = ICA_quad[0][2]
F_quad_D[0] = (-A*p)*Nquad(0)[0]
F_quad_D[1] = (-A*p)*Nquad(0)[1]
F_quad_D[2] = (-A*p)*Nquad(0)[2]

#integration

for i in range(0,elnum):
    x1,x2,x3 = ICA_quad[i]
    K_quad_loc[i] = A*E*integrate_func(Ngp,BT_Bquad,x1,x3)
    AF_quad_bod_loc[i] = np.matmul(integrate_func(Ngp,NT_Nquad,x1,x3),np.array([[b(x1)],[b(x2)],[b(x3)]])) #Alternative force vector coding

#Assembly
ICA_num_quad =  ICA_quad/(L/(elnum*el_ord_q))
K_quad = np.zeros( (nodnum_q,nodnum_q)) # globak K matrix of appropriate size to ndof
F_quad_bod = np.zeros((nodnum_q,1))
for i in range(elnum):
        g1 = int(ICA_num_quad[i][0]) #global index 1
        g2 = int(ICA_num_quad[i][1]) #global index 2
        g3 = int(ICA_num_quad[i][2]) #global index 3
        #stiffness
        K_quad[g1][g1] += K_quad_loc[i][0][0]
        K_quad[g2][g1] += K_quad_loc[i][1][0]
        K_quad[g1][g2] += K_quad[g2][g1]
        K_quad[g2][g2] += K_quad_loc[i][1][1]
       
        K_quad[g1][g3] += K_quad_loc[i][0][2]
        K_quad[g2][g3] += K_quad_loc[i][1][2]        
        K_quad[g3][g3] += K_quad_loc[i][2][2]
        K_quad[g3][g2] += K_quad[g2][g3]     
        K_quad[g3][g1] += K_quad[g1][g3]   
        #force
        F_quad_bod[g1] += AF_quad_bod_loc[i][0]
        F_quad_bod[g2] += AF_quad_bod_loc[i][1]
        F_quad_bod[g3] += AF_quad_bod_loc[i][2]

F_quad = F_quad_bod-F_quad_D #check signs

d_quad = np.zeros((nodnum_q,1)) #initialised displacement vector
for i in range(len(D_nodes)): #adds derichler BCs to the displacement vector
    pos = (D_nodes-1)[i]
    d_quad[pos] = D_disp[i]

#Partitioning
#Rearrangement of derichler rows
nodes = np.array(range(elnum*el_ord_q+1)) #array of node numbers
nod_r = np.delete(nodes,(D_nodes-1)) #deletes all Derichler indices (reduced nodal array)
new_order = np.concatenate(((D_nodes-1),nod_r)) #All derichler nodes zero indexed added to the front of the array
#Now index notation will help build a new array with the new_order
K_quad_alt = np.zeros((nodnum_q,nodnum_q))
F_quad_alt = np.zeros((nodnum_q,1))
d_quad_alt = np.zeros((nodnum_q,1))

for i in range(nodnum_q):
    row = new_order[i] #maps new to old indexing
    F_quad_alt[i] = F_quad[row] #Rearrangement of Force vector
    d_quad_alt[i] = d_quad[row]
    for j in range(nodnum_q):
        column = new_order[j]
        K_quad_alt[i][j] = K_quad[row][column] #Rearrangement of K matrix

#break up of the arrays for solving:
nn_cut = np.array(range(len(D_nodes))) #natural BC slice index
K_nn = np.delete(K_quad_alt,nn_cut,axis = 0)
K_nn = np.delete(K_nn,nn_cut,axis = 1)
#d_n = np.delete(d_quad_alt,nn_cut,axis = 0) #Displacement partitioning for force adjustment
F_n = np.delete(F_quad_alt,nn_cut,axis = 0)


ne_cut = np.array(range(len(D_nodes),nodnum_q)) #natural essential bc slice index
K_ne = np.delete(K_quad_alt,nn_cut,axis = 0)
K_ne = np.delete(K_ne,ne_cut,axis = 1)
d_e = np.delete(d_quad_alt,ne_cut,axis = 0) #Displacement partitioning for force adjustment

#invert K nn&ne
K_nn_inv = np.linalg.inv(K_nn)
#Solving for displacement:
d_n = np.matmul(K_nn_inv,(F_n-np.matmul(K_ne,d_e)))

d_quad_new = np.concatenate((d_e,d_n))

#swap rows back to original positions
u_quad = np.zeros((nodnum_q,1))
for i in range(nodnum_q):
    row = new_order[i] 
    u_quad[row] = d_quad_new[i] #maps old to new indexing
    
# Post processing

#displacement field:
def Nquad_glob(x): #Global Shape function array
    global x1,x2,x3,glob
    #x = 0.125
    glob = np.zeros((1,nodnum_q))
    for i in range(elnum):
        x1,x2,x3 = ICA_quad[i]
        glob[0][2*i] += Nquad(x)[0]
        glob[0][2*i+1] += Nquad(x)[1]
        glob[0][2*i+2] += Nquad(x)[2]
        #print(x1,x2,x3,Nquad(x))
    #glob = np.delete(glob,(nodnum_q+1))
    for k in range(elnum):
        if x == ICA_quad[k][2] and x in x_pos_q[1:-1]:
            glob = glob/2
    #print(np.sum(glob))
    return glob

def u_quadfunc(x): #displacement wrt x can be defined as follows
    u = (np.matmul(Nquad_glob(x),u_quad))
    return u[0][0]         
#Stress field:       
def Bquad_glob(x): #Global Shape function derivative array
    global x1,x2,x3,glob
    #x = 0.125
    glob = np.zeros((1,nodnum_q))
    for i in range(elnum):
        x1,x2,x3 = ICA_quad[i]
        glob[0][2*i] += Bquad(x)[0]
        glob[0][2*i+1] += Bquad(x)[1]
        glob[0][2*i+2] += Bquad(x)[2]
    '''for k in range(elnum):
        if x == ICA_quad[k][2] and x in x_pos_q[1:-1]:
            glob = glob/2'''
    #print(np.sum(glob))
    return glob

def s_quadfunc(x): #displacement derivative wrt x can be defined as follows
    u = (np.matmul(Bquad_glob(x),u_quad))
    return E*u[0][0] 

## Analytical
resolution = 1000
def u_analyt_A(x):
    return (-1/(A*E))*((2+A*p)*(x-L)+4*(np.exp(-x/2)-np.exp(-L/2)))

def s_analyt_A(x): #stress
    return E*(-1/(A*E))*((2+A*p)-2*(np.exp(-x/2)))

## Plotting Displacements
x_space = np.linspace(0,L,resolution)

def func_array(function,resolution,xstart,xstop):
    x_space = np.linspace(xstart,xstop,resolution)
    def_space = np.zeros((1,resolution))
    for i in range(resolution):
        x = x_space[i]
        def_space[0][i] = function(x)
    return def_space[0]

plt.figure(1)
plt.plot(x_space, u_analyt_A(x_space), 'k--')
plt.plot(x_space, func_array(u_linear,resolution,0,L), 'b-.') #linear
plt.plot(x_space, func_array(u_quadfunc,resolution,0,L), 'r')#quad
plt.xlabel('x (m)')
plt.ylabel('Displacement (m)')
plt.title('Displacement Approximation Overlay')
plt.legend(["Analytical","Linear","Quadratic"])
plt.show()

## Plotting Stress Curves
plt.figure(2)
plt.plot(x_space, s_analyt_A(x_space), 'k--')
plt.plot(x_space, func_array(s_linear,resolution,0,L), 'b-.')
plt.plot(x_space, func_array(s_quadfunc,resolution,0,L), 'r')
plt.xlabel('x (m)')
plt.ylabel('Stress (Nm^-2)')
plt.title('Stress Approximation Overlay')
plt.legend(["Analytical","Linear","Quadratic"])
plt.show()
## Error
err_lin = abs((u_analyt_A(x_space)-func_array(u_linear,resolution,0,L)))
err_quad = abs((u_analyt_A(x_space)-func_array(u_quadfunc,resolution,0,L)))
#L^2 Error
def u_diff_q(x):
    return (u_analyt_A(x)-u_quadfunc(x))**2
def u_diff_l(x):
    return (u_analyt_A(x)-u_linear(x))**2
def s_diff_q(x):
    return (s_analyt_A(x)-s_quadfunc(x))**2
def s_diff_l(x):
    return (s_analyt_A(x)-s_linear(x))**2
def H1e(function):
    return (integrate_func(6,function,0,L))**0.5



print("L^2 Error Norms:\n\tLinear:\n\t\tDisplacement: ",H1e(u_diff_l),"\n\t\tStress:       ",H1e(s_diff_l),"\n\tQuadratic:\n\t\tDisplacement: ",H1e(u_diff_q),"\n\t\tStress:       ",H1e(s_diff_q))

plt.figure(5)
plt.plot(x_space, err_lin, 'b')
plt.plot(x_space, err_quad, 'r--')
plt.xlabel('x')
plt.ylabel('|u-uh|')
plt.title('Error')
plt.legend(["Linear","Quadratic"])
plt.show()

print("Quadratic element:\n\tDisplacement:\n",u_quad)
print("\tStress:")
for i in x_pos_q:
    print(s_quadfunc(i))
    
print("Linear element:\n\tDisplacement:\n",u_lin)
print("\tStress:")
for i in x_pos_l:
    print(s_linear(i))

