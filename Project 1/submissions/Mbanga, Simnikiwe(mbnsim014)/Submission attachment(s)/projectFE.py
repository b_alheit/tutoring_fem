import numpy as np
import math as m
from matplotlib import pyplot as plt
from scipy.integrate import quad
#from numpy.linalg import inv


def shape_linear(x, x_r, x_l):

    n = np.array([(x_r - x)/(x_r - x_l), (x - x_l)/(x_r - x_l)])
    b = np.array([-1/(x_r-x_l), 1/(x_r-x_l)])
    f = m.exp(-x / 2)

    h = n * f

    w = 2
    if i == 0:
        print('force_vector_each_element=', (x_r - x_l) / 2 * w * h - 10 * np.array([(x_r - 0) / (x_r - x_l), (0 - x_l) / (x_r - x_l)]))

    elif i != 0:

        print('force_vector_each_element=', (x_r - x_l) / 2 * w * h)


    #print('n=', n)
    #print('b=', b)


    return n, f, b

#create global K

k = np.array([[-100, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [100, -200, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 100, -200, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 100, -200, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 100, -200, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 100, -200, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 100, -200, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 100, -200, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 100, -200, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0, 100, -200, 100, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 100, -200, 100, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 100, -200, 100, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 100, -200, 100, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 100, -200, 100, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 100, -200, 100, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 100, -200, 100, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 100, -200, 100, 0],
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 100, -200, 100],
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 100, -200]]) # the negative K

k_inverse = np.linalg.inv(-k)
#print('Inverse of K without the last d=',k_inverse)
gforce = np.array([[-9.9512345],
                   [0.09515267],
                   [0.08836002],
                   [0.08189866],
                   [0.07790442],
                   [0.07410498],
                   [0.07049083],
                   [0.06705295],
                   [0.06378274],
                   [0.06067202],
                   [0.05771301],
                   [0.05489831],
                   [0.05222089],
                   [0.13815201],
                   [0.13572938],
                   [0.04494694],
                   [0.04275485],
                   [0.04066967],
                   [0.03868619],
                   [0.01885962]])

force_i = np.array([[-9.9512345],
                    [0.09515267],
                    [0.08836002],
                    [0.08189866],
                    [0.07790442],
                    [0.07410498],
                    [0.07049083],
                    [0.06705295],
                    [0.06378274],
                    [0.06067202],
                    [0.05771301],
                    [0.05489831],
                    [0.05222089],
                    [0.13815201],
                    [0.13572938],
                    [0.04494694],
                    [0.04275485],
                    [0.04066967],
                    [0.03868619]]) # with out the last d
d_i = np.dot(k_inverse, -force_i)
print(d_i )
def function(x):
    n1 = np.array([(0.1 - x)*10, (x - 0)*10])
    n2 = np.array([(0.2 - x)*10, (x - 0.1)*10])
    n3 = np.array([(0.3 - x)*10, (x - 0.2)*10])
    n4 = np.array([(0.4 - x)*10, (x - 0.3)*10])
    n5 = np.array([(0.5 - x)*10, (x - 0.4)*10])
    n6 = np.array([(0.6 - x)*10, (x - 0.5)*10])
    n7 = np.array([(0.7 - x)*10, (x - 0.6)*10])
    n8 = np.array([(0.8 - x)*10, (x - 0.7)*10])
    n9 = np.array([(0.9 - x)*10, (x - 0.8)*10])
    n10 = np.array([(1 - x)*10, (x - 0.9)*10])
    n11 = np.array([(1.1 - x)*10, (x - 1)*10])
    n12 = np.array([(1.2 - x)*10, (x - 1.1)*10])
    n13 = np.array([(1.3 - x)*10, (x - 1.2)*10])
    n14 = np.array([(1.4 - x)*10, (x - 1.3)*10])
    n15 = np.array([(1.5 - x)*10, (x - 1.4)*10])
    n16 = np.array([(1.6 - x)*10, (x - 1.5)*10])
    n17 = np.array([(1.7 - x)*10, (x - 1.6)*10])
    n18 = np.array([(1.8 - x)*10, (x - 1.7)*10])
    n19 = np.array([(1.9 - x)*10, (x - 1.8)*10])
    n20 = np.array([(2 - x)*10, (x - 1.9)*10])
    d_1 = np.array([d_i[0],d_i[1]])
    d_2 = np.array([d_i[1],d_i[2]])
    d_3 = np.array([d_i[1],d_i[2]])
    d_4 = np.array([d_i[2],d_i[3]])
    d_5 = np.array([d_i[3],d_i[4]])
    d_6 = np.array([d_i[4],d_i[5]])
    d_7 = np.array([d_i[5],d_i[6]])
    d_8 = np.array([d_i[6],d_i[7]])
    d_9 = np.array([d_i[7],d_i[8]])
    d_10 = np.array([d_i[8],d_i[9]])
    d_11 = np.array([d_i[9],d_i[10]])
    d_12 = np.array([d_i[10],d_i[11]])
    d_13 = np.array([d_i[12],d_i[13]])
    d_14 = np.array([d_i[13],d_i[14]])
    d_15 = np.array([d_i[14],d_i[15]])
    d_16 = np.array([d_i[15],d_i[16]])
    d_17 = np.array([d_i[16],d_i[17]])
    d_18 = np.array([d_i[17],d_i[18]])
    u= n1.dot(d_1) + n2.dot(d_2) + n3.dot(d_3) + n4.dot(d_4) + n5.dot(d_5) + n6.dot(d_6) + n7.dot(d_7) + n8.dot(d_8) + n9.dot(d_9) + n10.dot(d_10) + n11.dot(d_11) + n12.dot(d_12) + n13.dot(d_13) + n14.dot(d_14 )+n15.dot(d_15) + n16.dot(d_16) + n17.dot(d_17) + n18.dot(d_18)
    return u/10
ans, error= quad(function,0,2)
print('approxvalue=', -ans)
print(error)
#print('u_1=',u(2))
#plt.plot(x, u(x))
#show()
#print('u_x=',u_x)
print('displacement with out the last d=', d_i)
def map(x_r, x_l, xi_points):

    y = (x_l + x_r) + xi_points*(x_r - x_l)
    x = y/2


    return x


elements = np.array([[0, .1],
                     [.1, .2],
                     [.2, .3],
                     [.3, .4],
                     [.4, .5],
                     [.5, .6],
                     [.6, .7],
                     [.7, .8],
                     [.8, .9],
                     [.9, 1.0],
                     [1.0, 1.1],
                     [1.1, 1.2],
                     [1.2, 1.3],
                     [1.3, 1.4],
                     [1.4, 15],
                     [1.5, 1.6],
                     [1.6, 1.7],
                     [1.7, 1.8],
                     [1.8, 1.9],
                     [1.9, 2.0]])

for i in range(np.alen(elements)):
    print('i=', i)
    current_node = elements[i, :]
    xi_points = np.array([0])
    x = map(current_node[1], current_node[0], xi_points)
    print('N=', shape_linear(x, current_node[1], current_node[0])[0])

    #print('x=', x)
    #print('B=', shape_linear(x, current_node[1], current_node[0])[1])


