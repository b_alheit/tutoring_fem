import matplotlib.pyplot as plt
from numpy import *
# This section contains all necessary functions
def shape_function(xStart, xEnd, nen, Numpoints):
    x = linspace(xStart, xEnd, Numpoints)    #linspace of points used within element
    xNodes = linspace(xStart,xEnd,nen)      #nodes in domain

    N = zeros((Numpoints, nen))
    B = zeros((Numpoints, nen))
    #Calculate Nx, the approximation within the element
    for i in range(nen):
        for q in range(Numpoints):
            Ntemp = 1
            for j in range(nen):
                if j!=i:
                    Ntemp = (x[q]-xNodes[j])/(xNodes[i]-xNodes[j])*Ntemp

            N[q, i] = Ntemp
    # calculate derivative of shape function B for each interval
    for i in range(nen):
        for q in range(Numpoints):
            Btemp = 0
            for k in range(nen):
                    Ntemp = 1
                    if k!=i:
                        for j in range(nen):
                            if j != i:
                                if j!=k:
                                    Ntemp = (x[q] - xNodes[j]) / (xNodes[i] - xNodes[j]) * Ntemp
                        Btemp = Ntemp *1/(xNodes[i]-xNodes[k]) + Btemp

            B[q,i] = Btemp

    return N,B,x

def GaussQuadrature(ngp,xStart,xEnd,inputFunction):
    x = [xStart, xEnd]                                  #setting limits of integration
    I = 0                                       #setting initial value of integral
    #building up a list of xi values
    xi1 = [0]
    xi2 = [1/sqrt(3), -1/sqrt(3)]
    xi3 = [0.7745966692, -0.7745966692, 0]
    xiVec = [xi1, xi2, xi3]
    #building up a list of weight values
    w1 = [2]
    w2 = [1, 1]
    w3 = [5/9, 5/9, 8/9]
    weightVec = [w1, w2, w3]

    J = (x[1] - x[0]) / 2                       #calculating the Jacobian

    for i in range(ngp):
        xi = (xiVec[int(ngp-1)][i])             #produce correct xi at relevant point, python is grumpy with multiplying multidimensional list elements
        w = (weightVec[int(ngp-1)][i])
        y = (x[1]+x[0])/2 + xi*(x[1]-x[0])/2
        I = w*J*inputFunction(xStart,xEnd,y) + I                 #where the terms containing y are the function to be integrated
    return I

#This function produces an array with the body force term at relevant elemental nodes
#This pair of bunctions calculates the body force at each node
def BodyForceStart(ElementStart, ElementEnd, x):
    N= (ElementEnd - x) / (ElementEnd - ElementStart)
    return exp(-0.5*x)*N

def BodyForceEnd(ElementStart, ElementEnd, x):
    N = (x - ElementStart) / (ElementEnd - ElementStart)
    return exp(-0.5*x)*N

# This is the end of the function section of the code

#This section actually performs the finite element method

NumElements = 20
NumberOfElementalNodes = 2          #Since this is for the linear case
NumberOfPointsForPlotting = 1000     #This is an arbitrary number chosen because it makes plots look smooth enough
xStart = 0
xEnd = 2
ElementLength = (xEnd-xStart)/NumElements

xAtNodes = linspace(xStart, xEnd, NumElements + 1)

K = zeros([NumElements + 1, NumElements + 1])
BodyForce = zeros([NumElements + 1, 1])


B = zeros([1,2])
B[0,0] = -10
B[0,1] = 10

Kelement = 100 * (B.T * B) * ElementLength
FBodyElement2 = 0

for i in range(NumElements):
    elementStart = xAtNodes[i]
    elementEnd = xAtNodes[i + 1]
    K[i:i + 2, i:i + 2] = K[i:i + 2, i:i + 2] + Kelement
    FBodyElement1 = -1*GaussQuadrature(3,elementStart,elementEnd,BodyForceStart)
    BodyForce[i, 0] = FBodyElement1 + FBodyElement2
    FBodyElement2 = -1*GaussQuadrature(3,elementStart,elementEnd,BodyForceEnd)
    BodyForce[i + 1, 0] = FBodyElement2

BoundaryForce = zeros([NumElements + 1, 1])
BoundaryForce[0, 0] = -10
TotalForce = BodyForce + BoundaryForce
# In order to invert the K matrix and thus solve for the unknown d values, K has to be partitioned along with F
Kpartition = K[0:NumElements, 0:NumElements]
Fpartition = TotalForce[0:NumElements, :]
#to get the d values, we invert the partitioned K and matrix multiply it with the partitioned F
dValues = matmul(Fpartition.T, linalg.inv(Kpartition))
#Here we implicitly apply the essential boundary condition and insert the newly derived d values
DisplacementAtNodes = zeros([NumElements+1, 1])
DisplacementAtNodes[0:NumElements, 0] = dValues
Displacement = zeros(NumElements*NumberOfPointsForPlotting)
Stress = zeros(NumElements*NumberOfPointsForPlotting)

index=-1

for q in range(NumElements):
    Nelement, Belement, xElement = shape_function(xAtNodes[q], xAtNodes[q + 1], NumberOfElementalNodes, NumberOfPointsForPlotting)
    for p in range(NumberOfPointsForPlotting):
        index+=1
        ElementDisplacement = dot(Nelement[p,:], DisplacementAtNodes[q:q + 2, 0])
        ElementStress = 100*dot(Belement[p,:], DisplacementAtNodes[q:q + 2, 0])
        Displacement[index] = -1*ElementDisplacement
        Stress[index] = -ElementStress


AnalyticSolution = -1*(linspace(0, 2, NumElements*NumberOfPointsForPlotting) * (0.12) + 0.04 * exp(-0.5 * linspace(0, 2, NumElements*NumberOfPointsForPlotting)) - ones(NumElements*NumberOfPointsForPlotting) * (0.24 + 0.04 / exp(1)))
AnalyticFirstDerivative = -(0.12 - 0.02 * exp(-0.5 * linspace(0, 2, NumElements*NumberOfPointsForPlotting)))

error = abs(AnalyticFirstDerivative*100 - Stress)

print('error=',size(error))
error = error*((xEnd-xStart)/(NumberOfPointsForPlotting*NumElements))
TotalError = sum(error)
print('err = ', error)
print('summed error=',TotalError)

plt.plot(linspace(0,2, NumElements*NumberOfPointsForPlotting), Displacement, 'g*', label='Approximation')
plt.plot(linspace(0,2, NumElements*NumberOfPointsForPlotting), AnalyticSolution, 'b.', label='Analytic solution')
plt.ylabel('displacement (m)')
plt.xlabel('position (m)')
plt.legend()
plt.show()

plt.plot(linspace(0,2, NumElements*NumberOfPointsForPlotting), Stress, 'g*-', label='Approximation')
plt.plot(linspace(0,2, NumElements*NumberOfPointsForPlotting), 100 * AnalyticFirstDerivative, 'b.-', label='Analytic solution')
plt.xlabel('position (m)')
plt.ylabel('stress')
plt.legend()
plt.show()

plt.plot(linspace(0,2, NumElements*NumberOfPointsForPlotting), error)
plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
plt.xlabel('position (m)')
plt.ylabel('stress error')

plt.show()


