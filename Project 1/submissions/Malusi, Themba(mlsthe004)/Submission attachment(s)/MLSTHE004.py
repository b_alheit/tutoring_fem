#!/usr/bin/env python
# coding: utf-8

# In[47]:


import numpy as np
from numpy import linalg as LA
from scipy.linalg import eigh, det, inv
import matplotlib.pyplot as plt 
from math import exp
import pylab 

def bar(num_elems):
    x = np.linspace(0,2,20+1)  # dividing the interval into 19 equal space imtervals
    restrained_dofs =[0,]
    A = 1.0  # uniform cross sectiona area
    E = 100.0 # Young's modulus
    le =x[1]-x[0]
    #% creating the local element stiffness matrix derived in the text
    k = np.array([[1, -1],[-1, 1]])*float(A*E/le)
    ke = np.array([[1, -1],[-1, 1]])*float(E)
    #% Assmebling the Global stiffness matrix
    K = np.zeros((num_elems+1, num_elems+1))
    B = np.zeros((num_elems+1, num_elems+1))
    for i in range(num_elems):
        B_in = np.zeros((num_elems+1, num_elems+1))
        K_temp = np.zeros((num_elems+1, num_elems+1))
        K_temp[i:i+2,i:i+2] =k
        B_in[i:i+2,i:i+2] =ke
        B += B_in
        K += K_temp
    K1 =K
    K2 = B
    for dof in restrained_dofs:
        for i in [0,1]:
            K1 = np.delete(K1, dof, axis=i)
            K2 = np.delete(K2, dof, axis=i)
    return K,K1,K2


# In[7]:


def shape_function(x, num_elems):
    a =0; #% first point of the interval
    b =2; #% last point point of the interval
    n = num_elems+1;
    xnd = np.linspace(a,b,n); 
    restrained_dofs =[0,];
    #% the nodal values; python creates an array of values with n-1 values, hence n+1 need to used.
    # if we want to include the last value, i.e x(20)=2.
    loc = np.array((-0.577350269, 0.577350269)) # defining the location
    le = (b-a)/num_elems; #% length of the elements
    #w = np.array([1.0,1.0]) # defining the weight
    Fb = np.zeros((num_elems), object)
    def f(x,y): return ((y-x)/2)*loc+(y+x)/2
    N = np.zeros((num_elems), object)
    FORCE = np.zeros((num_elems), object)
    for e in range(num_elems):
        N[e] = np.array((xnd[e+1]-x, x-xnd[e]))*float(le**(-1))
        J=(xnd[e+1]-xnd[e])/2
        coor = f(xnd[e],xnd[e+1])
        Fb[e] = J*np.array(((xnd[e+1]-coor[0])*exp(-coor[0])+(xnd[e+1]-coor[1])*exp(-coor[1]), (coor[0]-xnd[e])*exp(-coor[0])+(coor[1]-xnd[e])*exp(-coor[1])))*float(le**(-1))
        FORCE[e] = Fb[e]-10*N[e]
        
    Global_force = np.zeros((num_elems+1), object)
    for i in range(num_elems):
            A = np.zeros((num_elems+1), object)
            A[i:i+2]=FORCE[i]
            Global_force += A
    Force_body = Global_force
    for dof in restrained_dofs:
        for i in [0]:
            Force_body = np.delete(Global_force, dof, axis=i)
    
    return N, Fb, Global_force, Force_body


# In[55]:


E = 100 # Young's modulus
num_elems =20;
res = shape_function(0,num_elems)
K, K1, K2=bar(num_elems)
disp = inv(K1).dot(res[3])
stress = E*K1.dot(disp)
#print('Global stiffness matrix is ','\n',K)
#print('===========================================================================')
#print('Global stiffness matrix with imposed essential condition is ','\n',K1)
#print('===========================================================================')
#print('===========================================================================')
#print('Global force vector with imposed essential condition is','\n', res[2])
print('===========================================================================')
print('Global force vector with imposed essential condition is','\n', res[3])
print('===========================================================================')
print('approximated dispacement is','\n', disp)


## Potting the displacement versus nodal points 
nodal = np.linspace(0,2,num_elems)
print(nodal)
def exact(x):
    return (-1/25)*np.exp(-x/2)+(2/25)*x+(np.exp(-1)-4)/25
## calculating the error at each nodal point and sketching the results
error=exact(nodal)-disp
#rint('error at the nodal point is','\n', error)
print('===============================================================================')
## Potting the displacement, error versus nodal points
fig = plt.figure()
ax = plt.subplot(111)
ax.plot(nodal, disp, 'ro', label ='numerical')
ax.plot( nodal, exact(nodal), 'bo', label='exact')
ax.plot(nodal,error, 'g*', label='error')
plt.xlabel('x[m)')
plt.ylabel('displacement[m], error')
plt.title('displacement and error versus x')
ax.legend()


# In[56]:


plt.plot(nodal, stress, 'mo-', label ='stress')
ax.legend()


# In[ ]:




