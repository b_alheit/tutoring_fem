import matplotlib.pyplot as plt
from numpy import *

########################################################################Define functions needed
def shape_function(xStart, xEnd, nen, Numpoints):
    Npoints = Numpoints                                 #number of points for plotting
    xplot= linspace(xStart, xEnd, Npoints)              #linspace of plotting points
    xnodes = linspace(xStart,xEnd,nen)                  #nodes in domain

    N = zeros((Npoints, nen))
    B = zeros((Npoints, nen))

    for i in range(nen):                                #calculate shape function N for each interval between nodes
        for q in range(Npoints):                        #calculate N for each plotting point
            Ntemp = 1                                   #initialise N for product
            for j in range(nen):
                if j!=i:                                #calculate product for general N excluding ith term
                    Ntemp = (xplot[q]-xnodes[j])/(xnodes[i]-xnodes[j])*Ntemp

            N[q, i] = Ntemp

    for i in range(nen):                                #calculate derivative of shape function B for each interval
        for q in range(Npoints):                        #calculate B for each plotting point
            Btemp = 0                                   #initialise B for sum
            for k in range(nen):                        #calculate sum for general B
                    Ntemp = 1
                    if k!=i:
                        for j in range(nen):
                            if j != i:
                                if j!=k:                #calculate product for B excluding kth and ith term
                                    Ntemp = (xplot[q] - xnodes[j]) / (xnodes[i] - xnodes[j]) * Ntemp
                        Btemp = Ntemp *1/(xnodes[i]-xnodes[k]) + Btemp

            B[q,i] = Btemp

    return N,B,xplot

def gaussquadrature(ngp, xstart, xstop, func, rownum, colnum, displacement, stress):
    x = [xstart, xstop]                                     #define domain

    xi1 = [0]
    xi2 = [1/sqrt(3), -1/sqrt(3)]
    xi3 = [0.7745966692, -0.7745966692, 0]
    XI = [xi1, xi2, xi3]                                    #create matrix of known xi values

    w1 = [2]
    w2 = [1, 1]
    w3 = [5/9, 5/9, 8/9]
    W = [w1, w2, w3]                                        #create matrix of corresponding weights

    if displacement==0:                                         #standard gauss quadrature
        I = zeros([rownum,colnum])                              #initialise sum with I being right size for input function
        J = (x[1]-x[0])/2                                       #calculate the Jacobian

        for q in range(rownum):                                 #for each row in input function
            for m in range(colnum):                             #for each column in input function
                for i in range(ngp):                           #sum over Gauss points
                    xi = (XI[int(ngp-1)][i])                   #pull out correct xi
                    w = (W[int(ngp-1)][i])                     #pull out correct weight
                    y = (x[1]+x[0])/2 + xi*(x[1]-x[0])/2       #determine isoparametric variable at Gauss point
                    intfunc = func(xstart, xstop, y)[q, m]     #calculate value of function at Gauss point
                    I[q,m] = I[q,m] + w*J*intfunc              #calculate "approximate integral value" at Gauss point and sum over
        return I

    else:                                                       #gauss quadrature that takes in displacement and stress for error function calculations
        Idisp = 0
        Istress = 0
        J = (x[1] - x[0]) / 2

        for i in range(ngp):
            xi = (XI[int(ngp - 1)][i])
            w = (W[int(ngp - 1)][i])
            y = (x[1] + x[0]) / 2 + xi * (x[1] - x[0]) / 2
            Idisp = Idisp + w * J * func(displacement, stress, y)[0]
            Istress = Istress + w * J * func(displacement, stress, y)[1]
        return Idisp, Istress


def bodyfunc(elstart, elstop, x):                           #define body force function
    mid = (elstop+elstart)/2
    N = zeros([3,1])
    N[0,0]= ((x-mid)*(x-elstop))/((elstart-mid)*(elstart-elstop))   #hard-coded symbolic shape functions for quadratic approximation
    N[1,0] = ((x-elstart)*(x-elstop))/((mid-elstart)*(mid-elstop))
    N[2,0] = ((x-elstart)*(x-mid))/((elstop-mid)*(elstop-elstart))
    return -exp(-0.5*x)*N

def kint(elstart, elstop, x):                               #define function to be integrated for K vector
    mid = (elstop + elstart)/2
    lenelement = elstop-elstart
    B = zeros([3, 1])
    B[0, 0] = (1/lenelement**2)*(4*x-2*(mid+elstop))        #hard-coded symbolic derivative functions for linear approximation
    B[1, 0] = (1/lenelement**2)*(-8*x+4*(elstart+elstop))
    B[2, 0] = (1/lenelement**2)*(4*x-2*(elstart+mid))
    return -100*(B.T*B)


###################################################################################Define parameters and initialise vectors

nelements = 100                                           #number of elements
xstart = 0                                                  #domain
xstop = 2
xnodes = linspace(xstart,xstop, nelements+1)                #nodal values
nelementpoints = 10000                                        #number of plotting points on each element
Fbody = zeros([2*nelements+1,1])                            #initialise global body force vector
K = zeros([2*nelements+1,2*nelements+1])                    #initialise global stiffness matrix
Fboundary = zeros([2*nelements+1,1])                        #initialise global boundary force vector
Fboundary[0,0] = -10                                        #input boundary condition

###################################################################################Calculate element vectors

for j in range(nelements):
    elementstart = xnodes[j]                                #define element domain
    elementstop = xnodes[j+1]
    Kelement = gaussquadrature(3, elementstart, elementstop, kint, 3, 3, 0, 0)    #calculate stiffness matrix on element using Gauss Quadrature
    K[2*j:2*j+3, 2*j:2*j+3] = K[2*j:2*j+3, 2*j:2*j+3] + Kelement            #put element stiffness matrix into global stiffness matrix
    Fbodyelement = gaussquadrature(3, elementstart, elementstop, bodyfunc, 3, 1, 0, 0)  #calculate body force on each element using Gauss Quadrature
    for g in range(3):
        Fbody[2*j+g,0] = Fbody[2*j+g,0]+ Fbodyelement[g]                    #put element body force vector into global body force vector

##################################################################################Process global vectors

Forcetotal = Fbody + Fboundary                                      #calculate total force vector
partitionedK = K[0:2*nelements,0:2*nelements]                       #partition stiffness matrix
partitionedF = Forcetotal[0:2*nelements,:]                          #partition force vector
dPsolved = matmul(partitionedF.T, linalg.inv(partitionedK))         #invert partitioned K and multiply with partitioned F to get displacement node values
dispnodevalues = zeros([2*nelements+1,1])
dispnodevalues[0:2*nelements,0] = dPsolved                          #input boundary displacement node value

###################################################################################Post-processing

displacement = []                                                   #initialise displacement and stress vectors for plotting
stress = []

for q in range(nelements):
    Nelement, Belement, xelement = shape_function(xnodes[q], xnodes[q + 1], 3, nelementpoints)  #calculate shape functions and derivatives on each element with number of plotting points
    for p in range(nelementpoints):
        dispelement = dot(Nelement[p,:], dispnodevalues[2*q:2*q+3,0])                           #calculate displacement and stress on each element
        stresselement = 100*dot(Belement[p,:], dispnodevalues[2*q:2*q+3,0])
        displacement.append(dispelement)                                                        #put into total displacement and stress vectors
        stress.append(stresselement)

###################################################################################Analytic solution comparison

exactsol = -1*(linspace(0,2,nelementpoints*nelements)*(0.12)+ 0.04*exp(-0.5*linspace(0,2,nelementpoints*nelements))-ones(nelementpoints*nelements)*(0.24+0.04/exp(1))) #calculate analytic solution
exactderiv = -1*(0.12-0.02*exp(-0.5*linspace(0,2,nelementpoints*nelements)))                          #calculate analytic derivative

plt.plot(linspace(0,2, nelementpoints*nelements), displacement, '--rs', label='FEM approximation', markevery=50)    #plot approximation and exact solution
plt.plot(linspace(0,2, nelementpoints*nelements), exactsol, '--b.', label='Exact solution',  markevery=50)
plt.xlabel('Position along bar (m)')
plt.ylabel('Displacement (m)')
plt.title('Displacement of bar using quadratic approximation')
plt.legend()
#plt.show()

plt.plot(linspace(0,2, nelementpoints*nelements), stress, '--rs', label='FEM approximation', markevery=50)
plt.plot(linspace(0,2, nelementpoints*nelements), exactderiv*100, '--b.', label='Exact solution',  markevery=50)
plt.xlabel('Position along bar (m)')
plt.ylabel('Stress (N/m)')
plt.title('Stress along bar using quadratic approximation')
plt.legend()
#plt.show()

############################################################################################Error Analysis

error = abs(exactsol-displacement)                                      #error for plotting

plt.plot(linspace(0,2, nelementpoints*nelements), error, 'k-')          #plot error
plt.xlabel('Position along bar (m)')
plt.ylabel('Error (m)')
plt.title('Error on displacement for quadratic approximation')
#plt.show()

errorstress = abs(exactderiv*100-stress)

plt.plot(linspace(0,2, nelementpoints*nelements), errorstress, 'k-')
plt.xlabel('Position along bar (m)')
plt.ylabel('Error (N/m)')
plt.title('Error on stress for quadratic approximation')
#plt.show()

def errorint(displacement, stress, x):                                          #calculate L2 norm error
    xnodes = linspace(0,2, len(displacement))
    neededx = xnodes[0]                             #find required independent variable (bar position x here) that is closest to input independent variable in Gauss Quadrature
    for i in range(len(xnodes)-1):
        if abs(x-neededx)>abs(x-xnodes[i+1]):       #find nodal value closest to input independent value
            neededx = xnodes[i+1]
            neededindex = i+1                       #record index of closest nodal values
    disperror = abs(displacement[neededindex]+(0.12*x+0.04*exp(-0.5*x)-(0.24+0.04*exp(-1))))**2     #define L2 norm as absolute value (displacement/stress value at input indep. variable - exact solution at input variable)
    stresserror = abs(stress[neededindex]+(0.12-0.02*exp(-0.5*x))*100)**2
    return disperror, stresserror

totalL2errsqdisp = 0                                                         #initialise error
totalL2errsqstress = 0
for t in range(nelements):
    errelementdisp, errelementstress = gaussquadrature(2, xnodes[t], xnodes[t+1], errorint,0,0, displacement, stress)   #calculate L2 norm error on each element using Gauss quadrature
    totalL2errsqdisp  = totalL2errsqdisp + errelementdisp                                     #sum up integral on each element
    totalL2errsqstress = totalL2errsqstress + errelementstress

print('Displacement error=',sqrt(totalL2errsqdisp))
print('Stress error=',sqrt(totalL2errsqstress))