#####################################CODE IS PRACTICALLY IDENTICAL TO QUADRATIC CODE. DIFFERENCES ARE COMMENTED.#########################################
import matplotlib.pyplot as plt
from numpy import *

########################################################################Define functions needed

def shape_function(xStart, xEnd, nen, Numpoints):
    Npoints = Numpoints
    xplot= linspace(xStart, xEnd, Npoints)
    xnodes = linspace(xStart,xEnd,nen)

    N = zeros((Npoints, nen))
    B = zeros((Npoints, nen))

    for i in range(nen):
        for q in range(Npoints):
            Ntemp = 1
            for j in range(nen):
                if j!=i:
                    Ntemp = (xplot[q]-xnodes[j])/(xnodes[i]-xnodes[j])*Ntemp

            N[q, i] = Ntemp

    for i in range(nen):
        for q in range(Npoints):
            Btemp = 0
            for k in range(nen):
                    Ntemp = 1
                    if k!=i:
                        for j in range(nen):
                            if j != i:
                                if j!=k:
                                    Ntemp = (xplot[q] - xnodes[j]) / (xnodes[i] - xnodes[j]) * Ntemp
                        Btemp = Ntemp *1/(xnodes[i]-xnodes[k]) + Btemp

            B[q,i] = Btemp

    return N,B,xplot

def gaussquadrature(ngp, xstart, xstop, func, order, displacement, stress):
    x = [xstart, xstop]

    xi1 = [0]
    xi2 = [1/sqrt(3), -1/sqrt(3)]
    xi3 = [0.7745966692, -0.7745966692, 0]
    XI = [xi1, xi2, xi3]

    w1 = [2]
    w2 = [1, 1]
    w3 = [5/9, 5/9, 8/9]
    W = [w1, w2, w3]

    if displacement ==0:
        I = zeros([2,1])
        J = (x[1]-x[0])/2

        for q in range(order):
            for i in range(ngp):
                xi = (XI[int(ngp-1)][i])
                w = (W[int(ngp-1)][i])
                y = (x[1]+x[0])/2 + xi*(x[1]-x[0])/2
                intfunc = func(xstart, xstop, y)[q, 0]
                I[q,0] = I[q,0] + w*J*intfunc
        return I

    else:
        Idisp = 0
        Istress = 0
        J = (x[1] - x[0]) / 2

        for i in range(ngp):
            xi = (XI[int(ngp - 1)][i])
            w = (W[int(ngp - 1)][i])
            y = (x[1] + x[0]) / 2 + xi * (x[1] - x[0]) / 2
            Idisp = Idisp + w * J * func(displacement, stress, y)[0]
            Istress = Istress + w * J * func(displacement, stress, y)[1]
        return Idisp, Istress


def bodyfunc(elstart, elstop, x):
    N = zeros([2,1])
    N[0,0]=(elstop -x)/(elstop-elstart)                                 #hard-coded symbolic shape functions for linear approximation
    N[1,0] = (x-elstart)/(elstop-elstart)
    return -exp(-0.5*x)*N

###################################################################################Define parameters and initialise vectors

nelements = 20
xstart = 0
xstop = 2
xnodes = linspace(xstart,xstop,nelements+1)
nelementpoints = 100
Fbody = zeros([nelements+1,1])
K = zeros([nelements+1,nelements+1])
Fboundary = zeros([nelements+1,1])
N0, blank1 , blank2 = shape_function(xnodes[0], xnodes[1], 2, 1)
Fboundary[0,0] = -10

###################################################################################Calculate element vectors

for j in range(nelements):
    elementstart = xnodes[j]
    elementstop = xnodes[j+1]
    N, B, xplot = shape_function(elementstart, elementstop, 2, 1)                  #calculate B on element
    Kelement = -100*(B.T*B)*(elementstop-elementstart)                              #calculate stiffness matrix on element with integration already done
    K[j:j+2, j:j+2] = K[j:j+2, j:j+2] + Kelement
    Fbodyelement = gaussquadrature(3, elementstart, elementstop, bodyfunc, 2, 0, 0)
    Fbody[j,0] = Fbody[j,0]+ Fbodyelement[0]
    Fbody[j+1, 0] = Fbody[j+1, 0]+ Fbodyelement[1]

##################################################################################Process global vectors

Forcetotal = Fbody + Fboundary
partitionedK = K[0:nelements,0:nelements]
partitionedF = Forcetotal[0:nelements,:]

dPsolved = matmul(partitionedF.T, linalg.inv(partitionedK))
dispnodevalues = zeros([nelements+1,1])
dispnodevalues[0:nelements,0] = dPsolved

###################################################################################Post-processing

displacement = []
stress = []

for q in range(nelements):
    Nelement, Belement, xelement = shape_function(xnodes[q], xnodes[q + 1], 2, nelementpoints)
    for p in range(nelementpoints):
        dispelement = dot(Nelement[p,:], dispnodevalues[q:q+2,0])
        stresselement = 100*dot(Belement[p,:], dispnodevalues[q:q+2,0])
        displacement.append(dispelement)
        stress.append(stresselement)

###################################################################################Analytic solution comparison

exactsol = -1*(linspace(0,2,nelementpoints*nelements)*(0.12)+ 0.04*exp(-0.5*linspace(0,2,nelementpoints*nelements))-ones(nelementpoints*nelements)*(0.24+0.04/exp(1)))
exactderiv = -1*(0.12-0.02*exp(-0.5*linspace(0,2,nelementpoints*nelements)))

plt.plot(linspace(0,2, nelementpoints*nelements), displacement, '--rs', label='FEM approximation', markevery=50)
plt.plot(linspace(0,2, nelementpoints*nelements), exactsol, '--b.', label='Exact solution',  markevery=50)
plt.xlabel('Position along bar (m)')
plt.ylabel('Displacement (m)')
plt.title('Displacement of bar using linear approximation')
plt.legend()
plt.show()

plt.plot(linspace(0,2, nelementpoints*nelements), stress, '--rs', label='FEM approximation', markevery=50)
plt.plot(linspace(0,2, nelementpoints*nelements), exactderiv*100, '--b.', label='Exact solution',  markevery=50)
plt.xlabel('Position along bar (m)')
plt.ylabel('Stress (N/m)')
plt.title('Stress along bar using linear approximation')
plt.legend()
plt.show()

############################################################################################Error Analysis

error = abs(exactsol-displacement)

plt.plot(linspace(0,2, nelementpoints*nelements), error, 'k-')
plt.xlabel('Position along bar (m)')
plt.ylabel('Error (m)')
plt.title('Error on displacement for linear approximation')
#plt.show()

errorstress = abs(exactderiv*100-stress)

plt.plot(linspace(0,2, nelementpoints*nelements), errorstress, 'k-')
plt.xlabel('Position along bar (m)')
plt.ylabel('Error (N/m)')
plt.title('Error on stress for linear approximation')
#plt.show()


def errorint(displacement, stress, x):
    xnodes = linspace(0,2, len(displacement))
    neededx = xnodes[0]
    for i in range(len(xnodes)-1):
        if abs(x-neededx)>abs(x-xnodes[i+1]):
            neededx = xnodes[i+1]
            neededindex = i+1
    disperror = abs(displacement[neededindex]+(0.12*x+0.04*exp(-0.5*x)-(0.24+0.04*exp(-1))))**2
    stresserror = abs(stress[neededindex]+(0.12-0.02*exp(-0.5*x))*100)**2
    return disperror, stresserror

totalL2errsqdisp = 0
totalL2errsqstress = 0
for t in range(nelements):
    errelementdisp, errelementstress = gaussquadrature(2, xnodes[t], xnodes[t+1], errorint, 0, displacement, stress)
    totalL2errsqdisp  = totalL2errsqdisp + errelementdisp
    totalL2errsqstress = totalL2errsqstress + errelementstress

print('Displacement error=',sqrt(totalL2errsqdisp))
print('Stress error=',sqrt(totalL2errsqstress))