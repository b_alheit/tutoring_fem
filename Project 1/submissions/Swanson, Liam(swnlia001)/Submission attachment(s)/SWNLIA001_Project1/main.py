# MEC5063Z PROJECT 1
# LIAM SWANSON
# SWNLIA001
# 08 APRIL 2019

import numpy as np
import matplotlib.pyplot as plt

from mesher import mesher1D, elementTable
from NB_functions import shapeFunction
from stiffnessMatrix import localStiffnessMatrixCalculator, bodyForceMatrixCalculator
from postProcessor import postProcess, analytical_u, analytical_F
from L2Norm import L2error

##################################
# SYSTEM SET UP
##################################

# Define system Properties
E = 100         # N/m^-2
A = 1           # m^2
P = 10          # N/m^-2

# Define the domain of the 1D bar
x_bar = np.array([[0.0, 2.0]])

# Define x location of Dirichlet BC and Traction BC
x_dirichlet = 2 # m
x_traction = 0  # m

# Define BC
u_d = 0         # m

# Define Mesh Parameters
elements = 20                   # No. Elements
growth_factor = 1               # Kept at 1
order = 2                       # Element Order
nodes = (elements * order) + 1  # Number of nodes on element
n_gp = 3                        # Gauss quadrature order

##################################
# MESH GENERATION
##################################

# Generate 1D mesh with node number starting at dirichlet boundary
x_mesh = mesher1D(x_bar, elements, order, growth_factor, x_dirichlet)

# Generate ICA defining and ordering element nodes such that the nodes are in increasing x order
ICA = elementTable(elements, order, x_mesh)

# Construct an array of element nodal x positions
x_element = np.zeros((elements, order + 1))
for i in range(elements):
    for j in range(order + 1):
        x_element[i, j] = x_mesh[0, ICA[i, j]]

##################################
# SOLVING
##################################

# Construct an empty global stiffness matrix, global force and global displacement vectors
K = np.zeros((nodes, nodes))
F = np.zeros((nodes, 1))
d = np.zeros((nodes, 1))

# Populate displacement vector with dirichlet boundary conditions
for t in range(nodes):
    if x_mesh[0, t] == x_dirichlet:
        d[0, t] = u_d

# Loop through each element to calculate stiffness and force matrices for the element and populate global matrices
for k in range(elements):
    K_e = localStiffnessMatrixCalculator(x_element[k], n_gp, E, A)
    F_be = bodyForceMatrixCalculator(x_element[k], n_gp)

    # Calculate dirichlet boundary condition contribution
    F_te = np.zeros((1, order + 1)).T
    if x_element[k, 0] <= x_traction <= x_element[k, 1]:
        N_0 = shapeFunction(x_traction, x_element[k]).T
        F_te = np.multiply(N_0, A * P)

    # Calculate total element body force
    F_e = F_be + F_te

    # Place the element stiffness matrix entries into the global matrix
    for f in range(order + 1):
        for g in range(order + 1):
            row_kg = ICA[k, f]
            column_kg = ICA[k, g]
            K[row_kg, column_kg] += K_e[f, g]

    # Place the element force matrix entries into the global matrix
    for s in range(order + 1):
        row_fg = ICA[k, s]
        F[row_fg] += F_e[s]

# Partition the matrices and vectors
# Stiffness matrix partition
Kp_E = K[:0 + 1, :0 + 1]
Kp_EF = K[:0 + 1, 0 + 1:]
Kp_F = K[0 + 1:, 0 + 1:]

# Displacement matrix partition
dp_E = d[:0 + 1]

# Force matrix partition
fp_F = F[0 + 1:]

# Solve for unknown displacements
dp_F = np.dot(np.linalg.inv(Kp_F), (fp_F - np.dot(Kp_EF.T,  dp_E)))

# Rebuild global displacement vector using solved values
d[0] = dp_E
d[0 + 1:] = dp_F

# Solve for reaction forces
rp_E = np.dot(Kp_E, dp_E) + np.dot(Kp_EF, dp_F)

# Replace nodal value of reaction force in Global Force vector
F[0] = rp_E

##################################
# POST-PROCESSING
##################################

samples_el = 1000
u_computational = postProcess(elements, order, d, ICA, x_element, samples_el, True)
F_computational = E * postProcess(elements, order, d, ICA, x_element, samples_el, False)

##################################
# ERROR ANALYSIS
##################################

# NOTE: Arrays were populated based on results obtained through running the simulation for various mesh sizes

d_L2, d_enorm = L2error(elements, E, order, d, ICA, x_element, n_gp, True)
T_L2, T_enorm = L2error(elements, E, order, d, ICA, x_element, n_gp, False)
print('\nDisplacement L2Norm of Error:', d_L2)
print('\nStress L2Norm of Error:', T_L2)
print('\nDisplacement Normalized Error:', d_enorm)
print('\nStress Normalized Error:', T_enorm)

meshErrorD_lin = [0.0010491720615263657, 4.213435880210971e-05, 1.0534913706234786e-05, 1.6856535858753868e-06,
                  1.0536337318174329e-07]
meshErrorT_lin = [0.282515253839312, 0.054863311444212325, 0.027318055658036244, 0.010899512284311302,
                  0.0027213836780535633]

meshErrorD_quad = [2.327625929406047e-05, 1.8247475913993823e-07, 2.273615305818956e-08, 1.4535624948063251e-09,
                   2.735441988968659e-11]
meshErrorT_quad = [0.023242245115948787, 0.00093620172207076, 0.00023410154431383122, 3.7458448302005455e-05,
                   2.3410705525997686e-06]

normErrorD_lin = [0.0055609738878916685,0.0003079955219936542, 8.640378776241444e-05, 1.5817854875933566e-05,
                  1.1772651868183711e-06]
normErrorT_lin = [1.4974286075054932, 0.4010421970165925, 0.2240535200287333, 0.10227927994142293, 0.030408774255337057]

normErrorD_quad = [0.00012337274768995393, 1.3338752542188478e-06, 1.8649121366535119e-07, 1.3631530982387077e-08,
                   2.7485762947878544e-10]
normErrorT_quad = [0.1231919385067894, 0.006843487403528606, 0.0019200219304753006, 0.00035150366736969355,
                   2.6158528244392143e-05]

meshSize = [2 / 2, 2 / 10, 2 / 20, 2 / 50, 2 / 200]

##################################
# PLOTTING RESULTS
##################################

x_gspace = np.linspace(x_bar[0, 0], x_bar[0, -1], elements * (samples_el - 1) + 1)

u_analytical = []
F_analytical = []

for h in x_gspace:
    u_x = analytical_u(h)
    F_x = analytical_F(h)

    u_analytical.append(u_x)
    F_analytical.append(F_x)

u_analytical = np.array(u_analytical)
F_analytical = np.array(F_analytical)

# Figure 1
fig1 = plt.subplots(1, 2)
ax1 = plt.subplot(121, title='Computational and Analytical Solution For \n Displacement Distributions')
ax2 = plt.subplot(122, title='Computational and Analytical Solution For \n Stress Distributions')

ax1.plot(x_gspace, u_analytical,  color='black', linewidth=3, label='Analytical Displacement')
ax1.plot(x_gspace, u_computational, color='red', linewidth=2, linestyle='--', label='Computational Displacement')
ax1.set_xlabel('x (m)')
ax1.set_ylabel('Displacement (m)')
ax1.legend(loc='upper right')

ax2.plot(x_gspace, F_analytical,  color='black', linewidth=3, label='Analytical Stress')
ax2.plot(x_gspace, F_computational,  color='red', linewidth=2, linestyle='--', label='Computational Stress')
ax2.set_xlabel('x (m)')
ax2.set_ylabel('Stress (Pa)')
ax2.legend(loc='upper right')

# Figure 2
fig2 = plt.subplots(1, 2)
ax3 = plt.subplot(121, title='L2NORM OF ERROR IN DISPLACEMENT USING LINEAR AND QUADRATIC ELEMENTS')
plt.yscale('log')
plt.xscale('log')
ax4 = plt.subplot(122, title='L2NORM OF ERROR IN STRESS USING LINEAR AND QUADRATIC ELEMENTS')
plt.yscale('log')
plt.xscale('log')

ax3.plot(meshSize, meshErrorD_lin, color='black', linewidth=2, marker='o', label='Linear Elements')
ax3.plot(meshSize, meshErrorD_quad, color='red', linewidth=2, marker='o', label='Quadratic Elements')
ax3.set_xlabel('Mesh Element Size')
ax3.set_ylabel('L2Norm Error Of Displacement')
ax3.legend(loc='upper left')

ax4.plot(meshSize, meshErrorT_lin, color='black', linewidth=2, marker='o', label='Linear Elements')
ax4.plot(meshSize, meshErrorT_quad, color='red', linewidth=2, marker='o', label='Quadratic Elements')
ax4.set_xlabel('Mesh Element Size')
ax4.set_ylabel('L2Norm Error Of Stress')
ax4.legend(loc='upper left')

# Figure 4
fig4 = plt.subplots(1, 2)
ax7 = plt.subplot(121, title='NORMALIZED L2NORM OF ERROR IN DISPLACEMENT USING LINEAR AND QUADRATIC ELEMENTS')
plt.yscale('log')
plt.xscale('log')
ax8 = plt.subplot(122, title='NORMALIZED L2NORM OF ERROR IN STRESS USING LINEAR AND QUADRATIC ELEMENTS')
plt.yscale('log')
plt.xscale('log')

ax7.plot(meshSize, normErrorD_lin, color='black', linewidth=2, marker='o', label='Linear Elements')
ax7.plot(meshSize, normErrorD_quad, color='red', linewidth=2, marker='o', label='Quadratic Elements')
ax7.set_xlabel('Mesh Element Size')
ax7.set_ylabel('L2Norm Error Of Displacement')
ax7.legend(loc='upper left')

ax8.plot(meshSize, normErrorT_lin, color='black', linewidth=2, marker='o', label='Linear Elements')
ax8.plot(meshSize, normErrorT_quad, color='red', linewidth=2, marker='o', label='Quadratic Elements')
ax8.set_xlabel('Mesh Element Size')
ax8.set_ylabel('L2Norm Error Of Stress')
ax8.legend(loc='upper left')

plt.show()

##################################
# END
