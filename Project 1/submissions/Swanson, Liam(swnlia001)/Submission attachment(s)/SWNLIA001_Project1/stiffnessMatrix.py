# MEC5063Z PROJECT 1
# LIAM SWANSON
# SWNLIA001
# 08 APRIL 2019

import numpy as np
import math
from gaussQuad import gaussQuadratureInfo
from NB_functions import derivativeFunction, shapeFunction


def localStiffnessMatrixCalculator(x_el, orderGQ, youngs, area):
    """
    Function to create local elemental stiffness matrices
    :param x_el: array of element nodal positions
    :param orderGQ: order of gauss quadrature
    :param youngs: Young's modulus
    :param area: area of bar
    :return: element stiffness matrix
    """
    # Initialise element stiffness matrix
    K_e = np.zeros((len(x_el), len(x_el)))

    # Obtain gauss quadrature information based on element information
    chi, w, J = gaussQuadratureInfo(x_el, orderGQ - 1)

    # Loop to generate each entry in the local stiffness matrix
    for j in range(len(x_el)):
        for k in range(len(x_el)):
            for i in range(orderGQ):
                x_chi = ((x_el[0] + x_el[-1]) / 2) + ((x_el[-1] - x_el[0]) / 2) * chi[i]
                B = derivativeFunction(x_chi, x_el)
                K_e[j, k] += w[i] * B[0, k] * B[0, j]

    K_e = np.multiply(K_e, (J * youngs * area))

    return K_e


def bodyForce(x):
    """
    Function defining the body function on the element
    :param x: x-coordinate
    :return: value of function at the x coordinate
    """
    return math.exp(-x / 2)


def bodyForceMatrixCalculator(x_el, orderGQ):
    """
    Function calculating the force matrix due to the body force on the bar
    :param x_el: array of nodal positions of the local element
    :param orderGQ: order of gauss quadrature
    :return: array of local nodal body force values
    """
    # Initialise element force vector
    F_e = np.zeros((1, len(x_el))).T

    # Extract gauss quadrature information based on element information
    chi, w, J = gaussQuadratureInfo(x_el, orderGQ - 1)

    # Loop through the element nodes and calculate the nodal values of the body ofrce
    for n in range(len(x_el)):
        for m in range(orderGQ):
            x_chi = ((x_el[0] + x_el[-1]) / 2) + ((x_el[-1] - x_el[0]) / 2) * chi[m]
            N = shapeFunction(x_chi, x_el)
            F_e[n] += w[m] * N[0, n] * bodyForce(x_chi)

    F_e = np.multiply(F_e, J)

    return F_e

