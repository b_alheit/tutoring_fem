# MEC5063Z PROJECT 1
# LIAM SWANSON
# SWNLIA001
# 08 APRIL 2019

import numpy as np
from gaussQuad import gaussQuadratureInfo
from NB_functions import shapeFunction, derivativeFunction
from postProcessor import analytical_F, analytical_u


def L2error(n_el, youngs, n_order, disp_global, ICA, x_el_global, orderGQ, displacement):
    """
    Function which calculates the L2Norm of the error and the normalized L2Norm between the analytical and FEM
    solutions for both displacement and stress.
    :param n_el: number of elements
    :param youngs: Young's Modulus
    :param n_order: order of elements
    :param disp_global: global nodal displacement array
    :param ICA: ICA
    :param x_el_global: array holding all element node coordinates
    :param orderGQ: order of gauss quadrature
    :param displacement: True if finding L2Norm of displacement error. False if for stress
    :return: Value of the L2Norm over the domain of the bar
    """

    # Organise nodal displacements to correlate to their elements
    f_element = np.zeros((n_el, n_order + 1))
    for i in range(n_el):
        for j in range(n_order + 1):
            f_element[i, j] = disp_global[ICA[i, j]]

    # Initialise the global L2Norm value
    L2e_g = 0
    L2e_gnorm = 0

    # Loop through each element to find element L2Norm error
    for k in range(n_el):

        chi, w, J = gaussQuadratureInfo(x_el_global[k], orderGQ - 1)        # Extract gauss quadrature information
        L2e_l = 0                                                           # Initialise the elemental L2Norm value
        L2e_norm = 0                                                        # Initialise the analytical L2Norm norm

        # Run gauss quadrature to evaluate L2Norm Error integral on the element
        for m in range(orderGQ):
            x_chi = ((x_el_global[k, 0] + x_el_global[k, -1]) / 2) + \
                    ((x_el_global[k, -1] - x_el_global[k, 0]) / 2) * chi[m]

            # Choose method based on if solving for stress or displacement
            if displacement is True:
                interpelator_el = shapeFunction(x_chi, x_el_global[k])[0]
                L2e_l += w[m] * ((analytical_u(x_chi) - np.dot(interpelator_el, f_element[k])) ** 2) ** 0.5
                L2e_norm += w[m] * (analytical_u(x_chi))

            elif displacement is False:
                interpelator_el = derivativeFunction(x_chi, x_el_global[k])[0]
                L2e_l += w[m] * ((analytical_F(x_chi) - youngs * np.dot(interpelator_el, f_element[k])) ** 2) ** 0.5
                L2e_norm += w[m] * (analytical_u(x_chi))

        # Sum each element L2Norms to global L2Norm
        L2e_g += L2e_l * J
        L2e_gnorm += L2e_l * J / L2e_norm

    return L2e_g, L2e_gnorm
