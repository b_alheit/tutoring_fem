# MEC5063Z PROJECT 1
# LIAM SWANSON
# SWNLIA001
# 08 APRIL 2019

import numpy as np
import math
from NB_functions import shapeFunction, derivativeFunction


def analytical_u(x):
    """
    Return the value of the analytical solution to the displacement
    :param x: x coordinate
    :return: value of solution at x coordinate
    """
    return (1 / 100) * (-4 * math.exp(-x / 2) - 12 * x + 4 * math.exp(-1) + 24)


def analytical_F(x):
    """
    Return the value of the analytical solution to the stress
    :param x: x coordinate
    :return: value of solution at x coordinate
    """
    return 2 * math.exp(-x / 2) - 12


def postProcess(n_el, n_order, disp_global, ICA, x_el_global, n_sample, displacement):
    """
    Function which generates an array of plotting points of the FEM solution for displacement and stress based on
    the interpolation using shape functions and their derivatives
    :param n_el: number of elements
    :param n_order: order of the elements
    :param disp_global: global nodal displacement matrix
    :param ICA: ICA
    :param x_el_global: global element nodal coordinates
    :param n_sample: number of sample points over domain
    :param displacement: True if finding L2Norm of displacement error. False if for stress.
    :return: array of plotting values
    """

    # Organise the nodal displacements into array according to element numbering
    d_element = np.zeros((n_el, n_order + 1))
    for y in range(n_el):
        for z in range(n_order + 1):
            d_element[y, z] = disp_global[ICA[y, z]]

    # Use shape functions or derivative shape functions to generate plotting points of FEM solutions to
    # stress and displacement functions

    # Plot distributions over the element space
    v_distribution = []
    for w in range(n_el):
        x_elSpace = np.linspace(x_el_global[w, 0], x_el_global[w, -1], n_sample)

        if displacement is True:
            interpelator_el = shapeFunction(x_elSpace, x_el_global[w])[0]

        elif displacement is False:
            interpelator_el = derivativeFunction(x_elSpace, x_el_global[w])[0]

        v_e = np.zeros((1, n_sample))
        for v in range(n_order + 1):
            v_e += interpelator_el[v].T * d_element[w, v]

        # Append element distributions to global list
        v_distribution.append(v_e[0])

    v_elementDistribution = np.asarray(v_distribution)

    # Rearrange elemental distribution into a continuous global numpy array
    v_elementTotal = []
    for u in range(n_el):
        if u == 0:
            v_elementTotal.append(v_elementDistribution[-(u + 1)])

        else:
            v_elementTotal.append(v_elementDistribution[-(u + 1)][1:])

    v_elementTotal = np.asarray(v_elementTotal)
    v_elementTotal = np.concatenate(v_elementTotal)

    return v_elementTotal
