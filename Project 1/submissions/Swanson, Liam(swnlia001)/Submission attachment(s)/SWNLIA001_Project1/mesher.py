# MEC5063Z PROJECT 1
# LIAM SWANSON
# SWNLIA001
# 08 APRIL 2019

import numpy as np


def mesher1D(x_domain, n_elements, element_order, growth_ratio, x_dirichlet):
    """
    1D Mesh generator based on number of elements and element order
    :param x_domain: domain bounds as 1 x 2 array
    :param n_elements: number of desired elements
    :param element_order: desired element order
    :param growth_ratio: growth ratio based from 0 x coordinate node
    :param x_dirichlet: x-coordinate of Dirichlet BC
    :return: array of nodal x-coordinates
    """
    n_nodes_prim = n_elements + 1                       # Calculate number of element boundary nodes
    n_nodes_t = (n_elements * element_order) + 1        # Calculate total number of mesh nodes

    # Initialise arrays
    x_nodes_prim = np.zeros((1, n_nodes_prim))
    x_nodes_t = np.zeros((1, n_nodes_t))
    x_mesh = np.zeros((1, n_nodes_t))
    dx_i = 0

    # Find the initial element size
    if growth_ratio != 1 and growth_ratio > 0:
        dx_i = (x_domain[0, -1] - x_domain[0, 0]) * (1 - growth_ratio) / (1 - growth_ratio ** n_elements)

    elif growth_ratio == 1:
        dx_i = (x_domain[0, -1] - x_domain[0, 0])/n_elements

    elif growth_ratio <= 0:
        print("INVALID GROWTH FACTOR!")
        exit()

    # Generate all nodal coordinates for element of any order
    # First establish where element boundary nodes are
    for i in range(n_nodes_prim - 1):
        x_nodes_prim[0, i + 1] = x_nodes_prim[0, i] + dx_i * growth_ratio ** i

    # Fill in element nodes to match order of element
    for j in range(n_nodes_prim):
        x_nodes_t[0, element_order * j] = x_nodes_prim[0, j]

        if j < n_nodes_prim - 1:
            for k in range(element_order):
                x_nodes_t[0, (element_order * j) + k + 1] = x_nodes_t[0, (element_order * j) + k] + \
                                                            (x_nodes_prim[0, j + 1] - x_nodes_prim[0, j]) / element_order

    x_nodes_t = np.around(x_nodes_t, 7)              # Consequence of machine rounding to ensure mesh reaches boundary

    # Re order mesh to have first node at dirichlet boundary
    if x_nodes_t[0, 0] == x_dirichlet:
        x_mesh = x_nodes_t

    elif x_nodes_t[0, -1] == x_dirichlet:
        x_mesh = np.flip(x_nodes_t, 1)

    return x_mesh


def elementTable(n_elements, order, mesh):
    """
    Generate ICA to link element nodes to global nodal numbering
    :param n_elements: number of elements in mesh
    :param order: order of elements
    :param mesh: array of mesh nodal values
    :return: ICA
    """

    # Initialise a table of elemental coordinates
    ICA = np.zeros((n_elements, order + 1), dtype=int)

    # Loop through nodal coordinates adding them to element coordinates appropriately based on any order of element
    for l in range(n_elements):
        for m in range(order + 1):
            ICA[l, m] = int(l * order + m)

    # Loop through each element to number node
    for t in range(n_elements):
        n_left = int(ICA[t, 0])
        n_right = int(ICA[t, -1])
        if mesh[0, n_right] - mesh[0, n_left] <= 0:
            ICA[t] = np.flip(ICA[t], 0)

    return ICA
