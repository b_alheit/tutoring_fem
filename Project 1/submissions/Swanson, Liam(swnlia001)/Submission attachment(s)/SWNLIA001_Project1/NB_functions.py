# MEC5063Z PROJECT 1
# LIAM SWANSON
# SWNLIA001
# 08 APRIL 2019

import numpy as np


def shapeFunction(x_value, x_el):
    """
    Find the value of the local shape functions at an x-position
    :param x_value: x-coordinate
    :param x_el: array of local element nodal positions
    :return: array of value of each nodes shape function on the element at the point specified
    """

    # Define empty shape and derivative function arrays for values to be appended to
    M_shape = []

    # Loop which calculates the value of a shape function at a point of an element
    # The ith iteration indicates the loop over each global node, the jth iteration indicates the iteration
    # over each product in the sequence

    for xi in x_el:
        N_p = 1

        for xj in x_el:
            if xj != xi:
                N_p *= (x_value - xj)/(xi - xj)

        M_shape.append(N_p)

    # Cast appended lists as numpy arrays
    M_shape = np.array([M_shape])

    return M_shape


def derivativeFunction(x_value, x_el):
    """
    Find the value of the local shape functions at an x-position
    :param x_value: x-coordinate
    :param x_el: array of local element nodal positions
    :return: array of value of each nodes shape function on the element at the point specified
    """

    # Define empty derivative function array
    M_derivative = []

    # Loop which calculates the value of a derivative function at a point of an element
    # The ith iteration indicates the loop over each global node, the jth iteration indicates the iteration
    # over each product in the sequence

    for xi in x_el:
        sum_loop = 0
        prod_loop = 1

        for xj in x_el:
            if xj != xi:
                r = 1 / (xi - xj)

                for xm in x_el:
                    if xm != xj and xm != xi:
                        prod_loop = 1
                        prod_loop *= (x_value - xm) / (xi - xm)

                sum_loop += r * prod_loop
        M_derivative.append(sum_loop)

    M_derivative = np.array([M_derivative])

    return M_derivative
