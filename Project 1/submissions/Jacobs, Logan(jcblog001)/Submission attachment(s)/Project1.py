# import matplotlib as plt
import numpy as np
import math as math
import matplotlib.pyplot as plt

def shape(x, x_r, x_l):

    # define n
    n = np.array([(x_r - x)/(x_r - x_l), (x - x_l)/(x_r - x_l)])
    # n transpose
    nt = np.array([[(x_r - x) / (x_r - x_l)], [(x - x_l) / (x_r - x_l)]])
    # define gradient shape function
    g = np.array([(-1)/(x_r - x_l), 1 / (x_r - x_l)])
    # transpose
    gt = np.array([[(-1)/(x_r - x_l)], [1 / (x_r - x_l)]])
    # Jacobian
    j = (x_r - x_l)/2
    # state b(x) as given
    bx = math.exp(-x/2)
    w = 2  # we have the weight for Ngp = 1

    # Ke
    ke = np.array([100 * j * w * gt * g])
    # force on bound
    fe_o = j * w * nt * bx - 10 * np.array([[(x_r - 0)/(x_r - x_l)], [(0 - x_l)/(x_r - x_l)]])
    # force in element
    fe = j * w * bx * nt

    # Force vector
    if i == 0:
        print('FORCE per element', fe_o)  # J * w * N * e(-x(psi)/2) - 10* N(0), force for first element

    elif i != 0:
        #print('FORCE', fe[0])-9.9512345
        print('FORCE per element', fe)  # J * w * b * N, force for all other elements

    # Stiffness K
    print('Stiffness for element',  ke)  # J * b* b * w, global matrix for all elements

    return n, g, bx


def map(x_r, x_l, xi):
    # map f(x) to f(x(psi))
    x = (x_l + x_r)/2 + xi * (x_r - x_l)/2
    return x


# 20 element split
el = np.array([[0.0, 0.1],
                [0.1, 0.2],
                [0.2, 0.3],
                [0.3, 0.4],
                [0.4, 0.5],
                [0.5, 0.6],
                [0.6, 0.7],
                [0.7, 0.8],
                [0.8, 0.9],
                [0.9, 1.0],
                [1.0, 1.1],
                [1.1, 1.2],
                [1.2, 1.3],
                [1.3, 1.4],
                [1.4, 1.5],
                [1.5, 1.6],
                [1.6, 1.7],
                [1.7, 1.8],
                [1.8, 1.9],
                [1.9, 2.0]])


# create global K (manually)
# omitted last column and last row since u(L) = 0
k = np.array([[100, -100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [-100, 200, -100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, -100, 200, -100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, -100, 200, -100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, -100, 200, -100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, -100, 200, -100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, -100, 200, -100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, -100, 200, -100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, -100, 200, -100, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0, -100, 200, -100, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0, 0, -100, 200, -100, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -100, 200, -100, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -100, 200, -100, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -100, 200, -100, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -100, 200, -100, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -100, 200, -100, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -100, 200, -100, 0],
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -100, 200, -100],
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -100, 200]])

# observed that thrid gives negative d values, s=thus was corrected by -10*k
# left out a fact of 10 on k, so put it back
k1 = -10*k

# Inverse of k
k_i = np.linalg.inv(k1)

# create global F
f = np.array([[-9.9512345],
             [0.0487655+0.04638717],
             [0.04638717+0.04412488],
             [0.04412488+0.04197285],
             [0.04197285+0.03992581],
             [0.03992581+0.03797861],
             [0.03797861+0.03436446],
             [0.03436446+0.03268849],
             [0.03268849+0.03109425],
             [0.03109425+0.02957777],
             [0.02957777+0.02813524],
             [0.02813524+0.02676307],
             [0.02676307+0.02545782],
             [0.02545782+0.02421623],
             [0.02421623+0.02303219],
             [0.02303219+0.02191175],
             [0.02191175+0.0208431],
             [0.0208431+0.01982657],
             [0.01982657+0.01885962]])


for i in range(20):

    print('element = ', i+1)  # print the element number
    print(el[i])  # look at individual elements
    current_nodes = el[i, :]  # look at nodes for each element
    xi = np.array([0])  # value for psi given Ngp = 1
    x = map(current_nodes[1], current_nodes[0], xi)  # X(psi) for each element
    print('N = ', shape(x, current_nodes[1], current_nodes[0])[0])

print(f.shape)
d = np.dot(k_i, f)
print(d)

le = 0.1
N0 = np.array([(0.1 - x)/le, (x - 0)/le])
N1 = np.array([(0.2 - x)/le, (x - 0.1)/le])
N2 = np.array([(0.3 - x)/le, (x - 0.2)/le])
N3 = np.array([(0.4 - x)/le, (x - 0.3)/le])
N4 = np.array([(0.5 - x)/le, (x - 0.4)/le])
N5 = np.array([(0.6 - x)/le, (x - 0.5)/le])
N6 = np.array([(0.7 - x)/le, (x - 0.6)/le])
N7 = np.array([(0.8 - x)/le, (x - 0.7)/le])
N8 = np.array([(0.9 - x)/le, (x - 0.8)/le])
N9 = np.array([(1.0 - x)/le, (x - 0.9)/le])
N10 = np.array([(1.1 - x)/le, (x - 1.0)/le])
N11 = np.array([(1.2 - x)/le, (x - 1.1)/le])
N12 = np.array([(1.3 - x)/le, (x - 1.2)/le])
N13 = np.array([(1.4 - x)/le, (x - 1.3)/le])
N14 = np.array([(1.5 - x)/le, (x - 1.4)/le])
N15 = np.array([(1.6 - x)/le, (x - 1.5)/le])
N16 = np.array([(1.7 - x)/le, (x - 1.6)/le])
N17 = np.array([(1.8 - x)/le, (x - 1.7)/le])
N18 = np.array([(1.9 - x)/le, (x - 1.8)/le])

ux = np.sum(d[0] * N0 + d[1] * N1 + d[2] * N2 + d[3] * N3 + d[4] * N4 + d[5] * N5 + d[6] * N6 + d[7] * N7 + d[8] * N8
+ d[9] * N9 + d[10] * N10 + d[11] * N11 + d[12] * N12 + d[13] * N13 + d[14] * N14 + d[15] * N15 + d[16] * N16
+ d[17] * N17 + d[18] * N18)

# sigma
le = 0.1
B0 = np.array([(-1)/le, 1 / le])

sig = np.sum( 100 * [d[0] * B0 + d[1] * B0 + d[2] * B0 + d[3] * B0 + d[4] * B0 + d[5] * B0 + d[6] * B0 + d[7] * B0 + d[8] * B0
+ d[9] * B0 + d[10] * B0 + d[11] * B0 + d[12] * B0 + d[13] * B0 + d[14] * B0 + d[15] * B0 + d[16] * B0
+ d[17] * B0 + d[18] * B0])


print('u(x)= ', ux)
print('sig', sig)
plt.plot(el[:, 0].flatten()[:-1], d)
plt.show()
#print(np.sum(d))
#t = np.linspace(0, 2, 19)
#plt.plot(t, d)
#plt.show()
