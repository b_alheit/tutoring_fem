
import numpy as np
import matplotlib.pyplot as plt

def shape_functions(x_start, x_end, nen,):

    x = np.linspace(x_start, x_end, nen)
    #x = np.linspace(x_start, x_end, 1000)

    if nen == 2:
        # Linear Shape Function
        #As defined in class
        n1 = (x - x_end) / (x_start - x_end)
        n2 = (x - x_start) / (x_end - x_start)

        #The gradient of linear shape function
        #basically just the derivative of the shape function
        b1 = 1.0 / (x_start - x_end)
        b2 = 1.0 / (x_end - x_start)

        #create an array for shape function and its derivative
        n = np.array([n1, n2])
        b = np.array([b1, b2])

        #n = [n1, n2]
        #b = [b1, b2]

    if nen == 3:
        # quadratic shape function, number of nodes= 3, (2 elements)
        x_mid = (x_end - x_start) / 2  # Middle / 2nd Node
        # Linear Shape Function
        # As defined in class
        n1 = (x - x_mid) * (x - x_end) / ((x_start - x_mid) * (x_start - x_end))
        n2 = (x - x_start) * (x - x_end) / ((x_mid - x_start) * (x_mid - x_end))
        n3 = (x - x_start) * (x - x_mid) / ((x_end - x_start) * (x_end - x_mid))

        # The gradient of quadratic shape function
        # (the derivative of the shape function)
        b1 = (2 * x - x_mid - x_end) / ((x_start - x_mid) * (x_start - x_end))
        b2 = (2 * x - x_start - x_end) / ((x_mid - x_start) * (x_mid - x_end))
        b3 = (2 * x - x_start - x_mid) / ((x_end - x_start) * (x_end - x_mid))

        n = np.array([n1, n2, n3])
        b = np.array([b1, b2, b3])

    if nen == 4:
        # Cubic Shape Function
        x2 = (x_end - x_start)/3  # 2nd Node
        x3 = (x_start + x_end) - x2  # 3 rd Node

        n1 = ((x - x2) * (x - x3) * (x - x_end)) / ((x_start - x2) * (x_start - x3) * (x_start - x_end))
        n2 = (x - x_start) * (x - x3) * (x - x_end) / ((x2 - x_start) * (x2 - x3) * (x2 - x_end))
        n3 = (x - x_start) * (x - x2) * (x - x_end) / ((x3 - x_start) * (x3 - x2) * (x3 - x_end))
        n4 = ((x - x3) * (x - x2) * (x - x3)) / ((x_end - x_start) * (x_end - x2) * (x_end - x3))

    # The gradient of cubic shape function
        b1 = ((x - x3) * (x - x_end) + (x - x2) * (x - x_end) + (x - x2) * (x - x3)) / ((x_start - x2) * (x_start - x3) * (x_start - x_end))
        b2 = ((x - x3) * (x - x_end) + (x - x_start) * (x - x_end) + (x - x_start) * (x - x3)) / ((x2 - x_start) * (x2 - x3) * (x2 - x_end))
        b3 = ((x - x2) * (x - x_end) + (x - x_start) * (x - x_end) + (x - x_start) * (x - x2)) / ((x3 - x_start) * (x3 - x2) * (x3 - x_end))
        b4 = ((x - x2) * (x - x3) + (x - x_start) * (x - x3) + (x - x_start) * (x - x2)) / ((x_end - x_start) * (x_end - x2) * (x_end - x3))

        n = np.array([n1, n2, n3, n4])
        b = np.array([b1, b2, b3, b4])

    if nen == 5:  # Quartic shape function
        x3 = (x_end - x_start) / 2  # 3rd node
        x2 = (x_end - x_start) / 4  # 2nd node
        x4 = (x_end - x_start) * 3 / 4  # 4th node

        n1 = (x - x2) * (x - x3) * (x - x4) * (x - x_end) / ((x_start - x2) * (x_start - x3) * (x_start - x4) * (x_start - x_end))
        n2 = (x - x_start) * (x - x3) * (x - x4) * (x - x_end) / ((x2 - x_start) * (x2 - x3) * (x2 - x4) * (x2 - x_end))
        n3 = (x - x_start) * (x - x2) * (x - x4) * (x - x_end) / ((x3 - x_start) * (x3 - x2) * (x3 - x4) * (x3 - x_end))
        n4 = (x - x_start) * (x - x2) * (x - x3) * (x - x_end) / ((x4 - x_start) * (x4 - x2) * (x4 - x3) * (x4 - x_end))
        n5 = (x - x_start) * (x - x2) * (x - x3) * (x - x4) / ( (x_end - x_start) * (x_end - x2) * (x_end - x3) * (x_end - x4))

    ##The  gradient  of the Quartic shape function
        b1 = ((x - x3) * (x - x4) * (x - x_end) + (x - x2) * (x - x4) * (x - x_end) + (x - x2) * (x - x3) * (x - x_end) + (x - x2) * (x - x3) * (x - x4)) / ((x_start - x2) * (x_start - x3) * (x_start - x4) * (x_start - x_end))
        b2 = ((x - x3) * (x - x4) * (x - x_end) + (x - x_start) * (x - x4) * (x - x_end) + (x - x_start) * (x - x3) * ( x - x_end) + (x - x_start) * (x - x3) * (x - x4)) / ((x2 - x_start) * (x2 - x3) * (x2 - x4) * (x2 - x_end))
        b3 = ((x - x2) * (x - x3) * (x - x4) + (x - x_start) * (x - x4) * (x - x_end) + (x - x_start) * (x - x2) * ( x - x_end) + (x - x_start) * (x - x2) * (x - x4)) / ((x3 - x_start) * (x3 - x2) * (x3 - x4) * (x3 - x_end))
        b4 = ((x - x2) * (x - x3) * (x - x_end) + (x - x_start) * (x - x3) * (x - x_end) + (x - x_start) * (x - x2) * (x - x_end) + (x - x_start) * (x - x2) * (x - x3)) / ((x4 - x_start) * (x4 - x2) * (x4 - x3) * (x4 - x_end))
        b5 = ((x - x2) * (x - x3) * (x - x4) + (x - x_start) * (x - x3) * (x - x4) + (x - x_start) * (x - x2) * (x - x4) + (x - x_start) * (x - x2) * (x - x3)) / ((x_end - x_start) * (x_end - x2) * (x_end - x3) * (x_end - x4))

        n = [n1, n2, n3, n4, n5]
        b = [b1, b2, b3, b4, b5]

    print(np.sum(n))
    print(n1)
    print('SHAPE FUNCTION')
    print(n)
    print('Gradient shape function')
    print(b)
    plt.subplot(211)
    plt.plot(x, n, linewidth=2.0)
    plt.subplot(212)
    plt.plot(x, b, linewidth=2.0)
    plt.show()

shape_functions(0, 1,4)
