import numpy as np
import matplotlib.pyplot as plt

def shape_functions(x_start, x_end, nen):

    x = np.linspace(x_start, x_end, nen*8)  # by multiplying nen,we smooth out the function(chose 8 via trial and error)
    k = 0.2  # k value obtained by applying Fourier’s Law and assuming a uniform thermal conductivity of the material,
    t = np.array([273, 281, 288, 288, 286])  # we set temperature up as an array to be used later

##for nen = 2 i.e 2 nodes(one element)
    if nen == 2:
        # Linear Shape Function
        #As defined in class
        n1 = (x - x_end ) / (x_start - x_end)
        n2 = (x - x_start) / (x_end - x_start)

        #The gradient of linear shape function
        #basically just the derivative of the shape function
        b1 = 1.0/ (x_start - x_end)
        b2 = 1.0/ (x_end - x_start)

        #create an array for shape function and its derivative
        n = np.array([n1, n2])
        b = np.array([b1, b2])

        temp_approx = n1*t[0] + n2*t[4]       # temperature approximation equation
        flux = -k * (b1 * t[0] + b2 * t[4])     # flux approximation equation

        n = [n1, n2]
        b = [b1, b2]

    if nen == 3:
        # quadratic shape function, number of nodes= 3, (2 elements)
        x_mid = (x_end - x_start) / 2  # Middle / 2nd Node
        # Linear Shape Function
        # As defined in class
        n1 = (x - x_mid) * (x - x_end) / ((x_start - x_mid) * (x_start - x_end))
        n2 = (x - x_start) * (x - x_end) / ((x_mid - x_start) * (x_mid - x_end))
        n3 = (x - x_start) * (x - x_mid) / ((x_end - x_start) * (x_end - x_mid))

        # The gradient of quadratic shape function
        # (the derivative of the shape function)
        b1 = (2 * x - x_mid - x_end) / ((x_start - x_mid) * (x_start - x_end))
        b2 = (2 * x - x_start - x_end) / ((x_mid - x_start) * (x_mid - x_end))
        b3 = (2 * x - x_start - x_mid) / ((x_end - x_start) * (x_end - x_mid))

        temp_approx = n1 * t[0] + n2 * t[2] + n3 * t[4]     # temperature approximation equation
        flux = -k * (b1 * t[0] + b2 * t[2] + b3 * t[4])     # flux approximation equation

        n = np.array([n1, n2, n3])
        b = np.array([b1, b2, b3])

    if nen == 5:
        # Quartic shape function
        #number of nodes = 5, implies 4 elements
        #deine the nodes not on the bounds
        x3 = (x_end - x_start) / 2  # 3rd node
        x2 = (x_end - x_start) / 4  # 2nd node
        x4 = (x_end - x_start) * 3 / 4  # 4th node
        # Linear Shape Function
        # expanded on from general  case
        n1 = (x - x2) * (x - x3) * (x - x4) * (x - x_end) / ((x_start - x2) * (x_start - x3) * (x_start - x4) * (x_start - x_end))
        n2 = (x - x_start) * (x - x3) * (x - x4) * (x - x_end) / ((x2 - x_start) * (x2 - x3) * (x2 - x4) * (x2 - x_end))
        n3 = (x - x_start) * (x - x2) * (x - x4) * (x - x_end) / ((x3 - x_start) * (x3 - x2) * (x3 - x4) * (x3 - x_end))
        n4 = (x - x_start) * (x - x2) * (x - x3) * (x - x_end) / ((x4 - x_start) * (x4 - x2) * (x4- x3) * (x4 - x_end))
        n5 = (x - x_start) * (x - x2) * (x - x3) * (x - x4) / ((x_end - x_start) * (x_end - x2) * (x_end - x3) * (x_end - x4))

        # The  gradient  of the Quartic shape function
        # (the derivative of the shape function)
        b1 = ((x - x3)* (x - x4) * (x - x_end) + (x - x2) * (x - x4) * (x - x_end) + (x - x2) * (x - x3) * (x - x_end) + (x - x2) * (x - x3) * (x - x4)) / ((x_start - x2) * (x_start - x3) * (x_start - x4) * (x_start - x_end))
        b2 = ((x - x3) * (x - x4) * (x - x_end) + (x - x_start) * (x - x4) * (x - x_end) + (x - x_start) * (x - x3) * (x - x_end) + ( x - x_start) * (x - x3) * (x - x4)) / ((x2 - x_start) * (x2 - x3) * (x2 - x4) * (x2 - x_end))
        b3 = ((x - x2) * (x - x3) * (x - x4) + (x - x_start) * (x - x4) * (x - x_end) + (x - x_start) * (x - x2) * (x - x_end) + (x - x_start) * (x - x2) * (x - x4)) / ((x3 - x_start) * (x3 - x2) * (x3 - x4) * (x3 - x_end))
        b4 = ((x - x2) * (x - x3) * (x - x_end) + (x - x_start) * (x - x3) * (x - x_end) + (x - x_start) * (x - x2) * (x - x_end) + (x - x_start) * (x - x2) * (x - x3)) / ((x4 - x_start) * (x4 - x2) * (x4 - x3) * (x4 - x_end))
        b5 = ((x - x2) * (x - x3) * (x - x4) + (x - x_start) * (x - x3) * (x - x4) + (x - x_start) * (x - x2) * (x - x4) + (x - x_start) * (x - x2) * (x - x3)) / ((x_end - x_start) * (x_end - x2) * (x_end - x3) * (x_end - x4))

        temp_approx = n1 * t[0] + n2 * t[1] + n3 * t[2] + n4 * t[3] + n5 * t[4]   # temperature approximation equation
        flux = -k * (b1 * t[0] + b2 * t[1] + b3 * t[2] + b4 * t[3] + b5 * t[4])     # flux approximation equation

        n = [n1, n2, n3, n4, n5]
        b = [b1, b2, b3, b4, b5]

    print('SHAPE FUNCTION')
    print(n)  # print shape function vector
    print('GRADIENT SHAPE FUNCTION')
    print(b)       # print gradient of shape function

    print(temp_approx)

    plt.plot(x, temp_approx, linewidth=2.0)
    plt.xlabel('x')
    plt.ylabel('Approximate Temp')
    #plt.subplot(211)
    #plt.plot(x, temp_approx, linewidth=2.0)
    #plt.xlabel('x')
    #plt.ylabel('Approximate Temp')
    #plt.subplot(212)
    #plt.plot(x, flux, linewidth=2.0)
    #plt.xlabel('x')
    #plt.ylabel('Approximate Flux')
    plt.show()





shape_functions(0,10,2)
