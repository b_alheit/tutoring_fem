import numpy as np
import matplotlib.pyplot as plt
from functions import *

x = np.linspace(0, 10, 1000)
nen = 5
N, B = shape_functions(x, nen)

x_mes = np.linspace(0, 10, 5)
T_mes = np.array([273, 281, 288, 288, 286])


















if nen==2:
    use = [0, 4]
if nen == 3:
    use = [0, 2, 4]
if nen == 5:
    use = [0, 1, 2, 3, 4]

x_use = np.array(x_mes[use])
T_use = np.array([T_mes[use]])
print(T_use.T)
T = N * T_use.T
T = T.sum(axis=0)
plt.plot(x, T)
plt.grid()
plt.xlabel('bar position (m)')
plt.ylabel('Temperature (K)')
plt.show()