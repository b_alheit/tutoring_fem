import numpy as np
import matplotlib.pyplot as plt
from functions import *

x = np.linspace(-1, 1, 1000)
nen = 10
N, B = shape_functions(x, nen)

plt.figure(1)
plt.title('Shape functions')
plt.plot(np.outer(np.ones(nen), x).T, N.T)
plt.grid()

plt.figure(2)
plt.title('Shape function derivatives')
plt.plot(np.outer(np.ones(nen), x).T, B.T)
plt.grid()

plt.show()
