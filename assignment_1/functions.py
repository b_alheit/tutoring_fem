import numpy as np
import matplotlib.pyplot as plt


def N_s(x, x_nodes, i_node):
    nen = np.alen(x_nodes)
    N = np.ones(np.alen(x))
    for i in range(nen):
        if i != i_node:
            N *= (x - x_nodes[i]) / (x_nodes[i_node] - x_nodes[i])

    return N


def B_s(x, x_nodes, i_node):
    nen = np.alen(x_nodes)
    B = np.zeros(np.alen(x))
    for k in range(nen):
        if k != i_node:
            temp = (x_nodes[i_node] - x_nodes[k]) **(-1)
            for j in range(nen):
                if j!=i_node and j!=k:
                    temp = temp * (x - x_nodes[j])/(x_nodes[i_node] - x_nodes[j])
            B += temp

    return B


def shape_functions(x, nen):
    N = np.zeros([nen, np.alen(x)])
    B = np.zeros([nen, np.alen(x)])

    x_nodes = np.linspace(x[0], x[-1], nen)

    for i in range(nen):
        N[i, :] = N_s(x, x_nodes, i)
        B[i, :] = B_s(x, x_nodes, i)

    return N, B


