from numpy import *
import matplotlib.pyplot as plt

def Niso(xi, eta):                                      #isoparametric shape functions
    N = zeros([1,3])
    N[0,0] = xi
    N[0,1] = eta
    N[0,2] = 1-xi-eta
    return N

def VecN(xi, eta):                                      #returns N in correct form
    N = Niso(xi, eta)
    VecN = zeros([2, 2*len(N[0])])
    for c in range(len(N[0])):
        VecN[0, 2*c] = N[0,c]
        VecN[1, 2*c+1] = N[0,c]
    return VecN

def gaussquadtriangle(ngp, coordelement, func, elArea, bnodal):   #gauss quadrature in triangles
    if ngp ==2:
        return 'Triangle Gauss Quadrature cannot use 2 gauss points'

    xi1 = [1/3]
    xi3 = [1/6, 2/3, 1/6]
    xi4 = [1/3, 2/10, 6/10, 2/10]
    XI = [xi1, 0, xi3, xi4]                            #create matrix of known xi values

    eta1 = [1/3]
    eta3 = [1/6, 1/6, 2/3]
    eta4 = [1/3, 2/10, 2/10, 6/10]
    Eta = [eta1, 0, eta3, eta4]                         #create matrix of known eta values

    w1 = [1]
    w3 = [1/3, 1/3, 1/3]
    w4 = [-9/16, 25/48, 25/48, 25/48]
    W = [w1, 0, w3, w4]                               #create matrix of corresponding weights

    I = 0                                               #initialise sum
                                                        #triangle quadrature
    for i in range(ngp):
        xi = (XI[int(ngp-1)][i])
        eta = (Eta[int(ngp-1)][i])
        w = (W[int(ngp-1)][i])
        xiso = dot(Niso(xi, eta), coordelement[:,0])
        yiso = dot(Niso(xi, eta), coordelement[:,1])
        intfunc = func(xiso,yiso, bnodal)           #multiply by shape functions for body integral
        I = I + w*intfunc

    I = elArea*I                                    #multiply by area for total integral
    return I

def Jacobiantriangle(coordelement):                        #calculate Jacobian on element at specific xi and eta value
    GNmatrix = zeros([2,3])
    GNmatrix[0,0] = 1
    GNmatrix[0,2] = -1
    GNmatrix[1,1] = 1
    GNmatrix[1, 2] = -1
    J = matmul(GNmatrix, coordelement)                             #calculate the Jacobian
    return J, GNmatrix


def CBtoB(CB):
    B = zeros([3,2*len(CB[0])])
    count = 0
    for j in range(2*len(CB[0])):
        if remainder(j,2)== 0:
            B[0,j] = CB[0,count]
            B[2,j] = CB[1,count]
        elif remainder(j,2)== 1:
            B[1, j] = CB[1, count]
            B[2, j] = CB[0, count]
            count = count+1
    return B

def Areafunc(coordelement):    #calculates body force on each element
    xthis = coordelement[:,0]
    ythis = coordelement[:,1]
    len = zeros(3)
    for k in range(3):                                          #Heron's formula for calculating area
        if k == 0 or k == 1:
            len[k] = sqrt((xthis[k] - xthis[k + 1]) ** 2 + (ythis[k] - ythis[k + 1]) ** 2)
        if k == 2:
            len[k] = sqrt((xthis[2] - xthis[0]) ** 2 + (ythis[2] - ythis[0]) ** 2)
    p = (len[0] + len[1] + len[2]) / 2
    area = sqrt(p * (p - len[0]) * (p - len[1]) * (p - len[2]))
    return area

def constitD(nu, E):
    D = zeros([3,3])
    D[0,0] = 1
    D[1,1] = 1
    D[0,1] = nu
    D[1,0] = nu
    D[2,2] = (1-nu)/2
    D = (E/(1-nu**2))*D
    return D

numelements = 2
numnodesperelement = 3                              #triangular element
numnodes = 4
nu = 0.3
YME = 10**(7)
Dmatrix = constitD(nu, YME)
xnodes = array([0, 0,0,0, 1,1, 1,1])                #each coord repeated to account for double values in ICA
ynodes = array([0.5,0.5,0, 0,0, 0,0.5, 0.5])
Coordinates = array([xnodes, ynodes]).T
ICA = [[2,3,4,5,0,1], [4,5,6,7,0,1]]                #x and y  index position of each node



Globalbodyforce = zeros([2*numnodes,1])
Bodyforcenodes = zeros([2*numnodes,1])
for m in range(4):
    Bodyforcenodes[2*m+1,0] = -2*10**5              #nodal values of body force

def Bodyfunc(xi,eta, bnodal):
    Nvec = VecN(xi, eta)
    return matmul(matmul(Nvec.T, Nvec), bnodal)     #calculates body force



CBelement = zeros([2,3])                    #matrix for creating B, constant on each element
CBelement[0,1] = 1
CBelement[1,2] = 1
Belement = CBtoB(CBelement)                 #B matrix
Belement = array([[1, 0, 1, 0, 1, 0],
                     [0, 1, 0, 1, 0, 1],
                     [1, 1, 1, 1, 1, 1]])
BigB = matmul(matmul(Belement.T, Dmatrix), Belement) #B.T x D x B

GlobalK = zeros([2*numnodes, 2*numnodes])

for n in range(numelements):
    ICAelement = (ICA[n][:])
    coordelement = zeros([int(len(ICAelement)/2), 2])
    bnodal = zeros([(len(ICAelement)), 1])
    index = 0
    for q in range(numnodesperelement):
        coordelement[q, 0] = xnodes[ICAelement[2*q]]       #get right coords on element
        coordelement[q, 1] = ynodes[ICAelement[2*q]]
        bnodal[2*q,0] = Bodyforcenodes[ICAelement[2*q],0]   #get right b nodal values on element
        bnodal[2*q+1,0] = Bodyforcenodes[ICAelement[2*q+1],0]

    elArea = Areafunc(coordelement)                         #area of element

    Fbodyelement = gaussquadtriangle(3, coordelement, Bodyfunc, elArea, bnodal)     #calculate body force integral


    Jacobianelement, GN = Jacobiantriangle(coordelement)            #get Jacobian for element
    Jacobiandet = linalg.det(Jacobianelement)                       #determinant of Jacobian
    Kelement = BigB*Jacobiandet*0.5                                 #do integral for K

    for p in range(6):
        Globalbodyforce[ICAelement[p],0] = Globalbodyforce[ICAelement[p],0] + Fbodyelement[p]     #input element body force into global body force
        for m in range(6):
            GlobalK[ICAelement[p], ICAelement[m]] = GlobalK[ICAelement[p], ICAelement[m]]+ Kelement[p,m]    #input element K into global K ***************


PartitionedK = GlobalK[4:,4:]                       #partition
PartitionedBodyForce = Globalbodyforce[4:,:]
displacements = zeros([2*numnodes,1])

SolvedDisp = matmul(PartitionedBodyForce.T, linalg.inv(PartitionedK))   #invert and solve
displacements[4:,0] = SolvedDisp                                        #including boundary conditions


newnodalpositions = zeros([2*numnodes,2])
stress=zeros([2*numnodes, 1])
for n in range(numelements):
    ICAelement = (ICA[n][:])
    coordelement = zeros([int(len(ICAelement) / 2), 2])
    newcoords = zeros([int(len(ICAelement) / 2), 2])

    dispelement = zeros([6,1])
    for q in range(numnodesperelement):
        coordelement[q, 0] = xnodes[ICAelement[2 * q]]
        coordelement[q, 1] = ynodes[ICAelement[2 * q]]
        newcoords[q, 0] = coordelement[q, 0] + displacements[ICAelement[2*q]]       #calculate new coordinates adding displacement
        newcoords[q, 1] = coordelement[q, 1] + displacements[ICAelement[2*q+1]]
        newnodalpositions[ICAelement[2*q],0] = newcoords[q,0]                       #input new positions
        newnodalpositions[ICAelement[2*q],1] = newcoords[q,1]

    for k in range(6):
        dispelement[k,0]= displacements[ICAelement[k]]                              #get correct displacement values for element


    #plots
    plt.plot(coordelement[:, 0], coordelement[:, 1], 'k')
    plt.plot(array([coordelement[0, 0], coordelement[2, 0]]), array([coordelement[0, 1], coordelement[2, 1]]), 'k')
    plt.plot(newcoords[:, 0], newcoords[:, 1], 'r')
    plt.plot(array([newcoords[0, 0], newcoords[2, 0]]), array([newcoords[0, 1], newcoords[2, 1]]), 'r')


    strainelement = matmul(Belement, dispelement)                                   #compute strain and stress
    stresselement = matmul(Dmatrix, strainelement)
    for m in range(3):
        stress[ICAelement[2*m]] = stress[ICAelement[2*m]]+stresselement[m]          #input stress into global stress vector

newnodes = zeros([numnodes,2])
Stress = zeros([numnodes,1])
for k in range(numnodes):                                                           #cut down superfluous vector entries
    newnodes[k,:] = newnodalpositions[2*k,:]
    Stress[k,0] = stress[2*k]


print('Displacement at nodes:\n', displacements)
print('Stress:\n', Stress)
plt.show()














