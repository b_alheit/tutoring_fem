import numpy as np
from numpy import zeros, ones
# from numpy.core._multiarray_umath import ndarray
from scipy.linalg import det, inv
import matplotlib.pyplot as plt
# import matplotlib.tri as mtri
import matplotlib.tri as tri


# Gauss POINTS for Quadrature
xi = np.array([1/6, 2/3, 1/6])
eta = np.array([1/6, 1/6, 2/3])
Weight = np.array([1/3, 1/3, 1/3])


# The Global X and Y values
x_e = np.array([0, 0, 1, 1])
y_e = np.array([0.5, 0, 0, 0.5])

# The inter-connectivity array
ICA = np.array([[0, 1, 2], [0, 2, 3]])
# The inter-connectivity array for vectors
vec_ICA = np.array([[0, 1, 2, 3, 4, 5],[0, 1, 4, 5, 6, 7]])

nel = 2  # Number of elements
nn = nel + 2  # number of nodes, 4

# initializing matrices
# Globals
M = ones((3, 3))
K = zeros((8, 8))
F = zeros((8, 1))
d = zeros((8, 1))

# conductivity matrix
nu = 0.3
E = (1 * 10**7)
Enu = E/(1 - nu**2)
D = Enu * np.array([[1, nu, 0],
                    [nu, 1, 0],
                    [0, 0, (1-nu)/2]])

# constant body load, interpolating b
p = np.array([[0],
              [-200000],
              [0],
              [-200000],
              [0],
              [-200000]])


##########################################################
# solving for K and F
for a in range(2):  # for each element
    asm = ICA[a, :]  # each row represents an element
    dof_asm = vec_ICA[a, :]
    # x amd y for each element
    X_e = x_e[asm]
    Y_e = y_e[asm]

    # generate M matrix for each element.
    M[:, 1] = X_e
    M[:, 2] = Y_e

    A = det(M) / 2.0  # Area

    # gradient shape function taken from textbook
    B = (1/det(M)) * np.array([[Y_e[1] - Y_e[2], 0,  Y_e[2] - Y_e[0], 0, Y_e[0] - Y_e[1], 0],
                  [0,X_e[2] - X_e[1], 0, X_e[0] - X_e[2], 0,  X_e[1] - X_e[0]],
                  [X_e[2] - X_e[1], Y_e[1] - Y_e[2],  X_e[0] - X_e[2], Y_e[2] - Y_e[0], X_e[1] - X_e[0], Y_e[0] - Y_e[1] ]])


    # Initial values for GQ
    force_elem = 0
    k_elem = 0

    for i in range(3):
        # shape function)
        n1 = xi[i]
        n2 = eta[i]
        n3 = 1 - eta[i] - xi[i]
        # for boundary force
        Nt = np.array([[n1, 0],
                       [0, n1],
                       [n2, 0],
                       [0, n2],
                       [n3, 0],
                       [0, n3]])

        N = np.transpose(Nt)

        #interpolating b
        nb = np.dot(N, p)
        nnb = np.dot(Nt, nb)

        # matrix multiplication
        BDB = np.dot(np.transpose(B), np.dot(D, B))

        k_elem = k_elem + A * Weight[i] * BDB
        force_elem = force_elem +A* Weight[i] * nnb


    # compile the global stiffness matrix and the force over the domain
    for z in range(6):
        F[dof_asm[z]] += force_elem[z]
        for o in range(6):
            K[dof_asm[z], dof_asm[o]] += k_elem[z,o]



#first 4 values of d are zero
d[0:4, :] = np.array([[0], [0], [0], [0]])

# partitioning that matrices
K_F = K[4:8, 4:8]
F_F = F[4:8, :]

# nodal displacement
d[4:8, :] = np.dot((np.linalg.inv(K_F)), F_F)


###########################################################
#post processing
for q in range(2):
    asm = ICA[q, :]  # each row represents an element
    dof_asm = vec_ICA[q, :]
    # x amd y for each element
    X_e = x_e[asm]
    Y_e = y_e[asm]

    # generate M matrix for each element.
    M[:, 1] = X_e
    M[:, 2] = Y_e

    A = det(M) / 2.0  # Area

    # gradient shape function taken from textbook
    B = (1 / det(M)) * np.array([[Y_e[1] - Y_e[2], 0, Y_e[2] - Y_e[0], 0, Y_e[0] - Y_e[1], 0],
                                 [0, X_e[2] - X_e[1], 0, X_e[0] - X_e[2], 0, X_e[1] - X_e[0]],
                                 [X_e[2] - X_e[1], Y_e[1] - Y_e[2], X_e[0] - X_e[2], Y_e[2] - Y_e[0], X_e[1] - X_e[0],
                                  Y_e[0] - Y_e[1]]])
    #dof_asm = vec_ICA[q, :]
    d_elem = d[dof_asm]

    # Strain
    strain = np.dot(B, d_elem)

    # Stress
    stress = np.dot(D, strain)

    #print(d_e)
    #print(B)
    #print('strain', strain)
    #print('stresss', stress)

#print(K)
#print(F)
#print(d)

#############################################
#plots
x = np.asarray([0, 0, 1, 1])
y = np.asarray([0.5, 0, 0, 0.5])

x1 = np.asarray([0, 0, 1+d[4], 1+d[6]])
y1 = np.asarray([0.5, 0, 0+d[5], 0.5+d[7]])
#print(x1)
#print(y1)
triangles = [[0, 1, 2], [0, 2, 3]]
triangulation = tri.Triangulation(x, y, triangles)
triangulation1 = tri.Triangulation(x1, y1, triangles)

plt.triplot(triangulation, 'go-')
plt.triplot(triangulation1, 'bo-')
plt.title("Original and Deformed Mesh")
plt.show()
