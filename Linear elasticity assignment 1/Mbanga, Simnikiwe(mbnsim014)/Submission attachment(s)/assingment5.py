
import numpy as np
from numpy import zeros, ones
from numpy import transpose
from scipy.linalg import det, inv, norm
import matplotlib.pyplot as plt
import matplotlib.tri as tri

# inter-connectivity array
ICA = np.array([[0, 1, 2], [2, 3, 0]])
#nodal values
X_nod = np.array([0, 0, 1, 1])  # X nodal values
Y_nod = np.array([0.5, 0, 0, 0.5])  # Y nodal values
nodal_pos = np.array([X_nod, Y_nod])  # XY nodal coordinates

## plain strain
## initialize the values
E = 10**7
v = 0.3
b1 = 10**5
b = np.array([[0],
              [-2*b1]])  # force acting in the j direction (downwards)
D = E/(1-v**2) * np.array([[1, v, 0],
                           [v, 1, 0],
                           [0, 0, (1-v)/2]])  ## plain strain matrix
# for 4 gauss points we have
epsi = np.array([1 / 3, 2 / 10, 6 / 10, 2 / 10])  #epsi points
eta = np.array([1 / 3, 2 / 10, 2 / 10, 6 / 10])  # eta points
Weight = np.array([-9 / 16, 25 / 48, 25 / 48, 25 / 48])  #weights

# define isoparemetric space
def isop_space(epsi, eta):
    n = np.array([epsi, eta, 1-epsi-eta]) # the shape function for a triangular element in the isoparametric space
    N = np.array([[epsi, 0, eta, 0, 1-epsi-eta, 0],
                  [0, epsi, 0, eta, 0, 1-epsi-eta]])  # the shape function for a 2D linear elaticity problem
    return n, N

def quad_body(X_nodal, Y_nodal):
    GN = np.array([[1, 0, -1],
                   [0, 1, -1]])  #derivative of the triangular shape function
    M = ones((3, 3))  # finding the M matrix of an element
    M[1, :] = X_nodal
    M[2, :] = Y_nodal
    J = np.dot(GN, np.transpose(np.array([X_nodal, Y_nodal]))) # Jacobian matrix of an element
    B = np.dot(inv(J), GN)
    B_element = np.array([[B[0, 0], 0, B[0, 1], 0, B[0, 2], 0],
                          [0, B[1, 0], 0, B[1, 1], 0, B[1, 2]],
                          [B[1, 0], B[0, 0], B[1, 1], B[0, 1], B[1, 2], B[0, 2]]])  # the B element of 2D linear elasticity problem
    k_elem = det(M)*np.dot(B_element.T, np.dot(D, B_element))/2  # elemental stiffness

    return k_elem, M, B_element


# Initialize stiffness matrix and body force ,both global
Stiffness = zeros((8, 8))  # with two degrees of freedom for each node we then have 2*no. of nodes
body_Force = zeros((8, 1))  # the body force of the two elements
a = np.array([0, 1, 2, 3, 4, 5])  # degrees of freedom for element 1
b1 = np.array([4, 5, 6, 7, 0, 1])  # degrees of freedom for element 2
vec_ICA = np.column_stack((a, b1))  # the inter_connectivity array for the domain
K_elem = zeros((6, 6))  # elemental stiffness since 3 points then 3*2 =6

# finding the stiffness matrix of the domain
for m in range(len(ICA)):
    asm = ICA[m, :]
    X_e = X_nod[asm]
    Y_e = Y_nod[asm]
    D_asm = vec_ICA[:, m]
    K_elem = zeros((6, 6))  # initialize the element stiffness
    for k in range(len(Weight)):
        K_elem += Weight[k]*quad_body(X_e, Y_e)[0]  # calculate stiffness
    for j in range(len(vec_ICA)):
        for l in range(len(vec_ICA)):
            Stiffness[D_asm[j], D_asm[l]] += K_elem[j, l]  #j, l range from 0 to 6 with 6 being the length of the vec_ICA

# finding the body_force for the problem
for m in range(len(ICA)):
    asm = ICA[m, :]
    X_e = X_nod[asm]
    Y_e = Y_nod[asm]
    force = zeros((6, 2)) #initialize the force of an element
    for j in range(len(epsi)):
        Area = abs(det(quad_body(X_e, Y_e)[1])) / 2 # define area of each trianglur element
        force += Weight[j]*np.dot(np.transpose(isop_space(epsi[j], eta[j])[1]), b) * Area
    for n in range(len(vec_ICA)):
        D_asm = vec_ICA[n, :]  # for each point find the nodal x and y force
        body_Force[D_asm[m]] += force[n, 0]





### find the displacement
d = zeros((8, 1)) #initialize displacement
d[0:4, :] = np.array([[0],
                      [0],
                      [0],
                      [0]])  # since its clamped at node 1 and 2
K_F = Stiffness[4:8, 4:8]
K_EF = Stiffness[0:4, 0:4]
F_F = body_Force[4:8, :]
D_E = d[0:4, :]
d[4:8, :] = np.dot((inv(K_F)), (F_F - np.dot(np.transpose(K_EF), D_E))) # find for node 3 and 4 which are free to experience force

#print('Global stiffness', Stiffness)
#print('Global force', body_Force)
#print('Global displacement', d)

#stress and strain
epsilon = zeros((3, 1))  # initialize strain
stress = zeros((3, 1))   # initialize stress
a = np.array([0, 1, 2, 3, 4, 5])  # degrees of freedom for element 1
b1 = np.array([0, 1, 4, 5, 6, 7])  # degrees of freedom for element 2
vec_ICA = np.array([[0, 1, 2, 3, 4, 5], [0, 1, 4, 5, 6, 7]])  # the inter_connectivity array for the domain
#for each element find the stress and strain
for m in range(len(ICA)):
    ICA = np.array([[0, 1, 2], [0, 2, 3]])
    asm = ICA[m, :]
    X_e = X_nod[asm]
    Y_e = Y_nod[asm]
    #print(asm)
    D_asm = vec_ICA[m, :]
    #print(D_asm)
    d_element = d[D_asm]  # for each element use the nodal displacements for strain and stress
    #print(d_element)
    epsilon = np.dot(quad_body(X_e, Y_e)[2], d_element) # strain
    #print(quad_body(X_e, Y_e)[2])
    stress = np.dot(D, epsilon)  # stress
    #print('strain and stress for element', m)
    #print('strain_elem', epsilon)
    #print('stress_elem', stress)

#Finding the new nodal positions

d_1 = d[0:2, :]  # change of node 1
d_2 = d[2:4, :]  # change of node 2
d_3 = d[4:6, :]  # change of node 3
d_4 = d[6:8, :]  # change of node 4

#print('node_position1', d_1.T)
#print('node_position2', d_2.T)
#print('node_position3', d_3.T)
#print('node_position4', d_4.T)
change_pos = np.array([d_1.T[0, :], d_2.T[0, :], d_3.T[0, :], d_4.T[0, :]]) #changed positions
deformed_position = change_pos + nodal_pos.T  # the positions after deformations for both x and Y
X_deformed = deformed_position[:, 0]  # x position after deformation
#print('X_deformed', X_deformed)
Y_deformed = deformed_position[:, 1]  # y position after deformation
#print('Y_deformed', Y_deformed)
#print('def pos', def_pos + nodal_pos.T)

#### plot the deformed and undeformed elements
# the deformed element
triang = tri.Triangulation(X_deformed, Y_deformed, ICA)
#undeformed element
traing1 = tri.Triangulation(X_nod, Y_nod, ICA)
plt.figure()
plt.gca().set_aspect('equal')
#### labeling of the plot and legends
plt.triplot(triang, 'bo-', lw=1.0, label='deformed')
plt.triplot(traing1, 'ro-', lw=1.0, label='undeformed')
plt.title('deformed vs. undeformed')
plt.xlabel('X values')
plt.ylabel('Y values')
plt.legend(loc='upper left')
plt.grid()
plt.show()

