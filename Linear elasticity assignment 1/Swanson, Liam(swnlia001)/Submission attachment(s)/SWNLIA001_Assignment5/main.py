# Project 2 Liam Swanson
# MEC5063Z
# April 26 2019

import numpy as np
import math
import matplotlib.pyplot as plt
from plotter import meshPlotter
from mesher import mesh
from ghalerkinApproximation import stiffnessMatrix, bodyForce, boundaryForce
from partitionSolver import systemSolver
from postProcessor import postProcessTri, postProcessQuad
from interpolationFunctions import jacobianQ2, shapeFunctionCartP1, derShapeFunctionCartP1


# Define material properties
E = 1e7             # Pa
v = 0.3             # unitless
volume_body = 0.5 * 1 * 0.1 # m^3

# Elasticity matrices
# Plane stress
D_ps = (E / (1 - v ** 2)) * np.array([[1, v, 0],
                                      [v, 1, 0],
                                      [0, 0, (1 - v) / 2]])

# Plane Strain
D_pe = (E / ((1 + v) * (1 - 2 * v))) * np.array([[1 - v, v, 0],
                                                 [v, 1, 0],
                                                 [0, 0, (1 - 2 * v) / 2]])

# Define Dirichlet BC
u_dirichletx = 0
u_dirichlety = 0

# Define Neumann BC
# t_y = -20

# Solver properties
n_gp2D = 3          # order of gauss quadrature for body integral
n_gp1D = 3          # order of gauss quadrature for surface integral

# Define all node coordinates where Dirichlet BC nodes numbered first for Elements
# IMPLEMENTATION ASSIGNMENT
nodeCoord = np.array([[0, 0.5],
                      [0, 0],
                      [1, 0],
                      [1, 0.5]])
# nodeCoord = np.array([[0, 0],
#                       [0, 0.5],
#                       [0, 1],
#                       [1, 1],
#                       [1, 0.625],
#                       [1, 0.25],
#                       [2, 1],
#                       [2, 0.75],
#                       [2, 0.5]])
# Define inter connectivity array ensuring right handed numbering
# P1 Elements
ICA = np.array([[0, 1, 2],
                [0, 2, 3]])

# ICA = np.array([[0, 5, 1],
#                 [5, 4, 1],
#                 [5, 8, 4],
#                 [8, 7, 4],
#                 [4, 7, 3],
#                 [7, 6, 3],
#                 [4, 3, 2],
#                 [4, 2, 1]])

# Store global number of nodes
numNodes = len(nodeCoord)


# Define dirichlet boundary
def dirichletBoundary(nodeCoordinate):
    if abs(nodeCoordinate[0] - 0) == 0:
        return True

    else:
        return False


# Count number of dirichlet nodes and apply dirichlet BC to nodal displacement matrix
dirichlet_count = 0
d_initial = np.zeros((numNodes * 2, 1))

for i in range(numNodes):
    if dirichletBoundary(nodeCoord[i]) is True:
        d_initial[2 * i] = u_dirichletx
        d_initial[2 * i + 1] = u_dirichlety
        dirichlet_count += 1

dirichlet_count *= 2
# Meshing
# **************************************
# Generate mesh
elNode_x, elNode_y, elNodes, a_el, cent_el = mesh(nodeCoord, ICA, True)

# Ghalerkin Method
# **************************************
# Calculate stiffness matrix
K = stiffnessMatrix(elNodes, D_pe, a_el, elNode_x, elNode_y, ICA, n_gp1D, True)

# print(K)
# Calculate body source vector
Fb_source = bodyForce(elNodes, elNode_x, elNode_y, a_el, ICA, n_gp2D, volume_body, True)

# Calculate Neumann surface vector
Fb_surface = 0
# Fb_surface = boundaryForce(elNode_x, elNode_y, ICA, n_gp1D, True)

# Calculate total Force Vector
F_ghal = Fb_source - Fb_surface

# Partitioning
# **************************************
d_delta, F = systemSolver(K, d_initial, F_ghal, dirichlet_count)

# Post-Processing
# **************************************
# Plot contours of each component of stress
sample_points = 200
sigma_xx, sigma_yy, sigma_xy, X_grid, Y_grid = postProcessTri(d_delta, elNodes, elNode_x, elNode_y,
                                                              ICA, D_ps, sample_points)

# Reorganize into final nodal coordinates
nodeCoord_displacement = np.zeros(np.shape(nodeCoord))

for node in range(numNodes):
    nodeCoord_displacement[node, 0] = d_delta[2 * node]
    nodeCoord_displacement[node, 1] = d_delta[2 * node + 1]

nodeCoord_final = np.add(nodeCoord, nodeCoord_displacement)
print('Displaced Nodal Coordinates:\n', nodeCoord_final)

# Plotting
# **************************************
# Mesh plot
fig1, mesh = plt.subplots()
mesh = meshPlotter(nodeCoord, ICA, cent_el, mesh, '-', True, 'black', 1)
mesh.set_title('Mesh Node and Element Numbering')
mesh.set_xlabel('x (m)')
mesh.set_ylabel('y (m)')

# Displacement of Mesh plot
figa, mesh_displacement = plt.subplots()
mesh_displacement = meshPlotter(nodeCoord, ICA, cent_el, mesh_displacement, '-', False, 'red', 0.5)
meshPlotter(nodeCoord_final, ICA, cent_el, mesh_displacement, '--', False, 'black', 1.0)
mesh_displacement.set_title('Comparison of Displaced and Original State')
mesh_displacement.set_xlabel('x (m)')
mesh_displacement.set_ylabel('y (m)')

# Stress xx Distribution Plots for Tri Elements
fig0, s_xx = plt.subplots()
cplots_xx = s_xx.contourf(X_grid, Y_grid, sigma_xx, 50, cmap='jet')
cbars_xx = fig0.colorbar(cplots_xx)

s_xx.set_title('\u03C3xx Distribution Across Domain')
s_xx.set_xlabel('x (m)')
s_xx.set_ylabel('y (m)')
cbars_xx.set_label('Stress (Pa)')

# Stress yy Distribution Plots for Tri Elements
fig2, s_yy = plt.subplots()
cplots_yy = s_yy.contourf(X_grid, Y_grid, sigma_yy, 50, cmap='jet')
cbars_yy = fig2.colorbar(cplots_yy)

s_yy.set_title('\u03C3yy Distribution Across Domain')
s_yy.set_xlabel('x (m)')
s_yy.set_ylabel('y (m)')
cbars_yy.set_label('Stress (Pa)')

# Stress xy Distribution Plots for Tri Elements
fig3, s_xy = plt.subplots()
cplots_xy = s_xy.contourf(X_grid, Y_grid, sigma_xy, 50, cmap='jet')
cbars_xy = fig3.colorbar(cplots_xy)

s_xy.set_title('\u03C3xy Distribution Across Domain')
s_xy.set_xlabel('x (cm)')
s_xy.set_ylabel('y (cm)')
cbars_xy.set_label('Stress (Pa)')

# Uncomment to overlay mesh
# meshPlotter(nodeCoord, ICA, cent_el, temp, linop=0.25, centop=1, numop=1)

plt.show()
