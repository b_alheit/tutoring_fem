import numpy as np
import math


def gaussQuadratureInfoTri(orderGQ):
    """
    Function which stores the Gauss Quadrature numerical integration. It also calculates the jacobian of an element

    :param orderGQ: order of numerical integration scheme
    :return: 1 x orderGQ array of integration points in the isoparametric space, the weights at each point
    """

    # Create weight and location values in arrays
    location = np.array([[[1 / 3, 1 / 3], [0, 0], [0, 0], [0, 0]],
                         [[0, 0], [0, 0], [0, 0], [0, 0]],
                         [[1 / 6, 1 / 6], [2 / 3, 1 / 6], [1 / 6, 2 / 3], [0, 0]],
                         [[1 / 3, 1 / 3], [2 / 10, 2 / 10], [6 / 10, 2 / 10], [2 / 10, 6 / 10]]])

    weights = np.array([[1, 0, 0, 0],
                        [0, 0, 0, 0],
                        [1 / 3, 1 / 3, 1 / 3, 0],
                        [-9 / 16, 25 / 48, 25 / 48, 25 / 48]])

    return location[orderGQ], weights[orderGQ]


def gaussQuadratureInfoLin(orderGQ):
    """
    Function which stores the Gauss Quadrature numerical integration. It also calculates the jacobian of an element

    :param x_el: Array of element nodal positions
    :param orderGQ: order of numerical integration scheme
    :return: 1 x orderGQ array of integration points in the isoparametric space, the weights at each point and the
    value of the Jacobian
    """

    # Create weight and location values in arrays
    location = np.array([[0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                         [-1/math.sqrt(3), 1/math.sqrt(3), 0.0, 0.0, 0.0, 0.0],
                         [-0.7745966692, 0.0, 0.7745966692, 0.0, 0.0, 0.0],
                         [-0.8611363116, -0.3399810436, 0.3399810436, 0.8611363116, 0.0, 0.0],
                         [-0.9061798459, -0.5384693101, 0.0, 0.5384693101, 0.9061798459, 0.0],
                         [-0.9324695142, -0.6612093865, -0.2386191861, 0.2386191861, 0.6612093865, 0.9324695142]])

    weights = np.array([[2.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                        [1.0, 1.0, 0.0, 0.0, 0.0, 0.0],
                        [0.5555555556, 0.8888888889, 0.5555555556, 0.0, 0.0, 0.0],
                        [0.3478548451, 0.6521451549, 0.6521451549, 0.3478548451, 0.0, 0.0],
                        [0.2369268851, 0.4786286705, 0.5688888889, 0.4786286705, 0.2369268851, 0.0],
                        [0.1713244924, 0.3607615730, 0.4679139346, 0.4679139346, 0.3607615730, 0.1713244924]])

    return location[orderGQ], weights[orderGQ]
