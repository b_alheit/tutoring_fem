# Project 2 Liam Swanson
# MEC5063Z
# April 26 2019

import numpy as np
import math
from interpolationFunctions import derShapeFunctionCartP1, shapeFunctionIsoP1, \
    shapeFunctionIsoLin, shapeFunctionIsoQ1, derShapeFunctionIsoQ1, jacobianQ1, vecInterpolantReorginzer, gradMatrixQ1
from gaussQuadrature import gaussQuadratureInfoTri, gaussQuadratureInfoLin


def thickness(x, y):
    return 0.1


def bodyLoad(x, y, volume):
    return np.array([[0, -2e5]]) * volume        # N/m^3


def neumannSource(x, y):
    if abs(x - 0) <= 1e-10:
        return np.array([[0, 0]])

    else:
        return np.array([[0, 0]])


def stiffnessMatrix(nodeCoord_el, D, area_el, nodes_x, nodes_y, ICA_global, order_gq, tri):
    # Calculate number of nodes and elements
    num_nodes = np.amax(ICA_global) + 1
    num_el = len(ICA_global)

    # Initialise the global stiffness matrix
    K_global = np.zeros((2 * num_nodes, 2 * num_nodes))

    # Loop through each element, calculate local stiffness matrix and place into global matrix
    for i in range(num_el):
        if tri is True:
            thick = thickness(0, 0)
            B = vecInterpolantReorginzer(derShapeFunctionCartP1(nodeCoord_el[i]), True)
            K_el = np.matmul(B.T, np.matmul(D, B)) * area_el[i] * thick

        if tri is False:
            # Get gauss quad values
            iso_loc, iso_w = gaussQuadratureInfoLin(order_gq - 1)
            K_el = 0
            for j in range(order_gq):
                sum_chi = 0
                for k in range(order_gq):
                    N_iso = shapeFunctionIsoQ1(iso_loc[k], iso_loc[j])
                    derIso = gradMatrixQ1(iso_loc[k], iso_loc[j])

                    J = jacobianQ1(nodeCoord_el[i], derIso)
                    B = vecInterpolantReorginzer(derShapeFunctionIsoQ1(J, derIso), True)
                    detJ = np.linalg.det(J)

                    x1 = np.dot(N_iso, nodes_x)[0]
                    y1 = np.dot(N_iso, nodes_y)[0]
                    thick = thickness(x1, y1)

                    intergrand = np.matmul(B.T, np.matmul(D, B)) * thick
                    sum_chi += detJ * intergrand * iso_w[j] * iso_w[k]

                K_el += sum_chi

        ICA_el = ICA_global[i]
        nen = len(ICA_el)

        # Reconstruct Add to global stiffness matrix
        for j in range(nen):
            for k in range(nen):
                K_global[2 * ICA_el[j], 2 * ICA_el[k]] += K_el[2 * j, 2 * k]
                K_global[2 * ICA_el[j], 2 * ICA_el[k] + 1] += K_el[2 * j, 2 * k + 1]
                K_global[2 * ICA_el[j] + 1, 2 * ICA_el[k]] += K_el[2 * j + 1, 2 * k]
                K_global[2 * ICA_el[j] + 1, 2 * ICA_el[k] + 1] += K_el[2 * j + 1, 2 * k + 1]

    return K_global


def bodyForce(nodeCoord_el, xnode_el, ynode_el, area_el, ICA_global, order_gq, vol_body, tri):
    # Initialise global body force vector
    num_nodes = np.amax(ICA_global) + 1
    Fb_vector = np.zeros((2 * num_nodes, 1))

    # Loop through all elements of the geometry
    for i in range(len(ICA_global)):
        # Initialise element body force vector
        Fb_el = 0

        # Get element geometry
        nodes_x = xnode_el[i]
        nodes_y = ynode_el[i]

        if tri is True:
            # Get element geometry
            a_el = area_el[i]

            # Get qauss quadrature information
            iso_loc, iso_w = gaussQuadratureInfoTri(order_gq - 1)

            # Gauss quadrature
            for j in range(order_gq):
                N_iso = shapeFunctionIsoP1(iso_loc[j, 0], iso_loc[j, 1])
                x1 = np.dot(N_iso, nodes_x)[0]
                y1 = np.dot(N_iso, nodes_y)[0]

                thick = thickness(x1, y1)

                # Organize from scalar shape function array to 2D vector array
                N = vecInterpolantReorginzer(N_iso, False)

                # Solve
                integrand = np.matmul(N.T, bodyLoad(x1, y1, vol_body).T)
                Fb_el += a_el * integrand * iso_w[j] * thick

        if tri is False:
            # Get qauss quadrature information
            iso_loc, iso_w = gaussQuadratureInfoLin(order_gq - 1)

            # Gauss Quadrature
            for j in range(order_gq):
                sum_chi = 0
                for k in range(order_gq):
                    N_iso = shapeFunctionIsoQ1(iso_loc[k], iso_loc[j])
                    derIso = gradMatrixQ1(iso_loc[k], iso_loc[j])
                    J = jacobianQ1(nodeCoord_el[i], derIso)
                    detJ = np.linalg.det(J)

                    x1 = np.dot(N_iso, nodes_x)[0]
                    y1 = np.dot(N_iso, nodes_y)[0]
                    thick = thickness(x1, y1)

                    intergrand = N_iso * bodyLoad(x1, y1)
                    sum_chi += detJ * area_el * thick * intergrand.T * iso_w[j] * iso_w[k]

                Fb_el += sum_chi

        # Place nodal values into global array
        node_count = 0
        for m in ICA_global[i]:
            Fb_vector[2 * m] += Fb_el[2 * node_count]
            Fb_vector[2 * m + 1] += Fb_el[2 * node_count + 1]
            node_count += 1

    return Fb_vector


def boundaryForce(xnode_el, ynode_el, ICA_global, order_gq, tri):
    # Initialise global body force vector
    num_nodes = np.amax(ICA_global) + 1
    Fb_vector = np.zeros((num_nodes, 1))

    # Get qauss quadrature information
    iso_loc, iso_w = gaussQuadratureInfoLin(order_gq - 1)

    # Define iso-parametric triangle
    node_iso = np.array([[1, 0],
                         [0, 1],
                         [0, 0]])

    edge_ICA = np.array([[0, 1],
                         [1, 2],
                         [2, 0]])

    # Define iso-parametric quadrilateral
    node_isoQ = np.array([[-1, -1],
                          [1, -1],
                          [1, 1],
                          [-1, 1]])

    edge_ICAQ = np.array([[0, 1],
                         [1, 2],
                         [2, 3],
                         [3, 0]])

    # Loop through all elements of the geometry
    for i in range(len(ICA_global)):

        # Initialise element body force vector
        Fb_el = 0

        # Get element geometry
        nodes_x = xnode_el[i]
        nodes_y = ynode_el[i]

        if tri is True:
            # Loop through each edge in the isoparametric tri element
            for k in edge_ICA:
                edgeNode_chi = np.zeros((1, len(k)))
                edgeNode_eta = np.zeros((1, len(k)))

                node_count = 0
                for node in k:
                    edgeNode_chi[0, node_count] = node_iso[node, 0]
                    edgeNode_eta[0, node_count] = node_iso[node, 1]
                    node_count += 1

                # Find nodes bounding edge in cartesian space and calculate jacobian
                x1 = np.dot(shapeFunctionIsoP1(edgeNode_chi[0, 0], edgeNode_eta[0, 0]), nodes_x)
                y1 = np.dot(shapeFunctionIsoP1(edgeNode_chi[0, 0], edgeNode_eta[0, 0]), nodes_y)
                x2 = np.dot(shapeFunctionIsoP1(edgeNode_chi[0, 1], edgeNode_eta[0, 1]), nodes_x)
                y2 = np.dot(shapeFunctionIsoP1(edgeNode_chi[0, 1], edgeNode_eta[0, 1]), nodes_y)

                len_12 = math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)

                J = len_12 / 2

                # Initialise gauss sum
                gauss_sum = 0

                for m in range(order_gq):
                    # In 1D Isoparametric space get the GQ point, weight and the 1D shape function
                    chi_1 = iso_loc[m]
                    weight = iso_w[m]
                    N_lin = shapeFunctionIsoLin(chi_1)

                    # Using the 2D isoparametric triangle edge, find the chi and eta point relating to 1D GQ point
                    chi_2 = np.dot(N_lin[0], edgeNode_chi[0].T)
                    eta_2 = np.dot(N_lin[0], edgeNode_eta[0].T)

                    # Using the chi and eta value get the isoparametric triangle shape functions
                    N_iso = shapeFunctionIsoP1(chi_2, eta_2)        # 1x3 matrix

                    # Get x and y cartesian point relating to chi and eta
                    x_gq = np.dot(N_iso[0], nodes_x.T)
                    y_gq = np.dot(N_iso[0], nodes_y.T)

                    # Calculate value of boundary source
                    q_boundary = neumannSource(x_gq, y_gq)

                    # Do gauss summation
                    gauss_sum += N_iso.T * q_boundary * weight * J

                Fb_el += gauss_sum

        if tri is False:
            # Loop through each edge in the isoparametric quad element
            for k in edge_ICAQ:
                edgeNode_chi = np.zeros((1, len(k)))
                edgeNode_eta = np.zeros((1, len(k)))

                node_count = 0
                for node in k:
                    edgeNode_chi[0, node_count] = node_isoQ[node, 0]
                    edgeNode_eta[0, node_count] = node_isoQ[node, 1]
                    node_count += 1

                # Find nodes bounding edge in cartesian space and calculate jacobian
                x1 = np.dot(shapeFunctionIsoQ1(edgeNode_chi[0, 0], edgeNode_eta[0, 0]), nodes_x)
                y1 = np.dot(shapeFunctionIsoQ1(edgeNode_chi[0, 0], edgeNode_eta[0, 0]), nodes_y)
                x2 = np.dot(shapeFunctionIsoQ1(edgeNode_chi[0, 1], edgeNode_eta[0, 1]), nodes_x)
                y2 = np.dot(shapeFunctionIsoQ1(edgeNode_chi[0, 1], edgeNode_eta[0, 1]), nodes_y)

                len_12 = math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)

                J = len_12 / 2

                # Initialise gauss sum
                gauss_sum = 0

                for m in range(order_gq):
                    # In 1D Isoparametric space get the GQ point, weight and the 1D shape function
                    chi_1 = iso_loc[m]
                    weight = iso_w[m]
                    N_lin = shapeFunctionIsoLin(chi_1)

                    # Using the 2D isoparametric quad edge, find the chi and eta point relating to 1D GQ point
                    chi_2 = np.dot(N_lin[0], edgeNode_chi[0].T)
                    eta_2 = np.dot(N_lin[0], edgeNode_eta[0].T)

                    # Using the chi and eta value get the isoparametric quad shape functions
                    N_iso = shapeFunctionIsoQ1(chi_2, eta_2)

                    # Get x and y cartesian point relating to chi and eta
                    x_gq = np.dot(N_iso[0], nodes_x.T)
                    y_gq = np.dot(N_iso[0], nodes_y.T)

                    # Calculate value of boundary source
                    q_boundary = neumannSource(x_gq, y_gq)

                    # Do gauss summation
                    gauss_sum += N_iso.T * q_boundary * weight * J

                Fb_el += gauss_sum

        # Place element vector into global array
        node_count = 0
        for n in ICA_global[i]:
            Fb_vector[n] += Fb_el[node_count]
            node_count += 1

    return Fb_vector