import numpy as np


# Interpolents for 1D Element of any order
# **********************************************************************
def shapeFunctionCartLin(x_value, x_el):
    """
    Find the value of the local shape functions at an x-position
    :param x_value: x-coordinate
    :param x_el: array of local element nodal positions
    :return: array of value of each nodes shape function on the element at the point specified
    """

    # Define empty shape and derivative function arrays for values to be appended to
    M_shape = []

    # Loop which calculates the value of a shape function at a point of an element
    # The ith iteration indicates the loop over each global node, the jth iteration indicates the iteration
    # over each product in the sequence

    for xi in x_el:
        N_p = 1

        for xj in x_el:
            if xj != xi:
                N_p *= (x_value - xj)/(xi - xj)

        M_shape.append(N_p)

    # Cast appended lists as numpy arrays
    M_shape = np.array([M_shape])

    return M_shape


def derShapeFunctionCartLin(x_value, x_el):
    """
    Find the value of the local shape functions at an x-position
    :param x_value: x-coordinate
    :param x_el: array of local element nodal positions
    :return: array of value of each nodes shape function on the element at the point specified
    """

    # Define empty derivative function array
    M_derivative = []

    # Loop which calculates the value of a derivative function at a point of an element
    # The ith iteration indicates the loop over each global node, the jth iteration indicates the iteration
    # over each product in the sequence

    for xi in x_el:
        sum_loop = 0
        prod_loop = 1

        for xj in x_el:
            if xj != xi:
                r = 1 / (xi - xj)

                for xm in x_el:
                    if xm != xj and xm != xi:
                        prod_loop = 1
                        prod_loop *= (x_value - xm) / (xi - xm)

                sum_loop += r * prod_loop
        M_derivative.append(sum_loop)

    M_derivative = np.array([M_derivative])

    return M_derivative


def shapeFunctionIsoLin(chi):
    shapeFunc = np.array([[-(chi - 1) / 2, (chi + 1) / 2]])

    return shapeFunc


# Interpolents for P1 element
# **********************************************************************

# Cartestian shape functions
def shapeFunctionCartP1(nodeCoord_el, position):
    x1 = nodeCoord_el[0, 0]
    x2 = nodeCoord_el[1, 0]
    x3 = nodeCoord_el[2, 0]
    y1 = nodeCoord_el[0, 1]
    y2 = nodeCoord_el[1, 1]
    y3 = nodeCoord_el[2, 1]

    # Generate M matrix and its inverse
    M = np.array([[1, x1, y1],
                  [1, x2, y2],
                  [1, x3, y3]])

    M_inv = np.linalg.inv(M)

    # Use cartesian position to find value of shape functions
    p_vec = np.array([[1, position[0], position[1]]])
    shapeFunc = np.dot(p_vec, M_inv)

    return shapeFunc


def derShapeFunctionCartP1(nodeCoord_el):
    x1 = nodeCoord_el[0, 0]
    x2 = nodeCoord_el[1, 0]
    x3 = nodeCoord_el[2, 0]
    y1 = nodeCoord_el[0, 1]
    y2 = nodeCoord_el[1, 1]
    y3 = nodeCoord_el[2, 1]

    # Create M matrix and its inverse
    M = np.array([[1, x1, y1],
                  [1, x2, y2],
                  [1, x3, y3]])

    M_inv = np.linalg.inv(M)

    coeff_m = np.array([[0, 1, 0],
                        [0, 0, 1]])

    derShapeFunc = np.matmul(coeff_m, M_inv)

    return derShapeFunc


# Isoparametric element shape functions
def shapeFunctionIsoP1(chi, eta):
    return np.array([[chi, eta, round(1 - chi - eta, 10)]])


def derivativeFunctionIsoP1():
    return np.array([[1, 0, -1], [0, 1, -1]])


# Interpolants for Q1 element
# **********************************************************************
def shapeFunctionIsoQ1(chi, eta):
    iso_node = np.array([[-1, 1]])

    N_linx = shapeFunctionCartLin(chi, iso_node[0])
    N_liny = shapeFunctionCartLin(eta, iso_node[0])

    N0 = N_linx[0, 0] * N_liny[0, 0]
    N1 = N_linx[0, 1] * N_liny[0, 0]
    N2 = N_linx[0, 1] * N_liny[0, 1]
    N3 = N_linx[0, 0] * N_liny[0, 1]

    return np.array([[N0, N1, N2, N3]])


def gradMatrixQ1(chi, eta):
    iso_node = np.array([[-1, 1]])

    N_linx = shapeFunctionCartLin(chi, iso_node[0])
    N_liny = shapeFunctionCartLin(eta, iso_node[0])
    B_linx = derShapeFunctionCartLin(chi, iso_node[0])
    B_liny = derShapeFunctionCartLin(eta, iso_node[0])

    GN0x = B_linx[0, 0] * N_liny[0, 0]
    GN1x = B_linx[0, 1] * N_liny[0, 0]
    GN2x = B_linx[0, 1] * N_liny[0, 1]
    GN3x = B_linx[0, 0] * N_liny[0, 1]

    GN0y = N_linx[0, 0] * B_liny[0, 0]
    GN1y = N_linx[0, 1] * B_liny[0, 0]
    GN2y = N_linx[0, 1] * B_liny[0, 1]
    GN3y = N_linx[0, 0] * B_liny[0, 1]

    GN_Q1 = np.array([[GN0x, GN1x, GN2x, GN3x],
                      [GN0y, GN1y, GN2y, GN3y]])

    return GN_Q1


def jacobianQ1(nodeCoords, GN_Q1):
    return np.matmul(GN_Q1, nodeCoords)


def derShapeFunctionIsoQ1(jQ1, GN_Q1):
    j_inv = np.linalg.inv(jQ1)
    derShapeFunc = np.dot(j_inv, GN_Q1)

    return derShapeFunc


# Interpolants for Q2 element
# **********************************************************************
def shapeFunctionIsoQ2(chi, eta):
    iso_node = np.array([[-1, 0, 1]])

    N_linx = shapeFunctionCartLin(chi, iso_node[0])
    N_liny = shapeFunctionCartLin(eta, iso_node[0])

    N0 = N_linx[0, 0] * N_liny[0, 0]
    N1 = N_linx[0, 2] * N_liny[0, 0]
    N2 = N_linx[0, 2] * N_liny[0, 2]
    N3 = N_linx[0, 0] * N_liny[0, 2]
    N4 = N_linx[0, 1] * N_liny[0, 0]
    N5 = N_linx[0, 2] * N_liny[0, 1]
    N6 = N_linx[0, 1] * N_liny[0, 2]
    N7 = N_linx[0, 0] * N_liny[0, 1]
    N8 = N_linx[0, 1] * N_liny[0, 1]

    return np.array([[N0, N1, N2, N3, N4, N5, N6, N7, N8]])


def gradMatrixQ2(chi, eta):
    iso_node = np.array([[-1, 0, 1]])

    N_linx = shapeFunctionCartLin(chi, iso_node[0])
    N_liny = shapeFunctionCartLin(eta, iso_node[0])
    B_linx = derShapeFunctionCartLin(chi, iso_node[0])
    B_liny = derShapeFunctionCartLin(eta, iso_node[0])

    GN0x = B_linx[0, 0] * N_liny[0, 0]
    GN1x = B_linx[0, 2] * N_liny[0, 0]
    GN2x = B_linx[0, 2] * N_liny[0, 2]
    GN3x = B_linx[0, 0] * N_liny[0, 2]
    GN4x = B_linx[0, 1] * N_liny[0, 0]
    GN5x = B_linx[0, 2] * N_liny[0, 1]
    GN6x = B_linx[0, 1] * N_liny[0, 2]
    GN7x = B_linx[0, 0] * N_liny[0, 1]
    GN8x = B_linx[0, 1] * N_liny[0, 1]

    GN0y = N_linx[0, 0] * B_liny[0, 0]
    GN1y = N_linx[0, 2] * B_liny[0, 0]
    GN2y = N_linx[0, 2] * B_liny[0, 2]
    GN3y = N_linx[0, 0] * B_liny[0, 2]
    GN4y = N_linx[0, 1] * B_liny[0, 0]
    GN5y = N_linx[0, 2] * B_liny[0, 1]
    GN6y = N_linx[0, 1] * B_liny[0, 2]
    GN7y = N_linx[0, 0] * B_liny[0, 1]
    GN8y = N_linx[0, 1] * B_liny[0, 1]

    GN_Q2 = np.array([[GN0x, GN1x, GN2x, GN3x, GN4x, GN5x, GN6x, GN7x, GN8x],
                      [GN0y, GN1y, GN2y, GN3y, GN4y, GN5y, GN6y, GN7y, GN8y]])

    return GN_Q2


def jacobianQ2(GN_Q2, nodeCoords):
    return np.matmul(GN_Q2, nodeCoords)


def derShapeFunctionIsoQ2(jQ2, GN_Q2):
    j_inv = np.linalg.inv(jQ2)
    derShapeFunc = np.dot(j_inv, GN_Q2)

    return derShapeFunc


# Interpolant reorginizer for 2D Vector
# **********************************************************************
def vecInterpolantReorginzer(scalar_interp, derivative):

    numNodes = len(scalar_interp[0])
    interpolant = 0

    if derivative is True:
        interpolant = np.zeros((3, 2 * numNodes))

        for inode in range(numNodes):
            interpolant[0, 2 * inode] = scalar_interp[0, inode]
            interpolant[0, 2 * inode + 1] = 0

            interpolant[1, 2 * inode] = 0
            interpolant[1, 2 * inode + 1] = scalar_interp[1, inode]

            interpolant[2, 2 * inode] = scalar_interp[1, inode]
            interpolant[2, 2 * inode + 1] = scalar_interp[0, inode]

    if derivative is False:
        interpolant = np.zeros((2, 2 * numNodes))

        for jnode in range(numNodes):
            interpolant[0, 2 * jnode] = scalar_interp[0, jnode]
            interpolant[0, 2 * jnode + 1] = 0
            interpolant[1, 2 * jnode] = 0
            interpolant[1, 2 * jnode + 1] = scalar_interp[0, jnode]

    return interpolant
