import numpy as np
import Mesh_2D_Vec
from LinearTriangle import LinearTriangle as lt

def bmatrix(a):
    """Returns a LaTeX bmatrix

    :a: numpy array
    :returns: LaTeX bmatrix as a string
    """
    shape = a.shape
    dim = np.alen(shape)
    if dim > 1:
        columns = shape[1]
        rows = shape[0]
    else:
        columns = shape[0]
        rows = 0

    if len(a.shape) > 2:
        raise ValueError('bmatrix can at most display two dimensions')
    lines = str(a).replace('[', '').replace(']', '').splitlines()
    rv = [r'\begin{array}{' + 'c '*columns+'}']
    if dim > 1:
        rv += ['  ' + ' & '.join(str(a[l, :]).replace('[', '').replace(']', '').split()) + r'\\' for l in range(rows)]
    else:
        rv += ['  ' + ' & '.join(str(a).replace('[', '').replace(']', '').split()) + r'\\' ]

    rv +=  [r'\end{array}']
    return '\n'.join(rv)

def thick(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The thickness at position (x,y)
    """
    return 0.1


def v(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The Poison's ratio at position (x,y)
    """
    return 0.3

def E(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The Young's modulus at position (x,y)
    """
    return 1e7


def b(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The body force at position (x,y)
    """
    return np.array([0, - 2e5 * thick(x, y)])


def t_bar(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The prescribed traction at position (x,y)
    """
    return np.array([0, 0])



def u_bar(x, y):
    """
    :param x: x position
    :param y: y position
    :return: The prescribed displacement at position (x,y)
    """
    if x == 0:
        return np.array([0, 0])
    else:
        return np.array([None, None])


nodes_x = np.array([0, 0, 1, 1])    # The x positions of the nodes
nodes_y = np.array([0.5, 0, 0, 0.5])    # The y positions of the nodes

BC = np.zeros(len(nodes_x)*2)
BC[0:4] = 1  # 1 denotes Dirichlet bc nodes, 0 denotes Neumann bc nodes

ICA = np.array([[1, 2, 3],  # The interconnectivity array (indexed from 1, the mesh class re-indexes it from 0)
                [3, 4, 1]])
print(bmatrix(ICA))
# Creating a triangle mesh object with given set up
tri_mesh = Mesh_2D_Vec.Mesh_2D_Vec(nodes_x, nodes_y, ICA, E, v, t_bar, u_bar, b, BC, "strain", lt)

tri_mesh.int_K(1)   # Performing integration to create stiffness matricies
tri_mesh.int_F_bod(3)   # Performing integration to create body force vectors
tri_mesh.assemble()     # Assembling element matrices and vectors into global
print(bmatrix(tri_mesh.K))
print(bmatrix(tri_mesh.F))
tri_mesh.solve_d()      # Solving for global d
print(bmatrix(tri_mesh.d))

print("Deflection : ", tri_mesh.d)    # Displaying node deflections
print("Original node positions: ", tri_mesh.pos)    # Displaying old node positions
print("New node positions: ", tri_mesh.pos + tri_mesh.d)    # Displaying new node positions

e1 = tri_mesh.Elements[0]   # Creating a reference to element 1
e2 = tri_mesh.Elements[1]   # Creating a reference to element 2
print("Stress in el 1: ", np.matmul(tri_mesh.E_mat(0, 0), np.matmul(e1.B(0, 0), e1.d))) # Calculating and displaying stress in element 1
print("Stress in el 2: ", np.matmul(tri_mesh.E_mat(0, 0), np.matmul(e2.B(0, 0), e2.d))) # Calculating and displaying stress in element 2

for i in range(np.alen(tri_mesh.Elements)):
    el = tri_mesh.Elements[i]
    print('***********')
    print(el)
    print('K')
    print(el.el_K)
    print(bmatrix(el.el_K))
    print('F')
    print(bmatrix(el.el_F_bod))

print()

tri_mesh.plot_displacement("Plane strain beam solution", 5)   # Plotting the old and new node positions
