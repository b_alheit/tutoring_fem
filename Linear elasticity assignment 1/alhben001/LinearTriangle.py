import numpy as np


class LinearTriangle:
    def __init__(self, x_nodes, y_nodes, g_node_numbers, el_num):
        self.n_nodes = 3
        self.edges = 3
        self.x = x_nodes
        self.y = y_nodes
        self.g_nodes = g_node_numbers
        DOF = 2 * np.array([g_node_numbers[0],
                                   g_node_numbers[0]+0.5,
                                   g_node_numbers[1],
                                   g_node_numbers[1] + 0.5,
                                   g_node_numbers[2],
                                   g_node_numbers[2] + 0.5,
                                   ])
        self.g_DOF = DOF.astype(int)
        self.el_num = el_num
        self.M = np.array([[1, 1, 1],
                          x_nodes,
                          y_nodes]).T
        self.A = 0.5 * np.linalg.det(self.M)
        self.B_mat = (1/(2 * self.A)) * np.array([[y_nodes[1] - y_nodes[2], 0, y_nodes[2] - y_nodes[0], 0, y_nodes[0] - y_nodes[1], 0],
                                                  [0, x_nodes[2] - x_nodes[1], 0, x_nodes[0] - x_nodes[2], 0, x_nodes[1] - x_nodes[0]],
                                                  [x_nodes[2] - x_nodes[1], y_nodes[1] - y_nodes[2], x_nodes[0] - x_nodes[2], y_nodes[2] - y_nodes[0], x_nodes[1] - x_nodes[0], y_nodes[0] - y_nodes[1]]])
        self.el_K = np.zeros([6, 6])
        self.el_F = np.zeros(6).T
        self.el_F_bound = np.zeros(6).T
        self.el_F_bod = np.zeros(6).T
        self.d = np.zeros(6)
        self.pos = np.array([x_nodes[0], y_nodes[0], x_nodes[1], y_nodes[1], x_nodes[2], y_nodes[2]])

    def B(self, xi, eta):
        return self.B_mat

    def N(self, xi, eta):
        return np.array([[xi, 0, eta, 0, 1 - xi - eta, 0],
                         [0, xi, 0, eta, 0, 1 - xi - eta]])

    # def Nxy(self, x, y):
    #     P = np.array([1, x, y])
    #     return np.matmul(P, np.linalg.inv(self.M))

    def in_element(self, x, y):
        P = np.array([1, x, y])
        xi = np.matmul(P, np.linalg.inv(self.M))
        return np.sum(np.add(1 < xi, xi < 0)) == 0

    def display(self):
        print("Element number = ", self.el_num + 1, "\nx = ", self.x, "\ny = ", self.y, "\nGlobal nodes = ", self.g_nodes + 1)

    def quad_selector(self, n_gp):
        if n_gp == 1:
            xi = np.array([1 / 3])
            eta = np.array([1 / 3])
            weights = np.array([1])
        elif n_gp == 2:
            print("Cannot have 2 Gauss points. Three gauss points have been used instead")
            n_gp = 3
            xi = np.array([1 / 6, 2 / 3, 1 / 6])
            eta = np.array([1 / 6, 1 / 6, 2 / 3])
            weights = np.array([1 / 3, 1 / 3, 1 / 3])
        elif n_gp == 3:
            xi = np.array([1 / 6, 2 / 3, 1 / 6])
            eta = np.array([1 / 6, 1 / 6, 2 / 3])
            weights = np.array([1 / 3, 1 / 3, 1 / 3])
        elif n_gp == 4:
            xi = np.array([1 / 3, 1 / 5, 3 / 5, 1 / 5])
            eta = np.array([1 / 3, 1 / 5, 1 / 5, 3 / 5])
            weights = np.array([-9 / 16, 25 / 48, 25 / 48, 25 / 48])
        elif n_gp > 4:
            print("Max gauss points available is 4. Hence 4 gauss points used instead.")
            n_gp = 4
            xi = np.array([1 / 3, 1 / 5, 3 / 5, 1 / 5])
            eta = np.array([1 / 3, 1 / 5, 1 / 5, 3 / 5])
            weights = np.array([-9 / 16, 25 / 48, 25 / 48, 25 / 48])
        else:
            print("Invalid value chosen for number of Gauss points. 4 Gauss points used instead for accuracy")
            n_gp = 4
            xi = np.array([1 / 3, 1 / 5, 3 / 5, 1 / 5])
            eta = np.array([1 / 3, 1 / 5, 1 / 5, 3 / 5])
            weights = np.array([-9 / 16, 25 / 48, 25 / 48, 25 / 48])
        return n_gp, xi, eta, weights

    def iso_to_physical(self, xi, eta):
        return np.dot(self.N(xi, eta), self.pos)


    def N_edge(self, edge, xi):
        xi = (1+xi)/2
        if edge == 0:
            return self.N(xi, 1 - xi)
        elif edge == 1:
            return self.N(0, xi)
        elif edge == 2:
            return self.N(xi, 0)

    def gauss(self, n_gp, integrand):
        n_gp, xi, eta, weights = self.quad_selector(n_gp)
        integrated = 0
        for i in range(n_gp):
            integrated += self.A * weights[i] * integrand(xi[i], eta[i])
        return integrated



