# -*- coding: utf-8 -*-
"""
Created on Thu May  9 12:54:39 2019

@author: MLSTHE004
"""

import numpy as np
#import math
import matplotlib.pyplot as plt
import matplotlib.tri as tri
from scipy.linalg import det, inv
#import numpy.linalg.pinv as pinv


ICA = np.array([[1,2,3],[1,3,4]])-1 # interconnectivity array for the meshed 2D 
nodes = np.array([[0,0.5],[0,0],[1,0],[1,0.5]])  # nodal coordinates 
# interconnectivity array for vector , each node has 2 degrees of freedom
V_ICA = np.array([[1,2,3,4,5,6],[1,2,5,6,7,8]])-1

X = nodes[:,0]  # nodal x values
Y = nodes[:,1]  # nodal y values


DOF=2*np.shape(nodes)[0]; #Number of degrees of freedom

#preprocessing
E = 1e7; #Young's Modulus
t = 0.1; #Thickness of the plate
area = 0.5;
p = 2e5; # Body force in j- direction
b = np.array(([0,-p])).reshape(2,1);
v = 0.3;    #Poisson ratio
# 

coeff = (E/(1-v**2))
D = coeff*np.array(([1, v, 0],[ v, 1, 0], [0, 0, (1-v)/2])) #Hooken's matrix
nel = 2; #Number of Elements
nn = nel + 2; # Number of nodes


M = np.ones((3,3))
F = np.zeros((2*nn,1))
K = np.zeros((2*nn,2*nn))
d = np.zeros((2*nn,1))
U = np.zeros((2*nn,1))


# Guass points and weights
QuadRule =4
if QuadRule ==1:
    xi  = np.array([1/3])
    eta = np.array([1/3])
    Weight = np.array([1])
elif QuadRule ==3:
    xi = np.array([[float(1/6),float(2/3), float(1/6)]])
    eta = np.array([[float(1/6),float(1/6), float(2/3)]])
    Weight = np.array([[float(1/3),float(1/3), float(1/3)]])
elif QuadRule ==4:
    xi = np.array(([float(1/3),float(2/10), float(6/10), float(2/10)]))
    eta = np.array(([float(1/3),float(2/10), float(2/10), float(6/10)]))
    Weight = np.array(([float(-9/16),float(25/48), float(25/48), float(25/48)]))
    
    
#%%

for e in range(nel):
    asm = ICA[e,:]
    Vasm = V_ICA[e,:]
    x_e = X[asm]
    y_e = Y[asm]
# reproducing the mesh 
    #plt.plot([x_e, x_e[0]],[y_e, y_e[0]])
    
    M[1,:]=x_e.T
    M[2,:]=y_e.T
    M1 =M
   
    # Calculating the area for each element
    Area = det(M)*float(1/2)
    # B is constant gradient matrix is indpendent of x and y
    B = np.array(([y_e[1]-y_e[2],0, y_e[2]-y_e[0], 0, y_e[0]-y_e[1],0],
                  [0, x_e[2]-x_e[1],0, x_e[0]-x_e[2], 0, x_e[1]-x_e[0]],
                  [x_e[2]-x_e[1], y_e[1]-y_e[2], x_e[0]-x_e[2], y_e[2]-y_e[0],x_e[1]-x_e[0], y_e[0]-y_e[1]]))/float(det(M))
    
    #% Initilizing Gauss quadrature
    force_elem =0
    k_elem = 0;
    
    for i in range(QuadRule):
        # shape functions 
        
        N1 = xi[i]
        N2 = eta[i]
        N3 = 1 - xi[i]- eta[i]
        
        N = np.array(([N1,0,N2,0,N3,0],[0,N1,0,N2,0,N3]))
       
        
        #% transformation to isoparametric space 
        x = x_e[0]*N1+x_e[1]*N2 + x_e[2]*N3
        y = y_e[0]*N1+y_e[1]*N2 + y_e[2]*N3
        
        k_elem += 0.5*Weight[i]*np.dot(np.dot(B.T,D), B)
        force_elem += 0.5*Weight[i]*np.dot(N.T,b)
        
        
        K_elem = 2*Area*k_elem
        Force = 2*Area*force_elem
    for j in range(len(Vasm)):
        F[Vasm[j]] += Force[j]
    
    for z in range(len(Vasm)):
        for i in range(len(Vasm)):
            K[Vasm[z], Vasm[i]] += K_elem[z,i]
            
    ## assemblying the displacemnet vector
    d[0:4,0]=0
    K_FF = K[4:2*nn, 4:2*nn]
    K_EE = K[0:4,0:4]
    K_EF = K[0:4,4:2*nn]
    F_F  = F[4:2*nn,0]
    F_E  = F[0:4, 0]
    D_E  = d[0:4,0]
    d[4:2*nn,0]= np.dot((np.linalg.pinv(K_FF)), (F_F-np.dot(K_EF.T, D_E)))
    D_F = d[4:2*nn,0]
    ## updating the nodal displacements 
    dnode_1 = d[0:2,0]
    dnode_2 = d[2:4,0]
    dnode_3 = d[4:6,0]
    dnode_4 = d[6:8,0]
    
for i in range(len(V_ICA)):
    Vasm = V_ICA[i, :]
    asm = ICA[i,:]
    d_e = d[Vasm]
    x_e = X[asm]
    y_e = Y[asm]
    B = np.array(([y_e[1]-y_e[2],0, y_e[2]-y_e[0], 0, y_e[0]-y_e[1],0],
                  [0, x_e[2]-x_e[1],0, x_e[0]-x_e[2], 0, x_e[1]-x_e[0]],
                  [x_e[2]-x_e[1], y_e[1]-y_e[2], x_e[0]-x_e[2], y_e[2]-y_e[0],x_e[1]-x_e[0], y_e[0]-y_e[1]]))/float(det(M))
    
    
    # Dispacement 
    u = np.dot(N, d_e)
    
    # strain 
    strain = np.dot(B, d_e)    
    # stress 
    stress = np.dot(D, strain)
    print('==============================================================================')
    print('strain and stress for element')
    print('strain', strain)
    print('                                                      ')
    print('===============================================================================')
    print('stress', stress)

#% Deformed configuration
print('Deformed configuration')
print('new nodal x values')
Xdeformed = np.array(([0+dnode_1[0], 0+dnode_2[0], 1+dnode_3[0], 1+dnode_3[0]]))
print(Xdeformed.reshape(4,1))

print('new nodal x values')
Ydeformed = np.array(([0.5+dnode_1[1], 0+dnode_2[1], 0+dnode_3[1], 0.5+dnode_3[1]]))
print(Ydeformed.reshape(4,1))





tr = tri.Triangulation(Xdeformed, Ydeformed, ICA)
#undeformed element
tr1 = tri.Triangulation(X, Y, ICA)
plt.figure()
plt.gca().set_aspect('equal')
#### labeling of the plot and legends
plt.triplot(tr, 'bo--', lw=1.0, label='deformed')
plt.triplot(tr1, 'ro-', lw=1.0, label='original')
plt.title('deformed vs. original configuration')
plt.xlabel('X values')
plt.ylabel('Y values')
plt.legend(loc='best')
plt.grid()
plt.show()

    
 











