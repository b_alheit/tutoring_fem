############ This needs to do all the things with 2 triangular elements.
import numpy as np
import matplotlib.pyplot as plt
### Constructing the D matrix
# We need Young's modulus, E and the Poisson ration, nu
E = 1e7
nu = 0.3
### this D is for plane stress
# D = np.zeros((3,3))
# D[0,0] = 1
# D[1,1] = 1
# D[0,1] = nu
# D[1,0] = nu
# D[2,2] = (1-nu)/2
# D = (E/(1-nu**2))*D
### this D is for plane strain
D = np.zeros((3,3))
D[0,0] = 1-nu
D[1,1] = 1-nu
D[0,1] = nu
D[1,0] = nu
D[2,2] = (1-2*nu)/2
D = (E/((1-2*nu)*(1+nu)))*D
### Constructing the InterConnectivity Array
# We need an ICA which tells us where each pair of points is in relation to each element
ICA = [[0,1,2],[0,2,3]]             # this tells us which element has which nodes, each node hsa an x and a y
NodalPositionsX = np.array([[0],[0],[1.0],[1.0]])                 # these two tell us the x and y positions in counter-clockwise fashion, starting at the top left
NodalPositionsY = np.array([[0.5],[0],[0],[0.5]])

NodalPositions = np.concatenate((NodalPositionsX,NodalPositionsY),axis=1)   # this step combines the arrays into one with the correct dimensions

### We need a GN matrix to get our Jacobian and to make B, this guy is specific to triangles
GN = np.zeros((2,3))    # the dimension is since we have three shape functions for triangles and need derivatives wrt xi and eta
GN[0,0] = 1
GN[0,2] = -1
GN[1,1] = 1
GN[1,2] = -1

### We now need to make our Jacobian, which is element specific, hence he's a function. C is the matrix of nodal position values for the element in question
def Jacobian(GN,C):
    return np.matmul(GN,C)

### Now that we have a Jacobian and can obtain C matrices (the local node values for each element) we can move on to making the CB matrix which we'll use to make the B matrix for each element
def CB(Jacobian,GN):
    Jinverse = np.linalg.inv(Jacobian)
    return np.matmul(Jinverse,GN)

### Finally we can construct the element specific B matrix, it involves using the CB matrix and arranging its elements in a specific manner, this will be specific to triangles
def BOnElement(CB):
    B = np.zeros((3,6))
    for i in range(3):
        B[0,2*i] = CB[0,i]
        B[1,2*i+1] = CB[1,i]
        B[2,2*i] = CB[1,i]
        B[2,2*i+1] = CB[0,i]
    return B

### we need the F vector. For that, we need new shape functions
def IsoparametricShapeFunctions(xi, eta):
    N = np.zeros([2,6])
    N[0,0] = xi
    N[0,2] = eta
    N[0,4] = 1-xi-eta
    N[1,1] = xi
    N[1,3] = eta
    N[1,5] = 1 - xi - eta
    return N

def Integrand(bNodal,xi,eta):
    NInHere = IsoparametricShapeFunctions(xi,eta)
    NTN = np.matmul(NInHere.T,NInHere)
    return np.matmul(NTN,bNodal)

def GaussQuadratureForForce(JacobianDeterminant):
    # We need bnodal, the value fo the body force at each node so we can produce the integrand
    bNodal = np.zeros((6, 1))
    bNodal[1] = (2e5) * 0.1  # this is the p value times the thickness to get the right units
    bNodal[3] = (2e5) * 0.1
    bNodal[5] = (2e5) * 0.1
    bNodal = -1*bNodal
    I = 0                                       # setting initial value of integral
    # building up a list of xi values
    xi = [1/6,2/3,1/6]
    eta = [1/6,1/6,2/3]

    # building up a list of weight values
    w = 1/3
    ngp = 3

    for i in range(ngp):
        xi_i = (xi[i])             # produce correct xi at relevant point, python is grumpy with multiplying multidimensional list elements
        eta_i = (eta[i])
        Integran = Integrand(bNodal,xi_i,eta_i)

        I = w*JacobianDeterminant*Integran + I
    return I

### Now we want to construct K on each element, since we have 2 elements we'll just use a for loop with a range of 2. We'll use the same loop to make our global F
GlobalK = np.zeros((8,8))       # size since we have 2 degrees of freedom per node and 4 nodes
GlobalF = np.zeros((8,1))
StressState = [[],[]]
for NumElement in range(2):
    KLocal = np.zeros((6,6))
    ### We need to produce the C matrix for calculations
    C = np.zeros((3,2))
    relevantICA = ICA[NumElement]   # this gets us the relevant element's node numbers

    for index1 in range(3):
        C[index1] = NodalPositions[relevantICA[index1]] # we use the relevant node numbers to get the correct rows of the global node positions into our correct ordering

    # To create the relevant K matrix we need to make use of Gauss quadrature, in this case however we can use a simplified version since our B doesn't depend on xi or eta values
    weight = 1/3
    # we'll need the element specific B, this is where C comes in handy
    B = BOnElement(CB(Jacobian(GN,C),GN))
    # now comes the Gauss Quad, to streamline things I'm keeping everything out of the loop that I can
    JacobianDeterminant = np.linalg.det(Jacobian(GN, C))
    DB = np.matmul(D, B)
    BTDB = B.T
    BTDB = np.matmul(BTDB,DB)   # this is the B transposed times D times B
    for i in range(3):
        for j in range(3):
            KLocal = weight*weight*BTDB*JacobianDeterminant + KLocal
    # Now that we have a local K, we need to put its values into the global K
    row = 0
    for t1 in ICA[NumElement]:
        column = 0
        for t2 in ICA[NumElement]: # right now we're at a specific node, we need to get the correct values of Local K into the right positions of Global K
            # We want to create 2x2 matrices from KLocal, there are 36 entries in KLocal, so we need 9 little matrices, each matrix belongs to a single node combination, like (1,1) or (2,3)
            KLocalBits = np.zeros((2, 2))
            KLocalBits[0] = KLocal[2 * row][2 * column:2 * column + 2]  # the plus 2 is weird, python goes up to but not including the number, so it really reads 2*column to 2*column +1
            KLocalBits[1] = KLocal[2 * row + 1][2 * column:2 * column + 2]

            GlobalK[2 * t1][2 * t2:2 * t2 + 2] = KLocalBits[0] + GlobalK[2 * t1][2 * t2:2 * t2 + 2]
            GlobalK[2 * t1+1][2 * t2:2 * t2 + 2] = KLocalBits[1] + GlobalK[2 * t1+1][2 * t2:2 * t2 + 2]

            column +=1
        row+=1

    # we need the local element's forces to put into the global force array, for this we use our Gauss Quadrature which ahs been set up for this specific case.
    LocalF = GaussQuadratureForForce(JacobianDeterminant)
    index2 = 1
    # this puts the relevant forces into the global F in the correct places, the output has 4 non-zero entries and the two shared nodes have double the force as would be expected
    for k in ICA[NumElement]:
        GlobalF[2*k+1] = GlobalF[2*k+1]+LocalF[index2]            #only need the +1 since the x components are all zero
        index2 +=2

    StressState[NumElement] = np.matmul(D,B)

### Final push, we need to partition GlobalF in such a way as to take into account the boundary conditions. In this case that means that there is no displacement at nodes 1 and 2, this also allows us to partition GlobalK
GlobalFPartitioned = GlobalF[4:]           # the colon has a blank space at the end and this implies the end of the array
GlobalKPartitioned = GlobalK[4:,4:]

# we can now invert the partitioned K and multiply it together with the partitioned F producing our displacements
dSolved = np.matmul(GlobalFPartitioned.T, np.linalg.inv(GlobalKPartitioned))

NodalPositionsXPlotting = np.array([[0],[0],[1.0],[1.0],[0]])    # this is necessary for plotting, the first point has to be repeated to close the shape
NodalPositionsYPlotting = np.array([[0.5],[0],[0],[0.5],[0.5]])

NodalPositionsX[2] = NodalPositionsX[2]+dSolved[0,0]
NodalPositionsX[3] = NodalPositionsX[3]+dSolved[0,2]

NodalPositionsY[2] = NodalPositionsY[2]+dSolved[0,1]
NodalPositionsY[3] = NodalPositionsY[3]+dSolved[0,3]

plt.plot(NodalPositionsXPlotting,NodalPositionsYPlotting,'b',label='Undeformed')
NodalPositionsXPlotting[2] = NodalPositionsX[2]
NodalPositionsXPlotting[3] = NodalPositionsX[3]
NodalPositionsYPlotting[2] = NodalPositionsY[2]
NodalPositionsYPlotting[3] = NodalPositionsY[3]
plt.plot(NodalPositionsXPlotting,NodalPositionsYPlotting,'r',label='Deformed')
plt.legend()
plt.grid()
plt.xlabel('x (m)')
plt.ylabel('y (m)')
plt.title('Plane strain on a beam under a constant body load')
NodalPositions = np.concatenate((NodalPositionsX,NodalPositionsY),axis=1)
print('New Nodal Positions, x and y respectively',NodalPositions)
### Now to work out the Stress State, we need the displacements and B on each element
# i got lazy, don't judge me please

for NumElement in range(2):
    displacementMatrix = np.zeros(6)
    if NumElement==1:
        displacementMatrix[4] = dSolved[0,0]
        displacementMatrix[5] = dSolved[0,1]
    elif NumElement==2:
        displacementMatrix[2] = dSolved[0, 0]
        displacementMatrix[3] = dSolved[0, 1]
        displacementMatrix[4] = dSolved[0, 2]
        displacementMatrix[5] = dSolved[0, 3]

    C = np.zeros((3, 2))
    for index1 in range(3):
        C[index1] = NodalPositions[relevantICA[index1]] # we use the relevant node numbers to get the correct rows of the global node positions into our correct ordering

    # we'll need the element specific B, this is where C comes in handy
    B = BOnElement(CB(Jacobian(GN,C),GN))

    strain = np.matmul(B,displacementMatrix)
    Stress = np.matmul(D,strain)
    StressState[NumElement] = Stress
print('Stress state',StressState)
plt.show()

