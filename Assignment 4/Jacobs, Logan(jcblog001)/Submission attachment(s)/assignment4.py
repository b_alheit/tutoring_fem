
import numpy as np
from math import *
import matplotlib.pyplot as plt
# from scipy.linalg import det, inv
# import statistics as st
# from sympy import *

#####################################################

def Tri_gp(p):  # polynomial p
    # quadrature points (qp)
    # column 1 gives the xsi/x-coordinates of points
    # column 2 gives the eta/y-coordinates of points
    # column 3 gives the weights
    qp = np.zeros([p, 3])
    if p == 1:
        qp = np.array([1/3, 1/3, 1.0])
    elif p == 2:
        qp = np.array([[1/6, 1/6, 1/3],
                       [1/6, 2/3, 1/3],
                       [2/3, 1/6, 1/3]])
    elif p == 3:
        qp = np.array([[1/3, 1/3, -9/16],
                       [1/5, 1/5, 25/48],
                       [1/5, 3/5, 25/48],
                       [3/5, 1/5, 25/48]])
    return qp

# Next we convert th e cartesian coordinate to the iso-parametric space
# Therefore x and y are given by:
# x = x_e * N
# y = y_e * N


def iso(xi, eta, xe, ye):
    # shape_functions/mapping
    N = np.array([xi, eta, 1-xi-eta])
    x = np.dot(xe, N, out=None)
    y = np.dot(ye, N, out=None)
    return x,  y, N


# Now to compute the integral using Gauss quadrature:


def quadrature(p, xe, ye, F):
    # area of triangle
    A = abs(xe[0]*(ye[1]-ye[2])+xe[1]*(ye[2]-ye[0])+xe[2]*(ye[0]-ye[1]))/2.0
    if p == 1:
        w = Tri_gp(p)
        x1 = iso(w[0], w[1], xe, ye)[0]
        y1 = iso(w[0], w[1], xe, ye)[1]
        z = A*w[2]*F(x1, y1)

    else:
        w = Tri_gp(p)
        x1 = iso(w[:, 0], w[:, 1], xe, ye)[0]
        y1 = iso(w[:, 0], w[:, 1], xe, ye)[1]
        z = A*np.dot(F(x1, y1), w[:, 2], out=None)

    return z


######################################
# to get F for each element
def g_quad(p, xe, ye, F):
    # area of triangle
    A = abs(xe[0]*(ye[1]-ye[2])+xe[1]*(ye[2]-ye[0])+xe[2]*(ye[0]-ye[1]))/2.0
    if p == 1:
        w = Tri_gp(p)
        x1 = iso(w[0], w[1], xe, ye)[0]
        y1 = iso(w[0], w[1], xe, ye)[1]
        n = iso(w[0], w[1], xe, ye)[2]
        z = A*w[2]*F(x1, y1) *n

    else:
        w = Tri_gp(p)
        x1 = iso(w[:, 0], w[:, 1], xe, ye)[0]
        y1 = iso(w[:, 0], w[:, 1], xe, ye)[1]
        n = iso(w[:, 0], w[:, 1], xe, ye)[2]
        f = n * w[:, 2]
        z = A * np.dot(f, F(x1, y1), out=None)

    return z

# various S(x,y) functions


def sc(x, y):
    sc = 6*x**0*y**0
    return sc


def s1(x, y):
    s1 = 1 + x + y
    return s1


def s2(x, y):
    s2 = (x-4)**2 + (y-1)**2
    return s2


# ELEMENTS
M1 = np.array([[1, 1, 1], [0, 4, 0], [-1, 1, 3]])  # element 1
M2 = np.array([[1, 1, 1], [4, 4, 0], [1, 5, 3]])  # element 2
M3 = np.array([[1, 1, 1], [4, 8, 4], [1, 3, 5]])  # element 3
M4 = np.array([[1, 1, 1], [4, 8, 8], [1, -1, 3]])  # element 4
M5 = np.array([[1, 1, 1], [4, 4, 8], [1, -3, -1]])  # element 5
M6 = np.array([[1, 1, 1], [4, 0, 4], [1, -1, -3]])  # element 6

p = 1  # QUAD RULE 1
Z1 = quadrature(p, M1[1], M1[2], sc)
Z2 = quadrature(p, M2[1], M2[2], sc)
Z3 = quadrature(p, M3[1], M3[2], sc)
Z4 = quadrature(p, M4[1], M4[2], sc)
Z5 = quadrature(p, M5[1], M5[2], sc)
Z6 = quadrature(p, M6[1], M6[2], sc)

z = np.array([Z1, Z2, Z3, Z4, Z5, Z6])
Nel = 6  # number of elements
for k in range(Nel):
    print('value of the integral at element {} \t S(x,y)=6:  {}'.format(k+1, z[k]))

print('***********************************')
p = 3    # QUAD RULE 4

Z1 = quadrature(p, M1[1], M1[2], s1)
Z2 = quadrature(p, M2[1], M2[2], s1)
Z3 = quadrature(p, M3[1], M3[2], s1)
Z4 = quadrature(p, M4[1], M4[2], s1)
Z5 = quadrature(p, M5[1], M5[2], s1)
Z6 = quadrature(p, M6[1], M6[2], s1)


z = np.array([Z1, Z2, Z3, Z4, Z5, Z6])
Nel = 6  # number of elements
for k in range(Nel):
    print('value of the integral at element {} \t S(x,y)=1+x+y:  {}'.format(k+1, z[k]))

print('***********************************')

p = 3  # QUAD RULE 4
Z1 = quadrature(p, M1[1], M1[2], s2)
Z2 = quadrature(p, M2[1], M2[2], s2)
Z3 = quadrature(p, M3[1], M3[2], s2)
Z4 = quadrature(p, M4[1], M4[2], s2)
Z5 = quadrature(p, M5[1], M5[2], s2)
Z6 = quadrature(p, M6[1], M6[2], s2)

z = np.array([Z1, Z2, Z3, Z4, Z5, Z6])
Nel = 6  # number of elements
for k in range(Nel):
    print('value of the integral at element {} \t S(x,y)=(x-4)**2 + (y-1)**2:  {}'.format(k+1, z[k]))

print('***********************************')
print('***********************************')
print('***********************************')
print('***********************************')

print('FORCE VECTOR')
p = 1  # QUAD RULE 1
Z1 = g_quad(p, M1[1], M1[2], sc)
Z2 = g_quad(p, M2[1], M2[2], sc)
Z3 = g_quad(p, M3[1], M3[2], sc)
Z4 = g_quad(p, M4[1], M4[2], sc)
Z5 = g_quad(p, M5[1], M5[2], sc)
Z6 = g_quad(p, M6[1], M6[2], sc)
Nel = 6  # number of elements
nn = 7
Gforce = np.array([[Z1[0] + Z6[1]],
                   [Z1[1] + Z2[0] + Z3[0] + Z4[0] + Z5[0] + Z6[0]],
                   [Z5[2] + Z4[1]],
                   [Z4[2] + Z3[1]],
                   [Z2[1] + Z3[2]],
                   [Z1[2] + Z2[2]],
                   [Z5[1] + Z6[2]]])
for k in range(nn):
    print('value of the integral at node {} \t S(x,y)= 6:  {} '.format(k+1, Gforce[k]))

print('***********************************')
p = 3  # QUAD RULE 4
Z1 = g_quad(p, M1[1], M1[2], s1)
Z2 = g_quad(p, M2[1], M2[2], s1)
Z3 = g_quad(p, M3[1], M3[2], s1)
Z4 = g_quad(p, M4[1], M4[2], s1)
Z5 = g_quad(p, M5[1], M5[2], s1)
Z6 = g_quad(p, M6[1], M6[2], s1)
Nel = 6  # number of elements
nn = 7
Gforce = np.array([[Z1[0] + Z6[1]],
                   [Z1[1] + Z2[0] + Z3[0] + Z4[0] + Z5[0] + Z6[0]],
                   [Z5[2] + Z4[1]],
                   [Z4[2] + Z3[1]],
                   [Z2[1] + Z3[2]],
                   [Z1[2] + Z2[2]],
                   [Z5[1] + Z6[2]]])

for k in range(nn):
    print('value of the integral at node {} \t S(x,y)= 1 + x + y:  {} '.format(k+1, Gforce[k]))

print('***********************************')
p = 3  # QUAD RULE 4
Z1 = g_quad(p, M1[1], M1[2], s2)
Z2 = g_quad(p, M2[1], M2[2], s2)
Z3 = g_quad(p, M3[1], M3[2], s2)
Z4 = g_quad(p, M4[1], M4[2], s2)
Z5 = g_quad(p, M5[1], M5[2], s2)
Z6 = g_quad(p, M6[1], M6[2], s2)
Nel = 6  # number of elements
nn = 7 # no. of nodes
Gforce = np.array([[Z1[0] + Z6[1]],
                   [Z1[1] + Z2[0] + Z3[0] + Z4[0] + Z5[0] + Z6[0]],
                   [Z5[2] + Z4[1]],
                   [Z4[2] + Z3[1]],
                   [Z2[1] + Z3[2]],
                   [Z1[2] + Z2[2]],
                   [Z5[1] + Z6[2]]])
for k in range(nn):
    print('value of the integral at node {} \t S(x,y)=(x-4)**2 + (y-1)**2:  {} '.format(k+1, Gforce[k]))



