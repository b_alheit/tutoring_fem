##| Daniel Pons | PNSDAN002 | MEC5063Z | Assignment 4: Triangular Gauss
import numpy as np
import matplotlib.pyplot as plt
import scipy.linalg as sp
import statistics as stat
import matplotlib.pyplot as plt
import random as rand
#SEE Bottom for assignment answer.

##  MESH DATA INPUT

#X&Y coordiniate array
x = np.array([0 ,4,8,8,4,0,4]) #all x positions in order of numbering
y = np.array([-1,1,-1,3,5,3,-3]) #all y posiitons in order of numbering

ICA = np.array([[1,2,6],
                [6,2,5],
                [5,2,4],
                [4,2,3],
                [3,2,7],
                [7,2,1]])     #each internal array is for an element Using standard CCW numbering (global node numbers)

##Given functions
def S1(x,y):
    return 6
def S2(x,y):
    return (1+x+y)
def S3(x,y):
    return (((x-4)**2)+(y-1)**2)
                            
                            
##  Meshing
def MESH2D(ICA,x_nodal_array,y_nodal_array): #takes in an ICA, nodal arrays should do quad&tri
    ICA_0 = ICA-1   #0 indexed ICA for ease of use with position array
    X = np.zeros((len(ICA),len(ICA[0]))) #X&Y vectors to store all x-y points for the mesh&plotting
    Y = np.zeros((len(ICA),len(ICA[0])))
    
    for k in range(0,len(ICA)): #creates x&y points for each node to be plotted
        X[k] = x_nodal_array[ICA_0[k]]
        Y[k] = y_nodal_array[ICA_0[k]]
    #add node one again for plotting purposes
    
    X_plot = np.zeros((len(ICA),len(ICA[0])+1)) #additional point to close triangles
    Y_plot = np.zeros((len(ICA),len(ICA[0])+1))
    X_bar = np.zeros((len(ICA),1))
    Y_bar = np.zeros((len(ICA),1))
    for i in range(0,len(ICA)):
        X_plot[i] = np.append(X[i],X[i][0])
        Y_plot[i] = np.append(Y[i],Y[i][0])
        X_bar[i] = np.average(X[i]) #average x-pos
        Y_bar[i] = np.average(Y[i]) #av.y pos
    return [[X,Y],[X_plot,Y_plot],[X_bar,Y_bar]] #returns the x&y points for each, the plotted arrays and the centroids
mesh = MESH2D(ICA,x,y)
X = mesh[0][0] #nodal x points
Y = mesh[0][1] #nodal y

X_plot = mesh[1][0] #x points for plotting
Y_plot = mesh[1][1] #ypoints for plotting

X_bar = mesh[2][0] #average x-pos
Y_bar = mesh[2][1] #average y-pos


##PLOT MESH
fcolour = ['b','g','r','c','m','k','y','k--','b--','g--','r--','c--','m--','y--']    
plt.figure(0) #mesh
for i in range(0,len(ICA)):
    plt.plot(X_plot[i],Y_plot[i],fcolour[0])
    plt.scatter(X_bar[i],Y_bar[i],marker = "*",c="r")
plt.xlabel('x position - (m)')
plt.ylabel("y position - (m)")
plt.title("MESH")
#plt.legend(["Linear","Quadratic","Quartic"])
plt.tight_layout() #make room for axis labels and title
plt.grid()
plt.show()

##Gauss2D

#Map triangle to Isoparametric space
def ISO2C(xi,eta,X_nod,Y_nod): #takes xi,eta and returns xy position
    N = np.array([[xi],
                 [eta],
                 [1-xi-eta]])
                 
    x = np.matmul(X_nod,N) 
    y = np.matmul(Y_nod,N)
    C_pos = np.array([x,y])
    return C_pos

#Integration function
def int_tri(Ngp,function,X_nod,Y_nod): #function must be a previously defined function with an argument (x,y)
    #x_2_e & x_1_e are the upper and lower limits of integration for the element
    #gauss quad array GQA
    #global x1,x2,x3,y1,y2,y3
    #global M,J_det,qr_i,I_g
    GQ_xi_i = np.array([[1/3],
                        [1/6,2/3,1/6],
                        [1/3,2/10,6/10,2/10]]) #xi interpollation points
    GQ_eta_i = np.array([[1/3],
                        [1/6,1/6,2/3],
                        [1/3,2/10,2/10,6/10]]) #eta
    GQ_w_i = np.array([[1],
                    [1/3,1/3,1/3],
                    [-9/16,25/48,25/48,25/48]]) #weighting points for each 
    qr = [1,3,4] #used to relate index to number of gauss pts
    #area and determinant
    x1,x2,x3 = X_nod
    y1,y2,y3 = Y_nod
    M = np.array([[1,x1,y1],
                  [1,x2,y2],
                  [1,x3,y3]])
    J_det = np.linalg.det(M)
    # Constants
    qr_i = qr.index(Ngp) # ie. ngp = 3, index = 1
    # Integration
    I_g = 0
    for i in range(Ngp):
        xi  = GQ_xi_i[qr_i][i] #Gauss point xi
        eta = GQ_eta_i[qr_i][i] #Gauss point eta
        w_i = GQ_w_i[qr_i][i] #Gauss point xi
        
        x_y = ISO2C(xi,eta,X_nod,Y_nod)
        x = x_y[0] #x as a function of xi (map
        y = x_y[1]
        #Integral
        I_g += function(x,y)*w_i
        #print("xi:",xi,"\nEta:",eta,"\nW_i:",w_i,"\nx:",x,"\ny:",y,"\nI_g:",I_g,"\n")
    Integral = 0.5*I_g*J_det
    return Integral





#Shape fn.
def NT_tri(x,y,X_nod,Y_nod,elnum): #Takes in x&y pos, node arrays of the triangle nodes and returns shape fn.
    
    #constants for conciseness
    x1 = X_nod[elnum-1][0] #elnum adjusted for zero indexing
    x2 = X_nod[elnum-1][1]
    x3 = X_nod[elnum-1][2]
    y1 = Y_nod[elnum-1][0]
    y2 = Y_nod[elnum-1][1]
    y3 = Y_nod[elnum-1][2]
    # matrix form or eqn
    N_mat = np.array([[1 ,1 ,1 ],
                      [x1,x2,x3],
                      [y1,y2,y3]]) #straight from formula
    F = np.array([[1],
                  [x],
                  [y]]) 
                
    N_xy = np.matmul( np.linalg.inv(N_mat),F) #solve the matrix eqn for shape function in terms of x&y
    return N_xy
    
def B_tri(X_nod,Y_nod,elnum): #x&y dont matter, node arrays of the triangle nodes and returns derivative of the SF
    
    #constants for conciseness
    x1 = X_nod[elnum-1][0] #elnum adjusted for zero indexing
    x2 = X_nod[elnum-1][1]
    x3 = X_nod[elnum-1][2]
    y1 = Y_nod[elnum-1][0]
    y2 = Y_nod[elnum-1][1]
    y3 = Y_nod[elnum-1][2]
    # matrix form or eqn
    M = np.array([[1,x1,y1],
                  [1,x2,y2],
                  [1,x3,y3]])
    M_inv = np.linalg.inv(M)              

    B_coef = np.array([[0,1,0],
                       [0,0,1]])
    B_xy = np.matmul( B_coef,M_inv) #solve the matrix eqn for shape function in terms of x&y
    return B_xy
    
## ASSIGNMENT
el =1
def NT_S1(x_var,y_var):
    return S1(x_var,y_var)*NT_tri(x_var,y_var,X,Y,el)
    
def NT_S2(x_var,y_var):
    return S2(x_var,y_var)*NT_tri(x_var,y_var,X,Y,el)
    
def NT_S3(x_var,y_var):
    return S3(x_var,y_var)*NT_tri(x_var, y_var, X, Y, el)

#calculation/integration
S_list = [[],[],[]] #results list S
NT_S_list = [[],[],[]] #results list NT_S
nelem = len(ICA)  
numnod = len(x)          
for k in range(nelem):
    #S1
    S_list[0].append(int_tri(1,S1,X[k],Y[k])) #appends the integrated S1 fn
    #S2
    S_list[1].append(float(int_tri(1,S2,X[k],Y[k]))) #appends the integrated S2 fn and converts to float
    #S3
    S_list[2].append(float(int_tri(3,S3,X[k],Y[k]))) #similar to above
    
    el = k+1
    #NT_S1
    NT_S_list[0].append(int_tri(1, NT_S1,X[k],Y[k])) #similar to above, but for Ntranspose on S
    #NT_S2
    NT_S_list[1].append(int_tri(3, NT_S2,X[k],Y[k])) #similar to above,NT_S2
    #NT_S3
    NT_S_list[2].append(int_tri(4, NT_S3,X[k],Y[k])) #similar to above,NT_S3


# Assembly
#Force
def tri_semble_F(numnod,nelem,ICA,F_loc): #
    #K_lin = np.zeros((numnod, numnod))  # globak K matrix of appropriate size to ndof
    F_bod = np.zeros((numnod, 1))
    for i in range(nelem):
        g1 = int(ICA[i][0])-1  # global index 1,zero indexed
        g2 = int(ICA[i][1])-1  # global index 2,zero indexed
        g3 = int(ICA[i][2])-1  #gi3,zero indexed
        #Stiffness for later use
        '''# stiffness
        K_lin[g1][g1] += K_lin_loc[i][0][0]
        K_lin[g2][g1] += K_lin_loc[i][1][0]
        K_lin[g1][g2] += K_lin[g2][g1]
        K_lin[g2][g2] += K_lin_loc[i][1][1]'''
        # force
        F_bod[g1] += F_loc[i][0]
        F_bod[g2] += F_loc[i][1]
        F_bod[g3] += F_loc[i][2]
    return F_bod
    
F_S1 = tri_semble_F(numnod,nelem,ICA,NT_S_list[0]) #Integral of NT_S1 (force function)
F_S2 = tri_semble_F(numnod,nelem,ICA,NT_S_list[1]) #Integral of NT_S2 (force function)
F_S3 = tri_semble_F(numnod,nelem,ICA,NT_S_list[2]) #Integral of NT_S3 (force function)

## Writing results to EXCEL file:
import xlwt
from xlwt import Workbook

# Workbook is created
wb = Workbook()

# add_sheet is used to create sheet.
sheet1 = wb.add_sheet('Sheet 1')
#Headings
#S
sheet1.write(0, 0, "S1" )
sheet1.write(0, 1, "S2" )
sheet1.write(0, 2, "S3" )
#NTS
sheet1.write(0, 0+3, "NT S1" )
sheet1.write(0, 1+3, "NT S2" )
sheet1.write(0, 2+3, "NT S3" )
#writes data from output arrays and lists to the sheet
for j in range(3):
    for i in range(len(S_list[j])):
        sheet1.write(i+1, j, S_list[j][i] )

for k in range(numnod):
    sheet1.write(k+1, 3, F_S1[k][0])
    sheet1.write(k+1, 4, F_S2[k][0])
    sheet1.write(k+1, 5, F_S3[k][0])

wb.save('OUTPUT.xls')