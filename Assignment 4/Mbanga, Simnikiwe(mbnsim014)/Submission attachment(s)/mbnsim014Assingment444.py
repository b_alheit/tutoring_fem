
import numpy as np

#define the inter_connectivity array

ICA = np.array([[5, 0, 1], [4, 5, 1], [3, 4, 1], [2, 3, 1], [6, 2, 1], [0, 6, 1]])

# The nodal values
X_nod = np.array([0, 4, 8, 8, 4, 0, 4])
Y_nod = np.array([-1, 1, -1, 3, 5, 3, -3])

# elemental coordinates/ nodal values for each element
X_e1 = [X_nod[5], X_nod[0], X_nod[1]]  # first element
Y_e1 = [Y_nod[5], Y_nod[0], Y_nod[1]]
X_e2 = [X_nod[4], X_nod[5], X_nod[1]]  # second eleemnt
Y_e2 = [Y_nod[4], Y_nod[5], Y_nod[1]]
X_e3 = [X_nod[3], X_nod[4], X_nod[1]]  # third element
Y_e3 = [Y_nod[3], Y_nod[4], Y_nod[1]]
X_e4 = [X_nod[2], X_nod[3], X_nod[1]]  # fourth element
Y_e4 = [Y_nod[2], Y_nod[3], Y_nod[1]]
X_e5 = [X_nod[6], X_nod[2], X_nod[1]]  # fifth element
Y_e5 = [Y_nod[6], Y_nod[2], Y_nod[1]]
X_e6 = [X_nod[0], X_nod[6], X_nod[1]]  # sixth elemnt
Y_e6 = [Y_nod[0], Y_nod[6], Y_nod[1]]


# lets define the isoparametric space

def isop_space(epsi, eta, X_nodal_values, Y_nodal_values):
    n = np.array([epsi, eta, 1-epsi-eta])  # shape functions of a triangular element in this space
    x = np.dot(X_nodal_values, n)          #  the x value with relation to the shape functions
    y = np.dot(Y_nodal_values, n)          # the y values with relation to the shape functions

    return x, y, n

# the number of gauss points in a triangular element
# for each gauss point define weight value

def number_gp(i):
     # for one gauss point
    if i == 1:
        gpw = np.array([1/3, 1/3, 1]) # gpw  are the gauss points and weight, [epsi, eta, weight]
    # two gauss points
    elif i == 2:
        gpw = np.array([[1/6, 1/6, 1/3],[2/3, 1/6, 1/3],[1/6, 2/3, 1/3]])
    # four gauss points
    else:
        gpw = np.array([[1/3, 1/3, -9/16],  # first point [epsi, eta, weight]
                        [1/5, 1/5, 25/48],  # second point [epsi, eta, weight]
                        [3/5, 1/5, 25/48],  # third point [epsi, eta, weight]
                        [1/5, 3/5, 25/48]])  # fourth point [epsi, eta, weight]

    return gpw

# define the quadriture for the body force and the source term(function)

def quad_body(i, X_nodal, Y_nodal, body_force):
    ##  Area of triangle
    Area = abs(X_nodal[0]*(Y_nodal[1]-Y_nodal[2])+ X_nodal[1]*(Y_nodal[2] - Y_nodal[0]) + X_nodal[2]*(Y_nodal[0] - Y_nodal[1]))/2.0

    ## for the constant source term
    if i == 1:
        j = number_gp(i)  # making it easy to call the [epsi, eta, weight]
        x_i = isop_space(j[0], j[1], X_nodal, Y_nodal)[0]  # x values with respect to the called epsi and eta
        y_i = isop_space(j[0], j[1], X_nodal, Y_nodal)[1]  # y value with respect to the called epsi and eta
        int_body = Area * j[2] * body_force(x_i, y_i)     # integrating the source term using gauss quad
        # the force vector and the shape function
        n = isop_space(j[0], j[1], X_nodal, Y_nodal)[2]  # shape function with respect to the eat and epsi
        int_body_force = np.dot(int_body, n, out=None)  # integrating the shape function * source term using gauss quad
   # for the non_costant source term we have
    else:
        j = number_gp(i)  # making it easy to call the [epsi, eta, weight]
        x_i = isop_space(j[:, 0], j[:, 1], X_nodal, Y_nodal)[0]  # x values with respect to the called epsi and eta
        y_i = isop_space(j[:, 0], j[:, 1], X_nodal, Y_nodal)[1]  # y value with respect to the called epsi and eta
        int_body = Area * j[:, 2] * body_force(x_i, y_i)    # integrating the source term using gauss quad
        # the force vector
        n = isop_space(j[:, 0], j[:, 1], X_nodal, Y_nodal)[2]  # shape function with respect to the eat and epsi
        force = n* j[:, 2] # JUST NAMING IT FOR EASE OF PROGRAMMING
        int_body_force = Area*np.dot(force, body_force(x_i, y_i), out=None)  # integrating the shape function* source term using gauss quad

    return int_body, int_body_force


# define the source terms
def S1(x, y):
    S1 = 6 # costant source term
    return S1


def S2(x, y):
    S2 = 1 + x + y # linear complete source term
    return S2


def S3(x, y):
    S3 = (x-4)**2 + (y-1)**2 # quadratic complete source term
    return S3

#### quad rule for each source term
## print force vector each element
for i in range(1, 4):
    # for the integration of s(x,y) use [0]
    #integration of Ns(x,y) use [1]
    if i == 1:
        # integrating the constant source term
        #for each element
        # A1, A2,A3,A4,A5,A6 are for element 1,2,3,4,5,6 respectively
        body_force = S1
        A1 = quad_body(i, X_e1, Y_e1 , body_force)[0]
        A2 = quad_body(i, X_e2, Y_e2, body_force)[0]
        A3 = quad_body(i, X_e3, Y_e3, body_force)[0]
        A4 = quad_body(i, X_e4, Y_e4, body_force)[0]
        A5 = quad_body(i, X_e5, Y_e5, body_force)[0]
        A6 = quad_body(i, X_e6, Y_e6, body_force)[0]
        # global vector of the source term
        Global_source = np.array([[np.sum(A1)],
                                      [np.sum(A2)],
                                      [np.sum(A3)],
                                      [np.sum(A4)],
                                      [np.sum(A5)],
                                      [np.sum(A6)]])
        print('intagration of the Source vector for costant fun', Global_source)
        print('#######################################################')
        print('#######################################################')
        print('#######################################################')

        # integratiing the shape function* source term
        #for each element
        body_force = S1
        A1 = quad_body(i, X_e1, Y_e1 , body_force)[1]
        A2 = quad_body(i, X_e2, Y_e2 , body_force)[1]
        A3 = quad_body(i, X_e3, Y_e3 , body_force)[1]
        A4 = quad_body(i, X_e4, Y_e4 , body_force)[1]
        A5 = quad_body(i, X_e5, Y_e5 , body_force)[1]
        A6 = quad_body(i, X_e6, Y_e6 , body_force)[1]
        # the global vector of the shpae fun * source term
        Global_force = np.array([[A1[1] + A6[0]],
                                 [A1[2] + A2[2] + A3[2] + A4[2] + A5[2] + A6[2]],
                                 [A4[0] + A5[1]],
                                 [A3[0] + A4[1]],
                                 [A2[0] + A3[1]],
                                 [A1[0] + A2[1]],
                                 [A5[0] + A6[1]]])
        print('Global force vector for costant fun', Global_force)
        print('#######################################################')
        print('#######################################################')
        print('#######################################################')

    elif i == 2:
        # integrating the linear source term
        # for each element
        # C1, C2,C3,C4,C5,C6 are for element 1,2,3,4,5,6 respectively
        body_force = S2
        C1 = quad_body(i, X_e1, Y_e1, body_force)[0]
        C2 = quad_body(i, X_e2, Y_e2, body_force)[0]
        C3 = quad_body(i, X_e3, Y_e3, body_force)[0]
        C4 = quad_body(i, X_e4, Y_e4, body_force)[0]
        C5 = quad_body(i, X_e5, Y_e5, body_force)[0]
        C6 = quad_body(i, X_e6, Y_e6, body_force)[0]
        # the global vector for the source term
        Global_source = np.array([[np.sum(C1)],
                                  [np.sum(C2)],
                                  [np.sum(C3)],
                                  [np.sum(C4)],
                                  [np.sum(C5)],
                                  [np.sum(C6)]])
        print('intagration of the Source vector for linear complete', Global_source)

        C1 = quad_body(i, X_e1, Y_e1, body_force)[1]
        C2 = quad_body(i, X_e2, Y_e2, body_force)[1]
        C3 = quad_body(i, X_e3, Y_e3, body_force)[1]
        C4 = quad_body(i, X_e4, Y_e4, body_force)[1]
        C5 = quad_body(i, X_e5, Y_e5, body_force)[1]
        C6 = quad_body(i, X_e6, Y_e6, body_force)[1]
        print('#######################################################')
        print('#######################################################')
        print('#######################################################')
        # the global force vector
        Global_force = np.array([[C1[1] + C6[0]],
                                 [C1[2] + C2[2] + C3[2] + C4[2] + C5[2] + C6[2]],
                                 [C4[0] + C5[1]],
                                 [C3[0] + C4[1]],
                                 [C2[0] + C3[1]],
                                 [C1[0] + C2[1]],
                                 [C5[0] + C6[1]]])
        print('Global force vector for linear complete', Global_force)
        print('#######################################################')
        print('#######################################################')
        print('#######################################################')
    else:
        # integrating the constant source term
        # for each element
        # Z1, Z2,Z3,Z4,Z5,Z6 are for element 1,2,3,4,5,6 respectively
        body_force = S3
        Z1 = quad_body(i, X_e1, Y_e1, body_force)[0]
        Z2 = quad_body(i, X_e2, Y_e2, body_force)[0]
        Z3 = quad_body(i, X_e3, Y_e3, body_force)[0]
        Z4 = quad_body(i, X_e4, Y_e4, body_force)[0]
        Z5 = quad_body(i, X_e5, Y_e5, body_force)[0]
        Z6 = quad_body(i, X_e6, Y_e6, body_force)[0]
        # the global source vector
        Global_source = np.array([[np.sum(Z1)],
                                  [np.sum(Z2)],
                                  [np.sum(Z3)],
                                  [np.sum(Z4)],
                                  [np.sum(Z5)],
                                  [np.sum(Z6)]])
        print('intagration of the source vector for quad complete', Global_source)

        ####################
        print('#######################################################')
        print('#######################################################')
        print('#######################################################')
        print('#######################################################')
            #####find the force vector for each element
        Z1 = quad_body(i, X_e1, Y_e1, body_force)[1]
        Z2 = quad_body(i, X_e2, Y_e2, body_force)[1]
        Z3 = quad_body(i, X_e3, Y_e3, body_force)[1]
        Z4 = quad_body(i, X_e4, Y_e4, body_force)[1]
        Z5 = quad_body(i, X_e5, Y_e5, body_force)[1]
        Z6 = quad_body(i, X_e6, Y_e6, body_force)[1]
        # the gloabl force vector
        Global_force = np.array([[Z1[1] + Z6[0]],
                                 [Z1[2] + Z2[2] + Z3[2] + Z4[2] + Z5[2] + Z6[2]],
                                 [Z4[0] + Z5[1]],
                                 [Z3[0] + Z4[1]],
                                 [Z2[0] + Z3[1]],
                                 [Z1[0] + Z2[1]],
                                 [Z5[0] + Z6[1]]])


        print('Global force vector for quad complete', Global_force)
        print('#######################################################')
        print('#######################################################')




