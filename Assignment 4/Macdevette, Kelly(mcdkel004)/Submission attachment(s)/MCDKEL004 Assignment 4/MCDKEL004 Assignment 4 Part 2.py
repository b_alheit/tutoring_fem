import matplotlib.pyplot as plt
from numpy import *


def Niso(xi, eta):
    N = zeros([1,3])
    N[0,0] = xi
    N[0,1] = eta
    N[0,2] = 1-xi-eta
    return N

def S1(x,y):
    return 6

def S2(x,y):
    return 1+x+y


def S3(x,y):
    return (x-4)**2+(y-1)**2


def gaussquadtriangle(ngp, xel, yel, func, elArea):
    if ngp ==2:
        return 'Triangle Gauss Quadrature cannot use 2 gauss points'

    xi1 = [1/3]
    xi3 = [1/6, 2/3, 1/6]
    xi4 = [1/3, 2/10, 6/10, 2/10]
    XI = [xi1, 0, xi3, xi4]

    eta1 = [1/3]
    eta3 = [1/6, 1/6, 2/3]
    eta4 = [1/3, 2/10, 2/10, 6/10]
    Eta = [eta1, 0, eta3, eta4]

    w1 = [1]
    w3 = [1/3, 1/3, 1/3]
    w4 = [-9/16, 25/48, 25/48, 25/48]
    W = [w1, 0, w3, w4]
    I = 0

    for i in range(ngp):
        xi = (XI[int(ngp-1)][i])
        eta = (Eta[int(ngp-1)][i])
        w = (W[int(ngp-1)][i])
        xiso = dot(Niso(xi, eta), xel)
        yiso = dot(Niso(xi, eta), yel)
        intfunc = func(xiso,yiso)*Niso(xi,eta).T    #multiply function by shape functions
        I = I + w*intfunc
    I = elArea*I
    return I

def GlobalForce(x, y, ICA, gaussquadtriangle, integratingfunc): #define general function to calculate global force vector
    Force = zeros([7, 1])
    for i in range(6):
        ICAelement = (ICA[i][:])
        xthis = []
        ythis = []
        for q in ICAelement:
            xthis.append(x[q])
            ythis.append(y[q])
        len = zeros(3)
        for k in range(3):
            if k == 0 or k == 1:
                len[k] = sqrt((xthis[k] - xthis[k + 1]) ** 2 + (ythis[k] - ythis[k + 1]) ** 2)
            if k == 2:
                len[k] = sqrt((xthis[2] - xthis[0]) ** 2 + (ythis[2] - ythis[0]) ** 2)
        p = (len[0] + len[1] + len[2]) / 2
        area = sqrt(p * (p - len[0]) * (p - len[1]) * (p - len[2]))
        Int = gaussquadtriangle(3, xthis, ythis, integratingfunc, area)
        index = 0
        for q in ICAelement:                                        #loop through local numbering system
            Force[q] = Force[q]+Int[index]                          #put integral into correct place in glcbal force vector
            index = index+1
    return Force

################################################Plotting

x = [0, 4, 8, 8, 4, 0, 4]
y = [-1, 1, -1, 3, 5, 3, -3]

ICA = [[0,1,5], [1,4,5], [1,3,4], [1,2,3], [6,2,1], [0,6,1]]

xplots = zeros([6,4])
yplots = zeros([6,4])
centroid = zeros([6,2])
area = zeros(6)

for i in range(6):
    ICAelement = (ICA[i][:])
    xthis = []
    ythis = []
    for q in ICAelement:
        xthis.append(x[q])
        ythis.append(y[q])
    xcentroid = sum(xthis)/3
    ycentroid = sum(ythis)/3
    centroid[i,0] = xcentroid
    centroid[i,1] = ycentroid
    plt.plot(xcentroid, ycentroid, 'r*')
    xthis.append(x[int(ICAelement[0])])
    ythis.append(y[int(ICAelement[0])])
    xplots[i,:] = xthis
    yplots[i,:] = ythis
    plt.plot(xplots[i,:], yplots[i,:])
# plt.show()

GlobalF = GlobalForce(x, y, ICA, gaussquadtriangle, S3)              #calculate global force vector, changing final entry to choose which function
print(GlobalF)

#savetxt("file2.csv", column_stack([GlobalForce(x, y, ICA, gaussquadtriangle, S1), GlobalForce(x, y, ICA, gaussquadtriangle, S2),GlobalForce(x, y, ICA, gaussquadtriangle, S3)]), fmt= '%1.4g', delimiter=",")
