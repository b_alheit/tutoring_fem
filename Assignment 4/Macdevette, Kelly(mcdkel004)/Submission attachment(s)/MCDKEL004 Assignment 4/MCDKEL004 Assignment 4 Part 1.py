import matplotlib.pyplot as plt
from numpy import *

def Niso(xi, eta):                                  #isoparametric shape functions
    N = zeros([3,1])
    N[0,0] = xi
    N[1,0] = eta
    N[2,0] = 1-xi-eta
    return N.T

def S1(x,y):
    return 6

def S2(x,y):
    return 1+x+y


def S3(x,y):
    return (x-4)**2+(y-1)**2


x = [0, 4, 8, 8, 4, 0, 4]                               #nodal values
y = [-1, 1, -1, 3, 5, 3, -3]

ICA = [[0,1,5], [1,4,5], [1,3,4], [1,2,3], [6,2,1], [0,6,1]]    #interconnectivity array
xplots = zeros([6,4])                                           #arrays for plotting
yplots = zeros([6,4])
centroid = zeros([6,2])
area = zeros(6)                                                 #array for area

for i in range(6):                                              #loop through elements
    ICAelement = (ICA[i][:])                                    #extract ICA for each element
    xthis = []
    ythis = []
    for q in ICAelement:                                        #create arrays for local nodal values
        xthis.append(x[q])
        ythis.append(y[q])
    xcentroid = sum(xthis)/3                                    #calculate centroids
    ycentroid = sum(ythis)/3
    centroid[i,0] = xcentroid
    centroid[i,1] = ycentroid
    len = zeros(3)                                              #array of line lengths
    for k in range(3):                                          #calculate lengths of lines
        if k==0 or k==1:
            len[k] = sqrt((xthis[k]-xthis[k+1])**2+(ythis[k]-ythis[k+1])**2)
        if k==2:
            len[k] = sqrt((xthis[2] - xthis[0])**2 + (ythis[2] - ythis[0])**2)
    p = (len[0]+len[1]+len[2])/2
    area[i] = sqrt(p*(p-len[0])*(p-len[1])*(p-len[2]))          #calculate area of element using Heron's formula
    plt.plot(xcentroid, ycentroid, 'r*')                        #plot centroids
    xthis.append(x[int(ICAelement[0])])
    ythis.append(y[int(ICAelement[0])])
    xplots[i,:] = xthis
    yplots[i,:] = ythis
    plt.plot(xplots[i,:], yplots[i,:])                          #plot elements

# plt.show()


def gaussquadtriangle(ngp, xel, yel, func, elArea):             #gauss quadrature for triangles
    if ngp ==2:
        return 'Triangle Gauss Quadrature cannot use 2 gauss points'

    xi1 = [1/3]
    xi3 = [1/6, 2/3, 1/6]
    xi4 = [1/3, 2/10, 6/10, 2/10]
    XI = [xi1, 0, xi3, xi4]                            #create matrix of known xi values

    eta1 = [1/3]
    eta3 = [1/6, 1/6, 2/3]
    eta4 = [1/3, 2/10, 2/10, 6/10]
    Eta = [eta1, 0, eta3, eta4]                         #create matrix of known eta values

    w1 = [1]
    w3 = [1/3, 1/3, 1/3]
    w4 = [-9/16, 25/48, 25/48, 25/48]
    W = [w1, 0, w3, w4]                               #create matrix of corresponding weights

    I = 0                                          #initialise sum

    for i in range(ngp):                           #sum over Gauss points
        xi = (XI[int(ngp-1)][i])
        eta = (Eta[int(ngp-1)][i])
        w = (W[int(ngp-1)][i])
        xiso = dot(Niso(xi, eta), xel)
        yiso = dot(Niso(xi, eta), yel)
        intfunc = func(xiso,yiso)
        I = I + w*intfunc                        #calculate "approximate integral value" at Gauss point and sum over

    I = elArea*I                                 #multiply by area to get final integral value
    return I

numel = 6                                       #number of elements
numfunc = 3                                     #number of functions
Functions = array([S1, S2, S3])                 #array of functions
Integral = zeros([numfunc, numel])

for n in range(numfunc):                        #loop through functions
    integratingfunc = Functions[n]              #choose function to integrate
    Intforfunc = zeros(numel)
    for i in range(6):                          #loop through elements
        ICAelement = (ICA[i][:])
        xthis = []
        ythis = []
        for q in ICAelement:
            xthis.append(x[q])
            ythis.append(y[q])
        Int = gaussquadtriangle(3, xthis, ythis, integratingfunc, area[i])
        Intforfunc[i] = Int
    Integral[n,:] = Intforfunc                  #store value of integral on each element in total matrix

Printegral = Integral.T                         #invert to get right format
savetxt("file1.csv", column_stack([Printegral[:,0], Printegral[:,1], Printegral[:,2]]), fmt= '%1.4g', delimiter=",") #saves table of integrals
print(Printegral)                                #print table of integrals with columns corresponding to each function and rows corresponding to elements