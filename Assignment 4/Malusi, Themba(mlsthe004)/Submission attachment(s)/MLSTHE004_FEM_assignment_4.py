#!/usr/bin/env python
# coding: utf-8

# # AUTHOR: THEMBA MALUSI:
# # MEC5063Z: ASSIGNMENT

# In[1]:


import numpy as np
import matplotlib.pyplot as plt
import matplotlib.tri as tri


# In[2]:


nodes = np.array([[0,-1],[4,1],[4,-3], [8,-1], [8,3], [4,5], [0,3]]) # defining the nodes
# the x and y values as vector
x1 = nodes[:,0]
y1 = nodes[:,1]
## the interconnectivity array
ICA = np.asarray([[0, 1, 2],[1,2,3],[3,4,1],[1,4,5],[1,5,6],[0,1,6]])
# defining the nodes
nodes = np.array([[0,-1],[4,1],[4,-3], [8,-1], [8,3], [4,5], [0,3]]) # defining the nodes

# the x and y values as vector
x1 = nodes[:,0]
y1 = nodes[:,1]

## the interconnectivity array
ICA = np.asarray([[0, 1, 2],[1,2,3],[3,4,1],[1,4,5],[1,5,6],[0,1,6]])

# defining the matrix for each element

# ELEMENTS
M1 = np.array([[1, 1, 1], [0,0,4], [3,-1,1]])  # element 1
M2 = np.array([[1, 1, 1], [4,0,4], [5,3,1]])  # element 2
M3 = np.array([[1, 1, 1], [8,4,4], [3,5,1]])  # element 3
M4 = np.array([[1, 1, 1], [8, 8, 4], [-1, 3, 1]])  # element 4
M5 = np.array([[1, 1, 1], [4, 8, 4], [-3, -1, 1]])  # element 5
M6 = np.array([[1, 1, 1],  [0, 4, 4], [-1, -3, 1]])  # element 6

## Defining the x-values array and y-values array of each element
X1 = M1[1] # first element [0,0,4], [3,-1,1]
Y1 = M1[2]

X2 = M2[1]  # second element [4,0,4], [5,3,1]
Y2 = M2[2]

X3 = M3[1]  # third element [8,4,4], [3,5,1]
Y3 = M3[2]

X4 = M4[1]  # fourth element [8, 8, 4], [-1, 3, 1]
Y4 = M4[2]

X5 = M5[1] # fifth element [4, 8, 4], [-3, -1, 1]
Y5 = M5[2]

X6 = M6[1]  # sixth element [0, 4, 4], [-1, -3, 1]
Y6 = M6[2]


# In[ ]:





# In[3]:


# area 
def area(D):
    return round(1/2*abs(np.linalg.det(D)),2)
## centroid 
def centroid(D):
    return (round(sum(D[1])/len(D[1]),2), round(sum(D[2])/len(D[2]),2)), round(sum(D[1])/len(D[1]),2),round(sum(D[2])/len(D[2]),2)
#=====================================================================================================================


# In[4]:


Big_M=np.array([M1,M2,M3,M4,M5,M6])
xcentr = np.asarray(np.zeros(len(Big_M)), object)
ycentr = np.asarray(np.zeros(len(Big_M)), object)
for k in range(len(Big_M)):
    xcentr[k]= centroid(Big_M[k])[1]
    ycentr[k]= centroid(Big_M[k])[2]
    print(' element{}\t centroid {}\t area {}'.format(k+1, centroid(Big_M[k])[0], area(Big_M[k])))

#Plotting the mesh and the centroid for each element

## joining the nodes and plotting the results
triang = tri.Triangulation(x1, y1, ICA)

plt.figure()
plt.gca().set_aspect('equal')
plt.triplot(triang, 'bo-', lw=1.0)
plt.scatter(xcentr, ycentr)
plt.title('Triangular mesh grid')
plt.xlabel('x')
plt.ylabel('y')
plt.grid()
plt.show()


#print(' element{}\t centroid {}  \t area {}'.format( k+1, centroid(Big_M[k]), abs(area(Big_M[k]))))


# Function TriGaussPoint.py provides the Gauss Points and weights for the Guass quadrature of order n for the standard triangles.
# 
# Output: xw - a n by 3 matrix:
# 
#         1st column gives the x-coordinates of points
#         2nd column gives the y-coordinates of points
#         3rd column gives the weights

# In[5]:


def TriGaussPoints(n):
    xw = np.zeros([n, 3])
    if (n==1):
        xw = np.array([1/3, 1/3, 1.0])
    elif (n==2):
        xw=np.array([[1/6, 1/6, 1/3],
                     [1/6, 2/3, 1/3], 
                     [2/3, 1/6, 1/3]]);
    elif (n==3):
        xw=np.array([[1/3,1/3,-9/16],
                    [1/5,1/5, 25/48],
                    [1/5, 3/5, 25/48],
                    [3/5, 1/5, 25/48]]);    
    return xw


# Now we want to convert the cartesian coordinate to the isoparametric space using the shape functions over the isoparametric space. The shape functions in the isoparametric in the isoparametric space (s,t) are given by:
# \begin{align}
#     N_1 (s,t)&= 1-s-t\\
#     N_2 (s,t)&= s\\
#     N_3 (s,t)&=t
# \end{align}
# 
# Thefore x and y are given by:
# \begin{align}
#    x = \tilde{x}\cdot \mathbf{N}&= \sum^{3}_{i=1} {x_{i}}{N_{i}(s,t)}\\
#    y = \tilde{y}\cdot \mathbf{N}&= \sum^{3}_{i=1} {y_{i}}{N_{i}(s,t)}\\
# \end{align}
# 
# where $\tilde{x}=[x_1,x_2,x_3]^{T},\quad \tilde{y}=[y_1,y_2,y_3]^{T}, \quad \text{and} \quad \mathbf{N} = [N_1, N_2, N_3]$

# In[6]:


def isopar(s,t,xc,yc):
    N = np.array([1-s-t,s,t])
    x = np.dot(xc,N, out = None)
    y = np.dot(yc,N, out = None)
    return x,  y , N


# Now we want to compute the integral using Gauss quadrature with $N_{gp}$ point and the integral over the isoparematric space is given by:
# $\int \int_K f(x,y){dx}{dy} = {A^{e}}{\sum^{Ngp}_{j=1} w_{j}{f(s_j,t_j)} }$
# 
# and we also compute the force vector on each node:
# $\int \int_K {\mathbf{N}^T}{f(x,y)}{dx}{dy} = {A^{e}}{\sum^{Ngp}_{j=1} w_{j} {\mathbf{N}^T(s_j,t_j)}{f(s_j,t_j)} }$

# In[7]:


def quadrature(n, xc, yc, F):
    # calculate the area of the triangle
    A=abs(xc[0]*(yc[1]-yc[2])+xc[1]*(yc[2]-yc[0])+xc[2]*(yc[0]-yc[1]))/2.0
    if n ==1:
        w = TriGaussPoints(n)
        x1 = isopar(w[0], w[1],xc,yc)[0] # Converting the cartesian coordinates to isoparametric coordinates 
        y1 = isopar(w[0], w[1],xc,yc)[1] # Converting the cartesian coordinates to isoparametric coordinates
        node = isopar(w[0], w[1],xc,yc)[2]
        z  = A*w[2]*F(x1, y1)
        z1 = A*w[2]*F(x1, y1)*node
        
    else:
        w = TriGaussPoints(n)
        x1 = isopar(w[:,0], w[:,1],xc,yc)[0]
        y1 = isopar(w[:,0], w[:,1],xc,yc)[1]
        node = isopar(w[:, 0], w[:, 1], xc, yc)[2]
        z  = A*np.dot(F(x1,y1), w[:,2], out = None)
        
        f = node * w[:, 2]
        #print('f',f)
        z1 = A * np.dot(f, F(x1, y1), out=None)
        
      
    return z, z1


# In[8]:


def S(x,y):
    S1 = 6*x**0*y**0
    return S1
def S1(x,y):
    S2 = 1+ x+ y
    return S2
def S2(x,y):
    S3 = (x-4)**2 + (y-1)**2
    return S3


# In[9]:


print('VALUE OF THE INTEGRAL ON EACH ELEMENTS FOR DIFFERENT NUMBER POINTS ON THE ELEMENTS')
print('-----------------------------------------------------------------------------------')
print('                                                                                           ')
print('                                                                                           ')
print('                                                                                           ')

n= 1
Z1 = quadrature(n, X1, Y1, S)[0]
Z2 = quadrature(n, X2, Y2, S)[0]
Z3 = quadrature(n, X3, Y3, S)[0]
Z4 = quadrature(n, X4, Y4, S)[0]
Z5 = quadrature(n, X5, Y5, S)[0]
Z6 = quadrature(n, X6, Y6, S)[0]

z = np.array([Z1, Z2, Z3, Z4, Z5, Z6])
Nel = 6
for k in range(Nel):
    print('value of the integral at element {} \t S(x,y)=6:  {}'.format(k+1, z[k]))
print('                                                                           ')
print('                                                                           ')
print('                                                                           ')

n= 2
Z1 = quadrature(n, X1, Y1, S1)[0]
Z2 = quadrature(n, X2, Y2, S1)[0]
Z3 = quadrature(n, X3, Y3, S1)[0]
Z4 = quadrature(n, X4, Y4, S1)[0]
Z5 = quadrature(n, X5, Y5, S1)[0]
Z6 = quadrature(n, X6, Y6, S1)[0]


z = np.array([Z1, Z2, Z3, Z4, Z5, Z6])   ##  for each element
Nel = 6
for k in range(Nel):
    print('value of the integral at element {} \t S(x,y)=1+x+y:  {}'.format(k+1, round(z[k],2)))

print('                                                                                 ')
print('                                                                                 ')
print('                                                                                 ')

n =3
Z1 = quadrature(n, X1, Y1, S2)[0]
Z2 = quadrature(n, X2, Y2, S2)[0]
Z3 = quadrature(n, X3, Y3, S2)[0]
Z4 = quadrature(n, X4, Y4, S2)[0]
Z5 = quadrature(n, X5, Y5, S2)[0]
Z6 = quadrature(n, X6, Y6, S2)[0]

z = np.array([Z1, Z2, Z3, Z4, Z5, Z6])
Nel = 6
for k in range(Nel):
    print('value of the integral at element {} \t S(x,y)=(x-4)**2 + (y-1)**2:  {}'.format(k+1, round(z[k],2)))


print('                                                                                 ')
print('                                                                                 ')
print('                                                                                 ')
print('                                                                                 ')

print('----------------------------------------------------------------------------------')
print('----------------------------------------------------------------------------------')
print('                             FORCE VECTOR ON EACH ELEMENT')
print('----------------------------------------------------------------------------------')
print('                                                                                 ')
print('                                                                                 ')

# printing the integral on the elements for different values for n



n= 1
Z1 = quadrature(n, X1, Y1, S)[1]
Z2 = quadrature(n, X2, Y2, S)[1]
Z3 = quadrature(n, X3, Y3, S)[1]
Z4 = quadrature(n, X4, Y4, S)[1]
Z5 = quadrature(n, X5, Y5, S)[1]
Z6 = quadrature(n, X6, Y6, S)[1]


# Asssemblying the force vector.
F = np.array([[Z1[1] + Z6[0]],
            [Z1[2] + Z2[2] + Z3[2] + Z4[2] + Z5[2] + Z6[2]],
            [Z4[0] + Z5[1]],
            [Z3[0] + Z4[1]],
            [Z2[0] + Z3[1]],
            [Z1[0] + Z2[1]],
            [Z5[0] + Z6[1]]])


print('THE GLOBAL FORCE OF S(x,y)=6 on each node','\n','\n', F)
print('                                                                                 ')
print('                                                                                 ')


#z11 = np.array([Z1, Z2, Z3, Z4, Z5, Z6])
#print(z11)
#print('                                     ')
#Nel = 6
#for k in range(Nel):
#    print('value of the integral at element {} \t S(x,y)=6:  {}'.format(k+1, z11[k]))
    


n= 2
Z1 = quadrature(n, X1, Y1, S1)[1]
Z2 = quadrature(n, X2, Y2, S1)[1]
Z3 = quadrature(n, X3, Y3, S1)[1]
Z4 = quadrature(n, X4, Y4, S1)[1]
Z5 = quadrature(n, X5, Y5, S1)[1]
Z6 = quadrature(n, X6, Y6, S1)[1]

# Assemblying the force vector on each element
F = np.array([[Z1[1] + Z6[0]],
            [Z1[2] + Z2[2] + Z3[2] + Z4[2] + Z5[2] + Z6[2]],
            [Z4[0] + Z5[1]],
            [Z3[0] + Z4[1]],
            [Z2[0] + Z3[1]],
            [Z1[0] + Z2[1]],
            [Z5[0] + Z6[1]]])


print('THE GLOBAL FORCE OF S(x,y)= 1+x+y on each node','\n','\n', F)
print('                                                                                 ')
print('                                                                                 ')
print('                                                                                 ')

#z22 = np.array([Z1, Z2, Z3, Z4, Z5, Z6])   ##  for each element
#Nel = 6
#for k in range(Nel):
#    print('value of the integral at element {} \t S(x,y)=1+x+y:  {}'.format(k+1, z22[k]))

#print('                                                                                 ')
#print('                                                                                 ')
#print('                                                                                 ')


n= 3
Z1 = quadrature(n, X1, Y1, S2)[1]  # the value of the force on each elements 
Z2 = quadrature(n, X2, Y2, S2)[1]
Z3 = quadrature(n, X3, Y3, S2)[1]
Z4 = quadrature(n, X4, Y4, S2)[1]
Z5 = quadrature(n, X5, Y5, S2)[1]
Z6 = quadrature(n, X6, Y6, S2)[1]

# assemblying the global force vector. 
F = np.array([[Z1[1] + Z6[0]],
                        [Z1[2] + Z2[2] + Z3[2] + Z4[2] + Z5[2] + Z6[2]],
                        [Z4[0] + Z5[1]],
                        [Z3[0] + Z4[1]],
                        [Z2[0] + Z3[1]],
                        [Z1[0] + Z2[1]],
                        [Z5[0] + Z6[1]]])


print('THE GLOBAL FORCE OF S(x,y)=(x-1)**2 + (y-1)**2 on each node','\n','\n', F)
print('                                                                                 ')
print('                                                                                 ')
print('                                                                                 ')



#z33 = np.array([Z1, Z2, Z3, Z4, Z5, Z6])
#Nel = 6
#for k in range(Nel):
#    print('value of the integral at element {} \t S(x,y)=(x-4)**2 +(y-1)**2:  {}'.format(k+1, z33[k]))


# In[ ]:




