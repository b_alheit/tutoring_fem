import matplotlib.pyplot as plt
import numpy as np

def NodeValueLocator(desiredElementNumber):
    NodalValues = [[0, -1], [4, 1], [8, -1], [8, 3], [4, 5], [0, 3], [4, -3]]
    # ICA has element number and node numbers, element number forms first part of each elemenent, nodes are within second element
    ICA = [[1, [1, 2, 6]], [2, [6, 2, 5]], [3, [2, 4, 5]], [4, [2, 3, 4]], [5, [7, 3, 2]], [6, [1, 7, 2]]]
    a = ICA[desiredElementNumber-1]
    b = a[1]
    c = []
    for i in range(3):
        c.append(NodalValues[b[i]-1])
    return c

########3 GQ needs an x and a y vector
def GaussQuadrature(ngp,x,y,area,inputFunction):
    I = 0                                       #setting initial value of integral
    #building up a list of xi values
    xi1 = [1/3]
    eta1 = [1/3]
    xi2 = [1 / 3]
    eta2 = [1 / 3]
    xi3 = [1/6,2/3,1/6]
    eta3 = [1/6,1/6,2/3]
    xi4 = [1/3,2/10,6/10,2/10]
    eta4 = [1/3,2/10,2/10,6/10]
    xiVec = [xi1, xi2, xi3,xi4]
    etaVec = [eta1,eta2,eta3,eta4]
    #building up a list of weight values
    w1 = [1]
    w2 = [1, 1]
    w3 = [1/3, 1/3, 1/3]
    w4 = [-9/16,25/48,25/48,25/48]
    weightVec = [w1, w2, w3,w4]

    J = area                       #calculating the Jacobian

    for i in range(ngp):
        xi = (xiVec[int(ngp-1)][i])             #produce correct xi at relevant point, python is grumpy with multiplying multidimensional list elements
        eta = (etaVec[int(ngp-1)][i])
        w = (weightVec[int(ngp-1)][i])
        relevantX = x[0]*xi+x[1]*eta+x[2]*(1-eta-xi)
        relevantY = y[0]*xi+y[1]*eta+y[2]*(1-eta-xi)
        I = w*J*inputFunction(relevantX,relevantY) + I                 #where the terms containing y are the function to be integrated
    return I
#############
# Input Functions
def S1(x,y):
    # return 6
    return 1 + x + y
    # return (x-4)**2+(y-1)**2


# def S2(x,y):
#     return 1 + x + y
# def S3(x,y):
#     return (x-4)**2+(y-1)**2

x = np.zeros([4,1])
y = np.zeros([4,1])
NumElements = 6
for i in range(NumElements):
    sumx = 0
    sumy = 0
    diffx = 0
    a = NodeValueLocator(i+1)

    for j in range(3):
        x[j] = a[j][0]
        y[j] = a[j][1]
        sumx = sumx + x[j]
        sumy = sumy + y[j]

        if i % 2 ==0:
            yLength = abs(y[2]-y[0])
        else:
            yLength = abs(y[2]-y[1])

    xLength = abs(x[1] - x[0])
    area = (xLength * yLength) / 2
    Integral = GaussQuadrature(3, x, y, area, S1)   #input the desired function to integrate here
    print(Integral)

    x[3] = a[0][0]
    y[3] = a[0][1]
    xCentroid = sumx/3
    yCentroid = sumy/3
    plt.plot(xCentroid,yCentroid,'r*')
    plt.plot(x,y)

plt.show()


