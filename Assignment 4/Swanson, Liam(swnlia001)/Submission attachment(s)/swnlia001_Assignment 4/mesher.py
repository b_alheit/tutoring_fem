import numpy as np
import matplotlib.pyplot as plt


def triMesh(nodeList, ICA, fig_axis):
    # Initialise global array of element centroid and area
    centroids = []
    areas = []

    # Loop through each element
    for ICA_el in ICA:
        # Initialise element centroid values
        centroid_el = []
        area_el = []
        x_sum = 0
        y_sum = 0

        # Calculate element centroids
        for j in ICA_el:
            x_sum += nodeList[j, 0]
            y_sum += nodeList[j, 1]

        x_cent = x_sum / 3
        y_cent = y_sum / 3

        # Create element centroid co-ordinate vector
        centroid_el.append(x_cent)
        centroid_el.append(y_cent)

        # Add element centroid co-ordinate vector to global array
        centroids.append(centroid_el)

        # Calculate area of element
        a = abs((nodeList[ICA_el[0], 0] * (nodeList[ICA_el[1], 1] - nodeList[ICA_el[2], 1]) +
                 nodeList[ICA_el[1], 0] * (nodeList[ICA_el[2], 1] - nodeList[ICA_el[0], 1]) +
                 nodeList[ICA_el[2], 0] * (nodeList[ICA_el[0], 1] - nodeList[ICA_el[1], 1])) / 2)

        # Append area to global
        areas.append(a)

        # Plot elements and their centroids
        fig_axis.plot([nodeList[ICA_el[0], 0], nodeList[ICA_el[1], 0]], [nodeList[ICA_el[0], 1], nodeList[ICA_el[1], 1]],
                      color='black', marker='o')
        fig_axis.plot([nodeList[ICA_el[1], 0], nodeList[ICA_el[2], 0]], [nodeList[ICA_el[1], 1], nodeList[ICA_el[2], 1]],
                      color='black', marker='o')
        fig_axis.plot([nodeList[ICA_el[0], 0], nodeList[ICA_el[2], 0]], [nodeList[ICA_el[0], 1], nodeList[ICA_el[2], 1]],
                      color='black', marker='o')
        fig_axis.plot(x_cent, y_cent, color='red', marker='*')

    return areas, fig_axis
