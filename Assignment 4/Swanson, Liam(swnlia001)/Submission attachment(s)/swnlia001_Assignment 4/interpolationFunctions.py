# MEC5063Z ASSIGNMENT 3 Supporting Functions
# LIAM SWANSON
# SWNLIA001
# 3 APRIL 2019

import numpy as np


def shapeFunctionCart(nodeCoord_el, position):
    x1 = nodeCoord_el[0, 0]
    x2 = nodeCoord_el[1, 0]
    x3 = nodeCoord_el[2, 0]
    y1 = nodeCoord_el[0, 1]
    y2 = nodeCoord_el[1, 1]
    y3 = nodeCoord_el[2, 1]

    # Create M inverse matrix
    area = abs((x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)) / 2)

    M = np.array([[1, x1, y1],
                  [1, x2, y2],
                  [1, x3, y3]])

    M_inv = np.linalg.inv(M)

    p_vec = [1, position[0], position[1]]

    N = np.matmul(p_vec, M_inv)
    # print('N:\n', N)

    return N


def shapeFunctionIso(chi, eta):
    return np.array([[chi, eta, 1 - chi - eta]])


def derivativeFunctionIso():
    return np.array([[1, 0, -1], [0, 1, -1]])

