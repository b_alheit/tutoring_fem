# MEC5063Z PROJECT 1
# LIAM SWANSON
# SWNLIA001
# 08 APRIL 2019

import numpy as np
from interpolationFunctions import shapeFunctionIso



def gaussQuadratureInfo(orderGQ):
    """
    Function which stores the Gauss Quadrature numerical integration. It also calculates the jacobian of an element

    :param orderGQ: order of numerical integration scheme
    :return: 1 x orderGQ array of integration points in the isoparametric space, the weights at each point
    """

    # Create weight and location values in arrays
    location = np.array([[[1 / 3, 1 / 3], [0, 0], [0, 0], [0, 0]],
                         [[0, 0], [0, 0], [0, 0], [0, 0]],
                         [[1 / 6, 1 / 6], [2 / 3, 1 / 6], [1 / 6, 2 / 3], [0, 0]],
                         [[1 / 3, 1 / 3], [2 / 10, 2 / 10], [6 / 10, 2 / 10], [2 / 10, 6 / 10]]])

    weights = np.array([[1, 0, 0, 0],
                        [0, 0, 0, 0],
                        [1 / 3, 1 / 3, 1 / 3, 0],
                        [-9 / 16, 25 / 48, 25 / 48, 25 / 48]])

    return location[orderGQ], weights[orderGQ]