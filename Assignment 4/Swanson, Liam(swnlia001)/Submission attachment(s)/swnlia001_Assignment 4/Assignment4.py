# MEC5063Z ASSIGNMENT 3
# LIAM SWANSON
# SWNLIA001
# 3 APRIL 2019

import numpy as np
import matplotlib.pyplot as plt
from mesher import triMesh
from gaussQuadrature import gaussQuadratureInfo
from interpolationFunctions import shapeFunctionIso, shapeFunctionCart

# Set FEM parameters
n_gp = 3

# create array of nodal positions
g_nodeCoord = np.array([[0, -1],
                        [4, 1],
                        [8, -1],
                        [8, 3],
                        [4, 5],
                        [0, 3],
                        [4, -3]])

# Create node coordinate component arrays
g_xnodeCoord = []
g_ynodeCoord = []

for i in g_nodeCoord:
    g_xnodeCoord.append(i[0])
    g_ynodeCoord.append(i[1])

g_xnodeCoord = np.asarray(g_xnodeCoord)
g_ynodeCoord = np.asarray(g_ynodeCoord)

# Create ICA
g_ICA = np.array([[5, 0, 1],
                  [0, 6, 1],
                  [6, 2, 1],
                  [2, 3, 1],
                  [3, 4, 1],
                  [4, 5, 1]])

# Create nod
nodeCoord_el = []
xCoord_el = []
yCoord_el = []

for j in g_ICA:
    nodeCoord_el.append([g_nodeCoord[j[0]], g_nodeCoord[j[1]], g_nodeCoord[j[2]]])
    xCoord_el.append([g_xnodeCoord[j[0]], g_xnodeCoord[j[1]], g_xnodeCoord[j[2]]])
    yCoord_el.append([g_ynodeCoord[j[0]], g_ynodeCoord[j[1]], g_ynodeCoord[j[2]]])

nodeCoord_el = np.asarray(nodeCoord_el)
xCoord_el = np.asarray(xCoord_el)
yCoord_el = np.asarray(yCoord_el)
# print(xCoord_el[0])
#
def source(x1, x2):
    return 6


# def source(x1, y1):
#     return 1 + x1 + y1

#
# def source(x1, y1):
#     return (x1 - 4) ** 2 + (y1 - 1) ** 2


# Plot element, centroid and point
fig1, ax1 = plt.subplots()

# Extract element centroids and areas
el_area, ax1 = triMesh(g_nodeCoord, g_ICA, ax1)
plt.show()


s_function = 1

integral = 0

F_s = []
F_Ns = []

for k in range(len(g_ICA)):
    gq_s = 0
    gq_Ns = 0

    for l in range(n_gp):
        iso_loc, iso_w = gaussQuadratureInfo(n_gp - 1)
        N_iso = shapeFunctionIso(iso_loc[l, 0], iso_loc[l, 1])
        x = np.dot(N_iso, xCoord_el[k])[0]
        y = np.dot(N_iso, yCoord_el[k])[0]
        N_cart = shapeFunctionCart(nodeCoord_el[k], np.array([x, y]))

        gq_s += el_area[k] * iso_w[l] * source(x, y)

        gq_Ns += N_cart * source(x, y) * el_area[k] * iso_w[l]

    F_s.append(gq_s)
    F_Ns.append(gq_Ns)

F_s = np.asarray(F_s)
F_Ns = np.asarray(F_Ns)
print(F_s)
# np.savetxt('GQ4s3F_s.csv', F_s, delimiter=',')

# Build global array
F_vector = np.zeros((len(g_nodeCoord), 1))

for m in range(len(F_Ns)):
    node_count = 0
    for n in g_ICA[m]:
        F_vector[n] += F_Ns[m, node_count]
        node_count += 1

print('F_NS\n', F_Ns)
print(F_vector)

# np.savetxt('GQ4s3F_Ns.csv', F_vector, delimiter=',')

