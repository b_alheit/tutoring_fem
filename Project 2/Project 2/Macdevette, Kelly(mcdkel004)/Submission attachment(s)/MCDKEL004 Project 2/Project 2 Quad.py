from numpy import *
import matplotlib.pyplot as plt

def Niso(xi, eta):
    N = zeros([1,4])
    N[0,0] = 0.25*(1-xi)*(1-eta)
    N[0,1] = 0.25*(1+xi)*(1-eta)
    N[0,2] = 0.25*(1+xi)*(1+eta)
    N[0,3] = 0.25*(1-xi)*(1+eta)
    return N

def Jacobianquad(xi, eta, xel, yel):                        #calculate Jacobian on element at specific xi and eta value
    GNmatrix = zeros([2,4])
    nodematrix = zeros([4,2])
    GNmatrix[0,0] = -0.25*(1-eta)
    GNmatrix[0,1] = -GNmatrix[0,0]
    GNmatrix[0,2] = 0.25*(1+eta)
    GNmatrix[0,3] = -GNmatrix[0,2]
    GNmatrix[1,0] = -0.25*(1-xi)
    GNmatrix[1,1] = -0.25*(1+xi)
    GNmatrix[1, 2] = -GNmatrix[1,1]
    GNmatrix[1,3] = -GNmatrix[1,0]
    nodematrix[:,0] = xel
    nodematrix[:,1] = yel
    J = matmul(GNmatrix, nodematrix)                             #calculate the Jacobian
    return J, GNmatrix


def gaussquadQuad(ngp, xel, yel, func, typeint):    #gauss quadrature for quads

    xi1 = [0]
    xi2 = [1/sqrt(3), -1/sqrt(3)]
    xi3 = [0.7745966692, -0.7745966692, 0]
    XI = [xi1, xi2, xi3]                            #create matrix of known xi values

    w1 = [2]
    w2 = [1, 1]
    w3 = [5/9, 5/9, 8/9]
    W = [w1, w2, w3]                               #create matrix of corresponding weights

    I = 0                                          #initialise sum

    if typeint==1:
        x = array([-1,1])
        J = (x[1] - x[0]) / 2

        for i in range(ngp):
            xi = (XI[int(ngp - 1)][i])
            w = (W[int(ngp - 1)][i])
            y = (x[1] + x[0]) / 2 + xi * (x[1] - x[0]) / 2
            intfunc = func(y, yel)
            I = I + w * J * intfunc
        return I

    elif typeint==2:
        for i in range(ngp):
            xi = (XI[int(ngp - 1)][i])
            w1 = (W[int(ngp - 1)][i])
            for q in range(ngp):
                eta = (XI[int(ngp - 1)][q])
                w2 = (W[int(ngp-1)][q])
                xiso = dot(Niso(xi, eta), xel)
                yiso = dot(Niso(xi, eta), yel)
                Jacobian, GN = Jacobianquad(xi,eta,xel,yel)
                Jdet = linalg.det(Jacobian)
                I = I + w1*w2*Jdet*func(xi, eta, xiso,yiso, Jacobian, GN)
        return I

def Sourcefunc(xi, eta, x,y, blank1, blank2):   #body force function
    return 0.5*((8-x)**2)*((4-y)**2)*Niso(xi,eta).T

def Bintegral(blank3, blank4, blank5, blank6, Jacobian, GN):    #function to integrate for K on each element
    Jinv = linalg.inv(Jacobian)
    B = matmul(Jinv, GN)
    return matmul(B.T, B)


def TempInterpolate(Tnodes, xi, eta):       #interpolates temp at xi eta point
    Tiso = dot(Niso(xi, eta), Tnodes)
    return Tiso

x = [0, 4, 8, 8, 7, 8-sqrt(3), 6, 4, 0, 0, 4]
y = [0, 0, 0, 2, 4-sqrt(3), 3, 4, 4, 4, 1, 3]

ICA = [[0, 1, 10,9], [9, 10, 7, 8], [1,2,3,4], [1,4,5,10], [10,5,6,7]]

numelements = 5
conductivity = 5
fluxels = array([0,1])
Fline = zeros([11,1])

def Lineint(eta, yel):              #line integral function
    N = Niso(-1, eta)
    yiso = dot(N, yel)
    ylen = abs(yel[0]-yel[3])
    return -3*ylen*0.5*yiso**2*N.T

for q in fluxels:
    ICAelement = (ICA[q][:])
    ythis = []
    for b in ICAelement:
        ythis.append(y[b])
    ind = 0
    Flinelement = gaussquadQuad(3, 0, ythis, Lineint, 1)    #calculate line integral for flux
    for t in ICAelement:
        Fline[t,0] = Fline[t,0]+ Flinelement[ind]
        ind=ind+1

Globalbodyforce = zeros([len(x),1])
GlobalK = zeros([len(x),len(x)])

for n in range(numelements):                            #loop through elements and determine K and Fbody on each
    ICAelement = (ICA[n][:])                            #choose right ICA
    xthis = []
    ythis = []
    for q in ICAelement:                                #populate local x and y nodes
        xthis.append(x[q])
        ythis.append(y[q])
    Kelement = conductivity*gaussquadQuad(3, xthis, ythis, Bintegral, 2)    #gauss quadrature to calculate K
    Felement = gaussquadQuad(3, xthis, ythis, Sourcefunc, 2)                #gauss quadrature to calculate body force
    index = 0
    for q in ICAelement:
        Globalbodyforce[q] = Globalbodyforce[q] + Felement[index]
        index = index + 1
    for p in range(4):
        for m in range(4):
            GlobalK[ICAelement[p], ICAelement[m]] = GlobalK[ICAelement[p], ICAelement[m]] + Kelement[p, m]

Forcetotal = -Globalbodyforce + Fline

partitionedK = -GlobalK[3:len(x),3:len(x)]                       #partition stiffness matrix
partitionedF = Forcetotal[3:len(x), :]                          #partition force vector
dPsolved = matmul(partitionedF.T, linalg.inv(partitionedK))      #invert partitioned K and multiply with partitioned F to get displacement node values
Tnodevalues = zeros([len(x),1])
Tnodevalues[3:len(x),0] = dPsolved                               #temp value at nodes

print(Tnodevalues)

for k in range(numelements):
    xisopara = array([-1,1,1,-1])
    yisopara = array([-1, -1, 1, 1])
    ICAelement = (ICA[k][:])
    xthis = []
    ythis = []
    Tnodes = []
    for q in ICAelement:
        xthis.append(x[q])
        ythis.append(y[q])
        Tnodes.append(Tnodevalues[q])

    numpoints = 200
    xiplotlin = linspace(-1, 1, numpoints)          #grid of xi and eta points for mesh across isoparametric element
    etaplotlin = linspace(-1, 1, numpoints)
    Tempelement = zeros([numpoints,numpoints])
    for f in range(numpoints):
        for g in range(numpoints):
            Tempelement[g,f] = TempInterpolate(Tnodes, xiplotlin[f], etaplotlin[g])     #interpolate temp at xi eta point

    xcoordmatrix = zeros([numpoints,numpoints])     #grid of x y points to map grid of xi eta
    ycoordmatrix = zeros([numpoints,numpoints])
    for j in range(numpoints):
        for i in range(numpoints):
            xcoordmatrix[j,i] = dot(Niso(xiplotlin[i], etaplotlin[j]),xthis)
            ycoordmatrix[j,i] = dot(Niso(xiplotlin[i], etaplotlin[j]),ythis)

    cp = plt.contourf(xcoordmatrix, ycoordmatrix, Tempelement, linspace(0,90,101), vmin=0, vmax = 90)   #contour plot of temp in x y space

    ############################################################################### Plotting element points, element boundaries, element numbers
    plt.plot(xthis, ythis, 'ko')
    centroid = zeros(2)
    centroid[0] = (min(xthis)+max(xthis))/2
    centroid[1] = (min(ythis)+max(ythis))/2
    plt.plot(centroid[0], centroid[1], 'k')
    plt.text(centroid[0], centroid[1], str(k + 1), color="black", fontsize=12)

    xthis.append(x[int(ICAelement[0])])
    ythis.append(y[int(ICAelement[0])])
    xplots = xthis
    yplots = ythis
    plt.plot(xplots, yplots, 'k')

plt.xlabel('x')
plt.ylabel('y')
plt.colorbar(cp, label='Temperature (C)')
plt.show()



###################################################################### Q2 Jacobian determinant
def Jacobian9quad(xi, eta, xel, yel):
    GNmatrix = zeros([2,9])
    nodematrix = zeros([9,2])
    GNmatrix[0,0] = 0.25*eta*(1-eta)*(1-2*xi)
    GNmatrix[1,0] = 0.25*xi*(1-xi)*(1-2*eta)
    GNmatrix[0,1] = -0.25*eta*(1-eta)*(1+2*xi)
    GNmatrix[1,1] = -0.25*xi*(1+xi)*(1-2*eta)
    GNmatrix[0,2] = 0.25*eta*(1+eta)*(1+2*xi)
    GNmatrix[1,2] = 0.25*xi*(1+xi)*(1+2*eta)
    GNmatrix[0, 3] = -0.25*eta*(1+eta)*(1-2*xi)
    GNmatrix[1,3] = -0.25*xi*(1-xi)*(1+2*eta)
    GNmatrix[0, 4] = 0.5*xi*eta*(1-eta)
    GNmatrix[1, 4] = -0.25*(1-xi)*(1+xi)*(1-2*eta)
    GNmatrix[0, 5] = 0.25*(1-eta)*(1+eta)*(1+2*xi)
    GNmatrix[1, 5] = -xi*eta*(1+xi)*0.5
    GNmatrix[0, 6] = -xi*eta*(1+eta)*0.5
    GNmatrix[1, 6] = 0.5*(1-xi)*(1+xi)*(1+2*eta)*0.5
    GNmatrix[0, 7] = -0.25*(1-eta)*(1+eta)*(1-2*xi)
    GNmatrix[1, 7] = xi*eta*(1-xi)*0.5
    GNmatrix[0, 8] =-1*xi*(1-eta)*(1+eta)
    GNmatrix[1, 8] = -1*eta*(1-xi)*(1+xi)
    nodematrix[:,0] = xel
    nodematrix[:,1] = yel
    J = matmul(GNmatrix, nodematrix)                             #calculate the Jacobian
    return J, GNmatrix

npoints = 100
x9quad = array([4, 7, 8-sqrt(3), 4, 11/2, 6.63, (8-sqrt(3)+4)/2, 4, 5.5])           #nodes for Q2 on element 4
y9quad = array([0, 4-sqrt(3), 3, 3, (4-sqrt(3))/2, (4-sqrt(3)+3)/2, 3,  1.5, 1.5])
xi = linspace(-1,1,npoints)
eta = linspace(-1,1,npoints)
Jacobian1 = zeros([npoints, npoints])
for ins in range(npoints):
    for qs in range(npoints):
        J, GN = Jacobian9quad(xi[ins], eta[qs], x9quad, y9quad)
        Jacobian1[qs, ins] = (linalg.det(J))

Xi, Eta = meshgrid(xi, eta)
cpJ = plt.contourf(Xi, Eta, Jacobian1, 100)             #contour plot for jacobian determinant
plt.colorbar(cpJ, label=r'det$(\mathbf{J})$')
plt.xlabel(r'$\xi$')
plt.ylabel(r'$\eta$')
plt.show()
