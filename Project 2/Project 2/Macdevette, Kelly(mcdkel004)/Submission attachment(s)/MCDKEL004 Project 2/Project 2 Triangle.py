from numpy import *
import matplotlib.pyplot as plt


def Niso(xi, eta):                                      #isoparametric shape functions
    N = zeros([1,3])
    N[0,0] = xi
    N[0,1] = eta
    N[0,2] = 1-xi-eta
    return N

def gaussquadtriangle(ngp, xel, yel, func, elArea, type):   #gauss quadrature in triangles
    if ngp ==2:
        return 'Triangle Gauss Quadrature cannot use 2 gauss points'

    xi1 = [1/3]
    xi3 = [1/6, 2/3, 1/6]
    xi4 = [1/3, 2/10, 6/10, 2/10]
    XI = [xi1, 0, xi3, xi4]                            #create matrix of known xi values

    eta1 = [1/3]
    eta3 = [1/6, 1/6, 2/3]
    eta4 = [1/3, 2/10, 2/10, 6/10]
    Eta = [eta1, 0, eta3, eta4]                         #create matrix of known eta values

    w1 = [1]
    w3 = [1/3, 1/3, 1/3]
    w4 = [-9/16, 25/48, 25/48, 25/48]
    W = [w1, 0, w3, w4]                               #create matrix of corresponding weights


    xi1lin = [0]
    xi2lin = [1 / sqrt(3), -1 / sqrt(3)]
    xi3lin = [0.7745966692, -0.7745966692, 0]
    XIlin = [xi1lin, xi2lin, xi3lin]                   # create matrix of  xi values for linear quadrature

    w1lin = [2]
    w2lin = [1, 1]
    w3lin = [5 / 9, 5 / 9, 8 / 9]
    Wlin = [w1lin, w2lin, w3lin]                        #weights for linear quadrature

    I = 0                                               #initialise sum

    if type==1:                                         #linear quadrature in isoparametric
        x = array([0, 1])                               #isoparametric integration bounds
        J = (x[1] - x[0]) / 2                           #calculate the Jacobian

        for i in range(ngp):
            xi = (XIlin[int(ngp - 1)][i])
            w = (Wlin[int(ngp - 1)][i])
            yiso = (x[1] + x[0]) / 2 + xi * (x[1] - x[0]) / 2
            intfunc = func(yiso, yel)
            I = I + w * J * intfunc
        return I

    elif type==2:                                       #triangle quadrature
        for i in range(ngp):
            xi = (XI[int(ngp-1)][i])
            eta = (Eta[int(ngp-1)][i])
            w = (W[int(ngp-1)][i])
            xiso = dot(Niso(xi, eta), xel)
            yiso = dot(Niso(xi, eta), yel)
            intfunc = func(xiso,yiso)*Niso(xi,eta).T    #multiply by shape functions for body integral
            I = I + w*intfunc

        I = elArea*I                                    #multiply by area for total integral
        return I

def ElementForce(xthis, ythis, gaussquadtriangle, integratingfunc):    #calculates body force on each element
    len = zeros(3)
    for k in range(3):                                          #Heron's formula for calculating area
        if k == 0 or k == 1:
            len[k] = sqrt((xthis[k] - xthis[k + 1]) ** 2 + (ythis[k] - ythis[k + 1]) ** 2)
        if k == 2:
            len[k] = sqrt((xthis[2] - xthis[0]) ** 2 + (ythis[2] - ythis[0]) ** 2)
    p = (len[0] + len[1] + len[2]) / 2
    area = sqrt(p * (p - len[0]) * (p - len[1]) * (p - len[2]))
    Force = gaussquadtriangle(3, xthis, ythis, integratingfunc, area,2)     #triangular quadrature
    return Force, area

def SourceFunc(x,y):                                #body force function
    return 0.5*((8-x)**2)*((4-y)**2)

x = [0, 4, 8, 8, 7, 8-sqrt(3), 6, 4, 0, 0, 4]   #node x values
y = [0, 0, 0, 2, 4-sqrt(3), 3, 4, 4, 4, 1, 3]   #node y values

ICA = [[0, 1, 9], [9, 1, 10], [9, 10, 8], [8, 10, 7], [1,2,4], [2,3,4], [1,4,5],[1,5,10], [10,5,7], [5,6,7]]

numelements = 10
conductivity = 5
fluxels = array([0,2])                          #elements on which to calculate line integral of flux
Fline = zeros([11,1])                           #vector for line integral

def Lineint(eta, yel):                          #function in isoparametric space for line integral
    if yel[0] ==0:
        N = Niso(eta,0)
    if yel[0]==1:
        N = Niso(eta,0)
    yiso = dot(N, yel)
    ylen = abs(yel[0]-yel[2])
    return -3*ylen*yiso**2*N.T

def TempInterpolate(xel, yel, Tnodes, x, y):        #interpolates temperature at point x y in element

    #map from (x,y) to (xi, eta) isoparametric space
    a = xel[0]-xel[2]
    b = xel[1]-xel[2]
    c  = xel[2]
    d = yel[0] - yel[2]
    f = yel[1] - yel[2]
    g = yel[2]

    eta = (a*y-a*g-d*x+d*c)/(-d*b+a*f)
    xi = (b*y-b*g-f*x+f*c)/(-a*f+b*d)


    etabound = -xi + 1                          #defines upper line of isoparametric triangle

    if xi>1 or eta>1 or xi<0 or eta<0 or eta>etabound:  #check whether point is outside isoparametric triangle mapped from original triangle
        return nan

    M = ones([3, 3])                     #construct M matrix for point (x,y)
    M[1, :] = xel
    M[2, :] = yel
    M = M.T

    p = ones(3)                          #construct p matrix for point (x,y)
    p[1] = x
    p[2] = y

    N = matmul(p, linalg.inv(M))      #solve for value of shape functions N at (x,y)
    T = dot(N, Tnodes)                #Temperature value at point (x,y) is dot product of shape values at (x,y) with known temperature values at nodes
    return T

def FluxInterpolate(xel, yel, Tnodes):  #interpolates flux (should have done it in previous function but things were breaking)

    M = ones([3, 3])
    M[1, :] = xel
    M[2, :] = yel
    M = M.T

    Bmat = zeros([2, 3])
    Bmat[0, 1] = 1
    Bmat[1, 2] = 1
    B = matmul(Bmat, linalg.inv(M))

    Flux = -conductivity*dot(B,Tnodes)  # flux value at point (x,y) is dot product of shape values at (x,y) with known temperature values at nodes
    return Flux

for q in fluxels:                           #doing line integral
    ICAelement = (ICA[q][:])
    ythis = []
    for b in ICAelement:
        ythis.append(y[b])
    ind = 0

    Flinelement = gaussquadtriangle(3, 0, ythis, Lineint, 0, 1)
    for t in ICAelement:
        Fline[t,0] = Fline[t,0]+ Flinelement[ind]
        ind=ind+1

Globalbodyforce = zeros([len(x),1])
GlobalK = zeros([len(x),len(x)])

gradN = zeros([2,3])                    #matrix for creating B, constant on each element
gradN[0,1] = 1
gradN[1,2] = 1
for n in range(numelements):
    ICAelement = (ICA[n][:])
    xthis = []
    ythis = []
    for q in ICAelement:                #populate x and y on each element
        xthis.append(x[q])
        ythis.append(y[q])
    M = ones([3,3])
    M[1,:] = xthis
    M[2,:] = ythis
    M = M.T
    B = matmul(gradN, linalg.inv(M))
    Fel, area = ElementForce(xthis, ythis, gaussquadtriangle, SourceFunc)   #get body force on each element
    Kelement = conductivity*matmul(B.T, B)*area                             #don't need gauss quad for K because B doesn't depend on x or y
    index = 0
    for q in ICAelement:
        Globalbodyforce[q] = Globalbodyforce[q] + Fel[index]                #put element body force into global body force
        index = index + 1
    for p in range(3):
        for m in range(3):
            GlobalK[ICAelement[p], ICAelement[m]] = GlobalK[ICAelement[p], ICAelement[m]]+ Kelement[p,m]       #put element K into global K

Forcetotal = -Globalbodyforce + Fline                           #total force is body force + boundary body force
partitionedK = -GlobalK[3:len(x),3:len(x)]                      #partition stiffness matrix
partitionedF = Forcetotal[3:len(x), :]                          #partition force vector
dPsolved = matmul(partitionedF.T, linalg.inv(partitionedK))     #invert partitioned K and multiply with partitioned F to get temperature node values

Tnodevalues = zeros([len(x),1])                                 #Temp node values
Tnodevalues[3:len(x),0] = dPsolved                              #input boundary temperature node value

mesh_plot = plt

##############################################################Interpolating Temp
for k in range(numelements):
    ICAelement = (ICA[k][:])
    xthis = []
    ythis = []
    Tnodes = []
    for q in ICAelement:
        xthis.append(x[q])
        ythis.append(y[q])
        Tnodes.append(Tnodevalues[q])

    xplot = zeros(2)
    yplot = zeros(2)

    if abs(xthis[0]-xthis[1])>abs(xthis[0]-xthis[2]):       #find right node values for a linspace across each element
        xplot[0] = xthis[0]
        xplot[1] = xthis[1]
    else:
        xplot[0] = xthis[0]
        xplot[1] = xthis[2]
    if abs(ythis[0] - ythis[1]) > abs(ythis[0] - ythis[2]):
        yplot[0] = ythis[0]
        yplot[1] = ythis[1]
    else:
        yplot[0] = ythis[0]
        yplot[1] = ythis[2]

    if abs(yplot[0]-yplot[1])<abs(ythis[1]-ythis[2]):
        yplot[0] = ythis[1]
        yplot[1]= ythis[2]

    xplotlin = linspace(xplot[0], xplot[1], 200)                    #make x and y for mesh over each element
    yplotlin = linspace(yplot[0], yplot[1], 200)
    Tempelement = zeros([200,200])
    for f in range(200):
        for g in range(200):
            Tempelement[g, f] = TempInterpolate(xthis, ythis, Tnodes, xplotlin[f], yplotlin[g])     #interpolate temp at each point in mesh

    xplots = zeros(4)
    yplots = zeros(4)
    cp = mesh_plot.contourf(xplotlin, yplotlin, Tempelement, linspace(0,90, 101))           #contour plot
    plt.plot(xthis, ythis, 'ko')                                                            #plot nodes
    centroid = zeros(2)
    centroid[0] = sum(xthis)/3                                                              #for labeling elements at centre
    centroid[1] = sum(ythis)/3
    plt.plot(centroid[0], centroid[1], 'k')
    plt.text(centroid[0], centroid[1], str(k+1), color="black", fontsize=12)                #label elements

    xthis.append(x[int(ICAelement[0])])
    ythis.append(y[int(ICAelement[0])])
    xplots = xthis
    yplots = ythis
    plt.plot(xplots, yplots, 'k')                                                           #plot lines enclosing elements

plt.xlabel('x')
plt.ylabel('y')
plt.colorbar(cp, label='Temperature (C)')
plt.show()


################################################################Temp and flux along AB
npoints = 100

ICAelement4 = (ICA[3][:])                           #AB is along element 4 and 10 so I just hard coded those in
xthis4 = []
ythis4 = []
Tnodes4 = []
for q in ICAelement4:
    xthis4.append(x[q])
    ythis4.append(y[q])
    Tnodes4.append(Tnodevalues[q])

ICAelement10 = (ICA[9][:])
xthis10 = []
ythis10 = []
Tnodes10 = []
for p in ICAelement10:
    xthis10.append(x[p])
    ythis10.append(y[p])
    Tnodes10.append(Tnodevalues[p])

xlinplot = linspace(0,6,npoints)                    #x linspace along AB
temp = zeros(npoints)
fluxx = zeros(npoints)
fluxy = zeros(npoints)
fluxpoints=[]
for g in range(npoints):
    if xlinplot[g]<4:
        temp[g] = TempInterpolate(xthis4, ythis4, Tnodes4, xlinplot[g], 4)              #interpolate along whole line
        fluxx[g] = FluxInterpolate(xthis4, ythis4, Tnodes4)[0]
        fluxy[g] = FluxInterpolate(xthis4, ythis4, Tnodes4)[1]
        if xlinplot[g]==0 or xlinplot[g]==4:                                            #on element 4
            plt.plot(xlinplot[g],temp[g], 'ko')
            print(temp[g])                            #also plots values at nodes
            fluxpoints.append(fluxx[g])                                                #store flux node values for plotting later
    elif xlinplot[g]==4 or xlinplot[g]>4:                                               #on element 10
        temp[g] = TempInterpolate(xthis10, ythis10, Tnodes10, xlinplot[g], 4)
        fluxx[g] = FluxInterpolate(xthis10, ythis10, Tnodes10)[0]
        if xlinplot[g] == 4 or xlinplot[g] == 6:
            plt.plot(xlinplot[g], temp[g], 'ko')
            print(temp[g])
            fluxpoints.append(fluxx[g])
plt.plot(xlinplot, temp, 'b-x', label='Temperature')                            #plot temp interpolated
plt.xlabel('x')
plt.ylabel('Temperature (C)')
plt.title('Interpolated temperature along AB boundary')
plt.show()
print(fluxx)
print(fluxy)
plt.plot(xlinplot, fluxx, 'r-.', label='Flux')                #plot flux interpolated
plt.plot(array([0,4,6]), fluxpoints, 'ko')                                      #plot node values for flux
plt.xlabel('x')
plt.ylabel('|Flux| (C/cm)')
plt.title('Interpolated flux along AB boundary')
plt.show()

print()
