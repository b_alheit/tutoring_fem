##| Daniel Pons | PNSDAN002 | MEC5063Z | Project 2: Quad FEM
import numpy as np
import matplotlib.pyplot as plt
import scipy.linalg as sp
import statistics as stat
import matplotlib.pyplot as plt
import random as rand

##  MESH DATA INPUT
#coordinate map for circular cut out
def circx(y_value):
    alpha = 8
    beta = 4
    r = 2
    return (2*alpha - np.sqrt((2*alpha)**2+4*(r**2-(y_value-beta)**2-alpha**2)))/2
    
def circy(x_value):
    alpha = 8
    beta = 4
    r = 2
    return (2*beta - np.sqrt((2*beta)**2+4*(r**2-(x_value-alpha)**2-beta**2)))/2    

#X&Y coordiniate array
x_nodal_array = np.array([0,4,0,4,0,4,circx(3),7,8,8,circx(4)]) #all x positions in order of numbering
y_nodal_array = np.array([0,0,1,3,4,4,3,circy(7),0,circy(8),4]) #all y posiitons in order of numbering

ICA = np.array([[1,2,4,3],
                [3,4,6,5],
                [2,9,10,8],
                [2,8,7,4],
                [4,7,11,6]])     #each internal array is for an element Using standard CCW numbering (global node numbers)

##Given Heat Source
def S(x,y):
    return 0.5*((8-x)**2)*(4-y)**2

                            
                            
## TRI CODE

##Given functions&constants
def S(x,y):
    return 0.5*((8-x)**2)*(4-y)**2 #given source function
   
# Boundary Force Calculation
#Only flux condition is along AE due to symmetry. Needs a line integral.
#q_bar is given
def q_bar(x,y): #valid only along AE
    return -3*(y**2)   



k_cond = 5 # Wcm^-1C^-1
#assumed isotropic material
D = np.array([[k_cond,0],
              [0,k_cond]])
##  Meshing

def MESH2D(ICA,x_nodal_array,y_nodal_array): #takes in an ICA, nodal arrays should do quad&tri
    ICA_0 = ICA-1   #0 indexed ICA for ease of use with position array
    X = np.zeros((len(ICA),len(ICA[0]))) #X&Y vectors to store all x-y points for the mesh&plotting
    Y = np.zeros((len(ICA),len(ICA[0])))
    
    for k in range(0,len(ICA)): #creates x&y points for each node to be plotted
        X[k] = x_nodal_array[ICA_0[k]]
        Y[k] = y_nodal_array[ICA_0[k]]
    #add node one again for plotting purposes
    
    X_plot = np.zeros((len(ICA),len(ICA[0])+1)) #additional point to close triangles
    Y_plot = np.zeros((len(ICA),len(ICA[0])+1))
    X_bar = np.zeros((len(ICA),1))
    Y_bar = np.zeros((len(ICA),1))
    for i in range(0,len(ICA)):
        X_plot[i] = np.append(X[i],X[i][0])
        Y_plot[i] = np.append(Y[i],Y[i][0])
        X_bar[i] = np.average(X[i]) #average x-pos
        Y_bar[i] = np.average(Y[i]) #av.y pos
    return [[X,Y],[X_plot,Y_plot],[X_bar,Y_bar]] #returns the x&y points for each, the plotted arrays and the centroids

mesh = MESH2D(ICA,x_nodal_array,y_nodal_array)
X = mesh[0][0] #nodal x points
Y = mesh[0][1] #nodal y

X_plot = mesh[1][0] #x points for plotting
Y_plot = mesh[1][1] #ypoints for plotting

X_bar = mesh[2][0] #average x-pos
Y_bar = mesh[2][1] #average y-pos


##PLOT MESH
fcolour = ['b','g','r','c','m','k','y','k--','b--','g--','r--','c--','m--','y--']    
plt.figure(0) #mesh
for i in range(0,len(ICA)):
    plt.plot(X_plot[i],Y_plot[i],fcolour[0]) #plots all elements
    plt.text(X_bar[i],Y_bar[i],str(i+1),
                               color=fcolour[5],
                               fontweight="bold") #plots element labels at the centroid of the circle
for j in range(len(x_nodal_array)):
    plt.text(x_nodal_array[j],y_nodal_array[j], str(j+1),
                        color=fcolour[5],
                        fontsize=6,
                        fontweight="bold",
                        bbox=dict(boxstyle='circle',pad=0.3,facecolor='white',alpha=1)) #plots a circled node number at each node
plt.xlabel('x position - (m)')
plt.ylabel("y position - (m)")
plt.title("Bi-Quad Mesh")

plt.tight_layout() 
plt.grid()
plt.show()


##Gauss2D

#Isoparametric Shape fn
def NT_ISO(xi,eta):
    N1 = 0.25*(1-eta)*(1-xi)
    N2 = 0.25*(1-eta)*(1+xi)   
    N3 = 0.25*(1+eta)*(1+xi)
    N4 = 0.25*(1+eta)*(1-xi)    
    NT = np.array([[N1],
                   [N2],
                   [N3],
                   [N4]])
    if (xi<-1 or 1<xi) or (eta<-1 or 1<eta):
        return NT*0
    else:
        return NT #linear interpolation scheme for BIQUAD


#Map xi,eta from Isoparametric space to cartesian
def ISO2C(xi,eta,X_nod,Y_nod,elnum): #takes xi,eta and returns xy position
    NT = NT_ISO(xi,eta)
    x = np.matmul(X_nod[elnum-1],NT) 
    y = np.matmul(Y_nod[elnum-1],NT)
    C_pos = np.array([x,y])
    return np.transpose(C_pos)[0]

#Derivation of shape function
def GN(xi,eta):
    #derivative wrt xi
    dN1dxi = -0.25*(1-eta)
    dN2dxi =  0.25*(1-eta)
    dN3dxi =  0.25*(1+eta)
    dN4dxi = -0.25*(1+eta)
    #deriv wrt eta
    dN1deta = -0.25*(1-xi)
    dN2deta = -0.25*(1+xi)
    dN3deta =  0.25*(1+xi)
    dN4deta =  0.25*(1-xi)
    GN_mat = np.array([[dN1dxi, dN2dxi, dN3dxi, dN4dxi ],
                       [dN1deta,dN2deta,dN3deta,dN4deta]])
    return GN_mat

                       
def Jacobian(xi,eta,X_nod,Y_nod,elnum):
    Xtrans = X_nod[elnum-1] #nodal values of x (note zero index adjustment)
    Ytrans = Y_nod[elnum-1] #as above for y
    xy_mat = np.transpose(np.array([Xtrans,Ytrans])) #Xy mat must be 4x2
    return np.matmul(GN(xi,eta),xy_mat) #2x4 J into 4X2 xy_mat gives a 2x2 Jacobian
    
def B_quad(xi,eta,X_nod,Y_nod,elnum):
    try:
        J_inv = np.linalg.inv(Jacobian(xi,eta,X_nod,Y_nod,elnum))
    except:
        print("Check node numbering, non invertible jacobian")
    B_mat = np.matmul(J_inv,GN(xi,eta))
    return B_mat

##
#Integration function
def int_quad(Ngp,function,X_nod,Y_nod,elnum): #function must be a previously defined function with an argument (x,y)
    #x_2_e & x_1_e are the upper and lower limits of integration for the element
    #gauss quad array GQA
    #global x1,x2,x3,y1,y2,y3
    #global M,J_det,qr_i,I_g
    elnum = elnum
    GQ_xi_i = np.array([[0],
                        [-1 / np.sqrt(3), 1 / np.sqrt(3)],
                        [-np.sqrt(3 / 5), 0, np.sqrt(3 / 5)],
                        [-0.86113631, -0.33998104, 0.33998104, 0.86113631],
                        [-0.9061798459, -0.5384693101, 0, 0.5384693101, 0.9061798459],
                        [-0.9324695142, -0.6612093865, -0.2386191861, 0.2386191861, 0.6612093865, 0.9324695142]]) #xi/eta pts 
    GQ_w_i = np.array([[2],
                       [1, 1],
                       [5/9, 8/9, 5/9],
                       [0.3478548451, 0.6521451549, 0.6521451549, 0.3478548451],
                       [0.2369268851, 0.4786286705, 0.5688888889, 0.4786286705, 0.2369268851],
                       [0.1713244924, 0.3607615730, 0.4679139346, 0.4679139346, 0.3607615730, 0.1713244924]]) #weighting points for each 
    qr = Ngp-1 #used to relate index to number of gauss pts
    # Integration
    I_g = 0
    for i in range(Ngp):
        for j in range(Ngp):
            xi  = GQ_xi_i[qr][i] #Gauss point xi
            eta = GQ_xi_i[qr][j] #Gauss point eta
            w_i = GQ_w_i[qr][i] #Gauss point xi
            w_j = GQ_w_i[qr][j] #Gauss point xi
            
            #Integral
            #print("xi:",xi,"\neta:",eta)
            I_g += function(xi,eta)*w_i*w_j*np.linalg.det(Jacobian(xi,eta,X_nod,Y_nod,elnum))
    Integral = I_g
    return Integral


#boundary line integration
def int_bndry(Ngp, function, x_1_e, x_2_e):  # function must be a previously defined function with an argument
    # x_2_e & x_1_e are the upper and lower limits of integration for the element
    # gauss quad array GQA
    GQA = [[0], [[0], [2]], [[-1 / np.sqrt(3), 1 / np.sqrt(3)], [1, 1]],
           [[-np.sqrt(3 / 5), 0, np.sqrt(3 / 5)], [5 / 9, 8 / 9, 5 / 9]],
           [[-0.86113631, -0.33998104, 0.33998104, 0.86113631],
            [0.3478548451, 0.6521451549, 0.6521451549, 0.3478548451]],
           [[-0.9061798459, -0.5384693101, 0, 0.5384693101, 0.9061798459],
            [0.2369268851, 0.4786286705, 0.5688888889, 0.4786286705, 0.2369268851]],
           [[-0.9324695142, -0.6612093865, -0.2386191861, 0.2386191861, 0.6612093865, 0.9324695142],
            [0.1713244924, 0.3607615730, 0.4679139346, 0.4679139346, 0.3607615730, 0.1713244924]]]
    # Use this to call all values, format is [[gauss points],[location]] eg:GQA[3][0][1]

    # Constants
    l_int = x_2_e - x_1_e  # length of integral domain
    dx_dxi = l_int / 2  # dx/dxi to transfer into xi coordinates
    # Integration
    I_g = 0
    for i in range(0, Ngp):
        xi = GQA[Ngp][0][i]  # Gauss point xi
        w_i = GQA[Ngp][1][i]  # weighting of the point
        #x = 0.5 * (x_1_e * (1 - xi) + x_2_e * (1 + xi))  # x as a function of xi (map
        # Integral
        I_g += function(0,xi) * w_i

    Integral = I_g * dx_dxi
    return Integral


##Matrices & Matrix operations for integration    

def NT_S(xi,eta): #For integration, X,Y and elnum must be defined in each loop
    xy = ISO2C(xi,eta,X,Y,elnum)
    x,y = xy
    return S(x,y)*NT_ISO(xi,eta)
 
def BT_D_B(xi,eta): # general definition of the K operand, NB elnum must be defined 
    BT = np.transpose(B_quad(xi,eta,X,Y,elnum))
    B = B_quad(xi,eta,X,Y,elnum)
    return np.matmul(np.matmul(BT,D),B)




#Boundary force
# Boundary Force Calculation
#Only flux condition is along AE due to symmetry. Needs a line integral.

def NT_q_bar_AE(xi,eta): #NB define elnum with each integral
    xy = ISO2C(xi,eta,X,Y,elnum)
    x,y = xy 
    #print("x:",x,"\ny:",y)   
    if ( x == 0 or 4<=y<8):
        return np.zeros((4,1))
    else:
        return NT_ISO(-1,eta)*q_bar(0,y) #N(xi eta), and q is fn of y only



##Local Integration
K_loc_list = [] #results list S
NT_S_list = [] #results list NT_S for each element
nelem = len(ICA)  
numnod = len(x_nodal_array)      
Ngp_k = 4 #Number of integration points 
Ngp_s = 4 #Higher order used for this one.Number of triangular integration points(1,3,4 possible)   
for k in range(nelem):
    elnum = k+1 #defining element number in the loop
    #BT_D_B
    K_loc_list.append(int_quad(Ngp_k,BT_D_B,X,Y,elnum)) #integration for local stiffness matrices
    #NT_S
    NT_S_list.append(int_quad(Ngp_s,NT_S,X,Y,elnum)) #integration for calculation of the nodal body force

NT_q_bar_array = np.zeros((nelem,4, 1)) #results array for boundary loading on each element
#nodes
bndry_ICA = np.array([[1,3],
                      [3,5]])
bndry_el = np.array([1,2])

for n in range(len(bndry_el)):
    elnum = bndry_el[n]
    zeroICA = bndry_ICA-1
    zeroel = bndry_el-1
    el_int = int_bndry(Ngp_s,NT_q_bar_AE,-1,1)
    #print(el_int)
    NT_q_bar_array[zeroel[n]]=el_int
#elemental loads:

#NT_q_bar_list.append(int_bndry(Ngp_s,NT_q_bar_AE,X[k],Y[k]))



## Assembly
#stiffness
def quad_semble_K(numnod,nelem,ICA,K_lin_loc): #number of nodes,#elements,interconnectivity array,local stiffness matrix
    K_lin = np.zeros((numnod, numnod))  # globak K matrix of appropriate size to ndof
    for i in range(nelem):
        g1,g2,g3,g4 = ICA[i]-1 # global indices,zero indexed  
        #Stiffness 
        #row1
        K_lin[g1][g1] += K_lin_loc[i][0][0]
        K_lin[g2][g1] += K_lin_loc[i][1][0]
        K_lin[g3][g1] += K_lin_loc[i][2][0]
        K_lin[g4][g1] += K_lin_loc[i][3][0]
        #row2
        K_lin[g1][g2] = K_lin[g2][g1]
        K_lin[g2][g2] += K_lin_loc[i][1][1]
        K_lin[g3][g2] += K_lin_loc[i][2][1]
        K_lin[g4][g2] += K_lin_loc[i][3][1]
        #row3
        K_lin[g1][g3] = K_lin[g3][g1]
        K_lin[g2][g3] = K_lin[g3][g2]
        K_lin[g3][g3] += K_lin_loc[i][2][2]
        K_lin[g4][g3] += K_lin_loc[i][3][2]
        #row4
        K_lin[g1][g4] = K_lin[g4][g1]
        K_lin[g2][g4] = K_lin[g4][g2]
        K_lin[g3][g4] = K_lin[g4][g3]
        K_lin[g4][g4] += K_lin_loc[i][3][3]
    return K_lin
#Force
def quad_semble_F(numnod,nelem,ICA,F_loc): #
    #K_lin = np.zeros((numnod, numnod))  # globak K matrix of appropriate size to ndof
    F_bod = np.zeros((numnod, 1))
    for i in range(nelem):
        g1,g2,g3,g4 = ICA[i]-1 # global indices,zero indexed
        # force
        F_bod[g1] += F_loc[i][0]
        F_bod[g2] += F_loc[i][1]
        F_bod[g3] += F_loc[i][2]
        F_bod[g4] += F_loc[i][3]
        
    return F_bod
shift = 0  #Used to check the assembly process, default is zero
F_bod = quad_semble_F(numnod,nelem-shift,ICA,NT_S_list) #global body force
K_glob = quad_semble_K(numnod,nelem-shift,ICA,K_loc_list) #global stiffness matrix
F_bndry = quad_semble_F(numnod,nelem-shift,ICA,NT_q_bar_array) #global body force

F = F_bod-F_bndry

##Partition: Move Derichler BCs around to break the problem up



# Derichler/Essential BCs
D_nodes = np.array([1,2,9])  # array of Derichler node number indices 1-indexed
D_disp = np.array([0,0,0])  # Derichler temperature associated with the D_node position above

def partition_solve(D_nodes,D_disp,K_glob,F_glob): #Takes derichler nodes, nodal values, global K&F
    numnod = len(F_glob)
    d_lin = np.zeros((numnod, 1))  # initialised displacement vector
    for i in range(len(D_nodes)):  # adds derichler BCs to the displacement vector
        pos = (D_nodes - 1)[i]
        d_lin[pos] = D_disp[i]
    # Rearrangement of derichler rows
    nodes = np.array(range(numnod))  # array of node numbers
    nod_r = np.delete(nodes, (D_nodes - 1))  # deletes all Derichler indices (reduced nodal array)
    new_order = np.concatenate(((D_nodes - 1), nod_r))  # All derichler nodes zero indexed added to the front of the array
    # Now index notation will help build a new array with the new_order
    K_glob_alt = np.zeros((numnod, numnod))
    F_glob_alt = np.zeros((numnod, 1))
    d_lin_alt = np.zeros((numnod, 1))
    
    
    # KALT =np.array([[K_glob[i][j] for j in new_order] for i in new_order])
    for i in range(numnod):
        row = new_order[i]  # maps new to old indexing
        F_glob_alt[i] = F_glob[row]  # Rearrangement of Force vector
        d_lin_alt[i] = d_lin[row]
        for j in range(numnod):
            column = new_order[j]
            K_glob_alt[i][j] = K_glob[row][column]  # Rearrangement of K matrix
    
    # break up of the arrays for solving:
    nn_cut = np.array(range(len(D_nodes)))  # natural BC slice index
    K_nn = np.delete(K_glob_alt, nn_cut, axis=0)
    K_nn = np.delete(K_nn, nn_cut, axis=1)
    # d_n = np.delete(d_lin_alt,nn_cut,axis = 0) #Displacement partitioning for force adjustment
    F_n = np.delete(F_glob_alt, nn_cut, axis=0)
    
    ne_cut = np.array(range(len(D_nodes), numnod))  # natural essential bc slice index
    K_ne = np.delete(K_glob_alt, nn_cut, axis=0)
    K_ne = np.delete(K_ne, ne_cut, axis=1)
    d_e = np.delete(d_lin_alt, ne_cut, axis=0)  # Displacement partitioning for force adjustment
    
    # invert K nn&ne
    K_nn_inv = np.linalg.inv(K_nn)
    # Solving for displacement:
    d_n = np.matmul(K_nn_inv, (F_n - np.matmul(K_ne, d_e)))
    
    d_lin_new = np.concatenate((d_e, d_n))
    
    # swap rows back to original positions
    u_glob = np.zeros((numnod, 1))
    for i in range(numnod):
        row = new_order[i]
        u_glob[row] = d_lin_new[i]  # maps old to new indexing
    return u_glob

T_global = partition_solve(D_nodes,D_disp,K_glob,F)
# Post processing
#Global shape function not needed for this project. just plot it
# displacement field:



##Contour plotting

#Contour plot related fns    
def N_ISO(xi,eta): #Takes in x&y pos, node arrays of the triangle nodes and returns Xi shape fn.
    return np.transpose(NT_ISO(xi,eta))
    
    
def intrplt_ISO(xi,eta,T1,T2,T3,T4): #takes position and nodal temps, note that it depends on all code above.
    T_i = np.array([T1,T2,T3,T4])
    N_xi = NT_ISO(xi,eta)
    XI =  np.array([[-1,1,1,-1]])
    ETA = np.array([[-1,-1,1,1]])
    
    T_x_y = np.matmul(T_i,N_xi)
    return T_x_y[0] #Returns Scalar interpolation
    
#plot
resolution = 50
slices = 15
X1 = np.linspace(-1,1,resolution)
grid = np.meshgrid(X1,X1)

xi_mesh = grid[0]
eta_mesh = grid[1]


#Generate temperature array
T_ICA = T_global[ICA-1]
T_mesh = np.zeros((nelem,resolution,resolution))
X_mesh = np.zeros((nelem,resolution,resolution))
Y_mesh = np.zeros((nelem,resolution,resolution))
Tmax = np.max(T_global)
for el in range(nelem):
    for row in range(resolution):
        for column in range(resolution):
            xi = xi_mesh[row][column]
            eta = eta_mesh[row][column]
            T1,T2,T3,T4 = np.transpose(T_ICA[el])[0]
            T_mesh[el][row][column] = intrplt_ISO(xi,eta,T1,T2,T3,T4) #assigns interpolated temp to each elemental mesh
            if T_mesh[el][row][column]>Tmax: #calibrates colour plot
                Tmax = T_mesh[el][row][column] #only re-assigns if there is a greater value.
            #send xi&eta back to x&y
            xy = ISO2C(xi,eta,X,Y,(el+1))
            X_mesh[el][row][column] = xy[0]
            Y_mesh[el][row][column] = xy[1]
## PLOT
plt.figure(1)
for el in range(nelem):
    
    cont = plt.contourf(X_mesh[el],Y_mesh[el],T_mesh[el],np.linspace(0,Tmax,slices),extend='both')
                
plt.colorbar().set_label("Temp (deg C)")
#MESH OVERLAY
for i in range(0,len(ICA)):
    plt.plot(X_plot[i],Y_plot[i],"k") #plots all elements
    plt.text(X_bar[i],Y_bar[i],str(i+1),
                               color=fcolour[5],
                               fontweight="bold") #plots element labels at the centroid of the circle
for j in range(len(x_nodal_array)):
    plt.text(x_nodal_array[j],y_nodal_array[j], str(j+1),
                        color=fcolour[5],
                        fontsize=6,
                        fontweight="bold",
                        bbox=dict(boxstyle='circle',pad=0.3,facecolor='white',alpha=1)) #plots a circled node number at each node
plt.xlabel('x position - (m)')
plt.ylabel("y position - (m)")
plt.title("Bi-Quad Temperature Mesh Overlay")
#plt.tight_layout() 
#plt.grid()          
plt.show()

#Q2c
#9node shape function
def N_Q2_1D(xieta):
    N1 = -0.5*xieta*(1-xieta)
    N2 = (1-xieta)*(1+xieta)
    N3 = 0.5*xieta*(1+xieta)
    return [N1,N2,N3]

def dN_Q2_1D(xieta):
    dN1 = -0.5*(1-xieta)+0.5*xieta
    dN2 = -(1+xieta)+(1-xieta)
    dN3 = 0.5*(1+xieta)+0.5*xieta
    return [dN1,dN2,dN3]
    
def dN_Q2(xi,eta): #returns derivative SF function for Q2
    N_xi = N_Q2_1D(xi)
    N_eta = N_Q2_1D(eta)
    
    dN_xi = dN_Q2_1D(xi)
    dN_eta = dN_Q2_1D(eta)
    #dxi
    dN1dxi = dN_xi[0]*N_eta[0]
    dN2dxi = dN_xi[2]*N_eta[0]
    dN3dxi = dN_xi[2]*N_eta[2]
    dN4dxi = dN_xi[0]*N_eta[1]
    dN5dxi = dN_xi[1]*N_eta[0]
    dN6dxi = dN_xi[2]*N_eta[1]
    dN7dxi = dN_xi[1]*N_eta[2]
    dN8dxi = dN_xi[1]*N_eta[0]
    dN9dxi = dN_xi[1]*N_eta[1]

    #deta
    dN1deta = dN_eta[0]*N_xi[0]
    dN2deta = dN_eta[0]*N_xi[2]
    dN3deta = dN_eta[2]*N_xi[2]
    dN4deta = dN_eta[1]*N_xi[0]
    dN5deta = dN_eta[0]*N_xi[1]
    dN6deta = dN_eta[1]*N_xi[2]
    dN7deta = dN_eta[2]*N_xi[1]
    dN8deta = dN_eta[0]*N_xi[1]
    dN9deta = dN_eta[1]*N_xi[1]
    
    dN_mat = np.array([[dN1dxi,dN2dxi,dN3dxi,dN4dxi,dN5dxi,dN6dxi,dN7dxi,dN8dxi,dN9dxi],
                       [dN1deta,dN2deta,dN3deta,dN4deta,dN5deta,dN6deta,dN7deta,dN8deta,dN9deta]])
    return dN_mat
    
#Write 
def Q2J(xi,eta,X_nod,Y_nod,elnum):
    Xtrans = X_nod[elnum-1] #nodal values of x (note zero index adjustment)
    Ytrans = Y_nod[elnum-1] #as above for y
    xy_mat = np.transpose(np.array([Xtrans,Ytrans])) #Xy mat must be 9x2
    return np.matmul(dN_Q2(xi,eta),xy_mat) 

def detq2J(xi,eta,X_nod,Y_nod,elnum): #determinant of Q2 jacobian
    return np.linalg.det(Q2J(xi,eta,X_nod,Y_nod,elnum))
    
    
xq2 = np.array([4,7       ,circx(3),4,5.5       ,6.6       ,5,0,5.316]) #all x positions in order of numbering
yq2 = np.array([0,circy(7),3       ,3,circy(7)/2,circy(6.6),3,2,2.066 ]) #all y posiitons in order of numbering

ICAq2 = np.array([[1,2,3,4,5,6,7,8,9]])

meshq2 = MESH2D(ICAq2,xq2,yq2)
Xq2 = meshq2[0][0] #nodal x points
Yq2 = meshq2[0][1] #nodal y 

det1 = detq2J(-1,-1,Xq2,Yq2,1)
det2 = detq2J( 1,-1,Xq2,Yq2,1)
det3 = detq2J( 1, 1,Xq2,Yq2,1)
det4 = detq2J(-1, 1,Xq2,Yq2,1)
det5 = detq2J( 0,-1,Xq2,Yq2,1)
det6 = detq2J( 1,0,Xq2,Yq2,1)
det7 = detq2J( 0,1,Xq2,Yq2,1)
det8 = detq2J(-1,0,Xq2,Yq2,1)
det9 = detq2J( 0,0,Xq2,Yq2,1)


## Plotting jacobian contour plot
#Generate temperature array

det_mat = np.transpose(np.array([[det1,det2,det3,det4,det5,det6,det7,det8,det9]]))




 
    
## Writing results to EXCEL file: (Useful for viewing the GSM)
#First bit of code from online source
import xlwt 
from xlwt import Workbook
# Workbook is created 
wb = Workbook() 
# add_sheet is used to create sheet. 
sheet1 = wb.add_sheet('Sheet 1') 
#Headings
sheet1.write(0, 0, "Global Body Force" )
sheet1.write(0, 2, "GSM" )
sheet1.write(0, 1, "Temperatures" )
sheet1.write(0, 20, "det" )
#writes data from output arrays and lists to the sheet
for k in range(numnod):
    sheet1.write(k+1, 0, F_bod[k][0] ) #BODY Force array
    sheet1.write(k+1, 1, T_global[k][0] )
    
    for i in range(numnod):
        sheet1.write(k+1,i+2, K_glob[k][i] ) #GSM
for a in range(9):
    sheet1.write(a+1, 20, det_mat[a][0] )

wb.save('OUTPUT.xls')
