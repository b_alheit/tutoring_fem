##| Daniel Pons | PNSDAN002 | MEC5063Z | Project 2: Tri FEM
import numpy as np
import matplotlib.pyplot as plt
import scipy.linalg as sp
import statistics as stat
import matplotlib.pyplot as plt
import matplotlib.patches as patch
import random as rand

##  MESH DATA INPUT
#coordinate map for circular cut out
def circx(y_value): #takes y value gives corresponding x back
    alpha = 8
    beta = 4
    r = 2
    return (2*alpha - np.sqrt((2*alpha)**2+4*(r**2-(y_value-beta)**2-alpha**2)))/2
    
def circy(x_value): #takes x value gives corresponding y back
    alpha = 8
    beta = 4
    r = 2
    return (2*beta - np.sqrt((2*beta)**2+4*(r**2-(x_value-alpha)**2-beta**2)))/2    

#X&Y coordiniate array
x_nodal_array = np.array([0,4,0,4,0,4,circx(3),7,8,8,circx(4)]) #all x positions in order of numbering
y_nodal_array = np.array([0,0,1,3,4,4,3,circy(7),0,circy(8),4]) #all y posiitons in order of numbering

ICA = np.array([[1,2,3],
                [2,4,3],
                [5,3,4],
                [5,4,6],
                [8,2,9],
                [8,9,10],
                [7,2,8],
                [7,4,2],
                [7,6,4],
                [11,6,7]])     #each internal array is for an element Using standard CCW numbering (global node numbers)

#Boundary Definitions
#BND_ICA = np.array([1,3,5,6,11,7,8,10,9,2]) #outline ICA

##Given functions&constants
def S(x,y):
    return 0.5*((8-x)**2)*(4-y)**2 #given source function
   
# Boundary Force Calculation
#Only flux condition is along AE due to symmetry. Needs a line integral.
#q_bar is given
def q_bar(x,y): #valid only along AE
    return -3*(y**2)   



k_cond = 5 # Wcm^-1C^-1
#assumed isotropic material
D = np.array([[k_cond,0],
              [0,k_cond]])
##  Meshing


def MESH2D(ICA,x_nodal_array,y_nodal_array): #takes in an ICA, nodal arrays should do quad&tri
    ICA_0 = ICA-1   #0 indexed ICA for ease of use with position array
    X = np.zeros((len(ICA),len(ICA[0]))) #X&Y vectors to store all x-y points for the mesh&plotting
    Y = np.zeros((len(ICA),len(ICA[0])))
    
    for k in range(0,len(ICA)): #creates x&y points for each node to be plotted
        X[k] = x_nodal_array[ICA_0[k]]
        Y[k] = y_nodal_array[ICA_0[k]]
    #add node one again for plotting purposes
    
    X_plot = np.zeros((len(ICA),len(ICA[0])+1)) #additional point to close triangles
    Y_plot = np.zeros((len(ICA),len(ICA[0])+1))
    X_bar = np.zeros((len(ICA),1))
    Y_bar = np.zeros((len(ICA),1))
    for i in range(0,len(ICA)):
        X_plot[i] = np.append(X[i],X[i][0])
        Y_plot[i] = np.append(Y[i],Y[i][0])
        X_bar[i] = np.average(X[i]) #average x-pos
        Y_bar[i] = np.average(Y[i]) #av.y pos
    return [[X,Y],[X_plot,Y_plot],[X_bar,Y_bar]] #returns the x&y points for each, the plotted arrays and the centroids

mesh = MESH2D(ICA,x_nodal_array,y_nodal_array)
X = mesh[0][0] #nodal x points
Y = mesh[0][1] #nodal y

X_plot = mesh[1][0] #x points for plotting
Y_plot = mesh[1][1] #ypoints for plotting

X_bar = mesh[2][0] #average x-pos
Y_bar = mesh[2][1] #average y-pos


##PLOT MESH
fcolour = ['b','g','r','c','m','k','y','k--','b--','g--','r--','c--','m--','y--']    
plt.figure(0) #mesh
for i in range(0,len(ICA)):
    plt.plot(X_plot[i],Y_plot[i],fcolour[0]) #plots all elements
    plt.text(X_bar[i],Y_bar[i],str(i+1),
                               color=fcolour[5],
                               fontweight="bold") #plots element labels at the centroid of the circle
for j in range(len(x_nodal_array)):
    plt.text(x_nodal_array[j],y_nodal_array[j], str(j+1),
                        color=fcolour[5],
                        fontsize=6,
                        fontweight="bold",
                        bbox=dict(boxstyle='circle',pad=0.3,facecolor='white',alpha=1)) #plots a circled node number at each node
plt.xlabel('x position - (m)')
plt.ylabel("y position - (m)")
plt.title("Triangular Mesh")

plt.tight_layout() 
plt.grid()
plt.show()

##Gauss2D

#Map triangle to Isoparametric space
def ISO2C(xi,eta,X_nod,Y_nod): #takes xi,eta and returns xy position
    N = np.array([[xi],
                 [eta],
                 [1-xi-eta]])
                 
    x = np.matmul(X_nod,N) 
    y = np.matmul(Y_nod,N)
    C_pos = np.array([x,y])
    return C_pos

#Integration function
def int_tri(Ngp,function,X_nod,Y_nod): #function must be a previously defined function with an argument (x,y)
    #x_2_e & x_1_e are the upper and lower limits of integration for the element
    #gauss quad array GQA
    #global x1,x2,x3,y1,y2,y3
    #global M,J_det,qr_i,I_g
    GQ_xi_i = np.array([[1/3],
                        [1/6,2/3,1/6],
                        [1/3,2/10,6/10,2/10]]) #xi interpollation points
    GQ_eta_i = np.array([[1/3],
                        [1/6,1/6,2/3],
                        [1/3,2/10,2/10,6/10]]) #eta
    GQ_w_i = np.array([[1],
                    [1/3,1/3,1/3],
                    [-9/16,25/48,25/48,25/48]]) #weighting points for each 
    qr = [1,3,4] #used to relate index to number of gauss pts
    #area and determinant
    x1,x2,x3 = X_nod
    y1,y2,y3 = Y_nod
    M = np.array([[1,x1,y1],
                  [1,x2,y2],
                  [1,x3,y3]])
    J_det = np.linalg.det(M)
    # Constants
    qr_i = qr.index(Ngp) # ie. ngp = 3, index = 1
    # Integration
    I_g = 0
    for i in range(Ngp):
        xi  = GQ_xi_i[qr_i][i] #Gauss point xi
        eta = GQ_eta_i[qr_i][i] #Gauss point eta
        w_i = GQ_w_i[qr_i][i] #Gauss point xi
        
        x_y = ISO2C(xi,eta,X_nod,Y_nod)
        x = x_y[0] #x as a function of xi (map
        y = x_y[1]
        #Integral
        I_g += function(x,y)*w_i
        #print("xi:",xi,"\nEta:",eta,"\nW_i:",w_i,"\nx:",x,"\ny:",y,"\nI_g:",I_g,"\n")
    Integral = 0.5*I_g*J_det
    return Integral

#boundary line integration
def int_bndry(Ngp, function, x_1_e, x_2_e):  # function must be a previously defined function with an argument
    # x_2_e & x_1_e are the upper and lower limits of integration for the element
    # gauss quad array GQA
    GQA = [[0], [[0], [2]], [[-1 / np.sqrt(3), 1 / np.sqrt(3)], [1, 1]],
           [[-np.sqrt(3 / 5), 0, np.sqrt(3 / 5)], [5 / 9, 8 / 9, 5 / 9]],
           [[-0.86113631, -0.33998104, 0.33998104, 0.86113631],
            [0.3478548451, 0.6521451549, 0.6521451549, 0.3478548451]],
           [[-0.9061798459, -0.5384693101, 0, 0.5384693101, 0.9061798459],
            [0.2369268851, 0.4786286705, 0.5688888889, 0.4786286705, 0.2369268851]],
           [[-0.9324695142, -0.6612093865, -0.2386191861, 0.2386191861, 0.6612093865, 0.9324695142],
            [0.1713244924, 0.3607615730, 0.4679139346, 0.4679139346, 0.3607615730, 0.1713244924]]]
    # Use this to call all values, format is [[gauss points],[location]] eg:GQA[3][0][1]

    # Constants
    l_int = x_2_e - x_1_e  # length of integral domain
    dx_dxi = l_int / 2  # dx/dxi to transfer into xi coordinates
    # Integration
    I_g = 0
    for i in range(0, Ngp):
        xi = GQA[Ngp][0][i]  # Gauss point xi
        w_i = GQA[Ngp][1][i]  # weighting of the point
        x = 0.5 * (x_1_e * (1 - xi) + x_2_e * (1 + xi))  # x as a function of xi (map
        # Integral
        I_g += function(0,x) * w_i

    Integral = I_g * dx_dxi
    return Integral


##Shape fn.
def C2ISO(x,y,X_nod,Y_nod): #Takes in x&y pos, node arrays of the triangle nodes and returns Xi and ETA
    #constants for conciseness
    x13 = (X_nod[0]-X_nod[2]) #x1-x3
    x23 = (X_nod[1]-X_nod[2]) #x2-x3
    y13 = (Y_nod[0]-Y_nod[2]) #y...
    y23 = (Y_nod[1]-Y_nod[2]) #y...
    x3 = X_nod[2]
    y3 = Y_nod[2]
    #rewrite eqns from C2ISO in matrix form
    K_mat = np.array([[x13,x23],
                      [y13,y23]]) 
    F = np.array([[x-x3],
                 [y-y3]])
    d_xi_eta = np.matmul( np.linalg.inv(K_mat),F) #solve the matrix eqn for xi,eta
    return d_xi_eta
    
def is_p_el(x,y,Nodal_X,Nodal_Y,elnum): #algorithim that checks if a point is contained in the element of choice
    #x,y is point position
    #NodalX&Y are the arrays of the nodal points of X&Y
    #elnum is the element of choice
    #global isopos #Checking purposes
    isopos = C2ISO(x,y,Nodal_X[elnum-1],Nodal_Y[elnum-1])
    if isopos[0] < 0 or isopos[1] < 0 or isopos[1] > 1-isopos[0]: 
        #If point is below xi or eta axis or above the xieta line, it is outside the triangle
        #print("Error: point (",x,",",y,") is not found in element:",elnum)
        #print(isopos)
        return False
    else:
        #print(isopos)
        return True





def A_NT_tri(x,y,X_nod,Y_nod,elnum): #Takes in x&y pos, node arrays of the triangle nodes and returns shape fn.
    #Check if x,y are outside element 
    if is_p_el(x,y,X_nod,Y_nod,elnum):
        #constants for conciseness
        x1 = X_nod[elnum-1][0] #elnum adjusted for zero indexing
        x2 = X_nod[elnum-1][1]
        x3 = X_nod[elnum-1][2]
        y1 = Y_nod[elnum-1][0]
        y2 = Y_nod[elnum-1][1]
        y3 = Y_nod[elnum-1][2]
        # matrix form or eqn
        N_mat = np.array([[1 ,1 ,1 ],
                        [x1,x2,x3],
                        [y1,y2,y3]]) #straight from formula
        F = np.array([[1],
                    [x],
                    [y]]) 
                    
        N_xy = np.matmul( np.linalg.inv(N_mat),F) #solve the matrix eqn for shape function in terms of x&y
        return N_xy
    else:    
        return np.zeros((3,1))

def NT_tri(x,y,X_nod,Y_nod,elnum): #Takes in x&y pos, node arrays of the triangle nodes and returns shape fn.
    #Check if x,y are outside element 
    #constants for conciseness
    x1 = X_nod[elnum-1][0] #elnum adjusted for zero indexing
    x2 = X_nod[elnum-1][1]
    x3 = X_nod[elnum-1][2]
    y1 = Y_nod[elnum-1][0]
    y2 = Y_nod[elnum-1][1]
    y3 = Y_nod[elnum-1][2]
    # matrix form or eqn
    N_mat = np.array([[1 ,1 ,1 ],
                    [x1,x2,x3],
                    [y1,y2,y3]]) #straight from formula
    F = np.array([[1],
                [x],
                [y]]) 
                
    N_xy = np.matmul( np.linalg.inv(N_mat),F) #solve the matrix eqn for shape function in terms of x&y
    return N_xy



##Shape fn derivative
def BT_tri(X_nod,Y_nod,elnum): #x&y dont matter, node arrays of the triangle nodes and returns derivative of the SF
    
    #constants for conciseness
    x1 = X_nod[elnum-1][0] #elnum adjusted for zero indexing
    x2 = X_nod[elnum-1][1]
    x3 = X_nod[elnum-1][2]
    y1 = Y_nod[elnum-1][0]
    y2 = Y_nod[elnum-1][1]
    y3 = Y_nod[elnum-1][2]
    # matrix form or eqn
    M = np.array([[1,x1,y1],
                  [1,x2,y2],
                  [1,x3,y3]])
    M_inv = np.linalg.inv(M)              

    B_coef = np.array([[0,1,0],
                       [0,0,1]])
    B_xy = np.matmul( B_coef,M_inv) #solve the matrix eqn for shape function in terms of x&y
    return np.transpose(B_xy)



##Matrices & Matrix operations for integration    
def NT(x_var,y_var): #For integration, X,Y and elnum must be defined in each loop
    return NT_tri(x_var,y_var,X,Y,elnum)

def NT_S(x_var,y_var): #For integration, X,Y and elnum must be defined in each loop
    return S(x_var,y_var)*NT_tri(x_var,y_var,X,Y,elnum)
 
def BT_D_B(X_nod,Y_nod,elnum): # general definition of the K operand 
    return np.matmul(np.matmul(BT_tri(X_nod,Y_nod,elnum),D),np.transpose(BT_tri(X_nod,Y_nod,elnum)))

def dK_loc(x_var,y_var): #For integration (constant wrt x&y, but made a function for use in code)
    return BT_D_B(X,Y,elnum)


#Boundary force
# Boundary Force Calculation
#Only flux condition is along AE due to symmetry. Needs a line integral.

def NT_q_bar_AE(x_var,y_var): #NB define elnum with each integral
    if ( not x_var == 0 or 4<=y_var<8):
        return np.zeros((3,1))
    else:
        return NT(0,y_var)*q_bar(0,y_var)



##Local Integration
K_loc_list = [] #results list S
NT_S_list = [] #results list NT_S for each element
nelem = len(ICA)  
numnod = len(x_nodal_array)      
Ngp_k = 3 #Number of triangular integration points(1,3,4 possible) 
Ngp_s = 3 #Higher order used for this one.Number of triangular integration points(1,3,4 possible)   
for k in range(nelem):
    elnum = k+1 #defining element number in the loop
    #BT_D_B
    K_loc_list.append(int_tri(Ngp_k,dK_loc,X[k],Y[k])) #integration for local stiffness matrices
    #NT_S
    NT_S_list.append(int_tri(Ngp_s,NT_S,X[k],Y[k])) #integration for calculation of the nodal body force

NT_q_bar_array = np.zeros((nelem,3, 1)) #results array for boundary loading on each element
#nodes
bndry_ICA = np.array([[1,3],
                      [3,5]])
bndry_el = np.array([1,3])

for n in range(len(bndry_el)):
    elnum = bndry_el[n]
    zeroICA = bndry_ICA-1
    zeroel = bndry_el-1
    el_int = int_bndry(Ngp_s,NT_q_bar_AE,y_nodal_array[zeroICA[n][0]],y_nodal_array[zeroICA[n][1]])
    NT_q_bar_array[zeroel[n]]=el_int
#elemental loads:

#NT_q_bar_list.append(int_bndry(Ngp_s,NT_q_bar_AE,X[k],Y[k]))



## Assembly
#stiffness
def tri_semble_K(numnod,nelem,ICA,K_lin_loc): #number of nodes,#elements,interconnectivity array,local stiffness matrix
    K_lin = np.zeros((numnod, numnod))  # globak K matrix of appropriate size to ndof
    for i in range(nelem):
        g1,g2,g3 = ICA[i]-1 # global indices,zero indexed  
        #Stiffness 
        #row1
        K_lin[g1][g1] += K_lin_loc[i][0][0]
        K_lin[g2][g1] += K_lin_loc[i][1][0]
        K_lin[g3][g1] += K_lin_loc[i][2][0]
        #row2
        K_lin[g1][g2] = K_lin[g2][g1]
        K_lin[g2][g2] += K_lin_loc[i][1][1]
        K_lin[g3][g2] += K_lin_loc[i][2][1]
        #row3
        K_lin[g1][g3] = K_lin[g3][g1]
        K_lin[g2][g3] = K_lin[g3][g2]
        K_lin[g3][g3] += K_lin_loc[i][2][2]
    return K_lin
#Force
def tri_semble_F(numnod,nelem,ICA,F_loc): #
    #K_lin = np.zeros((numnod, numnod))  # globak K matrix of appropriate size to ndof
    F_bod = np.zeros((numnod, 1))
    for i in range(nelem):
        g1,g2,g3 = ICA[i]-1 # global indices,zero indexed
        # force
        F_bod[g1] += F_loc[i][0]
        F_bod[g2] += F_loc[i][1]
        F_bod[g3] += F_loc[i][2]
    return F_bod
shift = 0  #Used to check the assembly process, default is zero
F_bod = tri_semble_F(numnod,nelem-shift,ICA,NT_S_list) #global body force
K_glob = tri_semble_K(numnod,nelem-shift,ICA,K_loc_list) #global stiffness matrix
F_bndry = tri_semble_F(numnod,nelem-shift,ICA,NT_q_bar_array) #global body force

F = F_bod-F_bndry

##Partition: Move Derichler BCs around to break the problem up



# Derichler/Essential BCs
D_nodes = np.array([1,2,9])  # array of Derichler node number indices 1-indexed
D_disp = np.array([0,0,0])  # Derichler temperature associated with the D_node position above

def partition_solve(D_nodes,D_disp,K_glob,F_glob): #Takes derichler nodes, nodal values, global K&F
    numnod = len(F_glob)
    d_lin = np.zeros((numnod, 1))  # initialised displacement vector
    for i in range(len(D_nodes)):  # adds derichler BCs to the displacement vector
        pos = (D_nodes - 1)[i]
        d_lin[pos] = D_disp[i]
    # Rearrangement of derichler rows
    nodes = np.array(range(numnod))  # array of node numbers
    nod_r = np.delete(nodes, (D_nodes - 1))  # deletes all Derichler indices (reduced nodal array)
    new_order = np.concatenate(((D_nodes - 1), nod_r))  # All derichler nodes zero indexed added to the front of the array
    # Now index notation will help build a new array with the new_order
    K_glob_alt = np.zeros((numnod, numnod))
    F_glob_alt = np.zeros((numnod, 1))
    d_lin_alt = np.zeros((numnod, 1))
    
    
    # KALT =np.array([[K_glob[i][j] for j in new_order] for i in new_order])
    for i in range(numnod):
        row = new_order[i]  # maps new to old indexing
        F_glob_alt[i] = F_glob[row]  # Rearrangement of Force vector
        d_lin_alt[i] = d_lin[row]
        for j in range(numnod):
            column = new_order[j]
            K_glob_alt[i][j] = K_glob[row][column]  # Rearrangement of K matrix
    
    # break up of the arrays for solving:
    nn_cut = np.array(range(len(D_nodes)))  # natural BC slice index
    K_nn = np.delete(K_glob_alt, nn_cut, axis=0)
    K_nn = np.delete(K_nn, nn_cut, axis=1)
    # d_n = np.delete(d_lin_alt,nn_cut,axis = 0) #Displacement partitioning for force adjustment
    F_n = np.delete(F_glob_alt, nn_cut, axis=0)
    
    ne_cut = np.array(range(len(D_nodes), numnod))  # natural essential bc slice index
    K_ne = np.delete(K_glob_alt, nn_cut, axis=0)
    K_ne = np.delete(K_ne, ne_cut, axis=1)
    d_e = np.delete(d_lin_alt, ne_cut, axis=0)  # Displacement partitioning for force adjustment
    
    # invert K nn&ne
    K_nn_inv = np.linalg.inv(K_nn)
    # Solving for displacement:
    d_n = np.matmul(K_nn_inv, (F_n - np.matmul(K_ne, d_e)))
    
    d_lin_new = np.concatenate((d_e, d_n))
    
    # swap rows back to original positions
    u_glob = np.zeros((numnod, 1))
    for i in range(numnod):
        row = new_order[i]
        u_glob[row] = d_lin_new[i]  # maps old to new indexing
    return u_glob

T_global = partition_solve(D_nodes,D_disp,K_glob,F)
# Post processing
#Global shape function not needed for this project. just plot it
# displacement field:
'''def N_glob_tri(x,y,X_nod,Y_nod):  # Global Shape function array
    # x = 0.125
    #make a local shape function array
    N_loc = np.zeros((nelem,3,1))
    overlap = 1
    for i in range(nelem):
        if x in X_nod[i]:
            index = X_nod[i].index(x) 
            if y == Y_nod[i][index]:
               overlap+=1 
        elnum = i+1
        N_loc[i] = A_NT_tri(x,y,X_nod,Y_nod,elnum)
      
    N_glob = tri_semble_F(numnod,nelem,ICA,N_loc)
    
    return N_glob/overlap
#'''


##Contour plotting

#Contour plot related fns    
def N_ISO(xi,eta): #Takes in x&y pos, node arrays of the triangle nodes and returns Xi shape fn.
    #note elnum is not zero indexed
    #constants for conciseness
    x1 = 1 #elnum adjusted for zero indexing
    x2 = 0
    x3 = 0
    y1 = 0
    y2 = 1
    y3 = 0
    # matrix form or eqn
    N_mat = np.array([[1 ,1 ,1 ],
                      [x1,x2,x3],
                      [y1,y2,y3]]) #straight from formula
    F = np.array([[1],
                  [xi],
                  [eta]]) 
                
    N_xi = np.matmul( np.linalg.inv(N_mat),F) #solve the matrix eqn for xi shape functions
    return N_xi
    
def intrplt_ISO(xi,eta,T1,T2,T3): #takes position and nodal temps, note that it depends on all code above.
    T_i = np.array([T1,T2,T3])
    N_xi = N_ISO(xi,eta)
    XI = np.array([[1,0,0]])
    ETA = np.array([[0,0,1]])
    
    if is_p_el(xi,eta,XI,ETA,1):
        T_x_y = np.matmul(T_i,N_xi)
        return T_x_y[0] #Returns Scalar interpolation
    else:
        return np.nan #returns void if not inside the triangle
#plot
resolution = 100
slices = 15
X1 = np.linspace(0,1,resolution)
grid = np.meshgrid(X1,X1)

xi_mesh = grid[0]
eta_mesh = grid[1]


#Generate temperature array
T_ICA = T_global[ICA-1]
T_mesh = np.zeros((nelem,resolution,resolution))
X_mesh = np.zeros((nelem,resolution,resolution))
Y_mesh = np.zeros((nelem,resolution,resolution))
Tmax = np.max(T_global)
for el in range(nelem):
    for row in range(resolution):
        for column in range(resolution):
            xi = xi_mesh[row][column]
            eta = eta_mesh[row][column]
            T1,T2,T3 = np.transpose(T_ICA[el])[0]
            T_mesh[el][row][column] = intrplt_ISO(xi,eta,T1,T2,T3) #assigns interpolated temp to each elemental mesh
            if T_mesh[el][row][column]>Tmax: #calibrates colour plot
                Tmax = T_mesh[el][row][column] #only re-assigns if there is a greater value.
            #send xi&eta back to x&y
            xy = ISO2C(xi,eta,X[el],Y[el])
            X_mesh[el][row][column] = xy[0]
            Y_mesh[el][row][column] = xy[1]

plt.figure(1)
for el in range(nelem):
    
    plt.contourf(X_mesh[el],Y_mesh[el],T_mesh[el],np.linspace(0,Tmax,slices))            
plt.colorbar().set_label("Temp (deg C)")
#MESH OVERLAY
for i in range(0,len(ICA)):
    plt.plot(X_plot[i],Y_plot[i],"k") #plots all elements
    plt.text(X_bar[i],Y_bar[i],str(i+1),
                               color=fcolour[5],
                               fontweight="bold") #plots element labels at the centroid of the circle
for j in range(len(x_nodal_array)):
    plt.text(x_nodal_array[j],y_nodal_array[j], str(j+1),
                        color=fcolour[5],
                        fontsize=6,
                        fontweight="bold",
                        bbox=dict(boxstyle='circle',pad=0.3,facecolor='white',alpha=1)) #plots a circled node number at each node
plt.xlabel('x position - (m)')
plt.ylabel("y position - (m)")
plt.title("Triangular Temperature Mesh Overlay")
plt.tight_layout() 
plt.grid()          
plt.show()
##
#Plot Temp and Heat flux along AB
def T_AB(x_var):
    if x_var<4:
        Ntri = np.transpose(NT_tri(x_var,4,X,Y,4))
        return np.matmul(Ntri,T_global[ICA[3]-1])[0][0]
    if x_var>=4:
        Ntri = np.transpose(NT_tri(x_var,4,X,Y,10))
        return np.matmul(Ntri,T_global[ICA[9]-1])[0][0]
        
def q_AB(x_var):
    if x_var<4:
        
        Btri = np.transpose(BT_tri(X,Y,4))
        DB = np.matmul(D,Btri)
        return np.matmul(-DB,T_global[ICA[3]-1])[0][0]
    if x_var>=4:
        Btri = np.transpose(BT_tri(X,Y,10))
        DB = np.matmul(D,Btri)
        return np.matmul(-DB,T_global[ICA[9]-1])[0][0] 
##        
#plot Q&T AB
pres = 1000
x_space = np.linspace(0, 6, pres)


def func_array(function, resolution, xstart, xstop):
    x_space = np.linspace(xstart, xstop, resolution)
    def_space = np.zeros((1, resolution))
    for i in range(resolution):
        x = x_space[i]
        def_space[0][i] = function(x)
    return def_space[0]


plt.figure(2)

fig,ax1 = plt.subplots()
ax2 = ax1.twinx()
ax1.plot(x_space, func_array(q_AB, pres, 0, 6), 'k-.')  # flux
ax2.plot(x_space, func_array(T_AB, pres, 0, 6), 'b')  #Temp

ax1.set_xlabel('x-position along AB (m)')
ax1.set_ylabel('Flux (W/C)',color='k')
ax2.set_ylabel('Temp (deg C)',color='b')
plt.title('Flux and Temperature along AB')
plt.show()
##
#square mesh each element,if outside assign T= nan, then contour plot


#plt.contourf

## Writing results to EXCEL file: (Useful for viewing the GSM)
#First bit of code from online source
import xlwt 
from xlwt import Workbook
# Workbook is created 
wb = Workbook() 
# add_sheet is used to create sheet. 
sheet1 = wb.add_sheet('Sheet 1') 
#Headings
sheet1.write(0, 0, "Global Body Force" )
sheet1.write(0, 2, "GSM" )
sheet1.write(0, 1, "Temperatures" )
#writes data from output arrays and lists to the sheet
for k in range(numnod):
    sheet1.write(k+1, 0, F_bod[k][0] ) #BODY Force array
    sheet1.write(k+1, 1, T_global[k][0] )
    for i in range(numnod):
        sheet1.write(k+1,i+2, K_glob[k][i] ) #GSM
wb.save('OUTPUT.xls')
