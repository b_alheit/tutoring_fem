#!/usr/bin/env python
# coding: utf-8

# In[19]:


#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import matplotlib.pyplot as plt
from scipy.linalg import det, inv


# In[2]:


# Interconnectivity array ICA
ICA = np.array([[0,1,10,9],[1,2,3,4],[1,4,5,10],[10,5,6,7],[9,10,7,8]])
nodes = np.array([[0,0],[4,0],[8,0],[8,2],[7,(4-3**0.5)],[(8-3**0.5),3],[6,4],[4,4],[0,4],[0,1],[4,3]])

nel = 5; #Number of elements
nn = nel + 6; #Number oF nodes
k = 5; #Thermal conductivity
D = k*np.array([[1, 0], [0, 1]]); #D matrix


## Global X & Y vector
X = np.array([0, 4, 8, 8, 7, (8-3**0.5), 6, 4, 0, 0, 4])
Y = np.array([0,0, 0, 2, (4-3**0.5), 3, 4, 4, 4, 1, 3])


## defining the shape functions for the ispoarametric space
#xi, eta = symbols('xi eta')
def N(xi,eta):
    Ns = np.array([(1-xi)*(1-eta)/4,
                   (1+xi)*(1-eta)/4,
                   (1+xi)*(1+eta)/4,
                   (1-xi)*(1+eta)/4]).reshape(4,1)
    return Ns

## Defining the gradient of the shape functions
#xi, eta = Symbols('xi eta')
def gradNpar(xi, eta):
    gradient =0.25*np.array([[(eta-1), (xi-1) ],
                            [(1-eta) ,(-xi-1)],
                            [(1+eta), (xi +1)],
                            [(-1-eta), (1-xi)]]).reshape(2,4)
    return gradient
# In[5]:


## Four-point two-dimensional rule
# first column of xe is locations of xi and second column is locations of eta


xi =  np.array([[+0.861136],
               [+0.861136],
               [-0.339981],
               [-0.339981]])
    
eta = np.array([[-0.861136],
               [-0.861136],
               [0.339981],
               [0.339981]])
#xe = np.array([[+0.861136,+0.861136],[-0.861136,-0.861136],[-0.339981,0.339981],[+0.339981,-0.339981]])
W  = np.array([0.347855, 0.347855, 0.652145, 0.652145])
           
#xi = xe[:,0]
#eta = xe[:,1]




## Assemblying Global stifness matrix 
K = np.zeros((nn,nn))
F = np.zeros((nn,1))
M = np.ones((4,4))
T = np.zeros((nn,1))
Q = np.zeros((nn,1))

n = 4;
for e in np.arange(nel):
    asm = ICA[e,:]
    # x and y coordinate for each element
    xe= X[asm]
    ye= Y[asm]
    xy = np.array([xe,ye]).reshape(4,2)
    
    # initializing the values for gauss quadrature
    Gauss_int = 0
    force_elem = np.zeros((4,1))
    k_elem = np.zeros((4,4))
    for j in np.arange(n):
        for i in np.arange(n):
            ## evaluating the shape functions 
            N1 = N(xi[j], eta[i])
            # transformations to isoparametrics coordinates
            x = np.dot(xe,N1)
            y = np.dot(ye,N1)
            
            # source term s
            s = ((8-x)**2 + (4-y)**2)/2.0
            
            
            ## calculating the jacobian matrix
            GN = gradNpar(xi[j],eta[i])
            #print(GN)
            J =  np.dot(GN, xy, out = None) # jacobian for each element
            #print(det(J)) 
            # computing the determinant of J and the inverse of J
            J_inv = inv(J)  ## inverse of J
            #print(J_inv)
            J_det = det(J)  ## determinant of J
            Be    = np.dot(J_inv, gradNpar(xi[j], eta[i]))  ## gradient matrix for each element
            # stiffness matrix for each element
            Ke = np.dot(np.dot(Be.T, D),Be)*J_det   
            #print(Ke)
            #print('====================================')
            #print('====================================')
            force_elem += W[i]*W[j]*N1*s*J_det
            k_elem += W[i]*W[j]*Ke*J_det
            K_elem =k_elem
            K[asm[i], asm] += K_elem[i,:]
            F[asm[i]]  += sum(force_elem[i, :])
   
#%%              Looping through the flux 
# The flux only exit along the side AE where the flux is given by q =-3*y**2
# along this side x = 0 ==> xi = -1
# Therefore the shape funcstions of the element [1,2,11,10] and element
# [10,11,8,9] are given by N(xi =-1, eta) = 1/2*[1-eta, 0, 0, 1+eta]
# therefore the closed integral will be along eta axis and the jacobian 
# along that line is given by J = (b-a)/2 where a and b are the starting points
# and endpoints of the interval. Let A=(0,0), B=(0,1) and C = (0,4)
# therefore close integral will partioned into closed integral from A to B and 
# closed interval from B to  C and the jacobians along these lines are 1/2 and 
# 3/2 respectively. 

# INITIALIZING THE FLUX AND eta values and weights

nel1 = 2;
J = 0.5*np.array([3,1])  ## the values of the jacobian on the  interval 
H = 0.5*np.array([5,1])

flux = np.zeros((4,1))
Q = np.zeros((nn,1))
QuadRule =4
eta1 = np.array([-0.8611363116, -0.3399810436,0.8611363116,0.3399810436]) # eta values 
W  = np.array([0.347855, 0.347855, 0.652145, 0.652145]) # weights
# interconnectivity array
ICA1 = np.array([[0,1,10,9], [9,10,7,8]])

for e in np.arange(nel1):
    asm = ICA1[e,:]
    xe1 = X[asm]
    ye1 = Y[asm]

    #plt.plot(np.append(xe1, xe1[1]), np.append(ye1, ye1[1]))
    flux = 0 # initializing flux
    
    for k in np.arange(QuadRule):
        xi1 = -1
        N2 = N(xi1,eta[k])
        
        for i in np.arange(2):
            y1 = H[i]+J[i]*eta[k]
            
            ## calculating the flux 
            q = -3*y1**2
            flux = flux + J[i]*q*N2
    for m in range(len(asm)):
        Q[asm[m]] += sum(flux[m, :])
            
#%% TOTAL FORCE 
Total_force = F-Q  
## initializing d, which is the nodal temperature vector
d = np.zeros((nn,1))

#%% PARTIONING THE STIFF MATRIX AND FORCE VECTOR INTO UNKNOWN VARIABLES AND KNOW VARIABLES:
d[0:3, :] = 0
K_F = K[3:nn, 3:nn]
K_EF = K[0:3, 3:nn]
F_F = Total_force[3:nn, :]
D_E = d[0:3,:]
d[3:nn,:] = np.dot((np.linalg.inv(K_F)), (F_F - np.dot(np.transpose(K_EF), D_E))) # unknown nodal temperature

D_u = d[3:nn,:]  
#%% CALCULATING THE REACTION VECTOR OR RESIDUAL VECTOR    
Reaction_vector = -np.dot(K_EF, D_u)+Total_force[0:3,:]

# Now printing the tmperature at each nodes:
T = d.copy()  # making a copy of d
print('temperature at the nodes')
for index in np.arange(nn):
    print('Node', index + 1, 'T=', T[index])

##%% Plotting the contour plot for the temperature:
# Plot filled contours
plt.figure()
plt.gca().set_aspect('equal')
# setup three 1-d arrays for the x-coordinate, the y-coordinate, and the
# z-coordinate
node2dof =np.array([9,10,11,4,5,6,7,8,3,1,2])

xs = nodes[:,0].reshape(nn,) # one value per node
ys = nodes[:,1].reshape(nn,) # one value per node
ix = node2dof[np.arange(nn)] - 1
ICA1 = np.array([[9, 0, 1],[9, 1, 10],[8, 9, 10],[7, 8, 10],[1, 2, 4],[2, 3, 4],[1, 4, 5],[1, 5, 10],[10, 5, 7],[5, 6, 7]])
zs = (T).reshape(nn,) # one value per node
triangles = ICA1 # the triangles are defined by the connectivity arrays
plt.tricontourf(xs, ys,ICA1, zs)
plt.colorbar()
plt.title('Contour plot of temperature')
plt.xlabel('x (cm)')
plt.ylabel('y (cm)')
plt.show()


# In[12]:


d


# In[ ]:




