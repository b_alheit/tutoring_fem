#!/usr/bin/env python
# coding: utf-8

# In[2]:


# -*- coding: utf-8 -*-
"""
Created on Mon Apr 22 01:31:17 2019

@author: MR MATURED
"""



## PROJECT 2 WRITE-UP SCRIPT:

from numpy import zeros, ones
from numpy import transpose
import matplotlib.tri as tri
#from numpy import *
import numpy as np
import matplotlib.pyplot as plt
from scipy.linalg import det, inv


# Interconnectivity array ICA
ICA = np.array([[9, 0, 1],
                [9, 1, 10],
                [8, 9, 10],
                [7, 8, 10],
                [1, 2, 4],
                [2, 3, 4],
                [1, 4, 5],
                [1, 5, 10],
                [10, 5, 7],
                [5, 6, 7]])


nel = 10; #Number of elements
nn = nel + 1; #Number oF nodes
k = 5; #Thermal conductivity
D = k*np.array([[1, 0], [0, 1]]); #D matrix


## Global X & Y vector
X = np.array([0, 4, 8, 8, 7, (8-3**0.5), 6, 4, 0, 0, 4])
Y = np.array([0,0, 0, 2, (4-3**0.5), 3, 4, 4, 4, 1, 3])

#%%   Defining the Guass data for the quadrature 
# Function TriGaussPoint.py provides the Gauss Points and weights for the Guass quadrature of order n for the standard triangles.
# 
# Output: xw - a n by 3 matrix:
# 
#         1st column gives the x-coordinates of points
#         2nd column gives the y-coordinates of points
#         3rd column gives the weights

def TriGaussPoints(n):
    xw = np.zeros([n, 3])
    if (n==1):
        xw = np.array([1/3, 1/3, 1.0])
    elif (n==2):
        xw=np.array([[1/6, 1/6, 1/3],
                     [1/6, 2/3, 1/3], 
                     [2/3, 1/6, 1/3]]);
    elif (n==3):
        xw=np.array([[1/3,1/3,-9/16],
                    [1/5,1/5, 25/48],
                    [1/5, 3/5, 25/48],
                    [3/5, 1/5, 25/48]]);    
    return xw

#%%             
#M = np.matrix(np.ones((3,3))) # initializing the M matrix 
#F = np.matrix(np.zeros((nn,1))) # initialoizng the global force vector
#K = np.matrix(np.zeros((nn,nn))) ## initializing the global stiffenss matrix 
#Q = np.matrix(np.zeros((nn,1))) ## initializng the flux global vector
#T = np.matrix(np.zeros((nn,1))) ## initializng the temperature vector

#%% Transforming the x and y to the isoparametric space i.e s and t space

def isopar(s,t,xc,yc):
    N = np.array([s,t,1-s-t])
    x = np.dot(xc,N)
    y = np.dot(yc,N)
    return x,  y , N

#%% Defining Guass quadrature for the problem:
M = np.ones((3,3))
def quadrature(n, xc, yc, Force):
    # defining elemental matrix 
    M[:,1] = xc
    M[:,2] = yc
    # calculate the area of the triangle for each element
    A = abs(det(M))/2.0
    ## Defining the gradient matrix for each element
    Bel = np.array([[yc[1] - yc[2], yc[2] - yc[0], yc[0] - yc[1]],
                    [xc[2] - xc[1], xc[0] - xc[2], xc[1] - xc[0]]])/(2*A)
    w = TriGaussPoints(n) ## calling the weight functions 
    
    Kel = np.dot(np.dot(np.transpose(Bel), D), Bel)/2.0
    x1 = isopar(w[:, 0], w[:, 1], xc, xc)[0]  # x values with respect to the called epsi and eta
    y1 = isopar(w[:, 0], w[:, 1], xc, yc)[1]  # y value with respect to the called epsi and eta
    # int_body = A * w[:, 2] * F(x1, y1)    # integrating the source term using gauss quad
    N = isopar(w[:, 0], w[:, 1], xc, yc)[2]  ## Evaluating the shape function at isopara,metric space values i.e psi and eta
    f = N*w[:, 2]
    # computing the body force force vector for each element
    Body_integral = A*f*Force(x1, y1)

    return Body_integral, Kel

#%%  Defining the source term 
def S(x,y):
    return 0.5*((8-x)**2)*((4-y)**2)

#%% source term 

print('                                                                      ')
print('                                                                      ')
print('                                                                      ')
print('                                                                      ')

Force = S  ## defining the body force as the source term
n = 3  ## we are going to use 3 points in each trinagle to integrate the body force

force_elem = []
k_elem = []
K = np.zeros((nn, nn))
F = np.zeros((nn, 1))
d = np.zeros((nn, 1))
for e in range(0, nel):
    asm = ICA[e, :]
    X_e = X[asm]
    Y_e = Y[asm]
    force_elem = quadrature(n, X_e, Y_e, Force)[0]
    Kel = quadrature(n, X_e, Y_e, Force)[1]
    for i in range(0,len(asm)):
        K[asm[i], asm] += Kel[i, :]
        F[asm[i]]  =F[asm[i]]+ sum(force_elem[i, :])

#%%
######## findin the boundary force
ICA1 = np.array([[9, 0, 1],
                 [8, 9, 10]])

J = np.array([1, 3])
H = np.array([1, 5])
#%%
gauss_points = np.array([-3**(-0.5), 3**(-0.5)])
M = np.ones((3, 3))
flux = np.zeros((3, 1))
Q = np.zeros((11, 1))
for k in range(len(ICA1)):
    asm1 = ICA1[k, :]
    X_e = X[asm1]
    Y_e = Y[asm1]
    M[:, 1] = X_e
    M[:, 2] = Y_e
    for j in range(len(J)):
        y1 = ((H[k]) / 2) + ((gauss_points[j] * J[k]) / 2)
        N1 = X_e[1] * Y_e[2] - X_e[2]*Y_e[1] + (X_e[2] - X_e[1])*y1
        N2 = X_e[2] * Y_e[0] - X_e[0] * Y_e[2] + (X_e[0] - X_e[2]) * y1
        N3 = X_e[0] * Y_e[1] - X_e[1] * Y_e[0] + (X_e[1] - X_e[0]) * y1
        N = np.array([N1, N2, N3])/det(M)
        q = -3 * y1**2
        flux = flux + np.dot((J[k] / 2), np.transpose(N))*q

#%% TOTAL FORCE 
Total_force = F-Q  
## initializing d, which is the nodal temperature vector
d = np.zeros((nn,1))

#%% PARTIONING THE STIFF MATRIX AND FORCE VECTOR INTO UNKNOWN VARIABLES AND KNOW VARIABLES:
d[0:3, :] = 0
K_F = K[3:nn, 3:nn]
K_EF = K[0:3, 3:nn]
F_F = Total_force[3:nn, :]
D_E = d[0:3,:]
d[3:nn,:] = np.dot((np.linalg.inv(K_F)), (F_F - np.dot(np.transpose(K_EF), D_E))) # unknown nodal temperature

D_u = d[3:nn,:]  
#%% CALCULATING THE REACTION VECTOR OR RESIDUAL VECTOR    
Reaction_vector = -np.dot(K_EF, D_u)+Total_force[0:3,:]


# Now printing the tmperature at each nodes:
T = d.copy()  # making a copy of d
print('temperature at the nodes')
for index in np.arange(nn):
    print('Node', index + 1, 'T=', T[index]/100)

# nodes
nodes = np.array([[0,0],[4,0],[8,0],[8,2],[7,(4-3**0.5)],[(8-3**0.5),3],[6,4],[4,4],[0,4],[0,1],[4,3]])

##%% Plotting the contour plot for the temperature:
# Plot filled contours
plt.figure()
plt.gca().set_aspect('equal')
# setup three 1-d arrays for the x-coordinate, the y-coordinate, and the
# z-coordinate
node2dof =np.array([9,10,11,4,5,6,7,8,3,1,2])

xs = nodes[:,0].reshape(nn,) # one value per node
ys = nodes[:,1].reshape(nn,) # one value per node
ix = node2dof[np.arange(nn)] - 1

zs = (T).reshape(nn,) # one value per node
triangles = ICA1 # the triangles are defined by the connectivity arrays
# plt.tricontourf(xs, ys,ICA, zs/100)
plt.tricontourf(xs, ys,ICA, zs)
plt.colorbar()
plt.title('Contour plot of temperature')
plt.xlabel('x (m)')
plt.ylabel('y (m)')
plt.show()



# In[22]:



# calculating the flux and the temperature along side AB
# the is the same as calculating the flux along the element [11,8,9] and on element [6,7,8]

# generate ICA of these elements
ICA3 =np.array([[7,8,10],[5,6,7]])

M = np.ones((3,3))  ## INITIALIZING M
for e in range(2):
    asm3 = ICA3[e,:]
    xe3 = X[asm3-1]
    ye3 = Y[asm3-1]
    # MATRIX FOR EACH ELEMNT
    M[:,1] = xe3.T
    M[:,2] = ye3.T
    # AREA OF THE ELEMENTS
    A = abs(det(M))/2.0
    # GRADIENT FOR THE SHAPE FUNCTIONS
    Bel = np.array([[ye3[1] - ye3[2], ye3[2] - ye3[0], ye3[0] - ye3[1]],
                    [xe3[2] - xe3[1], xe3[0] - xe3[2], xe3[1] - xe3[0]]])/(2*A)
    ## flux 
    q = -k*np.dot(Bel, d[asm3]/100)
    ## definning the shape functions
    y = 4  # along side AB y is 4 and x is only changing
    x = np.linspace(0,6,5)  ## taking few values of x
    for xi in x:
        p = np.array([1,xi,y]).reshape(1,3)
        Minv= inv(M)
        N = np.dot(p, Minv)
        temp = np.dot(N, d[asm])/100
        print('element: {} \t (x,y): {}\t temperature: {}'.format(asm3, (xi,y),temp))
    print('============================================================================')
    
    
        
    


# In[ ]:




