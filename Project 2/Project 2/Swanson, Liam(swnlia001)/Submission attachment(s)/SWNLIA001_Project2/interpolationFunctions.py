import numpy as np


# Interpolants for 1D
def shapeFunctionLin(x_value, x_el):
    """
    Find the value of the local shape functions at an x-position
    :param x_value: x-coordinate
    :param x_el: array of local element nodal positions
    :return: array of value of each nodes shape function on the element at the point specified
    """

    # Define empty shape and derivative function arrays for values to be appended to
    M_shape = []

    # Loop which calculates the value of a shape function at a point of an element
    # The ith iteration indicates the loop over each global node, the jth iteration indicates the iteration
    # over each product in the sequence

    for xi in x_el:
        N_p = 1

        for xj in x_el:
            if xj != xi:
                N_p *= (x_value - xj)/(xi - xj)

        M_shape.append(N_p)

    # Cast appended lists as numpy arrays
    M_shape = np.array([M_shape])

    return M_shape


def derShapeFunctionLin(x_value, x_el):
    """
    Find the value of the local shape functions at an x-position
    :param x_value: x-coordinate
    :param x_el: array of local element nodal positions
    :return: array of value of each nodes shape function on the element at the point specified
    """

    # Define empty derivative function array
    M_derivative = []

    # Loop which calculates the value of a derivative function at a point of an element
    # The ith iteration indicates the loop over each global node, the jth iteration indicates the iteration
    # over each product in the sequence

    for xi in x_el:
        sum_loop = 0
        prod_loop = 1

        for xj in x_el:
            if xj != xi:
                r = 1 / (xi - xj)

                for xm in x_el:
                    if xm != xj and xm != xi:
                        prod_loop = 1
                        prod_loop *= (x_value - xm) / (xi - xm)

                sum_loop += r * prod_loop
        M_derivative.append(sum_loop)

    M_derivative = np.array([M_derivative])

    return M_derivative


# Interpolants for triangle

def shapeFunctionCartTri(nodeCoord_el, position):
    x1 = nodeCoord_el[0, 0]
    x2 = nodeCoord_el[1, 0]
    x3 = nodeCoord_el[2, 0]
    y1 = nodeCoord_el[0, 1]
    y2 = nodeCoord_el[1, 1]
    y3 = nodeCoord_el[2, 1]

    # Create M inverse matrix
    area = abs((x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)) / 2)

    M = np.array([[1, x1, y1],
                  [1, x2, y2],
                  [1, x3, y3]])

    M_inv = np.linalg.inv(M)
    # print("M_inv1\n", M_inv)

    p_vec = np.array([[1, position[0], position[1]]])

    shapeFunc = np.dot(p_vec, M_inv)

    return shapeFunc


def derShapeFunctionCartTri(nodeCoord_el):
    x1 = nodeCoord_el[0, 0]
    x2 = nodeCoord_el[1, 0]
    x3 = nodeCoord_el[2, 0]
    y1 = nodeCoord_el[0, 1]
    y2 = nodeCoord_el[1, 1]
    y3 = nodeCoord_el[2, 1]

    # Create M inverse matrix
    M = np.array([[1, x1, y1],
                  [1, x2, y2],
                  [1, x3, y3]])

    M_inv = np.linalg.inv(M)

    coeff_m = np.array([[0, 1, 0],
                        [0, 0, 1]])

    derShapeFunc = np.matmul(coeff_m, M_inv)

    return derShapeFunc


def shapeFunctionIsoTri(chi, eta):
    return np.array([[chi, eta, round(1 - chi - eta, 10)]])


def derivativeFunctionIsoTri():
    return np.array([[1, 0, -1], [0, 1, -1]])


def shapeFunctionIsoLin(chi):
    shapeFunc = np.array([[-(chi - 1) / 2, (chi + 1) / 2]])

    return shapeFunc


# Interpolants for quadrilaterals
def shapeFunctionIsoQuad(pchi, peta):
    shapeFunc = np.zeros((1, 4))

    N1 = 1 / 4 * (1 - peta) * (1 - pchi)
    N2 = 1 / 4 * (1 - peta) * (1 + pchi)
    N3 = 1 / 4 * (1 + peta) * (1 + pchi)
    N4 = 1 / 4 * (1 + peta) * (1 - pchi)

    shapeFunc[0, 0] = N1
    shapeFunc[0, 1] = N2
    shapeFunc[0, 2] = N3
    shapeFunc[0, 3] = N4

    return shapeFunc


def jacobianQuad(xnode_el, ynode_el, pchi, peta):

    dN1x = -1 / 4 * (1 - peta)
    dN2x = 1 / 4 * (1 - peta)
    dN3x = 1 / 4 * (1 + peta)
    dN4x = -1 / 4 * (1 + peta)
    dN1y = -1 / 4 * (1 - pchi)
    dN2y = -1 / 4 * (1 + pchi)
    dN3y = 1 / 4 * (1 + pchi)
    dN4y = 1 / 4 * (1 - pchi)

    GN = np.array([[dN1x, dN2x, dN3x, dN4x],
                   [dN1y, dN2y, dN3y, dN4y]])

    node_coord = np.array([xnode_el,
                           ynode_el])

    jacobian = np.dot(GN, node_coord.T)

    return GN, jacobian


def derShapeFunctionIsoQuad(jacobian, GN):
    j_inv = np.linalg.inv(jacobian)
    derShapeFunc = np.dot(j_inv, GN)

    return derShapeFunc


def shapeFunctionBilin(chi, eta):
    iso_node = np.array([[-1, 0, 1]])

    N_linx = shapeFunctionLin(chi, iso_node[0])
    N_liny = shapeFunctionLin(eta, iso_node[0])

    N0 = N_linx[0, 0] * N_liny[0, 0]
    N1 = N_linx[0, 2] * N_liny[0, 0]
    N2 = N_linx[0, 2] * N_liny[0, 2]
    N3 = N_linx[0, 0] * N_liny[0, 2]
    N4 = N_linx[0, 1] * N_liny[0, 0]
    N5 = N_linx[0, 2] * N_liny[0, 1]
    N6 = N_linx[0, 1] * N_liny[0, 2]
    N7 = N_linx[0, 0] * N_liny[0, 1]
    N8 = N_linx[0, 1] * N_liny[0, 1]

    return np.array([[N0, N1, N2, N3, N4, N5, N6, N7, N8]])


def jacobianBilin(chi, eta, nodeCoords):
    iso_node = np.array([[-1, 0, 1]])

    N_linx = shapeFunctionLin(chi, iso_node[0])
    N_liny = shapeFunctionLin(eta, iso_node[0])
    B_linx = derShapeFunctionLin(chi, iso_node[0])
    B_liny = derShapeFunctionLin(eta, iso_node[0])

    GN0x = B_linx[0, 0] * N_liny[0, 0]
    GN1x = B_linx[0, 2] * N_liny[0, 0]
    GN2x = B_linx[0, 2] * N_liny[0, 2]
    GN3x = B_linx[0, 0] * N_liny[0, 2]
    GN4x = B_linx[0, 1] * N_liny[0, 0]
    GN5x = B_linx[0, 2] * N_liny[0, 1]
    GN6x = B_linx[0, 1] * N_liny[0, 2]
    GN7x = B_linx[0, 0] * N_liny[0, 1]
    GN8x = B_linx[0, 1] * N_liny[0, 1]

    GN0y = N_linx[0, 0] * B_liny[0, 0]
    GN1y = N_linx[0, 2] * B_liny[0, 0]
    GN2y = N_linx[0, 2] * B_liny[0, 2]
    GN3y = N_linx[0, 0] * B_liny[0, 2]
    GN4y = N_linx[0, 1] * B_liny[0, 0]
    GN5y = N_linx[0, 2] * B_liny[0, 1]
    GN6y = N_linx[0, 1] * B_liny[0, 2]
    GN7y = N_linx[0, 0] * B_liny[0, 1]
    GN8y = N_linx[0, 1] * B_liny[0, 1]

    GN_9Q = np.array([[GN0x, GN1x, GN2x, GN3x, GN4x, GN5x, GN6x, GN7x, GN8x],
                      [GN0y, GN1y, GN2y, GN3y, GN4y, GN5y, GN6y, GN7y, GN8y]])

    jacobian = np.matmul(GN_9Q, nodeCoords)

    return jacobian, GN_9Q

