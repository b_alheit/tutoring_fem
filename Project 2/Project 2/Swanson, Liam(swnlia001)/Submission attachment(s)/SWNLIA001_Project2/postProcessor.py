import numpy as np
from interpolationFunctions import shapeFunctionCartTri, shapeFunctionIsoQuad


def postProcessTri(temp_global, nodeCoord_el, xnode_el, ynode_el, ICA_global, n_sample):
    """
    Function which generates an array of plotting points of the FEM solution for displacement and stress based on
    the interpolation using shape functions and their derivatives
    :param n_el: number of elements
    :param n_order: order of the elements
    :param temp_global: global nodal displacement matrix
    :param ICA_global: ICA
    :param xnode_el: global element nodal coordinates
    :param n_sample: number of sample points over domain
    :param temperature: True if finding L2Norm of displacement error. False if for stress.
    :return: array of plotting values
    """

    n_el = len(ICA_global)
    n_elnodes = len(ICA_global[0])

    # Organise the nodal temperatures into array according to element numbering
    T_element = np.zeros(ICA_global.shape)
    for y in range(n_el):
        for z in range(n_elnodes):
            T_element[y, z] = temp_global[ICA_global[y, z]]

    x_elSpace = np.linspace(np.amin(xnode_el), np.amax(xnode_el), n_sample)
    y_elSpace = np.linspace(np.amin(ynode_el), np.amax(ynode_el), n_sample)
    T_dist = np.zeros((n_sample, n_sample))
    X, Y = np.meshgrid(x_elSpace, y_elSpace)

    for j in range(n_sample):
        for k in range(n_sample):
            xs = X[j, k]
            ys = Y[j, k]

            included_points = False
            for i in range(n_el):
                el_nodes = nodeCoord_el[i]
                N_el = shapeFunctionCartTri(el_nodes, [xs, ys])

                for t in N_el[0]:
                    if 0 <= t <= 1:
                        included_points = True
                    else:
                        included_points = False
                        break

                if included_points is True:
                    T_dist[j, k] = np.dot(T_element[i], N_el.T)
                    break

                else:
                    continue

            if included_points is False:
                T_dist[j, k] = np.nan

    return T_dist, X, Y


def postProcessQuad(eltemp, elnode_x, elnode_y, n_sample):

    chi_space = np.linspace(-1, 1, n_sample)
    eta_space = np.linspace(-1, 1, n_sample)

    CHI, ETA = np.meshgrid(chi_space, eta_space)

    Tmesh_el = np.zeros((n_sample, n_sample))
    xmesh = np.zeros((n_sample, n_sample))
    ymesh = np.zeros((n_sample, n_sample))

    for Trow in range(n_sample):
        for Tcol in range(n_sample):
            etas = ETA[Trow, Tcol]
            chis = CHI[Trow, Tcol]
            N_el = shapeFunctionIsoQuad(chis, etas)

            x_cart = np.dot(elnode_x, N_el.T)
            y_cart = np.dot(elnode_y, N_el.T)
            Tsp = np.dot(eltemp, N_el.T)

            Tmesh_el[Trow, Tcol] = Tsp
            xmesh[Trow, Tcol] = x_cart
            ymesh[Trow, Tcol] = y_cart

    return Tmesh_el, xmesh, ymesh
