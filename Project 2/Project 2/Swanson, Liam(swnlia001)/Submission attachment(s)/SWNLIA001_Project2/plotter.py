import numpy as np
import matplotlib.pyplot as plt


def meshPlotter(nodeList, ICA_global, el_cent, fig_axis, linop, centop, numop):

    node_ID = np.zeros((1, len(nodeList)))
    el_ID = 0

    for ICA_el in ICA_global:
        # Plot elements and their centroids
        for i in range(-1, len(ICA_el) - 1):
            fig_axis.plot([nodeList[ICA_el[i], 0], nodeList[ICA_el[i + 1], 0]],
                          [nodeList[ICA_el[i], 1], nodeList[ICA_el[i + 1], 1]],
                          color='black', marker='o', alpha=linop)

        fig_axis.plot(el_cent[el_ID][0], el_cent[el_ID][1], color='red', marker='*', alpha=centop)

        elbox_props = dict(boxstyle="square,pad=0.3", fc="yellow", ec="y", lw=1, alpha=numop)
        fig_axis.text(el_cent[el_ID][0] + 0.1, el_cent[el_ID][1], str(el_ID), ha="center", va="center", size=7,
                      bbox=elbox_props, alpha=numop)

        for k in ICA_el:
            if node_ID[0, k] != 1:
                nodebox_props = dict(boxstyle="circle,pad=0.3", fc="cyan", ec="b", lw=1, alpha=numop)
                fig_axis.text(nodeList[k, 0], nodeList[k, 1], str(k), ha="center", va="center", size=7,
                              bbox=nodebox_props, alpha=numop)
                node_ID[0, k] = 1

            else:
                continue

        el_ID += 1

    return fig_axis


def quadMeshPlotter(nodeList, ICA_global, centroids, fig_axis):

    node_ID = np.zeros((1, len(nodeList)))
    el_ID = 0

    for ICA_el in ICA_global:
        # Plot elements and their centroids
        for i in range(-1, len(ICA_el) - 1):
            fig_axis.plot([nodeList[ICA_el[i], 0], nodeList[ICA_el[i + 1], 0]],
                          [nodeList[ICA_el[i], 1], nodeList[ICA_el[i + 1], 1]], color='black', marker='o')

        fig_axis.plot(centroids[el_ID][0], centroids[el_ID][1], color='red', marker='*')

        elbox_props = dict(boxstyle="square,pad=0.3", fc="yellow", ec="y", lw=1)
        fig_axis.text(centroids[el_ID][0] + 0.1, centroids[el_ID][1], str(el_ID), ha="center", va="center", size=7,
                      bbox=elbox_props)

        for k in ICA_el:
            if node_ID[0, k] != 1:
                nodebox_props = dict(boxstyle="circle,pad=0.3", fc="cyan", ec="b", lw=1)
                fig_axis.text(nodeList[k, 0], nodeList[k, 1], str(k), ha="center", va="center", size=7,
                              bbox=nodebox_props)
                node_ID[0, k] = 1

            else:
                continue

        el_ID += 1

    return fig_axis

