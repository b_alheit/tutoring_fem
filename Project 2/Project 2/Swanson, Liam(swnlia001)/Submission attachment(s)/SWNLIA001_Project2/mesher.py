import numpy as np


def triMesh(nodeList, global_ICA):

    # Initialise elemental coordinate lists
    xCoord_el = []
    yCoord_el = []
    nodeCoord_el = []

    for el_ICA in global_ICA:
        x_node = []
        y_node = []
        nodeCoord = []

        for i in el_ICA:
            x_node.append(nodeList[i, 0])
            y_node.append(nodeList[i, 1])
            nodeCoord.append(nodeList[i])

        xCoord_el.append(x_node)
        yCoord_el.append(y_node)
        nodeCoord_el.append(nodeCoord)

    xCoord_el = np.asarray(xCoord_el)
    yCoord_el = np.asarray(yCoord_el)
    nodeCoord_el = np.asarray(nodeCoord_el)

    # Initialise global lists of element centroid and area
    centroids = []
    areas_el = []

    for ICA_el in global_ICA:
        # Initialise element centroid values
        centroid_el = []
        x_sum = 0
        y_sum = 0

        # Calculate element centroids
        for j in ICA_el:
            x_sum += nodeList[j, 0]
            y_sum += nodeList[j, 1]

        x_cent = x_sum / len(ICA_el)
        y_cent = y_sum / len(ICA_el)

        # Create element centroid co-ordinate vector
        centroid_el.append(x_cent)
        centroid_el.append(y_cent)

        # Add element centroid co-ordinate vector to global array
        centroids.append(centroid_el)

        # Calculate area of element
        a = abs((nodeList[ICA_el[0], 0] * (nodeList[ICA_el[1], 1] - nodeList[ICA_el[2], 1]) +
                 nodeList[ICA_el[1], 0] * (nodeList[ICA_el[2], 1] - nodeList[ICA_el[0], 1]) +
                 nodeList[ICA_el[2], 0] * (nodeList[ICA_el[0], 1] - nodeList[ICA_el[1], 1])) / 2)

        # Append area to global
        areas_el.append(a)

    return xCoord_el, yCoord_el, nodeCoord_el, areas_el, centroids


def quadMesh(nodeList, global_ICA):

    # Initialise elemental coordinate lists
    xCoord_el = []
    yCoord_el = []
    nodeCoord_el = []

    for el_ICA in global_ICA:
        x_node = []
        y_node = []
        nodeCoord = []

        for i in el_ICA:
            x_node.append(nodeList[i, 0])
            y_node.append(nodeList[i, 1])
            nodeCoord.append(nodeList[i])

        xCoord_el.append(x_node)
        yCoord_el.append(y_node)
        nodeCoord_el.append(nodeCoord)

    xCoord_el = np.asarray(xCoord_el)
    yCoord_el = np.asarray(yCoord_el)
    nodeCoord_el = np.asarray(nodeCoord_el)

    # Initialise global lists of element centroid and area
    centroids = []

    for ICA_el in global_ICA:
        # Calculate a point near the centre
        x_cent = (nodeList[ICA_el[0], 0] + nodeList[ICA_el[1], 0] +
                  nodeList[ICA_el[2], 0] + nodeList[ICA_el[3], 0]) / 4
        y_cent = (nodeList[ICA_el[0], 1] + nodeList[ICA_el[1], 1] +
                  nodeList[ICA_el[2], 1] + nodeList[ICA_el[3], 1]) / 4

        # Add element centroid co-ordinate vector to global array
        centroids.append([x_cent, y_cent])

    return xCoord_el, yCoord_el, nodeCoord_el, centroids
