import numpy as np


def systemSolver(K, d, F, n_Enodes):
    # Initialise solved d and F vectors
    d_s = np.zeros(d.shape)
    F_s = np.zeros(F.shape)

    # Stiffness matrix partition
    Kp_E = K[:n_Enodes, :n_Enodes]
    Kp_EF = K[:n_Enodes, n_Enodes:]
    Kp_F = K[n_Enodes:, n_Enodes:]

    # Displacement matrix partition
    dp_E = d[:n_Enodes]

    # Force matrix partition
    fp_F = F[n_Enodes:]

    # Solve for unknown displacements
    dp_F = np.dot(np.linalg.inv(Kp_F), (fp_F - np.dot(Kp_EF.T, dp_E)))

    # Rebuild global displacement vector using solved values
    d_s[:n_Enodes] = dp_E
    d_s[n_Enodes:] = dp_F

    # Solve for reaction forces
    rp_E = np.dot(Kp_E, dp_E) + np.dot(Kp_EF, dp_F)

    # Replace nodal value of reaction force in Global Force vector
    F_s[:n_Enodes] = rp_E
    F_s[n_Enodes:] = fp_F

    return d_s, F_s
