# Project 2 Liam Swanson
# MEC5063Z
# April 26 2019

import numpy as np
import math
from interpolationFunctions import derShapeFunctionCartTri, shapeFunctionIsoTri, \
    shapeFunctionIsoLin, shapeFunctionIsoQuad, derShapeFunctionIsoQuad, jacobianQuad
from gaussQuadrature import gaussQuadratureInfoTri, gaussQuadratureInfoLin


def source(x, y):
    return 1 / 2 * (8 - x) ** 2 * (4 - y) ** 2


def neumannSource(x, y):
    if abs(x - 0) <= 1e-10:
        return -3 * (y ** 2)

    else:
        return 0


def stiffnessMatrix(nodeCoord_el, area_el, conduct, ICA_global, xnode_el, ynode_el, order_gq, tri):
    # Calculate number of nodes and elements
    num_nodes = np.amax(ICA_global) + 1
    num_el = len(ICA_global)

    # Initialise the global stiffness matrix
    K_global = np.zeros((num_nodes, num_nodes))

    # Create conductivity matrix
    D = np.array([[conduct, 0],
                  [0, conduct]])

    # Loop through each element, calculate local stiffness matrix and place place into global matrix
    for i in range(num_el):
        if tri is True:
            # print('iteration: ', i)
            B = derShapeFunctionCartTri(nodeCoord_el[i])

            K_el = np.matmul(B.T, np.matmul(D, B)) * area_el[i]
            # print('K_el:', i, '\n',K_el)

        if tri is False:
            # Get gauss quad values
            iso_loc, iso_w = gaussQuadratureInfoLin(order_gq - 1)
            K_el = 0
            for j in range(order_gq):
                sum_chi = 0
                for k in range(order_gq):
                    derIso, J = jacobianQuad(xnode_el[i], ynode_el[i], iso_loc[k], iso_loc[j])
                    B = derShapeFunctionIsoQuad(J, derIso)
                    detJ = np.linalg.det(J)

                    intergrand = np.matmul(B.T, np.matmul(D, B))
                    sum_chi += detJ * intergrand * iso_w[j] * iso_w[k]

                K_el += sum_chi

        ICA_el = ICA_global[i]

        for j in range(len(K_el)):
            for k in range(len(K_el)):
                K_global[ICA_el[j], ICA_el[k]] += K_el[j, k]


    return K_global


def bodyForce(xnode_el, ynode_el, area_el, ICA_global, order_gq, tri):
    # Initialise global body force vector
    num_nodes = np.amax(ICA_global) + 1
    Fb_vector = np.zeros((num_nodes, 1))

    # Loop through all elements of the geometry

    for i in range(len(ICA_global)):
        # Initialise element body force vector
        Fb_el = 0

        # Get element geometry
        nodes_x = xnode_el[i]
        nodes_y = ynode_el[i]

        if tri is True:
            # Get element geometry
            a_el = area_el[i]

            # Get qauss quadrature information
            iso_loc, iso_w = gaussQuadratureInfoTri(order_gq - 1)

            # Gauss quadrature
            for j in range(order_gq):
                N_iso = shapeFunctionIsoTri(iso_loc[j, 0], iso_loc[j, 1])
                x1 = np.dot(N_iso, nodes_x)[0]
                y1 = np.dot(N_iso, nodes_y)[0]

                integrand = N_iso * source(x1, y1)
                Fb_el += a_el * integrand.T * iso_w[j]

        if tri is False:
            # Get qauss quadrature information
            iso_loc, iso_w = gaussQuadratureInfoLin(order_gq - 1)

            # Gauss Quadrature
            for j in range(order_gq):
                sum_chi = 0
                for k in range(order_gq):
                    derIso, J = jacobianQuad(xnode_el[i], ynode_el[i], iso_loc[k], iso_loc[j])
                    N_iso = shapeFunctionIsoQuad(iso_loc[k], iso_loc[j])
                    detJ = np.linalg.det(J)

                    x1 = np.dot(N_iso, nodes_x)[0]
                    y1 = np.dot(N_iso, nodes_y)[0]

                    intergrand = N_iso * source(x1, y1)
                    sum_chi += detJ * intergrand.T * iso_w[j] * iso_w[k]

                Fb_el += sum_chi

        # Place nodal values into global array
        node_count = 0
        for m in ICA_global[i]:
            Fb_vector[m] += Fb_el[node_count]
            node_count += 1

    return Fb_vector


def boundaryForce(xnode_el, ynode_el, ICA_global, order_gq, tri):
    # Initialise global body force vector
    num_nodes = np.amax(ICA_global) + 1
    Fb_vector = np.zeros((num_nodes, 1))

    # Get qauss quadrature information
    iso_loc, iso_w = gaussQuadratureInfoLin(order_gq - 1)

    # Define iso-parametric triangle
    node_iso = np.array([[1, 0],
                         [0, 1],
                         [0, 0]])

    edge_ICA = np.array([[0, 1],
                         [1, 2],
                         [2, 0]])

    # Define iso-parametric quadrilateral
    node_isoQ = np.array([[-1, -1],
                          [1, -1],
                          [1, 1],
                          [-1, 1]])

    edge_ICAQ = np.array([[0, 1],
                         [1, 2],
                         [2, 3],
                         [3, 0]])

    # Loop through all elements of the geometry
    for i in range(len(ICA_global)):

        # Initialise element body force vector
        Fb_el = 0

        # Get element geometry
        nodes_x = xnode_el[i]
        nodes_y = ynode_el[i]

        if tri is True:
            # Loop through each edge in the isoparametric tri element
            for k in edge_ICA:
                edgeNode_chi = np.zeros((1, len(k)))
                edgeNode_eta = np.zeros((1, len(k)))

                node_count = 0
                for node in k:
                    edgeNode_chi[0, node_count] = node_iso[node, 0]
                    edgeNode_eta[0, node_count] = node_iso[node, 1]
                    node_count += 1

                # Find nodes bounding edge in cartesian space and calculate jacobian
                x1 = np.dot(shapeFunctionIsoTri(edgeNode_chi[0, 0], edgeNode_eta[0, 0]), nodes_x)
                y1 = np.dot(shapeFunctionIsoTri(edgeNode_chi[0, 0], edgeNode_eta[0, 0]), nodes_y)
                x2 = np.dot(shapeFunctionIsoTri(edgeNode_chi[0, 1], edgeNode_eta[0, 1]), nodes_x)
                y2 = np.dot(shapeFunctionIsoTri(edgeNode_chi[0, 1], edgeNode_eta[0, 1]), nodes_y)

                len_12 = math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)

                J = len_12 / 2
                # print('J: ', i,'\n', J)

                # Initialise gauss sum
                gauss_sum = 0

                for m in range(order_gq):
                    # In 1D Isoparametric space get the GQ point, weight and the 1D shape function
                    chi_1 = iso_loc[m]
                    weight = iso_w[m]
                    N_lin = shapeFunctionIsoLin(chi_1)

                    # Using the 2D isoparametric triangle edge, find the chi and eta point relating to 1D GQ point
                    chi_2 = np.dot(N_lin[0], edgeNode_chi[0].T)
                    eta_2 = np.dot(N_lin[0], edgeNode_eta[0].T)

                    # Using the chi and eta value get the isoparametric triangle shape functions
                    N_iso = shapeFunctionIsoTri(chi_2, eta_2)        # 1x3 matrix

                    # Get x and y cartesian point relating to chi and eta
                    x_gq = np.dot(N_iso[0], nodes_x.T)
                    y_gq = np.dot(N_iso[0], nodes_y.T)

                    # Calculate value of boundary source
                    q_boundary = neumannSource(x_gq, y_gq)

                    # Do gauss summation
                    gauss_sum += N_iso.T * q_boundary * weight * J

                Fb_el += gauss_sum

        if tri is False:
            # Loop through each edge in the isoparametric quad element
            for k in edge_ICAQ:
                edgeNode_chi = np.zeros((1, len(k)))
                edgeNode_eta = np.zeros((1, len(k)))

                node_count = 0
                for node in k:
                    edgeNode_chi[0, node_count] = node_isoQ[node, 0]
                    edgeNode_eta[0, node_count] = node_isoQ[node, 1]
                    node_count += 1

                # Find nodes bounding edge in cartesian space and calculate jacobian
                x1 = np.dot(shapeFunctionIsoQuad(edgeNode_chi[0, 0], edgeNode_eta[0, 0]), nodes_x)
                y1 = np.dot(shapeFunctionIsoQuad(edgeNode_chi[0, 0], edgeNode_eta[0, 0]), nodes_y)
                x2 = np.dot(shapeFunctionIsoQuad(edgeNode_chi[0, 1], edgeNode_eta[0, 1]), nodes_x)
                y2 = np.dot(shapeFunctionIsoQuad(edgeNode_chi[0, 1], edgeNode_eta[0, 1]), nodes_y)

                len_12 = math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)

                J = len_12 / 2
                # print('J: ', i,'\n', J)

                # Initialise gauss sum
                gauss_sum = 0

                for m in range(order_gq):
                    # In 1D Isoparametric space get the GQ point, weight and the 1D shape function
                    chi_1 = iso_loc[m]
                    weight = iso_w[m]
                    N_lin = shapeFunctionIsoLin(chi_1)

                    # Using the 2D isoparametric quad edge, find the chi and eta point relating to 1D GQ point
                    chi_2 = np.dot(N_lin[0], edgeNode_chi[0].T)
                    eta_2 = np.dot(N_lin[0], edgeNode_eta[0].T)

                    # Using the chi and eta value get the isoparametric quad shape functions
                    N_iso = shapeFunctionIsoQuad(chi_2, eta_2)  # 1x3 matrix

                    # Get x and y cartesian point relating to chi and eta
                    x_gq = np.dot(N_iso[0], nodes_x.T)
                    y_gq = np.dot(N_iso[0], nodes_y.T)

                    # Calculate value of boundary source
                    q_boundary = neumannSource(x_gq, y_gq)

                    # Do gauss summation
                    gauss_sum += N_iso.T * q_boundary * weight * J

                Fb_el += gauss_sum

        # Place element vector into global array
        node_count = 0
        for n in ICA_global[i]:
            Fb_vector[n] += Fb_el[node_count]
            node_count += 1

    return Fb_vector