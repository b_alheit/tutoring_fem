# Project 2 Liam Swanson
# MEC5063Z
# April 26 2019

import numpy as np
import math
import matplotlib.pyplot as plt
from plotter import meshPlotter, quadMeshPlotter
from mesher import triMesh, quadMesh
from ghalerkinApproximation import stiffnessMatrix, bodyForce, boundaryForce
from partitionSolver import systemSolver
from postProcessor import postProcessTri, postProcessQuad
from interpolationFunctions import jacobianBilin, shapeFunctionCartTri, derShapeFunctionCartTri


# Define material properties
k = 5               # W/(cm.C)
T_dirichlet = 0     # C

# Solver properties
n_gp2D = 3          # order of gauss quadrature for body integral
n_gp1D = 3          # order of gauss quadrature for surface integral

# Define all node coordinates where Dirichlet BC nodes numbered first for Elements
nodeCoord = np.array([[0, 0],
                     [4, 0],
                     [8, 0],
                     [8, 2],
                     [7, -math.sqrt(4 - (7 - 8) ** 2) + 4],
                     [-math.sqrt(4 - (3 - 4) ** 2) + 8, 3],
                     [6, 4],
                     [4, 4],
                     [0, 4],
                     [0, 1],
                     [4, 3]])

# Define inter connectivity array ensuring right handed numbering for Tris
ICA = np.array([[0, 1, 9],
                [9, 1, 10],
                [8, 9, 10],
                [7, 8, 10],
                [1, 2, 4],
                [2, 3, 4],
                [1, 4, 5],
                [1, 5, 10],
                [10, 5, 7],
                [5, 6, 7]])

# Store global number of nodes
numNodes = len(nodeCoord)


# Define dirichlet boundary
def dirichletBoundary(nodeCoordinate):
    if abs(nodeCoordinate[1] - 0) == 0:
        return True

    else:
        return False


# Count number of dirichlet nodes and apply dirichlet BC to nodal temperature matrix
dirichlet_count = 0
d = np.zeros((len(nodeCoord), 1))

for i in range(numNodes):
    if dirichletBoundary(nodeCoord[i]) is True:
        d[i, 0] = T_dirichlet
        dirichlet_count += 1

# **************************************
# QUESTION 1
# **************************************

# Generate mesh of 3-node triangles
elNode_x, elNode_y, elNodes, a_el, cent_el = triMesh(nodeCoord, ICA)

# Calculate stiffness matrix
K = stiffnessMatrix(elNodes, a_el, k, ICA, elNode_x, elNode_y, n_gp1D, True)

# Calculate body source vector
Fb_source = bodyForce(elNode_x, elNode_y, a_el, ICA, n_gp2D, True)

# Calculate Neumann surface vector
Fb_surface = boundaryForce(elNode_x, elNode_y, ICA, n_gp1D, True)

# Calculate total Force Vector
F_ghal = Fb_source - Fb_surface


# Partitioning
# **************************************
T, F = systemSolver(K, d, F_ghal, dirichlet_count)


# Post-Processing
# **************************************
sample_points = 50
T_sample, X_grid, Y_grid = postProcessTri(T, elNodes, elNode_x, elNode_y, ICA, sample_points)


# Plotting
# **************************************
# Mesh plot
fig1, mesh = plt.subplots()
mesh = meshPlotter(nodeCoord, ICA, cent_el, mesh, linop=1, centop=1, numop=1)
mesh.set_title('Triangular Mesh Node and Element Numbering')
mesh.set_xlabel('x (cm)')
mesh.set_ylabel('y (cm)')

# Temperature Distribution Plot for Tri Elements
fig0, temp = plt.subplots()
cplotT = temp.contourf(X_grid, Y_grid, T_sample, 50, cmap='jet')
cbarT = fig0.colorbar(cplotT)

temp.set_title('Temperature Distribution Across Domain')
temp.set_xlabel('x (cm)')
temp.set_ylabel('y (cm)')
cbarT.set_label('Temperature (C)')

# Uncomment to overlay mesh
# meshPlotter(nodeCoord, ICA, cent_el, temp, linop=0.25, centop=1, numop=1)

# **************************************
# QUESTION 1E
# **************************************
AB_elnode = np.array([elNodes[3],
                       elNodes[9]])

y_plot = 4
x_plot = np.linspace(0, 6, 100)
T_plot = np.zeros((1, 100))
Q_plot = np.zeros((1, 100))
T_elAB = np.array([[T[7, 0], T[8, 0], T[10, 0]],
                   [T[5, 0], T[6, 0], T[7, 0]]])

for AB_k in range(100):
    xs = x_plot[AB_k]
    ys = y_plot

    included_points = False
    for AB_i in range(2):
        el_nodesAB = AB_elnode[AB_i]
        N_el = shapeFunctionCartTri(el_nodesAB, [xs, ys])
        B_el = derShapeFunctionCartTri(el_nodesAB)

        for t in N_el[0]:
            if 0 <= t <= 1:
                included_points = True
            else:
                included_points = False
                break

        if included_points is True:
            T_plot[0, AB_k] = np.dot(T_elAB[AB_i], N_el.T)
            Q_plot[0, AB_k] = np.dot((-k * np.dot(T_elAB[AB_i], B_el.T)), np.array([[0, 1]]).T)
            break

        else:
            continue

    if included_points is False:
        T_plot[0, AB_k] = np.nan
        Q_plot[0, AB_k] = np.nan

fig1ET, ax1ET = plt.subplots()
ax1ET.plot(x_plot, T_plot[0], color='red')
ax1ET.set_title('Temperature distribution plot along the line AB')
ax1ET.set_xlabel('x (cm)')
ax1ET.set_ylabel('Temperature (C)')


fig1EQ, ax1EQ = plt.subplots()
ax1EQ.plot(x_plot, Q_plot[0], color='blue')
ax1EQ.set_title('Heat Flux distribution plot along the line AB')
ax1EQ.set_xlabel('x (cm)')
ax1EQ.set_ylabel('Heat flux (W/cm)')

# **************************************
# QUESTION 2A
# **************************************

# Node positions don't change and so use same node coordinate matrix
# Define inter connectivity array ensuring right handed numbering for Quads
ICA_q = np.array([[0, 1, 10, 9],
                  [7, 8, 9, 10],
                  [1, 2, 3, 4],
                  [4, 5, 10, 1],
                  [5, 6, 7, 10]])

# Store number of elements and number of nodes
n_el = len(ICA_q)
n_elnodes = len(ICA_q[0])

# Generate mesh of 4 node quad
Q_nodex, Q_nodey, Qnode_coord, Q_centres = quadMesh(nodeCoord, ICA_q)
Q_ael = np.zeros((1, len(ICA_q)))

# Calculate Stiffness Matrix
Q_K = stiffnessMatrix(Qnode_coord, Q_ael, k, ICA_q, Q_nodex, Q_nodey, n_gp1D, False)

# Calculate body source vector
Q_fsource = bodyForce(Q_nodex, Q_nodey, Q_ael, ICA_q, n_gp2D, False)

# Calculate Neumann surface vector
Q_fsurface = boundaryForce(Q_nodex, Q_nodey, ICA_q, n_gp1D, False)

# Find total force vector
QF_ghal = Q_fsource - Q_fsurface


# Partitioning
# **************************************
Q_T, Q_F = systemSolver(Q_K, d, QF_ghal, dirichlet_count)


# Post-Processing
# **************************************

# Organise the nodal temperatures into array according to element numbering
T_element = np.zeros(ICA_q.shape)
for y in range(n_el):
    for z in range(n_elnodes):
        T_element[y, z] = Q_T[ICA_q[y, z]]



# Initialise plotting arrays
Trange = []
Tmeshel = []
xmesh_el = []
ymesh_el = []


for i_el in range(n_el):
    T_el = T_element[i_el]
    xnode_el = Q_nodex[i_el]
    ynode_el = Q_nodey[i_el]

    T_elementmesh, X_el, Y_el = postProcessQuad(T_el, xnode_el, ynode_el, 10)

    Trange.append(np.amax(T_elementmesh))
    Trange.append(np.amin(T_elementmesh))
    Tmeshel.append(T_elementmesh)
    xmesh_el.append(X_el)
    ymesh_el.append(Y_el)

dfg = 0

# Plotting
# **************************************
# Plot Quad Mesh
fig2, Qmesh = plt.subplots()
quadMeshPlotter(nodeCoord, ICA_q, Q_centres, Qmesh)
Qmesh.set_title('Quad Mesh Node and Element Numbering')
Qmesh.set_xlabel('x (cm)')
Qmesh.set_ylabel('y (cm)')

# Plot Temperature distribution
lvls = 50

# Initialise plot
fig3, Tdist = plt.subplots()

for j_el in range(n_el):
    cplotQ = Tdist.contourf(xmesh_el[j_el], ymesh_el[j_el], Tmeshel[j_el],
                            np.linspace(min(Trange), max(Trange), lvls), cmap='jet')

cbarQ = fig3.colorbar(cplotQ)
cbarQ.set_label('Temperature (C)')
Tdist.set_title('Temperature Distribution Across Domain')
Tdist.set_xlabel('x (cm)')
Tdist.set_ylabel('y (cm)')

# **************************************
# QUESTION 2C
# **************************************
a = -math.sqrt(4 - (3 - 4) ** 2) + 8
b = -math.sqrt(4 - (7 - 8) ** 2) + 4
nodeCoord_c = np.array([[7, b],
                        [a, 3],
                        [4, 3],
                        [4, 0],
                        [(7 + a) / 2, (b + 3) / 2],
                        [(4 + a) / 2, (3 + 3) / 2],
                        [(4 + 4) / 2, (0 + 3) / 2],
                        [(4 + 7) / 2, (b) / 2],
                        [Q_centres[3][0], Q_centres[3][1]]])

MP = [(7 + a) / 2, (b + 3) / 2]
xn_9Q = np.zeros((1, 9))
yn_9Q = np.zeros((1, 9))

count = 0
for node in nodeCoord_c:
    xn_9Q[0, count] = node[0]
    yn_9Q[0, count] = node[1]
    count += 1

# Mesh parameters
samples = 11
chi_J = np.linspace(-1, 1, samples)
eta_J = np.linspace(-1, 1, samples)
CHI, ETA = np.meshgrid(chi_J, eta_J)
detJ = np.zeros((samples, samples))

for ieta in range(samples):
    for ichi in range(samples):
        jacobian, GN_biq = jacobianBilin(chi_J[ichi], eta_J[ieta], nodeCoord_c)
        detJ[ieta, ichi] = np.linalg.det(jacobian)

# Plot determinant
fig2, determinant = plt.subplots()
determinant.set_title('Determinant of the Jacobian Matrix Across the Isoparametrix \n '
                      'Biquadratic Element of element [4]')
determinant.set_xlabel('x (cm)')
determinant.set_ylabel('y (cm)')

cplotJ = determinant.contourf(CHI, ETA, detJ, 500, cmap='jet')
cbarT = fig2.colorbar(cplotJ)
cbarT.set_label('Jacobian Determinant')


plt.show()