import numpy as np
from numpy import zeros, ones
# from numpy.core._multiarray_umath import ndarray
from scipy.linalg import det, inv
import matplotlib.pyplot as plt
import matplotlib.tri as mtri
import matplotlib.tri as tri


# Quad_rule = 3
# Gauss POINTS for Quadrature
xi = np.array([1/6, 2/3, 1/6])
eta = np.array([1/6, 1/6, 2/3])
Weight = np.array([1/3, 1/3, 1/3])


ac = 8-(3**0.5)
bc = 4-(3**0.5)
# The Global X and Y values
x_e = np.array([0, 4, 8, 8, 7, ac, 6, 4, 0, 0, 4])
y_e = np.array([0, 0, 0, 2, bc, 3, 4, 4, 4, 1, 3])

# The inter-connectivity array
ICA = np.array([[0, 1, 9], [1, 2, 4], [2, 3, 4], [1, 4, 5], [1, 5, 10], [10, 5, 7], [5, 6, 7], [10, 7, 8],
                [8, 9, 10], [9, 1, 10]])

# Its wise to number the nodes, such that for bc T = 0, we have that as d(0,1,2) for simplicity

nel = 10  # Number of elements
nn = nel + 1  # number of nodes

# initializing matrices
M = ones((3, 3))
K = zeros((11, 11))
F = zeros((11, 1))
d = zeros((11, 1))
T = zeros((11, 1))
Q = zeros((11, 1))
flux = zeros((3, 1))

# conductivity matrix
kc = 5
D = np.array([[kc, 0], [0, kc]])

##########################################################
# Solving for boundary AE, generate a loop to get the flux
J = np.array([1, 3])  # Jacobian for 1D
L = np.array([1, 5])

# Gauss points
gauss_points = np.array([1/3, 1/3])
W = np.array([1.0, 1.0])

for j in range(2):
    ICAq = np.array([[0, 1, 9],
                     [8, 9, 10]])  # ICA FOr element 1 and 3
    asmq = ICAq[j, :]

    X_e = x_e[asmq]  # elements x's
    Y_e = y_e[asmq]  # elements y's

    # generate M matrix for each element.
    M[:, 1] = X_e
    M[:, 2] = Y_e

    # Initial value for GQ
    flux = 0

    for v in range(2):
        yq = ((L[j]) / 2) + ((gauss_points[v] * J[j]) / 2)  # y

        # shape functions
        n1q = X_e[1] * Y_e[2] - X_e[2]*Y_e[1] + (X_e[2] - X_e[1]) *yq
        n2q = X_e[2] * Y_e[0] - X_e[0] * Y_e[2] + (X_e[0] - X_e[2]) * yq
        n3q = X_e[0] * Y_e[1] - X_e[1] * Y_e[0] + (X_e[1] - X_e[0]) * yq
        N = np.array([n1q, n2q, n3q]) / det(M)

        # transformation to iso-parametric coordinates
        y1 = Y_e[0] * N[0] + Y_e[1] * N[1] + Y_e[2] * N[2]

        q1 = 3 * (y1**2)  # flux on AE

        E = (J[j] / 2)*q1  # simplify the gauss quadrature
        flux = flux + np.dot(E, np.transpose(N))  # G_Q flux approx
    for m in range(len(asmq)):
        Q[asmq[m]] += flux[m]  # Global flux Q

##########################################################
# solving for K and F
for a in range(10):  # for each element
    asm = ICA[a, :]  # each row represents an element

    # x amd y for each element
    X_e = x_e[asm]
    Y_e = y_e[asm]

    # generate M matrix for each element.
    M[:, 1] = X_e
    M[:, 2] = Y_e

    A = det(M) / 2.0  # Area

    # gradient shape function taken from textbook
    B = np.array([[Y_e[1] - Y_e[2], Y_e[2] - Y_e[0], Y_e[0] - Y_e[1]],
                  [X_e[2] - X_e[1], X_e[0] - X_e[2], X_e[1] - X_e[0]]]) / det(M)

    # Initial values for GQ
    force_elem = 0
    k_elem = 0

    for i in range(3):  # for each nodes in element
        # shape function)
        n1 = xi[i]
        n2 = eta[i]
        n3 = 1 - eta[i] - xi[i]
        N = np.array([n1, n2, n3])

        # transformation to iso-parametric coordinates
        x = np.dot(X_e, N, out=None)  # X(xi,eta)
        y = np.dot(Y_e, N, out=None)  # Y(xi,eta)

        # source term
        s = 0.5 * (((8 - x) ** 2) * ((4 - y) ** 2))

        # matrix multiplication
        DM = np.dot(np.transpose(B), np.dot(D, B))

        # Force and Stiffness(K) for elements
        force_elem = force_elem + Weight[i] * N * s * A
        k_elem = k_elem + Weight[i] * A * DM

    # compile the global stiffness matrix and the force over the domain
    for z in range(len(asm)):
        K[asm[z], asm] += k_elem[z, :]
        F[asm[z]] += force_elem[z]

# global FORCE,
Q *= 0
Q[0, 0] = 0.25
Q[8, 0] = 42.75
Q[9, 0] = 21
final_F = F + Q

# as previously mentioned, first three values of d are zero since T = 0
d[0:3, :] = np.array([[0], [0], [0]])

# partitioning that matrices
K_F = K[3:11, 3:11]
F_F = final_F[3:11, :]

# nodal displacement
d[3:11, :] = np.dot((np.linalg.inv(K_F)), F_F)

# Temperature approximation
T = n1*d + n2*d + n3*d

print(T)


##########################################################
##########################################################
# plots
# rearrange x,y to u,v such that they are in ascending y values
u = np.asarray([0, 4, 8, 0, 8, ac, 4, 7, 0, 4, 6])
v = np.asarray([0, 0, 0, 1, 2, 3, 3, bc, 4, 4, 4])

triangles = [[0, 1, 3], [1, 2, 7], [2, 4, 7], [1, 7, 5], [1, 5, 6], [6, 5, 9],
             [5, 10, 9], [6, 9, 8], [8, 3, 6], [3, 1, 6]]
triangulation = tri.Triangulation(u, v, triangles)

# Interpolate to regularly-spaced quad grid.
z = np.cos(1.5 * u) * np.cos(1.5 * v)
xi, yi = np.meshgrid(np.linspace(0, 3, 20), np.linspace(0, 3, 20))

interp_lin = mtri.LinearTriInterpolator(triangulation, z)
zi_lin = interp_lin(xi, yi)

# Set up the figure
fig, axs = plt.subplots(nrows=1, ncols=2)
axs = axs.flatten()

# Plot the triangulation.
cm = axs[1].tricontourf(triangulation, z, cmap='YlOrBr')
axs[1].triplot(triangulation, 'ko-')
axs[1].set_title('Temperature for Triangular grid')

# Plot linear interpolation to quad grid.
axs[0].contourf(xi, yi, zi_lin)
axs[0].plot(xi, yi, 'k-', lw=0.5, alpha=0.5)
axs[0].plot(xi.T, yi.T, 'k-', lw=0.5, alpha=0.5)
axs[0].set_title("Linear interpolation")
plt.colorbar(cm)
fig.tight_layout()
plt.show()

