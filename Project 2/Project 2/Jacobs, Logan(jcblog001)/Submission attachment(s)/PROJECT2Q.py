import numpy as np
from numpy import zeros, ones
import scipy.interpolate
from scipy.linalg import det, inv
import matplotlib.pyplot as plt
import matplotlib
import matplotlib.tri as tri

# n_gp = 4
# Gauss POINTS for Quadrature
xi = np.array([-0.8611363116, 0.8611363116, -0.3399810436, 0.3399810436])
eta = np.array([-0.8611363116, 0.8611363116, -0.3399810436, 0.3399810436])
Weight = np.array([0.3478548451, 0.3478548451, 0.6521451549, 0.6521451549])

ac = 8-(3**0.5)
bc = 4-(3**0.5)
# Global X and Y vector
X = np.array([0, 4, 8, 8, 7, ac, 6, 4, 0, 0, 4])
Y = np.array([0, 0, 0, 2, bc, 3, 4, 4, 4, 1, 3])

# define the inter_connectivity array
ICA = np.array([[0, 1, 10, 9], [9, 10, 7, 8], [1, 2, 3, 4], [1, 4, 5, 10], [10, 5, 6, 7]])
# Its wise to number the nodes, such that for bc T = 0, we have that as d(0,1,2) for simplicity

nel = 5  # Number of elements
nn = nel + 6  # number of node

# initializing matrices
M = ones((4, 4))
K = zeros((11, 11))
F = zeros((11, 1))
d = zeros((11, 1))
T = zeros((11, 1))
Q = zeros((11, 1))
flux = zeros((4, 1))

# conductivity matrix
kc = 5
D = np.array([[kc, 0],
              [0, kc]])

##########################################################
# Solving for boundary AE, generate a loop to get the flux
J = np.array([1/2, 3/2])  # Jacobian/2 for 1D

# Gauss points
gauss_points = np.array([-0.8611363116, -0.3399810436, 0.8611363116, 0.3399810436])
W = np.array([0.3478548451, 0.6521451549, 0.3478548451, 0.6521451549])

for i in range(2):
    ICAq = np.array([[0, 1, 10, 9],
                     [9, 10, 7, 8]])  # ICA FOr element 1 and 2
    asmq = ICAq[i, :]
    X_e = X[asmq]  # elements x's
    Y_e = Y[asmq]  # elements y's
    # generate M matrix for each element.
    M[:, 1] = X_e
    M[:, 2] = Y_e

    # Initial value for GQ
    flux = 0

    for v in range(4):
        # shape functions
        N1f = (1 - gauss_points[v])/2
        N2f = 0
        N3f = 0
        N4f = (1 + gauss_points[v]) / 2
        Nf = np.array([N1f, N2f, N3f, N4f])

        # transformation to iso-parametric coordinates
        x1 = X_e[0] * N1f + X_e[1] * N2f + X_e[2] * N3f + X_e[3] * N4f
        y1 = Y_e[0] * N1f + Y_e[1] * N2f + Y_e[2] * N3f + Y_e[3] * N4f

        q = -3*(y1**2)  # flux on AE

        E = (J[i]) * q
        flux = flux + W[v] * np.dot(E, np.transpose(Nf))
    for m in range(len(asmq)):
        Q[asmq[m]] += flux[m]


##########################################################
# solving for K and F
for e in range(5):  # for each element
    asm = ICA[e, :]  # each row represents an element

    # x amd y for each element
    X_e = X[asm]
    Y_e = Y[asm]

    # Initial values for GQ
    force_elem = 0
    k_elem = 0

    # now to sum over etas and xis
    for i in range(4):
        for k in range(4):
            # shape functions
            n1 = 0.25 * ((1 - eta[k]) * (1 - xi[i]))
            n2 = 0.25 * ((1 - eta[k]) * (1 + xi[i]))
            n3 = 0.25 * ((1 + eta[k]) * (1 + xi[i]))
            n4 = 0.25 * ((1 + eta[k]) * (1 - xi[i]))
            N = np.array([n1, n2, n3, n4])

            # transformation to iso-parametric coordinates
            x = np.dot(X_e, N, out=None)
            y = np.dot(Y_e, N, out=None)

            # source term
            s = 0.5 * (((8 - x) ** 2) * (4 - y) ** 2)

            # generalised shape function, GN
            gn = np.array([[0.25 * (eta[k] - 1), 0.25 * (1 - eta[k]), 0.25 * (eta[k] + 1), 0.25 * (-eta[k] - 1)],
                           [0.25 * (xi[i] - 1), 0.25 * (-xi[i] - 1), 0.25 * (xi[i] + 1), 0.25 * (1 - xi[i])]])

            # steps to acquire Jacobian
            m = np.array([X_e, Y_e])
            mt = np.transpose(m)

            j = np.dot(gn, mt)  # Jacobian

            B = np.dot(inv(j), gn)  # gradient shape function

            A = abs(np.linalg.det(j))  # determinant of Jacobian

            # Force and Stiffness(K) for elements
            force_elem = force_elem + Weight[k] * Weight[i] * N * s * A
            k_elem = k_elem + Weight[k] * Weight[i] * A * np.dot(np.dot(np.transpose(B), D), B)

    for z in range(len(asm)):
        K[asm[z], asm] += k_elem[z, :]
        F[asm[z]] += force_elem[z]

# global FORCE
final_F = F - Q

# as previously mentioned, first three values of d are zero since T = 0
d[0:3, :] = np.array([[0], [0], [0]])

# partitioning that matrices
K_F = K[3:11, 3:11]
F_F = final_F[3:11, :]

# nodal displacement
d[3:11, :] = np.dot((np.linalg.inv(K_F)), F_F)

# Temperature approximation
T = n1*d + n2*d + n3*d + n4*d

print(T)

##########################################################
##########################################################
##########################################################
# plots
flx = zeros((2, 4))
xy1 = zeros((2, 4))
for g in range(5):
    asmp = ICA[g, :]

    t_e = T[asmp]
    x_ef = X[asmp]
    y_ef = Y[asmp]

    xy = np.array([X_e, Y_e])
    nz = 0
    for ai in range(4):
        gn = np.array([[0.25 * (eta[ai] - 1), 0.25 * (1 - eta[ai]), 0.25 * (eta[ai] + 1), 0.25 * (-eta[ai] - 1)],
                       [0.25 * (xi[ai] - 1), 0.25 * (-xi[ai] - 1), 0.25 * (xi[ai] + 1), 0.25 * (1 - xi[ai])]])

        mt = np.transpose(xy)

        j = np.dot(gn, mt)  # Jacobian

        B = np.dot(inv(j), gn)  # gradient shape function

        n1 = 0.25 * (1 - eta[ai]) * (1 - xi[ai])
        n2 = 0.25 * (1 - eta[ai]) * (1 + xi[ai])
        n3 = 0.25 * (1 + eta[ai]) * (1 + xi[ai])
        n4 = 0.25 * (1 + eta[ai]) * (1 - xi[ai])
        N = np.array([n1, n2, n3, n4])

        flux = np.dot(-5 * B, t_e) / (np.linalg.norm(np.dot(5 * B, t_e)))
        #print(flx)

        xy1 = xy * N
        #print(xy1[:, 0])
        nz = nz + 1

    flux_elemx = flux[0, :]
    flux_elemy = flux[1, :]

sfdgvsd = 0

#color="None")
#plt.plot(X,Y, marker="o", ls="", color="crimson")
#u = np.asarray([0, 4, 8, 0, 8, ac, 4, 7, 0, 4, 6])
#v = np.asarray([0, 0, 0, 1, 2, 3, 3, bc, 4, 4, 4])
#U, V = np.meshgrid(u, v)
#Z = np.cos(1.5 * U) * np.cos(1.5 * V)
#plt.figure()
#cp = plt.contour(U, V, Z)
#plt.colorbar(cp)
#plt.clabel(cp, inline=True, fontsize=10)
#plt.title('Contour Plot')

#plt.show()
























