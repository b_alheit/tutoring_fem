import numpy as np
from numpy import zeros, ones
from numpy import transpose
from scipy.linalg import det, inv, norm
import matplotlib.pyplot as plt
import matplotlib
#import matplotlib.quad as quad
# inter-connectivity array
ICA = np.array([[0, 1, 10, 9],
                [1, 2, 3, 4],
                [1, 4, 5, 10],
                [10, 5, 6, 7],
                [9, 10, 7, 8]])
# The nodal values
X_nod = np.array([0, 4, 8, 8, 7, 8-(3**0.5), 6, 4, 0, 0, 4])
Y_nod = np.array([0, 0, 0, 2, 4-(3**0.5), 3, 4, 4, 4, 1, 3])
###  isotrapic material
K = 5
D = np.array([[K, 0],
              [0, K]])

# for 4 gauss points we have
eta = np.array([-0.861136, 0.339981, 0.8611363, -0.339981])
epsi = np.array([0.861136, -0.339981,  -0.861136, 0.339981])
Weight = np.array([0.347854, 0.652145, 0.347854, 0.652145])

# define isoparemetric space
def isop_space(epsi, eta, X_nodal, Y_nodal):
    n = np.array([(1 - epsi)*(1 - eta)/4, (1 + epsi)*(1 - eta)/4, (1 + epsi)*(1 + eta)/4, (1 - epsi)*(1 + eta)/4])
    x = np.dot(X_nodal, n)
    y = np.dot(Y_nodal, n)
    s = 0.5 * ((8 - x) ** 2) * ((4 - y) ** 2)
    return x, y, n, s
#define the function source
def s3(x,y):
    s = 0.5 * ((8 - x) ** 2) * ((4 - y) ** 2)
    return s
#define the quadrature that will be used

def quad_body(epsi, eta, X_nodal, Y_nodal):
    GN = np.array([[(eta - 1)/4, (1 - eta)/4, (1 + eta)/4, (-eta - 1)/4],
                   [(epsi - 1)/4, (-epsi - 1)/4, (epsi + 1)/4, (1 - epsi)/4]])
    J = np.dot(GN, np.transpose(np.array([X_nodal, Y_nodal])))
    B_element = np.dot(inv(J), GN)
    k_elem = 5*np.dot(transpose(B_element), B_element)*abs(det(J))

    return k_elem, J


Final_force = zeros((11, 1))
Stiffness = zeros((11, 11))
T = zeros((11, 1))
Q1 = np.array([[-1/8.0], [0], [0], [0], [0], [0], [0], [0], [-42.75], [-20.2968], [0]])
force_elem = zeros((4, 4))
for m in range(len(ICA)):
    asm = ICA[m, :]
    X_e = X_nod[asm]
    Y_e = Y_nod[asm]
    print(asm)
    Force_elem = zeros((4, 4))
    K_elem = zeros((4, 4))
    for j in range(len(epsi)):
        for i in range(len(eta)):

            NN = isop_space(epsi[j], eta[i], X_e, Y_e)[2]
            VV = isop_space(epsi[j], eta[i], X_e, Y_e)[3]
            VV1 = isop_space(epsi[j], eta[i], X_e, Y_e)[1]
            force_elem = np.transpose(NN) * det(quad_body(epsi[j], eta[i], X_e, Y_e)[1]) * VV
            Force_elem += Weight[j] * Weight[i]*force_elem
            K_elem += Weight[j]*Weight[i]*quad_body(epsi[j], eta[i], X_e, Y_e)[0]
    for k in range(len(asm)):
        Stiffness[asm[k], asm] += K_elem[k, :]
        Final_force[asm[k]] += Force_elem[k, 1]
final_F = Final_force - Q1

# lets find the temp at the nodes
T = zeros((11, 1))
T[0:3, :] = np.array([[0],
                      [0],
                      [0]])
K_F = Stiffness[3:11, 3:11]
K_EF = Stiffness[[0, 1, 2], 3:11]
F_F = final_F[3:11, :]
D_E = np.array([[0], [0], [0]])
T[3:11, :] = np.dot((inv(K_F)), (F_F - np.dot(np.transpose(K_EF), D_E)))

# find temp for each element or line
def temperature(epsi, eta,X_nodal, Y_nodal):

   N1 = isop_space(epsi, eta, X_nodal, Y_nodal)[2]
   Temp = N1*T
   return Temp

#The jacobain for the biquadratic element 4

def Jacobian(epsi, eta):
    GN = np.array([[(1-eta)*(eta +2*epsi)/4, (1-eta)*(2*epsi-eta)/4,(1+eta)*(2*epsi+eta)/4, (1+eta)*(2*epsi-eta)/4,-epsi*(1-eta), 0.5*(1-eta**2),-epsi*(1+eta),-0.5*(1-eta**2)],
                   [(1-epsi)*(2*eta+epsi)/4, (1+epsi)*(2*eta-epsi)/4, (1+epsi)*(2*eta +epsi)/4, (1-epsi)*(2*eta-epsi)/4, -0.5*(1-epsi**2), -eta*(1+epsi), 0.5*(1-epsi**2), -eta*(1-epsi)]])

    return GN
#lets find the determinant for each
#for easenes
a  = 8-(3**2)
b  = 4-(3**2)
X_nod1 = np.array([4, 5.5, 7, (a+7)/2, a, (4 +a)/2, 4, 2])
Y_nod1 = np.array([0, b/2, b, (3+b)/2, 3, 3, 3, 3/2])
determinant1 = zeros((2,2))
determinant = zeros((2,2))
# looping over 4 points
for i in range(len(epsi)):
    determinant1 += np.dot(Jacobian(epsi[i], eta[i]),transpose(np.array([X_nod1,Y_nod1])))

    print('determinant for each i,j combo',det(determinant1))
#looping over all comninations6 combinations
for i in range(len(epsi)):
    for j in range(len(eta)):
        determinant += np.dot(Jacobian(epsi[i], eta[j]),transpose(np.array([X_nod1,Y_nod1])))

    print('determinant for each i,j combo',det(determinant))
print('Total determint', det(determinant1))
print('global stiffness', Stiffness)
print('Q', Final_force)
print('displacement', T)
print('global force', final_F)