
import numpy as np
from numpy import zeros, ones
from numpy import transpose
from scipy.linalg import det, inv, norm
import matplotlib.pyplot as plt
import matplotlib.tri as tri
#define the inter_connectivity array

ICA = np.array([[0, 1, 9], [9, 1, 10], [8, 9, 10], [7, 8, 10], [1, 2, 4], [2, 3, 4], [1, 4, 5], [1, 5, 10], [10, 5, 7],
                [5, 6, 7]])

# The nodal values
X_nod = np.array([0, 4, 8, 8, 7, 8-(3**0.5), 6, 4, 0, 0, 4])
Y_nod = np.array([0, 0, 0, 2, 4-(3**0.5), 3, 4, 4, 4, 1, 3])
###  isotrapic material
K = 5
D = np.array([[K, 0],
              [0, K]])

# lets define the isoparametric space

def isop_space(epsi, eta, X_nodal_values, Y_nodal_values):
    n = np.array([epsi, eta, 1-epsi-eta])  # shape functions of a triangular element in this space
    x = np.dot(X_nodal_values, n)          #  the x value with relation to the shape functions
    y = np.dot(Y_nodal_values, n)          # the y values with relation to the shape functions

    return x, y, n

# the number of gauss points in a triangular element
# for each gauss point define weight value

def number_gp(gp):
    # for 4 gauss points
    if gp == 3:
        gpw = np.array([[1 / 6, 1 / 6, 1 / 3],
                        [2 / 3, 1 / 6, 1 / 3],
                        [1 / 6, 2 / 3, 1 / 3]])
        return gpw


# define the quadriture for the body force and the source term(function)
M = ones((3, 3))
def quad_body(i, X_nodal, Y_nodal, body_force):
    M[:, 1] = X_nodal
    M[:, 2] = Y_nodal
    Area = det(M)/2.0
    B_element = np.array([[Y_nodal[1] - Y_nodal[2], Y_nodal[2] - Y_nodal[0], Y_nodal[0] - Y_nodal[1]],
                          [X_nodal[2] - X_nodal[1], X_nodal[0] - X_nodal[2], X_nodal[1] - X_nodal[0]]]) / det(M)
    j = number_gp(i)  # making it easy to call the [ epsi , eta, weight]
    k_element = 5*np.dot(np.transpose(B_element), B_element)*Area
    x_i = isop_space(j[:, 0], j[:, 1], X_nodal, Y_nodal)[0]  # x values with respect to the called epsi and eta
    y_i = isop_space(j[:, 0], j[:, 1], X_nodal, Y_nodal)[1]  # y value with respect to the called epsi and eta
    int_body = 1/3 * body_force(x_i, y_i)    # integrating the source term using gauss quad
    n = isop_space(j[:, 0], j[:, 1], X_nodal, Y_nodal)[2]
    # the force vector
    # shape function with respect to the eat and epsi
    force = n*1/3  # JUST NAMING IT FOR EASE OF PROGRAMMING
    int_body_force_domain = np.dot(force, body_force(x_i, y_i))# integrating the shape function* source term using gauss quad

    return int_body,  k_element, Area

# define a function for force
def quad_force(epsi,eta,X_nodal,Y_nodal, body_force):
    n = isop_space(epsi,eta,X_nodal,Y_nodal)[2]
    x = isop_space(epsi,eta,X_nodal,Y_nodal)[0]
    y = isop_space(epsi, eta, X_nodal, Y_nodal)[1]
    int_body_force_domain = np.dot(n, body_force(x, y))
    return int_body_force_domain


# define the source terms
def S3(x, y):
    S3 = 0.5*((8-x)**2) * ((4-y)**2)  # quadratic complete source term
    return S3


body_force = S3
gp = 3
print('#######################################################')
print('#######################################################')
weight = np.array([1/3, 1/3, 1/3])
epsi = np.array([1/6,1/6,2/3])
eta = np.array([1/6, 2/3, 1/6])

k_elem = []
K = zeros((11, 11))
F = zeros((11, 1))
T = zeros((11, 1))
for e in range(0, 10):
    asm = ICA[e, :]
    X_e = X_nod[asm]
    Y_e = Y_nod[asm]
    A =quad_body(3, X_e, Y_e, body_force)[2]
    force_elem = zeros((3, 3))
    for i in range(len(weight)):
        for j in range(len(epsi)):
            force_elem += weight[i]*0.5*quad_force(epsi[i], eta[j], X_e, Y_e, body_force)
        print(force_elem)
    k_elem = quad_body(gp, X_e, Y_e, body_force)[1]
    for i in range(len(asm)):
        K[asm[i], asm] += k_elem[i, :]
        F[asm[i]] += np.sum(force_elem[i, :])



######## findin the boundary force
#hand calculated
Q1 = np.array([[-1/4.0], [0], [0], [0], [0], [0], [0], [0], [-1/12], [-11/6], [0]])
final_F = F - Q1
T[0:3, :] = np.array([[0], [0], [0]])
K_F = K[3:11, 3:11]
K_EF = K[0:3, 3:11]
F_F = final_F[3:11, :]
D_E = T[0:3, :]
T[3:11, :] = np.dot((inv(K_F)), (F_F - np.dot(np.transpose(K_EF), D_E)))

def temperature(epsi, eta):


    N = np.asarray([epsi, eta, 1 - epsi - eta])
    Temp = N[0] * T + N[1]*T + N[2]*T
    return Temp
########
#Temperature
temp_X = zeros((11, 1))
temp_Y = zeros((11, 1))
for h in range(0, 10):
    asm3 = ICA[h, :]
    X_e = X_nod[asm3]
    Y_e = Y_nod[asm3]
    xy = np.array([X_e, Y_e])
    M[:, 1] = X_e
    M[:, 2] = Y_e
    gp = 3
    j = number_gp(gp)
    temp_elem = temperature(j[:, 0], j[:, 1])[asm3]
    for i in range(len(asm3)):
        temp_X[asm3[i]] += temp_elem[0, i]
        temp_Y[asm3[i]] += temp_elem[1, i]



#x_mesh, y_mesh = np.meshgrid(temp_X, temp_Y)
#Z = x_mesh**2 + y_mesh**2
#np.linalg.dim(Z)
#print(Z)
#cp = plt.contourf(x_mesh, y_mesh, Z, levels=100)
#plt.figure()
#plt.gca().set_aspect('equal')
#plt.triplot(triang, 'bo-', lw=1.0, zorder=2)

#plt.colorbar(cp)
#plt.show()

#triang = tri.Triangulation(X_nod, Y_nod, ICA)
#plt.figure()
#plt.gca().set_aspect('equal')
#plt.triplot(triang, 'bo-', lw=1.0)
#plt.grid()
#plt.show()






print('#######################################################')
print('#######################################################')
print('#######################################################')
print('#######################################################')
print('#######################################################')
print('Global Stiffness matrix', K)
print('Global Force of the problem', final_F)
print('displacement vector =', T)







