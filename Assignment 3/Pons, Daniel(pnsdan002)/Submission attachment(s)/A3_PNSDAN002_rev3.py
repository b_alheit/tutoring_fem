##| Daniel Pons | PNSDAN002 | MEC5063Z | Assignment 3: Triangular Shape Functions
import numpy as np
import matplotlib.pyplot as plt
import scipy.linalg as sp
import statistics as stat
import matplotlib.pyplot as plt
import random as rand
#SEE Bottom for assignment answer.

##  MESH DATA INPUT

x = np.array([1,1.5,2,3,3]) #all x positions
y = np.array([1,0.0,1,1,2]) #all y posiitons
# x = x + 1
ICA = np.array([[1,2,3],
                [2,4,3],
                [3,4,5]])     #each internal array is for an element Using standard


##  Meshing

ICA_0 = ICA-1   #0 indexed ICA for ease of use with position array
X = np.zeros((len(ICA),len(ICA[0]))) #X&Y vectors to store all x-y points for the mesh&plotting
Y = np.zeros((len(ICA),len(ICA[0])))

for k in range(0,len(ICA)): #creates x&y points for each node to be plotted
    X[k] = x[ICA_0[k]]
    Y[k] = y[ICA_0[k]]
#add node one again for plotting purposes

X_plot = np.zeros((len(ICA),len(ICA[0])+1)) #additional point to close triangles
Y_plot = np.zeros((len(ICA),len(ICA[0])+1))
X_bar = np.zeros((len(ICA),1))
Y_bar = np.zeros((len(ICA),1))
for i in range(0,len(ICA)):
    X_plot[i] = np.append(X[i],X[i][0])
    Y_plot[i] = np.append(Y[i],Y[i][0])
    X_bar[i] = np.average(X[i]) #average x-pos
    Y_bar[i] = np.average(Y[i]) #av.y pos
    

##PLOT MESH
fcolour = ['b','g','r','c','m','k','y','k--','b--','g--','r--','c--','m--','y--']    
plt.figure(0) #mesh
for i in range(0,len(ICA)):
    plt.plot(X_plot[i],Y_plot[i],fcolour[0])
    plt.scatter(X_bar[i],Y_bar[i],marker = "*",c="r")
plt.xlabel('x position - (m)')
plt.ylabel("y position - (m)")
plt.title("MESH")
#plt.legend(["Linear","Quadratic","Quartic"])
plt.tight_layout() #make room for axis labels and title
plt.grid()
plt.show()

## p(x,y) Algorithm

#Map triangle to Isoparametric space
def ISO2C(xi,eta,X_nod,Y_nod): #takes xi,eta and returns xy position
    N = np.array([[xi],
                 [eta],
                 [1-xi-eta]])
                 
    x = np.matmul(X_nod,N)
    y = np.matmul(Y_nod,N)
    C_pos = np.array([x,y])
    return C_pos

def C2ISO(x,y,X_nod,Y_nod): #Takes in x&y pos, node arrays of the triangle nodes and returns Xi and ETA
    #constants for conciseness
    x13 = (X_nod[0]-X_nod[2]) #x1-x3
    x23 = (X_nod[1]-X_nod[2]) #x2-x3
    y13 = (Y_nod[0]-Y_nod[2]) #y...
    y23 = (Y_nod[1]-Y_nod[2]) #y...
    x3 = X_nod[2]
    y3 = Y_nod[2]
    #rewrite eqns from C2ISO in matrix form
    K_mat = np.array([[x13,x23],
                      [y13,y23]]) 
    F = np.array([[x-x3],
                 [y-y3]])
    d_xi_eta = np.matmul( np.linalg.inv(K_mat),F) #solve the matrix eqn for xi,eta
    return d_xi_eta
    
def is_p_el(x,y,Nodal_X,Nodal_Y,elnum): #algorithim that checks if a point is contained in the element of choice
    #x,y is point position
    #NodalX&Y are the arrays of the nodal points of X&X
    #elnum is the element of choice
    #global isopos #Checking purposes
    isopos = C2ISO(x,y,Nodal_X[elnum-1],Nodal_Y[elnum-1])
    if isopos[0] < 0 or isopos[1] < 0 or isopos[1] > 1-isopos[0]: 
        #If point is below xi or eta axis or above the xieta line, it is outside the triangle
        #print("Error: point (",x,",",y,") is not found in element:",elnum)
        #print(isopos)
        return False
    else:
        #print(isopos)
        return True

def p_plot(x,y,Nodal_X,Nodal_Y,elnum): #plots point p in mesh,will be a triangle outside element.blue dot otherwise.
    if is_p_el(x,y,Nodal_X,Nodal_Y,elnum) ==True:
        mark = "o"
        colour = "b"
    else:
        mark = "v"
        colour = "r"
    plt.figure(1) #mesh
    for i in range(0,len(ICA)):
        plt.plot(X_plot[i],Y_plot[i],fcolour[0])
        plt.scatter(X_bar[i],Y_bar[i],marker = "*",c="r")
    plt.scatter(x,y,marker = mark,c= colour)    
    plt.xlabel('x position - (m)')
    plt.ylabel("y position - (m)")
    plt.title("MESH")
    plt.tight_layout() #make room for axis labels and title
    plt.grid()
    plt.show()
    
def TRISHAPE(x,y,X_nod,Y_nod,elnum): #Takes in x&y pos, node arrays of the triangle nodes and returns Xi shape fn.
    #note elnum is not zero indexed
    #constants for conciseness
    x1 = X_nod[elnum-1][0] #elnum adjusted for zero indexing
    x2 = X_nod[elnum-1][1]
    x3 = X_nod[elnum-1][2]
    y1 = Y_nod[elnum-1][0]
    y2 = Y_nod[elnum-1][1]
    y3 = Y_nod[elnum-1][2]
    # matrix form or eqn
    N_mat = np.array([[1 ,1 ,1 ],
                      [x1,x2,x3],
                      [y1,y2,y3]]) #straight from formula
    F = np.array([[1],
                  [x],
                  [y]]) 
                
    N_xi = np.matmul( np.linalg.inv(N_mat),F) #solve the matrix eqn for xi shape functions
    return N_xi
    
    
## ASSIGNMENT

def intrplt(x,y,T1,T2,T3): #takes position and nodal temps, note that it depends on all code above.
    T_i = np.array([T1,T2,T3])
    N_xi = TRISHAPE(x,y,X,Y,1)
    p_plot(x,y,X,Y,1)
    if is_p_el(x,y,X,Y,1):
        T_x_y = np.matmul(T_i,N_xi)
        return T_x_y
        
    else:
        print("Error: point (",x,",",y,") is not found in element:",1)
    
    
#example
T1 = 100
T2 = 300
T3 = 50

x_eg = 1.5
y_eg = 0
print(intrplt(x_eg,y_eg,T1,T2,T3))    
    