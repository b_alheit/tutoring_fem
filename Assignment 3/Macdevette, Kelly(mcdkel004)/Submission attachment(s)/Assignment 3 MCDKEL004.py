import numpy as np


def TempInterpolate(x, y, Tnodes):
    xdomain = [1, 1.5, 2]
    ydomain = [1, 0, 1]

    xi = 1-y
    eta = x+0.5*y-1.5

    if xi>1 or eta>1 or xi<0 or eta<0:
        print('Point (%3.2f'%x,',%3.2f'%y,') is outside element')
        return

    etabound = -xi+1

    if eta>etabound:
        print('Point (%3.2f'%x,',%3.2f'%y,') is outside element')
        return

    M = np.ones([3, 3])
    M[1, :] = xdomain
    M[2, :] = ydomain

    p = np.ones(3)
    p[1] = x
    p[2] = y
    N = np.matmul(p, np.linalg.inv(M))

    T = np.dot(N, Tnodes)
    print('T(%3.2f'% x,',%3.2f'% y,')= %1.4f'% T)
    return


TempInterpolate(1.5, 0, np.array([270, 280, 290]))
print()
print('^ This value should be the value of the temperature at Node 2 (280), \nand it definitely shouldn\'t be lower than '
      'the lowest temperature value of all the nodes')