import numpy as np

def Triangle_linear(x, y, T1, T2, T3):
    # nodal displacemnent
    t = np.array([T1, T2, T3])

    # create M matrix
    m = np.array([[1, 1, 1], [0, 1.5, 2], [1, 0, 1]])
    print('M matrix')
    print(m)

    # find M inverse
    m_i= np.linalg.inv(m)
    print('M inverse matrix' )
    print(m_i)

    #print('check inverse')
    #print( np.dot(m, m_i))

    # define p(x,y)
    p = np.array([1, x, y])
    print('p(x,y)=', p)

    # solve for n
    n = np.dot(m_i, p)
    print('N matrix' )
    print(n)
    # Attemmpt at bomus question
    if n[0] <0:
            print('POINT:IES OUTSIDE ELEMENT')
    if n[1] < 0:
        print('POINT:IES OUTSIDE ELEMENT')
    if n[2] < 0:
        print('POINT:IES OUTSIDE ELEMENT')

    elif n[0] >=0 and n[1] >=0 and n[2] >=0:

            Tvalue_c = np.dot(t,n)  #Tvalue
            print('Tvalue;', Tvalue_c)
    #print(Tvalue_c)
    #Tvalue_s = np.sum(Tvalue_c)
    #print(Tvalue_s)

            return(Tvalue_c)




#tester


#Note through multpile testing it was observed that if point is outside element we have one of the componenets in the n vector being negative.
Triangle_linear(1, 1, 281, 256, 300)

# MARKERS COMMENTS
print()
print('^ This value should be the value of the temperature at Node 1 (281).')





