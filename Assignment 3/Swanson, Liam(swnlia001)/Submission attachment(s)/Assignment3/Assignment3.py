# MEC5063Z ASSIGNMENT 3
# LIAM SWANSON
# SWNLIA001
# 3 APRIL 2019

from interpolationFunctions import interpolator
import numpy as np
import matplotlib.pyplot as plt

# create array of nodal positions
nodes = np.array([[1, 1],
                  [1.5, 0],
                  [2, 1],
                  [3, 1],
                  [3, 2]])

# Create ICA
ICA = np.array([[0, 1, 2],
                [1, 2, 3],
                [2, 3, 4]])

# Define nodal temperatures for all elements
unit = 'C'
nodeTemp = np.array([[15],
                     [25],
                     [30],
                     [0],
                     [0]])

# Choose element to look at
n_el = 0

# Extract relevant ICA
el_ICA = ICA[n_el]


# Plot centroids
fig1, ax1 = plt.subplots()

# Find centroid
x_sum = 0
y_sum = 0

for i in el_ICA:
    x_sum += nodes[i, 0]
    y_sum += nodes[i, 1]

x_cent = x_sum / 3
y_cent = y_sum / 3

# Define point in element
p = np.array([[1.5, 1]])

# Check if P lies within element else end code
# Return the interpolated value of T based on the chosen element number and location
p_in, T_p = interpolator(p, nodes, nodeTemp, el_ICA)

if p_in is True:
    print('Point lies within element. \n')

if p_in is not True:
    print('Point lies outside of element. \n')
    exit()

print('Temperature at x =', p[0, 0], 'and y = ', p[0, 1], '\n', T_p[0, 0], unit)

# Plot element, centroid and point
# ax1.plot([nodes[el_ICA[0], 0], nodes[el_ICA[1], 0]], [nodes[el_ICA[0], 1], nodes[el_ICA[1], 1]], color='black', marker='o')
# ax1.plot([nodes[el_ICA[1], 0], nodes[el_ICA[2], 0]], [nodes[el_ICA[1], 1], nodes[el_ICA[2], 1]], color='black', marker='o')
# ax1.plot([nodes[el_ICA[0], 0], nodes[el_ICA[2], 0]], [nodes[el_ICA[0], 1], nodes[el_ICA[2], 1]], color='black', marker='o')
# ax1.plot(x_cent, y_cent, color='red', marker='*')
# ax1.plot(p[0, 0], p[0, 1], color='cyan', marker='o', markersize='3')
# plt.show()
