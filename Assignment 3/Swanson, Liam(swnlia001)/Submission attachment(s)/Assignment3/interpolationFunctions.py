# MEC5063Z ASSIGNMENT 3 Supporting Functions
# LIAM SWANSON
# SWNLIA001
# 3 APRIL 2019

import numpy as np


def interpolator(point, nodeList, nodeTemperatures, ICA_el):

    element = []
    temperature = []

    for k in ICA_el:
        element.append(nodeList[k])
        temperature.append(nodeTemperatures[k])

    element = np.asarray(element)
    temperature = np.asarray(temperature)

    x1 = element[0, 0]
    x2 = element[1, 0]
    x3 = element[2, 0]
    y1 = element[0, 1]
    y2 = element[1, 1]
    y3 = element[2, 1]

    # Create M inverse matrix

    area = abs((x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)) / 2)

    M_inv = np.array([[(x2 * y3) - (x3 * y2), y2 - y3, x3 - x2],
                     [(x3 * y1) - (x1 * y3), y3 - y1, x1 - x3],
                     [(x1 * y2) - (x2 * y1), y1 - y2, x2 - x1]]) * (1 / (2 * area))

    cart_column = np.array([[1, point[0, 0], point[0, 1]]]).T
    iso_column = np.around(np.dot(M_inv, cart_column), 15)

    included_points = False

    if 0 <= iso_column[0] <= 1 and 0 <= iso_column[1] <= 1 and 0 <= iso_column[2] <= 1:
        included_points = True

    if included_points is True:
        int_t = np.dot(temperature.T, iso_column)

    elif included_points is False:
        int_t = 'nan'

    return included_points, int_t
