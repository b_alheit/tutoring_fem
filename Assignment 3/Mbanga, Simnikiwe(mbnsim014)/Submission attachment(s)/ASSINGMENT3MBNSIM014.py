import numpy as np
import math
from numpy.linalg import inv as z
# LET'S DEFINE A FUNCTION THAT WILL DO THE TASK AT HAND

def triangles(x, y, T1, T2, T3):


    # NODAL VALUES FOR HEAT
    t = np.array([T1, T2, T3])
    # THE M MATRIX
    m = np.array([[1, 1, 1], [0, 1.5, 2], [1, 0, 1]])
    m_inverse = z(m)
    print('THE INVERSE OF m IS', m_inverse)
    # LET'S LOOK AT THIS M MATRIX
    print('M =', m)
    # DEFINE THE LINEAR COMPLETE FUNCTION AND THE SHAPE FUNCTIONS
    p = np.array([1, x, y])
    n = m_inverse * p
    # PRINT IT
    print('p(x,y)=', p)
    # N1 runs from 0 to 1,5
    # N2 runs from 1,5 to 2
    # N3 runs from 0 to 2
    center = np.array([3/2, 2/3])
    x_point = np.array([x, y])
    # the distance from the center to any point on the edge of the triangle
    d = math.sqrt(85)/6
    # How far are we from the triangle
    # we can also Interpret this as our error
    error = math.sqrt((x_point[0] - center[0])**2 + (x_point[1] - center[1])**2) - d

    if error <= 0:
        # The case where we choose a point that lies within the triangle
        # checking if its within bound
        print('DISTANCE TO THE EDGE IS', abs(error))
        print('WITHIN BOUND')
        t_value = np.sum(np.dot(t, n))
        print('THE TEMPERATURE IN KELVINS AT THIS GIVEN POINT (X,Y) IS', t_value)
    else:
        # the case where we have the point outside the triangle
        # checking if its out of bound
        print('ERROR OUT OF BOUND')
        print('THE DISTANCE FROM THE NEAREST POINT OF AN EDGE IS', error)
    print('N =', n)

    # to check temperature outside the triangle
    # t_value = np.sum(np.dot(t, n))
    # print('THE TEMPERATURE IN KELVINS AT THIS GIVEN POINT (X,Y) IS', t_value)
    return n
# ASSIGN AN X VALUE
# ASSIGN A Y VALUE
# ASSIGN TEMPERATURES T1, T2, T3 WHICH ARE IN KELVINS
# RUN THE CODE
# for the case we have T, x and y have been assign values
# x=0
# y=0


triangles(1, 1, 281, 256, 300)

# MARKERS COMMENTS
print()
print('^ This value should be the value of the temperature at Node 1 (281).')
print('Also... your code and Logan\'s code has the exact same error...')


