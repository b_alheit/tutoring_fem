#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
from math import *
import matplotlib.pyplot as plt
from scipy.linalg import det, inv
import statistics as st


# In[125]:


# creatting the matrix M;
def Trian_lin(x,y,t1,t2,t3):
    # Here t1,t2,t3 are temperatures at the nodes. 
    t = np.array([t1,t2,t3])  # creating the vector of the temperature at the nodes:
    # creating the matrix M
    M = np.array([[1,1,1],[1, 3/2, 2],[1,0,1]])
    # calculating the inverse of M
    M_inv=inv(M)
    # creating vector P
    p =np.array([1,x,y])
    # creating the vector psi_k's 
    N= M_inv.dot(p)
    #interpolating the temperature from the shape functions
    Temp = round(t.dot(N),2)
    ## calculating the area of the element
    Area = 1/2*det(M)
    ## calculating the centroid of the element
    centroid =(round(st.mean(M[1]),2),round(st.mean(M[2]),2))  
    ## checking wether point (x,y) is inside of the triangle
    ## put A=(0,1);B=(1.5,0);C=(2,1); P=(x,y)
    ## Calculate area of following triangles:
    ## triangle A1,P,A3 AND triangle A1,P,A2 AND triangle A2,P,A3
    def Isinside(x,y):
        # Calculate area of triangle PBC
        M1=np.array([[1,1,1],[x,1,1.5],[y,1,0]])
        A1 =0.5*det(M1)
        # Calculate area of triangle PAC
        M2=np.array([[1,1,1],[x,1.5,2],[y,0,1]])
        A2 =0.5*det(M2)
        # Calculate area of triangle PAB
        M3 =np.array([[1,1,1],[1,x,2],[1,y,1]])
        A3 = 0.5*det(M3)
        A = A1 + A2 + A3 ## Total area of the three triangles  A1,P,A3 AND triangle A1,P,A2 AND triangle A2,P,A3
        if  A == Area :
                     return 'inside'
        else:
                    return 'Error' 
    check = Isinside(x,y)
    return Temp,check, centroid, Area  


# In[135]:


t1 =10; t2=15; t3=9;
print(Trian_lin(1.5, 0, t1, t2, t3)[0])

# x=np.linspace(0,2,31)
# y=np.linspace(0,1,31)
# print('Centroid of the elemenet is',Trian_lin(2,1,t1,t2,t3)[2])
# print('==================================================')
# for x,y in zip(x,y):
#     if Trian_lin(x,y,t1,t2,t3)[1] != str('Error'):
#         #print(round(i,2),round(k,2))
#         print('(x,y): {}\t Temperature: {} \t inside:{}'.format((round(x,2),round(y,2)),Trian_lin(x,y,t1,t2,t3)[0],Trian_lin(x,y,t1,t2,t3)[1]))
#     else:
#         print('(x,y): {}\t Temperature: {} \t inside:{}'.format((round(x,2),round(y,2)),0,Trian_lin(x,y,t1,t2,t3)[1]))
#
