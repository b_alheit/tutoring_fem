def TriangleTemperature(x,y,T1,T2,T3):
    import numpy as np

    ################### Check if point is outside element
    eta = x + y/2 - 3/2
    xi = 1 - y

    UpperBoundEta = 1 - xi

    if xi<0 or xi>1 or eta<0 or eta>1 or eta>UpperBoundEta:
        print('Input lies outside of the element')
        return

    #################### Produce M matrix
    M = np.ones([3,3])
    M[1,:] = [1,3/2,2]      # putting in specific x domain
    M[2,:] = [1,0,1]        # specific y domain

    #################### Produce p, the specific vector that our points are on
    p = np.ones(3)
    p[1] = x
    p[2] = y

    #################### Produce shape functions
    N = np.matmul(p, np.linalg.inv(M))

    #################### make Temperature at nodes into a vector
    NodalTemperatures = [T1,T2,T3]

    #################### Take dot product of Nodal Temps and shape functions, producing Temperature at x,y point
    Temperature = np.dot(N, NodalTemperatures)

    print('Temperature at (',x,',',y,') =', Temperature)
    return

TriangleTemperature(1.5, 0, 270, 280, 290)
print()
print('^ This value should be the value of the temperature at Node 2 (280), \nand it definitely shouldn\'t be lower than '
      'the lowest temperature value of all the nodes')
